/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

import dao.BD;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Richard
 */
public class documento_ordensalida {

    private Connection cnn;

    public documento_ordensalida() {
        cnn = BD.getConnection();
    }

    public void run_viewdocumento_ordensalida(int int_id) {
        try {
            String master = "";
            JasperPrint jasperPrint = null;
            master = System.getProperty("user.dir") + "\\src\\reporte\\documento_ordensalida.jasper";
            //System.out.println("master" + master);
            if (master == null) {
                System.out.println("no encuentro el archivo de reporte maestro");
                System.exit(2);
            }
            JasperReport masterReport = null;
            try {
                masterReport = (JasperReport) JRLoader.loadObject(master);
            } catch (JRException e) {
                System.out.println("error cargando el reporte maestro:" + e.getMessage());
                System.exit(3);
            }
            Map parametro = new HashMap();
            parametro.put("int_id", int_id);
            jasperPrint = JasperFillManager.fillReport(masterReport, parametro, cnn);
            if (jasperPrint.getPages().isEmpty() == false) {
                JasperViewer verDocumento = new JasperViewer(jasperPrint, false);
                verDocumento.setTitle("DOCUMENTO ORDEN - SALIDA");
                verDocumento.setVisible(true);
            }
            cnn.close();
        } catch (Exception e) {
            System.out.println("mensaje de error:  " + e);
        }
    }
    
}
