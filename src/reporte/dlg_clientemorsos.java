/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporte;

import dao.BD;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JDialog;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JRViewer;

/**
 *
 * @author Richard
 */
public class dlg_clientemorsos {

    private Connection cnn;

    public dlg_clientemorsos() {
        cnn = BD.getConnection();
    }

    public JRViewer runLista_Morosos(Date fechaInicio, Date fechaFin, JDialog inicio) {
        JRViewer viewer = null;
        try {
            String master = System.getProperty("user.dir") + "\\src\\reporte\\dlg_clientemorosos.jasper";

            if (master == null) {
                System.out.println("no encuentro el archivo de reporte maestro");
                System.exit(2);
            }
            JasperReport masterReport = null;
            try {
                masterReport = (JasperReport) JRLoader.loadObject(master);
            } catch (JRException e) {
                System.out.println("error cargando el reporte maestro:" + e.getMessage());
                System.exit(3);
            }
            Map parametro = new HashMap();
            parametro.put("fechaInicio", fechaInicio);
            parametro.put("fechaFin", fechaFin);
            JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parametro, cnn);
            if (jasperPrint.getPages().isEmpty() == false) {
                viewer = new JRViewer(jasperPrint);
                viewer.setOpaque(true);
                viewer.setVisible(true);
                viewer.setZoomRatio(Float.parseFloat("0.99"));
                viewer.setSize(new Dimension(960, 520));
                viewer.setLocation(10, 115); // x,y ..con el y bajas
                inicio.getContentPane().add(viewer);
                inicio.pack();
            }
        } catch (Exception e) {
            System.out.println("mensaje de error:  " + e.getMessage());
        } finally {
            try {
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("mensaje :" + ex.getMessage());
            }
        }

        return viewer;
    }
}
