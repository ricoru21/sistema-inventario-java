/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.BLCaja;
import controlador.BLCategoria;
import controlador.BLCliente;
import controlador.BLCodigo_Documento;
import controlador.BLConstante;
import controlador.BLMarca;
import controlador.BLMovimiento;
import controlador.BLOrden_Ingreso;
import controlador.BLOrden_Salida;
import controlador.BLProducto;
import controlador.BLProveedor;
import controlador.BLUsuario;
import controlador.BLVenta;
import controlador.BLVenta_Anulada;
import controlador.BLVenta_Credito;
import dao.Categoria;
import dao.Cliente;
import dao.Codigo_Documento;
import dao.Constante;
import dao.CronogramaPago;
import dao.Detalle_OrdenIngreso;
import dao.Detalle_OrdenSalida;
import dao.Documento;
import dao.ListaCliente;
import dao.ListaCronogramaPago;
import dao.ListaMovimiento_Stock;
import dao.ListaOrdenIngreso;
import dao.ListaOrdenSalida;
import dao.ListaProducto;
import dao.ListaVenta;
import dao.Marca;
import dao.Producto;
import dao.Proveedor;
import dao.Transaccion;
import dao.Usuario;
import dao.Venta_Credito;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import net.sf.jasperreports.view.JRViewer;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import reporte.dlg_clientemorsos;
import reporte.dlg_cuadrecaja;
import reporte.dlg_detallecaja;
import reporte.dlg_movimiento;
import reporte.dlg_venta_anulada;
import reporte.dlg_ventaxcliente;
import reporte.documento_cronogramapago;
import reporte.documento_ordeningreso;
import reporte.documento_ordensalida;
import reporte.documento_venta;
import utilitario.CellRenderer;
import utilitario.CloseDialogEscape;
import utilitario.ControlLogin;
import utilitario.Funciones;
import utilitario.HeaderCellRenderer;
import utilitario.exportar_excel;

/**
 *
 * @author Richard
 */
public class Home extends javax.swing.JFrame implements Runnable {

    Dimension tam = Toolkit.getDefaultToolkit().getScreenSize();
    Usuario userDefault = null;
    String[] serie_numero = new String[2];
    int idProductoIngProd = 0, idProductoVenta = 0, idProductoSalProd = 0, idClienteVenta = 0, idCliente = 0, idUsuario = 0,
            idProveedor = 0, idMarca = 0, idCategoria = 0, idProducto = 0, idTipoProducto = 0, idVenta = 0, idCronoPago = 0,
            idClientePrestamo = 0, idDetallePrestamo = 0, id_prestamo = 0, idCaja, idClienteAlquiler = 0,
            id_alquiler = 0, id_cpa_alquiler = 0, tipoEstado_Alquiler = 0;
    double Producto_stockactual = 0.0, acuenta_anterior_cp = 0.0, temp_totalFacturado = 0.0, temp_prestamo = 0.0,
            totapagar_cpa;
    /* -- FECHA Y HORA -- */
    Thread h1;
    String fechaActual, hora, minutos, segundos, ampm;
    /*-------------------*/

    /**
     * Creates new form home
     */
    public Home() {
        this.setResizable(false);
        initComponents();
        this.setDefaultCloseOperation(0);
        this.setResizable(false);
        this.setSize(990, 680);
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        Dimension df = Toolkit.getDefaultToolkit().getScreenSize();
        this.setMinimumSize(df);
        this.setLocation(0, 0);
        /*Funciones que se ejecutaran al cargar el sistema*/
        formatearAll_Tablas();
        cargarDatosIniciales();
        //Ejecutar Siempre esta consulta, cuando inicie el aplictivo
        h1 = new Thread(this);
        h1.start();
    }

    private void formatearAll_Tablas() {
        //----------------- Lista Proveedor General -----------------------------------------------
        ListaProveedor.setColumnIdentifiers(new String[]{"#", "RUC", "RAZÓN SOCIAL", "DIRECCION", "TELÉFONO", "ESTADO"});
        ((DefaultTableCellRenderer) jtListaProveedor.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        jtListaProveedor.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtListaProveedor.getColumnModel().getColumn(1).setPreferredWidth(100);
        jtListaProveedor.getColumnModel().getColumn(2).setPreferredWidth(160);
        jtListaProveedor.getColumnModel().getColumn(3).setPreferredWidth(160);
        jtListaProveedor.getColumnModel().getColumn(4).setPreferredWidth(50);
        jtListaProveedor.getColumnModel().getColumn(5).setPreferredWidth(40);
        jtListaProveedor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtListaProveedor.setRowHeight(22);
        JTableHeader jtableHeaderLProv = jtListaProveedor.getTableHeader();
        jtableHeaderLProv.setDefaultRenderer(new HeaderCellRenderer());
        jtListaProveedor.setTableHeader(jtableHeaderLProv);

        //----------------- Lista Usuario General -----------------------------------------------
        ListaUsuario.setColumnIdentifiers(new String[]{"#", "Nombre y Apellido", "DNI", "ID NAME", "TELÉFONO", "ESTADO"});
        ((DefaultTableCellRenderer) jtListaUsuario.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        jtListaUsuario.getColumnModel().getColumn(0).setPreferredWidth(20);
        jtListaUsuario.getColumnModel().getColumn(1).setPreferredWidth(210);
        jtListaUsuario.getColumnModel().getColumn(2).setPreferredWidth(70);
        jtListaUsuario.getColumnModel().getColumn(3).setPreferredWidth(70);
        jtListaUsuario.getColumnModel().getColumn(4).setPreferredWidth(60);
        jtListaUsuario.getColumnModel().getColumn(5).setPreferredWidth(40);
        jtListaUsuario.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtListaUsuario.setRowHeight(22);
        JTableHeader jtableHeaderLU = jtListaUsuario.getTableHeader();
        jtableHeaderLU.setDefaultRenderer(new HeaderCellRenderer());
        jtListaUsuario.setTableHeader(jtableHeaderLU);

        //----------------- Lista Producto General -----------------------------------------------
        ListaProducto.setColumnIdentifiers(new String[]{"Codigo", "Producto", "Tipo", "Marca", "Categoria", "Precio Unit S/.", "Precio Venta S/.", "Stock", "Stock Minimo", "Estado"});
        ((DefaultTableCellRenderer) jtListaProducto.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        jtListaProducto.getColumnModel().getColumn(0).setPreferredWidth(80);
        jtListaProducto.getColumnModel().getColumn(1).setPreferredWidth(130);
        jtListaProducto.getColumnModel().getColumn(2).setPreferredWidth(80);
        jtListaProducto.getColumnModel().getColumn(3).setPreferredWidth(80);
        jtListaProducto.getColumnModel().getColumn(4).setPreferredWidth(80);
        jtListaProducto.getColumnModel().getColumn(5).setPreferredWidth(80);
        jtListaProducto.getColumnModel().getColumn(6).setPreferredWidth(100);
        jtListaProducto.getColumnModel().getColumn(7).setPreferredWidth(50);
        jtListaProducto.getColumnModel().getColumn(8).setPreferredWidth(60);
        jtListaProducto.getColumnModel().getColumn(9).setPreferredWidth(60);
        jtListaProducto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtListaProducto.setRowHeight(22);
        JTableHeader jtableHeaderLP = jtListaProducto.getTableHeader();
        jtableHeaderLP.setDefaultRenderer(new HeaderCellRenderer());
        jtListaProducto.setTableHeader(jtableHeaderLP);

        //----------------- Lista Busqueda Producto en IngProducto General -----------------------------------------------
        ListaProductoBusquedaIngProd.setColumnIdentifiers(new String[]{"#", "Codigo", "Producto", "Tipo", "Marca", "Categoria", "Precio Costo S/.", "Estado"});
        ListaProductoBusquedaSalProd.setColumnIdentifiers(new String[]{"#", "Codigo", "Producto", "Tipo", "Marca", "Categoria", "Stock Actual", "Estado"});
        ((DefaultTableCellRenderer) jtListaProductoBusquedaIngProd.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(0).setPreferredWidth(40);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(1).setPreferredWidth(80);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(2).setPreferredWidth(180);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(3).setPreferredWidth(80);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(4).setPreferredWidth(80);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(5).setPreferredWidth(80);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(6).setPreferredWidth(80);
        jtListaProductoBusquedaIngProd.getColumnModel().getColumn(7).setPreferredWidth(70);
        jtListaProductoBusquedaIngProd.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtListaProductoBusquedaIngProd.setRowHeight(22);
        JTableHeader jtableHeaderLPBIP = jtListaProductoBusquedaIngProd.getTableHeader();
        jtableHeaderLPBIP.setDefaultRenderer(new HeaderCellRenderer());
        jtListaProductoBusquedaIngProd.setTableHeader(jtableHeaderLPBIP);

        ListaProductoBusquedaVenta.setColumnIdentifiers(new String[]{"#", "Codigo", "Producto", "Tipo", "Marca", "Categoria", "Precio Venta S/.", "Stock", "Estado"});
        ((DefaultTableCellRenderer) jtListaProductoBusquedaVenta.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(0).setPreferredWidth(40);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(1).setPreferredWidth(80);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(2).setPreferredWidth(180);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(3).setPreferredWidth(80);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(4).setPreferredWidth(80);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(5).setPreferredWidth(80);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(6).setPreferredWidth(80);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(7).setPreferredWidth(70);
        jtListaProductoBusquedaVenta.getColumnModel().getColumn(8).setPreferredWidth(70);
        jtListaProductoBusquedaVenta.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtListaProductoBusquedaVenta.setRowHeight(22);
        JTableHeader jtableHeaderLPBV = jtListaProductoBusquedaVenta.getTableHeader();
        jtableHeaderLPBV.setDefaultRenderer(new HeaderCellRenderer());
        jtListaProductoBusquedaVenta.setTableHeader(jtableHeaderLPBV);

        //----------------- Lista Cliente General -----------------------------------------------
        ListaCliente.setColumnIdentifiers(new String[]{"#", "Cliente", "DNI", "RUC", "TELÉFONO", "DIRECCIÓN", "ESTADO"});
        ((DefaultTableCellRenderer) jtListaCliente.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        jtListaCliente.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtListaCliente.getColumnModel().getColumn(1).setPreferredWidth(200);
        jtListaCliente.getColumnModel().getColumn(2).setPreferredWidth(30);
        jtListaCliente.getColumnModel().getColumn(3).setPreferredWidth(40);
        jtListaCliente.getColumnModel().getColumn(4).setPreferredWidth(40);
        jtListaCliente.getColumnModel().getColumn(5).setPreferredWidth(220);
        jtListaCliente.getColumnModel().getColumn(6).setPreferredWidth(40);
        jtListaCliente.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtListaCliente.setRowHeight(22);
        JTableHeader jtableHeaderLC = jtListaCliente.getTableHeader();
        jtableHeaderLC.setDefaultRenderer(new HeaderCellRenderer());
        jtListaCliente.setTableHeader(jtableHeaderLC);

        //---------------- Lista Cliente Busqueda -------------------------------------------------
        ListaClienteBusqueda.setColumnIdentifiers(new String[]{"#", "Cliente", "DNI", "RUC"});
        ((DefaultTableCellRenderer) jtListaClienteBusq.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        jtListaClienteBusq.getColumnModel().getColumn(0).setPreferredWidth(10);
        jtListaClienteBusq.getColumnModel().getColumn(1).setPreferredWidth(250);
        jtListaClienteBusq.getColumnModel().getColumn(2).setPreferredWidth(20);
        jtListaClienteBusq.getColumnModel().getColumn(3).setPreferredWidth(20);
        jtListaClienteBusq.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtListaClienteBusq.setRowHeight(22);
        //Se Asgina un nuevo color y nuevo Header a la Tabla
        JTableHeader jtableHeaderLCB = jtListaClienteBusq.getTableHeader();
        jtableHeaderLCB.setDefaultRenderer(new HeaderCellRenderer());
        jtListaClienteBusq.setTableHeader(jtableHeaderLCB);

        /* Movimiento Inventario */
        jtListaMovimientoInventario.setGridColor(new java.awt.Color(214, 213, 208));
        jtListaMovimientoInventario.setRowHeight(22);
        jtListaMovimientoInventario.getColumnModel().getColumn(0).setCellRenderer(new CellRenderer("fecha"));
        jtListaMovimientoInventario.getColumnModel().getColumn(1).setCellRenderer(new CellRenderer("text"));
        jtListaMovimientoInventario.getColumnModel().getColumn(2).setCellRenderer(new CellRenderer("icon"));
        jtListaMovimientoInventario.getColumnModel().getColumn(3).setCellRenderer(new CellRenderer("num"));
        jtListaMovimientoInventario.getColumnModel().getColumn(4).setCellRenderer(new CellRenderer("text center"));

        //Se Asgina un nuevo color y nuevo Header a la Tabla
        JTableHeader jtableHeaderMI = jtListaMovimientoInventario.getTableHeader();
        jtableHeaderMI.setDefaultRenderer(new HeaderCellRenderer());
        jtListaMovimientoInventario.setTableHeader(jtableHeaderMI);

        //----------------- Detalle Producto Venta -----------------------------------------------
        jtDetalleProductoVenta.setRowHeight(22);
        JTableHeader jtableHeaderDPV = jtDetalleProductoVenta.getTableHeader();
        jtableHeaderDPV.setDefaultRenderer(new HeaderCellRenderer());
        jtDetalleProductoVenta.setTableHeader(jtableHeaderDPV);
        //----------------- Buscar Venta -----------------------------------------------
        jtListaBusquedaVenta.setRowHeight(22);
        JTableHeader jtableHeaderLBV = jtListaBusquedaVenta.getTableHeader();
        jtableHeaderLBV.setDefaultRenderer(new HeaderCellRenderer());
        jtListaBusquedaVenta.setTableHeader(jtableHeaderLBV);
        //----------------- Detalle Ingreso Producto -----------------------------------------------
        jtDetalleProductoIngProd.setRowHeight(22);
        JTableHeader jtableHeaderDPIP = jtDetalleProductoIngProd.getTableHeader();
        jtableHeaderDPIP.setDefaultRenderer(new HeaderCellRenderer());
        jtDetalleProductoIngProd.setTableHeader(jtableHeaderDPIP);
        //----------------- Lista Ingreso Producto -----------------------------------------------
        jtListaBusquedaIngProducto.setRowHeight(22);
        JTableHeader jtableHeaderLBIP = jtListaBusquedaIngProducto.getTableHeader();
        jtableHeaderLBIP.setDefaultRenderer(new HeaderCellRenderer());
        jtListaBusquedaIngProducto.setTableHeader(jtableHeaderLBIP);
        //----------------- Detalle Salida Producto -----------------------------------------------
        jtDetalleProductoSalidaProd.setRowHeight(22);
        JTableHeader jtableHeaderDPSP = jtDetalleProductoSalidaProd.getTableHeader();
        jtableHeaderDPSP.setDefaultRenderer(new HeaderCellRenderer());
        jtDetalleProductoSalidaProd.setTableHeader(jtableHeaderDPSP);
        //----------------- Lista Salida Producto -----------------------------------------------
        jtListaBusquedaSalidaProd.setRowHeight(22);
        JTableHeader jtableHeaderLBSP = jtListaBusquedaSalidaProd.getTableHeader();
        jtableHeaderLBSP.setDefaultRenderer(new HeaderCellRenderer());
        jtListaBusquedaSalidaProd.setTableHeader(jtableHeaderLBSP);
        //----------------- Lista Tipo Producto -----------------------------------------------
        jtListaTipoProducto.setRowHeight(22);
        JTableHeader jtableHeaderLTP = jtListaTipoProducto.getTableHeader();
        jtableHeaderLTP.setDefaultRenderer(new HeaderCellRenderer());
        jtListaTipoProducto.setTableHeader(jtableHeaderLTP);
        //----------------- Lista Marca -----------------------------------------------
        jtListaMarca.setRowHeight(22);
        JTableHeader jtableHeaderLM = jtListaMarca.getTableHeader();
        jtableHeaderLM.setDefaultRenderer(new HeaderCellRenderer());
        jtListaMarca.setTableHeader(jtableHeaderLM);
        //----------------- Lista Categoria -----------------------------------------------
        jtListaCategoria.setRowHeight(22);
        JTableHeader jtableHeaderLCat = jtListaCategoria.getTableHeader();
        jtableHeaderLCat.setDefaultRenderer(new HeaderCellRenderer());
        jtListaCategoria.setTableHeader(jtableHeaderLCat);
    }

    private void cargarDatosIniciales() {
        txtFechaEmisionIngProd.setDate(new Date());
        txtFechaInicioIngProd.setDate(new Date());
        txtFechaFinIngProd.setDate(new Date());
        txtFechaInicioVenta.setDate(new Date());
        txtFechaFinVenta.setDate(new Date());
        txtFechaEmisionSalidaProd.setDate(new Date());
        txtFechaInicioSalidaProd.setDate(new Date());
        txtFechaFinSalidaProd.setDate(new Date());
        txtFechaInicioInventario.setDate(new Date());
        txtFechaFinInventario.setDate(new Date());
        txtCodigoBarraProducto.setText(new BLProducto().generar_codigobarra_serie());
        cargardatos_proveedor("", 1);
        cargardatos_usuario("", 2);
        cargardatos_tipoProducto();
        cargardatos_categoria();
        cargardatos_marca();
        cargardatos_producto("", "Descripcion");
        cargardatos_tipoproducto("", 0);
        cargardatos_marca("", 0);
        cargardatos_categoria("", 0);
        cargardatos_cliente("", 0);
        cargardatos_tipoOperacion();
        cargardatos_tipoDocumentoMov();
        cargardatos_FormaPago();
        cargardatos_tipoMovitoIngresoProducto();
        cargardatos_motivoSalidaProd();
        cargardatos_proveedorIngProd();
        cargardatos_tipoDocumentoVenta();
        cargardatos_codigo_documento_impresion();
        cargardatos_cliente_busqueda_morosos("", 0);
        cargar_stock_actual("");
    }

    /* SALIDA PRODUCTO */
    private void cargardatos_busqueda_salidaproducto(String palabra, String condicion) {
        DefaultTableModel tempIngProd = (DefaultTableModel) jtListaProductoBusquedaSalidaProd.getModel();
        tempIngProd.setRowCount(0);
        for (ListaProducto p : new BLProducto().get_lista_productos_all_activo(palabra, condicion)) {
            Object[] datos = {p.getInt_id(), p.getCodigobarra(), p.getProducto(), p.getTipoProducto(), p.getMarca(), p.getCategoria(), p.getStock_actual(), p.getEstado()};
            tempIngProd.addRow(datos);
        }
    }

    private void calcular_cantidad_total_salidaprod() {
        double totalproducto = 0;
        DefaultTableModel listadetalle = (DefaultTableModel) jtDetalleProductoSalidaProd.getModel();
        int fila = listadetalle.getRowCount();
        for (int f = 0; f < fila; f++) {
            totalproducto += Double.parseDouble(String.valueOf(jtDetalleProductoSalidaProd.getModel().getValueAt(f, 2)));
        }
        txtCantProductoSalidaProd.setText("" + totalproducto);
    }

    private void cargardatos_motivoSalidaProd() {
        cboMotivoRegSalidaProd.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(7)) {
            cboMotivoRegSalidaProd.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboMotivoRegSalidaProd);
    }

    private void cargadatos_consultar_ordensalida() {
        int contador = 0;
        ArrayList<String> lista = new ArrayList();
        String condicionFinal = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        boolean fecha = chkFechasSalidaProd.isSelected();
        boolean proveedor = chkPersonalSalidaProd.isSelected();
        boolean motivo = chkMotivoBusquedaSalidaProd.isSelected();
        if (fecha == true) {
            lista.add(" ( date(fecha) between '" + sdf.format(txtFechaInicioSalidaProd.getDate()) + "' and '" + sdf.format(txtFechaFinSalidaProd.getDate()) + "' ) ");
            contador++;
        }
        if (proveedor == true) {
            lista.add(" ( id_personal =" + ((Usuario) cboPersonalSalidaProd.getSelectedItem()).getInt_id() + " )");
            contador++;
        }
        if (motivo == true) {
            lista.add(" ( id_motivo =" + ((Constante) cboMotivoBusSalidaProd.getSelectedItem()).getInt_valor() + " )");
            contador++;
        }

        switch (contador) {
            case 1:
                condicionFinal = lista.get(0);
                break;
            case 2:
                condicionFinal = lista.get(0) + " and " + lista.get(1);
                break;
            case 3:
                condicionFinal = lista.get(0) + " and " + lista.get(1) + " and " + lista.get(2);
                break;
        }

        DefaultTableModel tempIngProd = (DefaultTableModel) jtListaBusquedaSalidaProd.getModel();
        tempIngProd.setRowCount(0);
        for (ListaOrdenSalida los : new BLOrden_Salida().get_lista_ordensalida_all(condicionFinal)) {
            Object datos[] = {
                los.getId(), los.getFecha(), los.getMotivo(), los.getCantidad(), los.getComentario(),
                los.getCliente(),
                los.getPersonal(), los.getEstado()
            };
            tempIngProd.addRow(datos);
        }
    }

    private void limpiarCajaTexto_SalidaProd() {
        try {
            txtFechaEmisionSalidaProd.setDate(new Date());
            cboPersonalRegSalidaProd.setSelectedIndex(0);
            cboMotivoRegSalidaProd.setSelectedIndex(0);
            txtComentarioSalidaProd.setText("");
            txtNombreProductoSalidaProd.setText("");
            txtCantidadProductoSalidaProd.setText("0");
            cboPersonalRegSalidaProd.requestFocus();
            txtCantProductoSalidaProd.setText("0");
            ((DefaultTableModel) jtDetalleProductoSalidaProd.getModel()).setRowCount(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public void llama_excel_salida_producto() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "LISTA_ORDEN_SALIDA.xls");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* ----------------- */
    /* CONSULTAR VENTA - CRONOGRAMA PAGO */
    private void cargadatos_consultar_venta() {
        try {
            int contador = 0;
            ArrayList<String> lista = new ArrayList();
            String condicionFinal = "";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            boolean fecha = chkFechasVenta.isSelected();
            boolean proveedor = chkClienteVenta.isSelected();
            boolean documento = chkDocumentoVenta.isSelected();
            boolean formapago = chkFormaPagoVenta.isSelected();
            if (fecha == true) {
                lista.add(" ( date(fechaBusq) between '" + sdf.format(txtFechaInicioVenta.getDate()) + "' and '" + sdf.format(txtFechaFinVenta.getDate()) + "' ) ");
                contador++;
            }
            if (proveedor == true) {
                lista.add(" ( id_cliente =" + ((ListaCliente) cboClienteVenta.getSelectedItem()).getId() + " )");
                contador++;
            }
            if (documento == true) {
                lista.add(" ( serie ='" + txtSerieDocBusqVenta.getText().trim() + "' and numero='" + txtNumeroDocBusqVenta.getText().trim() + "' ) ");
                contador++;
            }
            if (formapago == true) {
                lista.add(" ( id_formapago =" + ((Constante) cboBusqFormaPagoVenta.getSelectedItem()).getInt_valor() + " )");
                contador++;
            }

            switch (contador) {
                case 1:
                    condicionFinal = lista.get(0);
                    break;
                case 2:
                    condicionFinal = lista.get(0) + " and " + lista.get(1);
                    break;
                case 3:
                    condicionFinal = lista.get(0) + " and " + lista.get(1) + " and " + lista.get(2);
                    break;
                case 4:
                    condicionFinal = lista.get(0) + " and " + lista.get(1) + " and " + lista.get(2) + " and " + lista.get(3);
                    break;

            }
            DefaultTableModel tempIngProd = (DefaultTableModel) jtListaBusquedaVenta.getModel();
            tempIngProd.setRowCount(0);
            for (ListaVenta lv : new BLVenta().get_lista_venta_all(condicionFinal)) {
                Object datos[] = {
                    lv.getId(), lv.getFecha(), lv.getSerie() + " - " + lv.getNumero(), lv.getFormaPago(),
                    lv.getMontoFacturado(), lv.getMontoPagado(), lv.getSaldo(),
                    lv.getCliente(), lv.getPersonal(), lv.getTipoPago(), lv.getEstado()
                };
                tempIngProd.addRow(datos);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }

    private void cargardatos_listar_cronogramapago(int id_venta) {
        Venta_Credito vc = new BLVenta_Credito().get_ventacredito_byId_Venta(id_venta);
        DefaultTableModel tempIngProd = (DefaultTableModel) jtListaPagosProgramados.getModel();
        tempIngProd.setRowCount(0);
        for (ListaCronogramaPago lcp : new BLVenta_Credito().get_lista_cronogramapago_byId_VentaCredito(vc.getInt_id())) {
            Object datos[] = {
                lcp.getInt_id(), lcp.getInt_numerocuota(), lcp.getDat_fechapago(),
                lcp.getDat_fechapagorealizado(), lcp.getDec_monto(), lcp.getDec_acuenta(), lcp.getEstado()
            };
            tempIngProd.addRow(datos);
        }
    }

    public void llama_excel_venta() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "LISTA_VENTAS.xls");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* CREACION DE VENTA */
    private void obtener_serienumero_venta(int tipoDocumento) {
        serie_numero = new BLVenta().get_serienumero_documento(tipoDocumento);
        lblNroDocumentoVenta.setText("" + serie_numero[0] + " - " + serie_numero[1]);
        /*switch (tipoDocumento) {
         case 2:
         txtIGVPorcentaje.setValue(0);
         break;
         }*/
    }

    private void cargardatos_tipoDocumentoVenta() {
        cboTipoDocumentoVenta.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(6)) {
            cboTipoDocumentoVenta.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboTipoDocumentoVenta);

        cboTipoDocumentoDoc.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(6)) {
            cboTipoDocumentoDoc.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboTipoDocumentoDoc);

    }

    private void cargardatos_busqueda_venta(String palabra, String condicion) {
        DefaultTableModel tempIngProd = (DefaultTableModel) jtListaProductoBusquedaVenta.getModel();
        tempIngProd.setRowCount(0);
        for (ListaProducto p : new BLProducto().get_lista_productos_all_activo(palabra, condicion)) {
            Object[] datos = {p.getInt_id(), p.getCodigobarra(),
                p.getProducto(), p.getTipoProducto(), p.getMarca(), p.getCategoria(),
                p.getPrecioventa(), p.getStock_actual(),
                p.getEstado()};
            tempIngProd.addRow(datos);
        }
    }

    private void calcular_montos_ventas() {
        double igvporc = txtIGVPorcentaje.getValue();
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", simbolos);
        double totalproducto = 0, totalFacturado = 0.0, subTotal = 0.0, montoigv = 0.0, acuenta = 0.0;
        acuenta = Double.parseDouble(txtAcuentaVenta.getText().equals("") ? "0.0" : txtAcuentaVenta.getText());
        DefaultTableModel listadetalle = (DefaultTableModel) jtDetalleProductoVenta.getModel();
        int fila = listadetalle.getRowCount();
        for (int f = 0; f < fila; f++) {
            totalproducto += Double.parseDouble(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 3)));
            subTotal = subTotal + (Double.parseDouble(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 4))));
        }
        txtCantProductoVenta.setText("" + totalproducto);
        totalFacturado = Double.parseDouble(df.format(subTotal));
        igvporc = (igvporc / 100);
        montoigv = Double.parseDouble(df.format(igvporc * subTotal));
        subTotal = Double.parseDouble(df.format(totalFacturado - montoigv));
        temp_totalFacturado = totalFacturado;
        txtIGVMontoVenta.setText("" + montoigv);
        txtSubTotalMonto.setText("" + subTotal);
        txtMontoTotalVenta.setText("" + totalFacturado);
        txtSaldoVenta.setText("" + (totalFacturado - acuenta));
    }

    private String sumarFechasDias(Date fecha, int dias) {
        SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = new GregorianCalendar();
        cal.setTime(fecha);
        cal.add(Calendar.DATE, dias);
        return "" + formatofecha.format(cal.getTime());
    }

    private void crearVenta() {
        int result = 0, tipVenta = 0, tipDocumento = 0;
        double igvmonto = 0.0, igvporcentaje = 0.0, subtotal = 0.0, montoFacturado = 0.0, acuenta = 0.0, saldo = 0.0, descuentomonetario = 0.0;
        int descuento = 0;
        String estado = "";
        try {
            if (idClienteVenta != 0) {
                tipVenta = cboTipoVentta.getSelectedItem().toString().equalsIgnoreCase("Credito") ? 2 : 1;
                tipDocumento = ((Constante) cboTipoDocumentoVenta.getSelectedItem()).getInt_valor();
                subtotal = Double.parseDouble(txtSubTotalMonto.getText());
                igvporcentaje = txtIGVPorcentaje.getValue();
                igvmonto = Double.parseDouble(txtIGVMontoVenta.getText());
                descuento = txtDescuentoVenta.getValue();
                montoFacturado = Double.parseDouble(txtMontoTotalVenta.getText());
                acuenta = Double.parseDouble(txtAcuentaVenta.getText().equals("") ? "0.0" : txtAcuentaVenta.getText());
                saldo = Double.parseDouble(txtSaldoVenta.getText());
                estado = Double.parseDouble(txtSaldoVenta.getText()) >= 0.0 ? "1" : "2";
                //Registro Venta y Transaccion
                ArrayList<Transaccion> lista_tra = new ArrayList<Transaccion>();
                Transaccion tra = null;
                ArrayList<Detalle_OrdenSalida> lista_dos = new ArrayList<Detalle_OrdenSalida>();
                Detalle_OrdenSalida dos = null;
                DefaultTableModel listadetalle = (DefaultTableModel) jtDetalleProductoVenta.getModel();
                int nroFilas = listadetalle.getRowCount();
                for (int f = 0; f < nroFilas; f++) {
                    tra = new Transaccion();
                    tra.setInt_producto(Integer.parseInt(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 0))));
                    tra.setDec_preciounitario(Double.parseDouble(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 2))));
                    tra.setDec_cantidad(Double.parseDouble(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 3))));
                    tra.setDec_importe(Double.parseDouble(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 4))));
                    lista_tra.add(tra);

                    dos = new Detalle_OrdenSalida();
                    dos.setDec_cantidad(Double.parseDouble(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 3))));
                    dos.setInt_producto(Integer.parseInt(String.valueOf(jtDetalleProductoVenta.getModel().getValueAt(f, 0))));
                    dos.setVar_estado("1");
                    lista_dos.add(dos);
                }
                Documento d = new Documento();
                d.setVar_serie(serie_numero[0]);
                d.setVar_numero(serie_numero[1]);
                d.setInt_tipodocumento(tipDocumento);
                d.setInt_tipooperacionInterna(1);
                /*Venta al Credito si es que lo hay*/
                int formapago = 0,  frecuenciapago = 0, nrodias = 0, nrocuota =0;
                double montocuota=0.0;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                ArrayList<CronogramaPago> lista_cp = null;
                CronogramaPago cp = null;
                if (tipVenta == 2) {
                    formapago = ((Constante) cboFormaPagoVenta.getSelectedItem()).getInt_valor();
                    frecuenciapago = ((Constante) cboFrecuenciaPagoVentaCredito.getSelectedItem()).getInt_valor();
                    nrocuota = txtNroCuotaVentaCredito.getValue();
                    montocuota = Double.parseDouble(txtMontoXCuotaVentaCredito.getText().compareTo("") != 0 ? "0.0" : txtMontoXCuotaVentaCredito.getText());
                    estado = "3";
                    switch (frecuenciapago) {
                        case 1:
                            nrodias = 1;
                            break;
                        case 2:
                            nrodias = 7;
                            break;
                        case 3:
                            nrodias = 15;
                            break;
                        case 4:
                            nrodias = 30;
                            break;
                    }
                    lista_cp = new ArrayList<CronogramaPago>();
                    String fechaInicio = sdf.format(txtFechaPrimerPagoVentaCredito.getDate());
                    for (int i = 1; i <= nrocuota; i++) {
                        cp = new CronogramaPago();
                        try {
                            String fecha = fechaInicio;
                            fecha = sumarFechasDias(sdf.parse(fecha), nrodias);
                            cp.setDat_fechapago(new Timestamp(sdf.parse(fecha).getTime()));
                            fechaInicio = fecha;
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                        cp.setDec_monto(montocuota);
                        cp.setDec_acuenta(0.0);
                        cp.setInt_numerocuota(i);
                        cp.setVar_estado("1");
                        lista_cp.add(cp);
                    }
                }

                result = new BLVenta().registar_venta(idClienteVenta, montoFacturado, subtotal, descuento, descuentomonetario, acuenta, saldo, tipVenta, igvporcentaje, igvmonto, userDefault.getInt_id(), estado, lista_tra, d,
                        lista_dos, nrocuota, montocuota, frecuenciapago, nrodias, formapago, lista_cp);
                if (result != 0) {
                    JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    /* Imprimir Documento */
                    doc_venta = new documento_venta();
                    doc_venta.run_viewdocumento_venta_directa(result);
                    limpiarCajaTexto_Venta();
                    btnCrearVenta.setEnabled(false);
                } else {
                    JOptionPane.showMessageDialog(null, " [No se puede, registrar contrato] ", "Mensaje", 0);
                }
            } else {
                JOptionPane.showMessageDialog(null, " No debe seleccionar Cliente", "Mensaje", 1);
            }
        } catch (Exception e) {
            System.out.println("Erro al CrearVenta :" + e.toString());
        } finally {
            temp_totalFacturado = 0.0;
        }

    }

    private void limpiarCajaTexto_Venta() {
        try {
            txtClienteVenta.setText("");
            txtRucClienteVenta.setText("");
            txtDNIClienteVenta.setText("");
            txtNombreProductoVenta.setText("");
            ((DefaultTableModel) jtDetalleProductoVenta.getModel()).setRowCount(0);
            cboTipoDocumentoVenta.setSelectedIndex(0);
            cboFormaPagoVenta.setSelectedIndex(0);
            txtCantProductoVenta.setText("0");
            txtSubTotalMonto.setText("0.0");
            txtIGVPorcentaje.setValue(18);
            txtIGVMontoVenta.setText("0.0");
            txtDescuentoVenta.setValue(0);
            txtMontoTotalVenta.setText("0.0");
            txtAcuentaVenta.setText("0.0");
            txtSaldoVenta.setText("0.0");
            btnBuscarClienteVenta.requestFocus();
        } catch (Exception e) {
            throw e;
        }
    }

    private void cargardatos_codigo_documento_impresion() {
        DefaultTableModel temp = (DefaultTableModel) jtListaCodigoDocImpresion.getModel();
        temp.setRowCount(0);
        for (Codigo_Documento cd : new BLCodigo_Documento().selectCodigoDocumento_all()) {
            Object[] datos = {cd.getInt_id(), cd.getVar_tipodocumento(), cd.getVar_serie(), cd.getVar_numero()};
            temp.addRow(datos);
        }
    }
    /* ------------------------------------------------------------------------ */

    /* INGRESO PRODUCTO */
    private void cargardatos_busqueda_ingproducto(String palabra, String condicion) {
        DefaultTableModel tempIngProd = (DefaultTableModel) jtListaProductoBusquedaIngProd.getModel();
        tempIngProd.setRowCount(0);
        for (ListaProducto p : new BLProducto().get_lista_productos_all_activo(palabra, condicion)) {
            Object[] datos = {p.getInt_id(), p.getCodigobarra(), p.getProducto(), p.getTipoProducto(), p.getMarca(), p.getCategoria(), p.getPreciounitario(), p.getEstado()};
            tempIngProd.addRow(datos);
        }
    }

    private void cargardatos_tipoMovitoIngresoProducto() {
        cboMotivoRegIngProducto.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(5)) {
            cboMotivoRegIngProducto.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboMotivoRegIngProducto);

        cboMotivoBusIngProd.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(5)) {
            cboMotivoBusIngProd.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboMotivoBusIngProd);
    }

    private void cargardatos_proveedorIngProd() {
        cboProveedorRegIngProveedor.removeAllItems();
        for (Proveedor p : new BLProveedor().listar_proveedor("", 1)) {
            cboProveedorRegIngProveedor.addItem(p);
        }
        AutoCompleteDecorator.decorate(cboProveedorRegIngProveedor);

        cboProveedorIngProveedor.removeAllItems();
        for (Proveedor p : new BLProveedor().listar_proveedor("", 1)) {
            cboProveedorIngProveedor.addItem(p);
        }
        AutoCompleteDecorator.decorate(cboProveedorIngProveedor);
    }

    private void limpiarCajaTexto_IngProd() {
        try {
            txtSerieDocIngProd.setText("000000");
            txtNumDocIngProd.setText("00000000000");
            txtFechaEmisionIngProd.setDate(new Date());
            cboTipoDocumentoIngProd.setSelectedIndex(0);
            cboProveedorRegIngProveedor.setSelectedIndex(0);
            cboMotivoRegIngProducto.setSelectedIndex(0);
            txtComentarioIngProd.setText("");
            txtPrecioCostoingProd.setText("");
            txtCantidadProductoingProd.setText("0");
            cboTipoDocumentoIngProd.requestFocus();
            txtNombreProductoIngProd.setText("");
            txtMontoTotalIngProd.setText("");
            txtCantProductoIngProd.setText("0");
            ((DefaultTableModel) jtDetalleProductoIngProd.getModel()).setRowCount(0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void calcular_cantidad_total_ingprod() {
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", simbolos);
        double totalproducto = 0;
        double subTotal = 0;
        DefaultTableModel listadetalle = (DefaultTableModel) jtDetalleProductoIngProd.getModel();
        int fila = listadetalle.getRowCount();
        for (int f = 0; f < fila; f++) {
            totalproducto += Double.parseDouble(String.valueOf(jtDetalleProductoIngProd.getModel().getValueAt(f, 3)));
            subTotal = subTotal + (Double.parseDouble(String.valueOf(jtDetalleProductoIngProd.getModel().getValueAt(f, 4))));
        }
        txtMontoTotalIngProd.setText("" + Double.parseDouble(df.format(subTotal)));
        txtCantProductoIngProd.setText("" + totalproducto);
    }

    private void cargadatos_consultar_ordeningreso() {
        int contador = 0;
        ArrayList<String> lista = new ArrayList();
        String condicionFinal = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        boolean fecha = chkFechasIngProd.isSelected();
        boolean proveedor = chkProveedorIngProd.isSelected();
        boolean documento = chkDocumentoIngProd.isSelected();
        boolean motivo = chkMotivoBusquedaIngProd.isSelected();
        if (fecha == true) {
            lista.add(" ( date(fecha) between '" + sdf.format(txtFechaInicioIngProd.getDate()) + "' and '" + sdf.format(txtFechaFinIngProd.getDate()) + "' ) ");
            contador++;
        }
        if (proveedor == true) {
            lista.add(" ( id_proveedor =" + ((Proveedor) cboProveedorIngProveedor.getSelectedItem()).getInt_id() + " )");
            contador++;
        }
        if (documento == true) {
            lista.add(" ( serie ='" + txtSerieDocBusquedaIngProd.getText().trim() + "' and numero='" + txtNumeroDocBusquedaIngProd.getText().trim() + "' ) ");
            contador++;
        }
        if (motivo == true) {
            lista.add(" ( id_motivo =" + ((Constante) cboMotivoBusSalidaProd.getSelectedItem()).getInt_valor() + " )");
            contador++;
        }

        switch (contador) {
            case 1:
                condicionFinal = lista.get(0);
                break;
            case 2:
                condicionFinal = lista.get(0) + " and " + lista.get(1);
                break;
            case 3:
                condicionFinal = lista.get(0) + " and " + lista.get(1) + " and " + lista.get(2);
                break;
            case 4:
                condicionFinal = lista.get(0) + " and " + lista.get(1) + " and " + lista.get(2) + " and " + lista.get(3);
                break;
        }

        DefaultTableModel tempIngProd = (DefaultTableModel) jtListaBusquedaIngProducto.getModel();
        tempIngProd.setRowCount(0);
        for (ListaOrdenIngreso loi : new BLOrden_Ingreso().get_lista_ordeningreso_all(condicionFinal)) {
            Object datos[] = {
                loi.getId(), loi.getDocumento(), loi.getSerie(), loi.getNumero(), loi.getFecha(), loi.getMotivo(),
                loi.getProveedor(), loi.getPersonal(), loi.getEstado()
            };
            tempIngProd.addRow(datos);
        }
    }

    public void llama_excel_ingreso_producto() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "LISTA_ORDEN_INGRESO.xls");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /* ---------------- */

    /*MOVIMIENTO*/
    private void limpiarCajaTexto_movimiento() {
        txtMotivoMovimiento.setText("");
        txtMontoMovimiento.setText("0.0");
        txtDocumentoMov.setText("");
        cboTipoOperacionMov.setSelectedIndex(0);
        cboFormaPagoMov.setSelectedIndex(0);
        cboTipoDocumentoMov.setSelectedIndex(0);
    }

    private void registrar_movimiento(double monto, int venta, int tipooperacion, String motivo, int tipodocumento,
            String documento, int tipopago, int usuario) {
        int resultado = 0;
        resultado = new BLMovimiento().registar_moviento(monto, venta, tipooperacion, motivo, tipodocumento,
                documento, tipopago, usuario);
        if (resultado != 0) {
            limpiarCajaTexto_movimiento();
            JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void llama_excel_movimiento_stock() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "STOCK_ACTUAL.xls");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**/
    /* TIPO OPERACION */

    private void cargardatos_tipoOperacion() {
        cboTipoOperacionMov.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(2)) {
            cboTipoOperacionMov.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboTipoOperacionMov);
    }
    /* ------------- */

    /* TIPO DOCUMENTO MOV */
    private void cargardatos_tipoDocumentoMov() {
        cboTipoDocumentoMov.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(3)) {
            cboTipoDocumentoMov.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboTipoDocumentoMov);

        cboTipoDocumentoIngProd.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(3)) {
            cboTipoDocumentoIngProd.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboTipoDocumentoIngProd);

    }
    /* ------------- */
    /* TIPO DOCUMENTO MOV */

    private void cargardatos_FormaPago() {
        cboFormaPagoMov.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(4)) {
            cboFormaPagoMov.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboFormaPagoMov);

        cboFormaPagoVenta.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(4)) {
            cboFormaPagoVenta.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboFormaPagoVenta);

        cboBusqFormaPagoVenta.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(4)) {
            cboBusqFormaPagoVenta.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboBusqFormaPagoVenta);
        //Dato Inicial
        obtener_serienumero_venta(((Constante) cboFormaPagoVenta.getSelectedItem()).getInt_valor());
    }
    /* ------------- */
    /* CLIENTE */

    private void cargardatos_cliente(String palabra, int condicion) {
        DefaultTableModel temp = (DefaultTableModel) jtListaCliente.getModel();
        temp.setRowCount(0);
        for (ListaCliente lc : new BLCliente().get_lista_cliente_all(palabra, condicion)) {
            Object[] datos = {lc.getId(), lc.getCliente(), lc.getDni(), lc.getRuc(), lc.getTelefono(), lc.getDireccion(), lc.getEstado().equals("1") ? "Habilitada" : "Inhabilitada"};
            temp.addRow(datos);
        }
    }

    private void cargardatos_clientebusqueda(String palabra, int condicion) {
        DefaultTableModel temp = (DefaultTableModel) jtListaClienteBusq.getModel();
        temp.setRowCount(0);
        for (ListaCliente lc : new BLCliente().get_lista_cliente_all_activos(palabra, condicion)) {
            Object[] datos = {lc.getId(), lc.getCliente(), lc.getDni(), lc.getRuc()};
            temp.addRow(datos);
        }
    }

    private void cargardatos_cliente_busqueda_morosos(String palabra, int condicion) {
        cboClienteListaForVenta.removeAllItems();
        for (ListaCliente lc : new BLCliente().get_lista_cliente_all_activos(palabra, condicion)) {
            cboClienteListaForVenta.addItem(lc);
        }
        AutoCompleteDecorator.decorate(cboClienteListaForVenta);

        cboClienteVenta.removeAllItems();
        for (ListaCliente lc : new BLCliente().get_lista_cliente_all_activos(palabra, condicion)) {
            cboClienteVenta.addItem(lc);
        }
        AutoCompleteDecorator.decorate(cboClienteVenta);

    }

    private void limpiarCajaTexto_cliente() {
        txtApellidoPCliente.setText("");
        txtApellidoMCliente.setText("");
        txtRazonSocial.setText("");
        txtNombreCliente.setText("");
        txtRUCCliente.setText("");
        txtDNICliente.setText("");
        txtTelefonoCliente.setText("");
        txtDireccionCliente.setText("");
        ckConRUC.setSelected(false);
    }

    public void llama_excel_cliente() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "LISTA_CLIENTE.xls");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /* TIPO PRODUCTO */
    private void cargardatos_tipoproducto(String palabra, int condicion) {
        DefaultTableModel temp = (DefaultTableModel) jtListaTipoProducto.getModel();
        temp.setRowCount(0);
        for (Constante c : new BLConstante().get_lista_constante_tipoproducto(palabra, condicion)) {
            Object[] datos = {c.getInt_id(), c.getVar_descripcion()};
            temp.addRow(datos);
        }
    }

    private void limpiarCajaTexto_TipoProducto() {
        txtDescripcionTipo.setText("");
    }

    /* MARCA */
    private void cargardatos_marca(String palabra, int condicion) {
        DefaultTableModel temp = (DefaultTableModel) jtListaMarca.getModel();
        temp.setRowCount(0);
        for (Marca m : new BLMarca().get_lista_marca_all(palabra, condicion)) {
            Object[] datos = {m.getInt_id(), m.getVar_descripcion()};
            temp.addRow(datos);
        }
    }

    private void limpiarCajaTexto_Marca() {
        txtDescripcionMarca.setText("");
    }
    /* --------- */

    /* CATEGORIA */
    private void cargardatos_categoria(String palabra, int condicion) {
        DefaultTableModel temp = (DefaultTableModel) jtListaCategoria.getModel();
        temp.setRowCount(0);
        for (Categoria c : new BLCategoria().get_lista_categoria_all(palabra, condicion)) {
            Object[] datos = {c.getInt_id(), c.getVar_descripcion()};
            temp.addRow(datos);
        }
    }

    private void limpiarCajaTexto_Categoria() {
        txtDescripcionCategoria.setText("");
    }

    /* PRODUCTO */
    private void cargardatos_tipoProducto() {
        cboTipoProducto.removeAllItems();
        for (Constante c : new BLConstante().selectConstante_byClase(1)) {
            cboTipoProducto.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboTipoProducto);
    }

    private void cargardatos_categoria() {
        cboCategoria.removeAllItems();
        for (Categoria c : new BLCategoria().selectCategoria_all()) {
            cboCategoria.addItem(c);
        }
        AutoCompleteDecorator.decorate(cboCategoria);
    }

    private void cargardatos_marca() {
        cboMarca.removeAllItems();
        for (Marca m : new BLMarca().selectMarca_all()) {
            cboMarca.addItem(m);
        }
        AutoCompleteDecorator.decorate(cboMarca);
    }

    private void limpiarCajaTexto_Producto() {
        txtCodigoBarraProducto.setText("");
        txtDescripcionProducto.setText("");
        txtStockMinimo.setText("");
        txtStockMaximo.setText("");
        txtPrecioVentaProducto.setText("");
        cboTipoProducto.setSelectedIndex(0);
        cboCategoria.setSelectedIndex(0);
        cboMarca.setSelectedIndex(0);
        txtUtilidadPorcentajeProducto.setValue(0);
        txtCodigoBarraProducto.setText(new BLProducto().generar_codigobarra_serie());
    }

    private void cargardatos_producto(String palabra, String condicion) {

        cboProductoInventario.removeAllItems();

        DefaultTableModel temp = (DefaultTableModel) jtListaProducto.getModel();
        temp.setRowCount(0);
        for (ListaProducto p : new BLProducto().get_lista_productos_all(palabra, condicion)) {
            Object[] datos = {p.getCodigobarra(), p.getProducto(), p.getTipoProducto(), p.getMarca(),
                p.getCategoria(), p.getPreciounitario(), p.getPrecioventa(), p.getStock_actual(),
                p.getStock_minimo(), p.getEstado()};
            temp.addRow(datos);

            cboProductoInventario.addItem(p);

        }

        AutoCompleteDecorator.decorate(cboProductoInventario);

    }

    private void seleccionar_tipoproducto_byNombre(String nombre) {
        for (int i = 0; i < cboTipoProducto.getItemCount(); i++) {
            if (cboTipoProducto.getItemAt(i).toString().equalsIgnoreCase(nombre)) {
                cboTipoProducto.setSelectedIndex(i);
                i = cboTipoProducto.getItemCount();
            }
        }
    }

    private void seleccionar_marca_byNombre(String nombre) {
        for (int i = 0; i < cboMarca.getItemCount(); i++) {
            if (cboMarca.getItemAt(i).toString().equalsIgnoreCase(nombre)) {
                cboMarca.setSelectedIndex(i);
                i = cboMarca.getItemCount();
            }
        }
    }

    private void seleccionar_categoria_byNombre(String nombre) {
        for (int i = 0; i < cboCategoria.getItemCount(); i++) {
            if (cboCategoria.getItemAt(i).toString().equalsIgnoreCase(nombre)) {
                cboCategoria.setSelectedIndex(i);
                i = cboCategoria.getItemCount();
            }
        }
    }

    public void llama_excel_producto() {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + "LISTA_PRODUCTOS.xls");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void cargar_stock_actual(String filtro) {
        DefaultTableModel temp = (DefaultTableModel) jtStockActual.getModel();
        temp.setRowCount(0);
        for (ListaProducto p : new BLProducto().get_lista_productos_all(filtro, "Descripcion")) {
            Object[] datos = {p.getProducto(), p.getTipoProducto(), p.getMarca(),
                p.getCategoria(), p.getStock_actual()};
            temp.addRow(datos);
        }
    }

    /* ----------- */
    /* PROVEEDORES */
    private void cargardatos_proveedor(String palabra, int condicion) {
        DefaultTableModel temp = (DefaultTableModel) jtListaProveedor.getModel();
        temp.setRowCount(0);
        for (Proveedor p : new BLProveedor().listar_proveedor(palabra, condicion)) {
            Object[] datos = {p.getInt_id(), p.getVar_ruc(), p.getVar_razonsocial(), p.getVar_direccion(), p.getVar_telefono(), p.getVar_estado().equals("1") ? "Habilitada" : "Inhabilitada"};
            temp.addRow(datos);
        }

    }

    private void limpiarCajaTexto_proveedor() {
        txtRucProveedor.setText("");
        txtRazonSocialProveedor.setText("");
        txtTelefonoProveedor.setText("");
        txtDireccionProveedor.setText("");
    }

    /* --- FIN PROVEEDOR --- */
    /* USUARIO */
    private void cargardatos_usuario(String palabra, int condicion) {
        DefaultTableModel temp = (DefaultTableModel) jtListaUsuario.getModel();
        temp.setRowCount(0);
        for (Usuario u : new BLUsuario().listar_usuario(palabra, condicion)) {
            Object[] datos = {u.getInt_id(), u.getVar_nombreapellido(), u.getVar_dni(), u.getVar_idname(), u.getVar_telefono(), u.getVar_estado().equals("1") ? "Habilitada" : "Inhabilitada"};
            temp.addRow(datos);
        }

        cboPersonalRegSalidaProd.removeAllItems();
        for (Usuario u : new BLUsuario().listar_usuario(palabra, condicion)) {
            cboPersonalRegSalidaProd.addItem(u);
        }
        AutoCompleteDecorator.decorate(cboPersonalRegSalidaProd);

        cboPersonalInventario.removeAllItems();
        for (Usuario u : new BLUsuario().listar_usuario(palabra, condicion)) {
            cboPersonalInventario.addItem(u);
        }
        AutoCompleteDecorator.decorate(cboPersonalInventario);

    }

    private void limpiarCajaTexto_usuario() {
        txtNomApeUsuario.setText("");
        txtDNIUsuario.setText("");
        txtTelefonoUsuario.setText("");
        txtDireccionProveedor.setText("");
        txtIDNameUsuario.setText("");
        txtClaveUsuario.setText("");
    }

    /* --- FIN USUARIO --- */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgCliente = new javax.swing.JDialog();
        pnlCliente = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        txtApellidoPCliente = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txtNombreCliente = new javax.swing.JTextField();
        txtDireccionCliente = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        txtDNICliente = new javax.swing.JTextField();
        btnAgregarCliente = new javax.swing.JButton();
        btnSalirCliente = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();
        txtApellidoMCliente = new javax.swing.JTextField();
        txtTelefonoCliente = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txtRazonSocial = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtRUCCliente = new javax.swing.JTextField();
        ckConRUC = new javax.swing.JCheckBox();
        btnEditarCliente = new javax.swing.JButton();
        dlgValidarUsuarioMov = new javax.swing.JDialog();
        jpValidacion = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        txtClaveMovimiento = new javax.swing.JPasswordField();
        btnValidarUsuarioMov = new javax.swing.JButton();
        dlgBuscarProductoIngProd = new javax.swing.JDialog();
        jLabel55 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        jtListaProductoBusquedaIngProd = new javax.swing.JTable();
        cboTipoBusquedaProductoIngProd = new javax.swing.JComboBox();
        txtBuscarProductoIngProd = new javax.swing.JTextField();
        dlgClienteBusqVenta = new javax.swing.JDialog();
        jScrollPane13 = new javax.swing.JScrollPane();
        jtListaClienteBusq = new javax.swing.JTable();
        jLabel71 = new javax.swing.JLabel();
        txtBuscarClienteVenta = new javax.swing.JTextField();
        dlgBuscarProductoVenta = new javax.swing.JDialog();
        jLabel72 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        jtListaProductoBusquedaVenta = new javax.swing.JTable();
        cboTipoBusquedaProductoVenta = new javax.swing.JComboBox();
        txtBuscarProductoVenta = new javax.swing.JTextField();
        dlgDocumentos = new javax.swing.JDialog();
        jScrollPane15 = new javax.swing.JScrollPane();
        jtListaCodigoDocImpresion = new javax.swing.JTable();
        jLabel73 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        cboTipoDocumentoDoc = new javax.swing.JComboBox();
        jLabel75 = new javax.swing.JLabel();
        btnAgregarDatosDoc = new javax.swing.JButton();
        btnEliminarDatosDoc = new javax.swing.JButton();
        jLabel76 = new javax.swing.JLabel();
        txtSerieInicialDoc = new javax.swing.JTextField();
        txtNumeroInicialDoc = new javax.swing.JTextField();
        dlgValidarUsuarioVenta = new javax.swing.JDialog();
        jpValidacionVenta = new javax.swing.JPanel();
        jLabel78 = new javax.swing.JLabel();
        txtClaveVenta = new javax.swing.JPasswordField();
        btnValidarUsuarioVenta = new javax.swing.JButton();
        popupUsuario = new javax.swing.JPopupMenu();
        muEditar = new javax.swing.JMenuItem();
        muEliminar = new javax.swing.JMenuItem();
        muRestaurar = new javax.swing.JMenuItem();
        popupProveedor = new javax.swing.JPopupMenu();
        mpEditar = new javax.swing.JMenuItem();
        mpEliminar = new javax.swing.JMenuItem();
        mpRestaurar = new javax.swing.JMenuItem();
        mpExportarExcellAll = new javax.swing.JMenuItem();
        popupMarca = new javax.swing.JPopupMenu();
        mmEditar = new javax.swing.JMenuItem();
        popupCategoria = new javax.swing.JPopupMenu();
        mcEditar = new javax.swing.JMenuItem();
        popupProducto = new javax.swing.JPopupMenu();
        mpdEditar = new javax.swing.JMenuItem();
        mpdEliminar = new javax.swing.JMenuItem();
        mpdExportarExcellAll = new javax.swing.JMenuItem();
        mpdRestaurar = new javax.swing.JMenuItem();
        popupTipoProducto = new javax.swing.JPopupMenu();
        mtpEditar = new javax.swing.JMenuItem();
        dlgBuscarProductoSalidaProd = new javax.swing.JDialog();
        jLabel84 = new javax.swing.JLabel();
        jScrollPane19 = new javax.swing.JScrollPane();
        jtListaProductoBusquedaSalidaProd = new javax.swing.JTable();
        cboTipoBusquedaProductoSalidaProd = new javax.swing.JComboBox();
        txtBuscarProductoSalidaProd = new javax.swing.JTextField();
        dlgVentaCredito = new javax.swing.JDialog();
        jpVentaCredito = new javax.swing.JPanel();
        jLabel93 = new javax.swing.JLabel();
        btnCrearVentaCredito = new javax.swing.JButton();
        jLabel94 = new javax.swing.JLabel();
        cboFrecuenciaPagoVentaCredito = new javax.swing.JComboBox();
        jLabel95 = new javax.swing.JLabel();
        jLabel96 = new javax.swing.JLabel();
        jLabel97 = new javax.swing.JLabel();
        txtFechaPrimerPagoVentaCredito = new com.toedter.calendar.JDateChooser();
        txtNroCuotaVentaCredito = new com.toedter.components.JSpinField();
        txtMontoTotalVentaCredito = new javax.swing.JTextField();
        txtMontoXCuotaVentaCredito = new javax.swing.JTextField();
        popupConsultarVenta = new javax.swing.JPopupMenu();
        midocumento = new javax.swing.JMenuItem();
        micronograma = new javax.swing.JMenuItem();
        mianularventa = new javax.swing.JMenuItem();
        mimpcronograma = new javax.swing.JMenuItem();
        miExportarExcelAll = new javax.swing.JMenuItem();
        dlgCronogramaPago = new javax.swing.JDialog();
        jpVentaCredito1 = new javax.swing.JPanel();
        btnPagarCronogramaPago = new javax.swing.JButton();
        jLabel99 = new javax.swing.JLabel();
        jLabel101 = new javax.swing.JLabel();
        jScrollPane22 = new javax.swing.JScrollPane();
        jtListaPagosProgramados = new javax.swing.JTable();
        jLabel98 = new javax.swing.JLabel();
        txtMontoAbonarCronogramaP = new javax.swing.JTextField();
        txtMontoCuotaCronogramaP = new javax.swing.JTextField();
        dlgAnularVenta = new javax.swing.JDialog();
        jpVentaCredito2 = new javax.swing.JPanel();
        jLabel103 = new javax.swing.JLabel();
        jScrollPane23 = new javax.swing.JScrollPane();
        txtMotivoAnulacion = new javax.swing.JTextArea();
        jLabel104 = new javax.swing.JLabel();
        txtClaveVentaAnulacion = new javax.swing.JPasswordField();
        btnValidarUsuarioVentaAnulacion = new javax.swing.JButton();
        popupCliente = new javax.swing.JPopupMenu();
        mclEliminar = new javax.swing.JMenuItem();
        mclRestaurar = new javax.swing.JMenuItem();
        mclExportarExcelAll = new javax.swing.JMenuItem();
        popupOrdenIngreso = new javax.swing.JPopupMenu();
        moiAnular = new javax.swing.JMenuItem();
        moidocumento = new javax.swing.JMenuItem();
        moiExportarExcelAll = new javax.swing.JMenuItem();
        popupOrdenSalida = new javax.swing.JPopupMenu();
        mosAnular = new javax.swing.JMenuItem();
        mosdocumento = new javax.swing.JMenuItem();
        mosExportarExcelAll = new javax.swing.JMenuItem();
        popupMovInventario = new javax.swing.JPopupMenu();
        mmovTodos = new javax.swing.JMenuItem();
        dlgDetalleMovimiento = new javax.swing.JDialog();
        jpDetalleMovimientoReporte = new javax.swing.JPanel();
        jLabel120 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel121 = new javax.swing.JLabel();
        txtFechaCajaMovimiento = new com.toedter.calendar.JDateChooser();
        btnDetalleMovimientoCaja = new javax.swing.JButton();
        btnSalirDetalleMovimientoCaja = new javax.swing.JButton();
        dlgDetalleCaja = new javax.swing.JDialog();
        jpDetalleMovimientoReporte1 = new javax.swing.JPanel();
        jLabel122 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jLabel123 = new javax.swing.JLabel();
        txtFechaCaja = new com.toedter.calendar.JDateChooser();
        btnDetalleCaja = new javax.swing.JButton();
        btnSalirDetalleCaja = new javax.swing.JButton();
        dlgListaVentaXCliente = new javax.swing.JDialog();
        jpListaVentasXCliente = new javax.swing.JPanel();
        jLabel124 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel125 = new javax.swing.JLabel();
        btnMostrarListaVentaXCliente = new javax.swing.JButton();
        btnSalirListaVentaXCliente = new javax.swing.JButton();
        cboClienteListaForVenta = new javax.swing.JComboBox();
        dlgListaVentasAnuladas = new javax.swing.JDialog();
        jpDetalleMovimientoReporte3 = new javax.swing.JPanel();
        jLabel126 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel127 = new javax.swing.JLabel();
        txtFechaInicioAlquiler = new com.toedter.calendar.JDateChooser();
        btnListaVentaAnulada = new javax.swing.JButton();
        btnSalirListaVentaAnulada = new javax.swing.JButton();
        txtFechaFinAlquiler = new com.toedter.calendar.JDateChooser();
        jLabel128 = new javax.swing.JLabel();
        dlgListaClientesMorosos = new javax.swing.JDialog();
        jpDetalleClientesMorosos = new javax.swing.JPanel();
        jLabel129 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jLabel130 = new javax.swing.JLabel();
        txtFechaInicioListaMorosos = new com.toedter.calendar.JDateChooser();
        btnListaClienteMorosos = new javax.swing.JButton();
        btnSalirListaClienteMoroso = new javax.swing.JButton();
        txtFechaFinListaClienteMorsos = new com.toedter.calendar.JDateChooser();
        jLabel131 = new javax.swing.JLabel();
        dlgCuadreCaja = new javax.swing.JDialog();
        jpDetalleClientesMorosos1 = new javax.swing.JPanel();
        jLabel132 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jLabel133 = new javax.swing.JLabel();
        txtFechaCuadreCaja = new com.toedter.calendar.JDateChooser();
        btnListaCuadreCaja = new javax.swing.JButton();
        btnSalirCuadreCaja = new javax.swing.JButton();
        dlgPermisosUsuario = new javax.swing.JDialog();
        lblUsuario = new javax.swing.JLabel();
        txtCodigoUsuario = new javax.swing.JTextField();
        txtNomCompletoUsuario = new javax.swing.JTextField();
        jScrollPane21 = new javax.swing.JScrollPane();
        tbListaPermiso = new javax.swing.JTable();
        btnSalirPermisoUsuario = new javax.swing.JButton();
        btnGuardarPermisoUsuario = new javax.swing.JButton();
        popupStockActual = new javax.swing.JPopupMenu();
        jtpPrincipal = new javax.swing.JTabbedPane();
        jpCliente = new javax.swing.JPanel();
        btnAgregarNuevoCliente = new javax.swing.JButton();
        btnEditarNuevoCliente = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtListaCliente = new javax.swing.JTable();
        txtBuscarCliente = new javax.swing.JTextField();
        jpVentas = new javax.swing.JPanel();
        jtbVenta = new javax.swing.JTabbedPane();
        jpCaja = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        btnAperturarCaja = new javax.swing.JButton();
        btnCierreCaja = new javax.swing.JButton();
        txtMontoInicialCaja = new javax.swing.JTextField();
        jpGenerarVenta = new javax.swing.JPanel();
        jLabel59 = new javax.swing.JLabel();
        txtCantProductoVenta = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        txtIGVPorcentaje = new com.toedter.components.JSpinField();
        jLabel62 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        lblfechaActual = new javax.swing.JLabel();
        jScrollPane12 = new javax.swing.JScrollPane();
        jtDetalleProductoVenta = new javax.swing.JTable();
        jLabel56 = new javax.swing.JLabel();
        btnQuitarDetProductoVenta = new javax.swing.JButton();
        btnAgregarDetProdructoVenta = new javax.swing.JButton();
        btnBuscarProductoVenta = new javax.swing.JButton();
        txtNombreProductoVenta = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        txtClienteVenta = new javax.swing.JTextField();
        btnBuscarClienteVenta = new javax.swing.JButton();
        txtRucClienteVenta = new javax.swing.JTextField();
        txtDNIClienteVenta = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        txtDescuentoVenta = new com.toedter.components.JSpinField();
        jLabel68 = new javax.swing.JLabel();
        btnCancelarVenta = new javax.swing.JButton();
        btnCrearVenta = new javax.swing.JButton();
        jLabel69 = new javax.swing.JLabel();
        cboTipoDocumentoVenta = new javax.swing.JComboBox();
        jLabel70 = new javax.swing.JLabel();
        cboFormaPagoVenta = new javax.swing.JComboBox();
        jLabel77 = new javax.swing.JLabel();
        lblNroDocumentoVenta = new javax.swing.JLabel();
        txtCantidadProductoVenta = new javax.swing.JTextField();
        txtPrecioVentaVenta = new javax.swing.JTextField();
        txtIGVMontoVenta = new javax.swing.JTextField();
        txtSubTotalMonto = new javax.swing.JTextField();
        txtMontoTotalVenta = new javax.swing.JTextField();
        txtSaldoVenta = new javax.swing.JTextField();
        txtAcuentaVenta = new javax.swing.JTextField();
        jLabel134 = new javax.swing.JLabel();
        txtVueltoVenta = new javax.swing.JTextField();
        jLabel135 = new javax.swing.JLabel();
        cboTipoVentta = new javax.swing.JComboBox();
        jpConsultarVenta = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        chkFechasVenta = new javax.swing.JCheckBox();
        chkClienteVenta = new javax.swing.JCheckBox();
        chkDocumentoVenta = new javax.swing.JCheckBox();
        chkFormaPagoVenta = new javax.swing.JCheckBox();
        cboBusqFormaPagoVenta = new javax.swing.JComboBox();
        txtFechaInicioVenta = new com.toedter.calendar.JDateChooser();
        jLabel79 = new javax.swing.JLabel();
        txtFechaFinVenta = new com.toedter.calendar.JDateChooser();
        cboClienteVenta = new org.jdesktop.swingx.JXComboBox();
        btnBuscarVenta = new javax.swing.JButton();
        txtSerieDocBusqVenta = new javax.swing.JTextField();
        txtNumeroDocBusqVenta = new javax.swing.JTextField();
        jLabel80 = new javax.swing.JLabel();
        jScrollPane16 = new javax.swing.JScrollPane();
        jtListaBusquedaVenta = new javax.swing.JTable();
        jpMovimientoVenta = new javax.swing.JPanel();
        btnLimpiarM = new javax.swing.JButton();
        jScrollPane9 = new javax.swing.JScrollPane();
        txtMotivoMovimiento = new javax.swing.JTextArea();
        jLabel31 = new javax.swing.JLabel();
        txtMontoMovimiento = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        cboFormaPagoMov = new javax.swing.JComboBox();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        cboTipoOperacionMov = new javax.swing.JComboBox();
        jLabel40 = new javax.swing.JLabel();
        cboTipoDocumentoMov = new javax.swing.JComboBox();
        jLabel41 = new javax.swing.JLabel();
        txtDocumentoMov = new javax.swing.JTextField();
        btnGuardarMovimiento = new javax.swing.JButton();
        jpInventario = new javax.swing.JPanel();
        jtbInventario = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jpRegistrarIngProducto = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jtDetalleProductoIngProd = new javax.swing.JTable();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        cboProveedorRegIngProveedor = new org.jdesktop.swingx.JXComboBox();
        btnAgregarDetProdructo = new javax.swing.JButton();
        btnQuitarDetProducto = new javax.swing.JButton();
        cboMotivoRegIngProducto = new javax.swing.JComboBox();
        jLabel48 = new javax.swing.JLabel();
        txtSerieDocIngProd = new javax.swing.JTextField();
        txtNumDocIngProd = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        cboTipoDocumentoIngProd = new org.jdesktop.swingx.JXComboBox();
        txtFechaEmisionIngProd = new com.toedter.calendar.JDateChooser();
        jLabel52 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel50 = new javax.swing.JLabel();
        txtComentarioIngProd = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();
        txtNombreProductoIngProd = new javax.swing.JTextField();
        btnBuscarProductoIngProd = new javax.swing.JButton();
        btnGuardarIngProd = new javax.swing.JButton();
        btnCancelarIngProd = new javax.swing.JButton();
        txtCantProductoIngProd = new javax.swing.JTextField();
        txtMontoTotalIngProd = new javax.swing.JTextField();
        txtCantidadProductoingProd = new javax.swing.JTextField();
        txtPrecioCostoingProd = new javax.swing.JTextField();
        jpConsultarIngProducto = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jtListaBusquedaIngProducto = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        chkFechasIngProd = new javax.swing.JCheckBox();
        chkProveedorIngProd = new javax.swing.JCheckBox();
        txtFechaInicioIngProd = new com.toedter.calendar.JDateChooser();
        jLabel43 = new javax.swing.JLabel();
        txtFechaFinIngProd = new com.toedter.calendar.JDateChooser();
        cboProveedorIngProveedor = new org.jdesktop.swingx.JXComboBox();
        btnBuscarIngProducto = new javax.swing.JButton();
        cboMotivoBusIngProd = new javax.swing.JComboBox();
        chkMotivoBusquedaIngProd = new javax.swing.JCheckBox();
        txtNumeroDocBusquedaIngProd = new javax.swing.JTextField();
        jLabel92 = new javax.swing.JLabel();
        txtSerieDocBusquedaIngProd = new javax.swing.JTextField();
        chkDocumentoIngProd = new javax.swing.JCheckBox();
        jLabel45 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jpRegistrarSalidaProd = new javax.swing.JPanel();
        jScrollPane17 = new javax.swing.JScrollPane();
        jtDetalleProductoSalidaProd = new javax.swing.JTable();
        jLabel81 = new javax.swing.JLabel();
        jLabel82 = new javax.swing.JLabel();
        cboPersonalRegSalidaProd = new org.jdesktop.swingx.JXComboBox();
        btnAgregarDetProdructoSalidaProd = new javax.swing.JButton();
        btnQuitarDetProductoSalidaProd = new javax.swing.JButton();
        cboMotivoRegSalidaProd = new javax.swing.JComboBox();
        jLabel83 = new javax.swing.JLabel();
        txtFechaEmisionSalidaProd = new com.toedter.calendar.JDateChooser();
        jLabel86 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel87 = new javax.swing.JLabel();
        txtComentarioSalidaProd = new javax.swing.JTextField();
        jLabel88 = new javax.swing.JLabel();
        txtNombreProductoSalidaProd = new javax.swing.JTextField();
        btnBuscarProductoSalidaProd = new javax.swing.JButton();
        btnGuardarSalidaProd = new javax.swing.JButton();
        btnCancelarSalidaProd = new javax.swing.JButton();
        txtCantProductoSalidaProd = new javax.swing.JTextField();
        txtCantidadProductoSalidaProd = new javax.swing.JTextField();
        jpConsultarSalidaProd = new javax.swing.JPanel();
        jScrollPane18 = new javax.swing.JScrollPane();
        jtListaBusquedaSalidaProd = new javax.swing.JTable();
        jPanel9 = new javax.swing.JPanel();
        chkFechasSalidaProd = new javax.swing.JCheckBox();
        chkPersonalSalidaProd = new javax.swing.JCheckBox();
        chkMotivoBusquedaSalidaProd = new javax.swing.JCheckBox();
        cboMotivoBusSalidaProd = new javax.swing.JComboBox();
        txtFechaInicioSalidaProd = new com.toedter.calendar.JDateChooser();
        jLabel89 = new javax.swing.JLabel();
        txtFechaFinSalidaProd = new com.toedter.calendar.JDateChooser();
        cboPersonalSalidaProd = new org.jdesktop.swingx.JXComboBox();
        btnBuscarSalidaProd = new javax.swing.JButton();
        jLabel91 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        chkFechasInventario = new javax.swing.JCheckBox();
        chkPersonalInventario = new javax.swing.JCheckBox();
        chkMotivoInventario = new javax.swing.JCheckBox();
        cboMotivoInventario = new javax.swing.JComboBox();
        txtFechaInicioInventario = new com.toedter.calendar.JDateChooser();
        jLabel90 = new javax.swing.JLabel();
        txtFechaFinInventario = new com.toedter.calendar.JDateChooser();
        btnBuscarInventario = new javax.swing.JButton();
        cboPersonalInventario = new org.jdesktop.swingx.JXComboBox();
        jScrollPane20 = new javax.swing.JScrollPane();
        jtListaMovimientoInventario = new javax.swing.JTable();
        jLabel85 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        chkProductoStockActual = new javax.swing.JCheckBox();
        btnBuscarStockActual = new javax.swing.JButton();
        cboProductoInventario = new org.jdesktop.swingx.JXComboBox();
        jLabel136 = new javax.swing.JLabel();
        jScrollPane27 = new javax.swing.JScrollPane();
        jtStockActual = new javax.swing.JTable();
        jpAdministracion = new javax.swing.JPanel();
        jtbAdministracion = new javax.swing.JTabbedPane();
        jpProducto = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtDescripcionProducto = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtListaProducto = new javax.swing.JTable();
        cboCategoria = new org.jdesktop.swingx.JXComboBox();
        cboMarca = new org.jdesktop.swingx.JXComboBox();
        jLabel7 = new javax.swing.JLabel();
        txtPrecioVentaProducto = new javax.swing.JTextField();
        txtStockMinimo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtStockMaximo = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtCodigoBarraProducto = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        btnGuardarProducto = new javax.swing.JButton();
        btnActualizarProducto = new javax.swing.JButton();
        btnCancelarProducto = new javax.swing.JButton();
        txtUtilidadPorcentajeProducto = new javax.swing.JSpinner();
        cboTipoBusquedaProducto = new javax.swing.JComboBox();
        cboTipoProducto = new org.jdesktop.swingx.JXComboBox();
        jLabel54 = new javax.swing.JLabel();
        txtPrecioCostoProducto = new javax.swing.JTextField();
        txtBuscarProducto = new javax.swing.JTextField();
        jpUsuario = new javax.swing.JPanel();
        txtIDNameUsuario = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtDNIUsuario = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtTelefonoUsuario = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtNomApeUsuario = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtClaveUsuario = new javax.swing.JPasswordField();
        btnGuardarUsuario = new javax.swing.JButton();
        btnActualizarUsuario = new javax.swing.JButton();
        btnCancelarUsuario = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jtListaUsuario = new javax.swing.JTable();
        btnPermisoUsuario = new javax.swing.JButton();
        cboTipoBusquedaUsuario = new javax.swing.JComboBox();
        txtBuscarUsuario = new javax.swing.JTextField();
        jpProveedor = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txtRucProveedor = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtRazonSocialProveedor = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txtTelefonoProveedor = new javax.swing.JTextField();
        txtDireccionProveedor = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        btnGuardarProveedor = new javax.swing.JButton();
        btnActualizarProveedor = new javax.swing.JButton();
        btnCancelarProveedor = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtListaProveedor = new javax.swing.JTable();
        cboTipoBusquedaProveedor = new javax.swing.JComboBox();
        txtBuscarProveedor = new javax.swing.JTextField();
        jpTipoMarcaCategoria = new javax.swing.JPanel();
        jpTipoProducto = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jtListaTipoProducto = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtDescripcionTipo = new javax.swing.JTextField();
        btnGuardarTipoProducto = new javax.swing.JButton();
        btnEditarTipoProducto = new javax.swing.JButton();
        btnCancelarTipoProducto = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        txtBuscarTipoProducto = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jtListaMarca = new javax.swing.JTable();
        txtDescripcionMarca = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnGuardarMarca = new javax.swing.JButton();
        btnEditarMarca = new javax.swing.JButton();
        btnCancelarMarca = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        txtBuscarMarca = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jtListaCategoria = new javax.swing.JTable();
        txtDescripcionCategoria = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        btnGuardarCategoria = new javax.swing.JButton();
        btnEditarCategoria = new javax.swing.JButton();
        btnCancelarCategoria = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        txtBuscarCategoria = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        lblNameTrabajador = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        lblHora = new javax.swing.JLabel();
        mBarraPrincipal = new javax.swing.JMenuBar();
        miHome = new javax.swing.JMenu();
        jmiConfiguracionDocImpresion = new javax.swing.JMenuItem();
        jmiSalirSistema = new javax.swing.JMenuItem();
        miReporte = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        miDetalleMovimientoCaja = new javax.swing.JMenuItem();
        miDetalleCaja = new javax.swing.JMenuItem();
        miCuadreCaja = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        miVentaCliente = new javax.swing.JMenuItem();
        miVentasAnuladas = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        miClienteMorosos = new javax.swing.JMenuItem();
        miAYUDA = new javax.swing.JMenu();

        dlgCliente.setTitle("MAESTRO CLIENTE");
        dlgCliente.setResizable(false);

        pnlCliente.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar Cliente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel27.setText("Nombre(s):");

        txtApellidoPCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtApellidoPClienteKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoPClienteKeyTyped(evt);
            }
        });

        jLabel28.setText("Apellido Paterno:");

        jLabel29.setText("Dirección:");

        txtNombreCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNombreClienteKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreClienteKeyTyped(evt);
            }
        });

        jLabel30.setText("Teléfono:");

        jLabel32.setText("Tipo de Cliente:");

        jLabel33.setText("DNI :");

        txtDNICliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDNIClienteKeyTyped(evt);
            }
        });

        btnAgregarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Add-icon-16x16.png"))); // NOI18N
        btnAgregarCliente.setText("Agregar Cliente               ");
        btnAgregarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarClienteActionPerformed(evt);
            }
        });

        btnSalirCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Login-out-icon-16x16.png"))); // NOI18N
        btnSalirCliente.setText("Atras");
        btnSalirCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirClienteActionPerformed(evt);
            }
        });

        jLabel34.setText("Apellido Materno:");

        txtApellidoMCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtApellidoMClienteKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoMClienteKeyTyped(evt);
            }
        });

        txtTelefonoCliente.setColumns(9);
        txtTelefonoCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtTelefonoCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoClienteKeyTyped(evt);
            }
        });

        jLabel35.setText("Razón Social:");

        jLabel36.setText("RUC");

        txtRUCCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRUCClienteKeyTyped(evt);
            }
        });

        ckConRUC.setText("con RUC");
        ckConRUC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ckConRUCActionPerformed(evt);
            }
        });

        btnEditarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/edit.png"))); // NOI18N
        btnEditarCliente.setText("Editar");
        btnEditarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlClienteLayout = new javax.swing.GroupLayout(pnlCliente);
        pnlCliente.setLayout(pnlClienteLayout);
        pnlClienteLayout.setHorizontalGroup(
            pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlClienteLayout.createSequentialGroup()
                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel28)
                            .addComponent(jLabel27)
                            .addComponent(jLabel33)
                            .addComponent(jLabel32)
                            .addComponent(jLabel29))
                        .addGap(18, 18, 18)
                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlClienteLayout.createSequentialGroup()
                                .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlClienteLayout.createSequentialGroup()
                                        .addComponent(txtDNICliente, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(238, 238, 238)
                                        .addComponent(jLabel30)
                                        .addGap(18, 18, 18))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlClienteLayout.createSequentialGroup()
                                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(txtDireccionCliente, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlClienteLayout.createSequentialGroup()
                                                .addComponent(txtApellidoPCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jLabel34)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtApellidoMCliente)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel35)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(pnlClienteLayout.createSequentialGroup()
                                .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ckConRUC)
                                    .addComponent(txtNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel36)
                                .addGap(47, 47, 47)))
                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtRUCCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10))
                    .addGroup(pnlClienteLayout.createSequentialGroup()
                        .addComponent(btnAgregarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSalirCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlClienteLayout.setVerticalGroup(
            pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlClienteLayout.createSequentialGroup()
                .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ckConRUC)
                    .addComponent(jLabel32))
                .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlClienteLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(txtRUCCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlClienteLayout.createSequentialGroup()
                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtApellidoPCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel28)
                            .addComponent(jLabel34)
                            .addComponent(txtApellidoMCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel35)
                            .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel27))
                            .addComponent(jLabel36))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtDNICliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel33))
                            .addComponent(txtTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel30))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDireccionCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel29))
                .addGap(18, 18, 18)
                .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlClienteLayout.createSequentialGroup()
                        .addGroup(pnlClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnEditarCliente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAgregarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnSalirCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout dlgClienteLayout = new javax.swing.GroupLayout(dlgCliente.getContentPane());
        dlgCliente.getContentPane().setLayout(dlgClienteLayout);
        dlgClienteLayout.setHorizontalGroup(
            dlgClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        dlgClienteLayout.setVerticalGroup(
            dlgClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        dlgValidarUsuarioMov.setTitle("INGRESE CONTRASEÑA");
        dlgValidarUsuarioMov.setResizable(false);

        jpValidacion.setBackground(new java.awt.Color(255, 255, 255));
        jpValidacion.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel42.setText("Contraseña :");

        txtClaveMovimiento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtClaveMovimiento.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnValidarUsuarioMov.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnValidarUsuarioMov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Key.png"))); // NOI18N
        btnValidarUsuarioMov.setText("OK");
        btnValidarUsuarioMov.setIconTextGap(8);
        btnValidarUsuarioMov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarUsuarioMovActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpValidacionLayout = new javax.swing.GroupLayout(jpValidacion);
        jpValidacion.setLayout(jpValidacionLayout);
        jpValidacionLayout.setHorizontalGroup(
            jpValidacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpValidacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel42)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpValidacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnValidarUsuarioMov, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                    .addComponent(txtClaveMovimiento))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpValidacionLayout.setVerticalGroup(
            jpValidacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpValidacionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpValidacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(txtClaveMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(btnValidarUsuarioMov)
                .addContainerGap())
        );

        javax.swing.GroupLayout dlgValidarUsuarioMovLayout = new javax.swing.GroupLayout(dlgValidarUsuarioMov.getContentPane());
        dlgValidarUsuarioMov.getContentPane().setLayout(dlgValidarUsuarioMovLayout);
        dlgValidarUsuarioMovLayout.setHorizontalGroup(
            dlgValidarUsuarioMovLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpValidacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgValidarUsuarioMovLayout.setVerticalGroup(
            dlgValidarUsuarioMovLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpValidacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgBuscarProductoIngProd.setResizable(false);

        jLabel55.setText("Producto :");

        jtListaProductoBusquedaIngProd.setAutoCreateRowSorter(true);
        jtListaProductoBusquedaIngProd.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        jtListaProductoBusquedaIngProd.setModel(ListaProductoBusquedaIngProd);
        jtListaProductoBusquedaIngProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtListaProductoBusquedaIngProdMouseClicked(evt);
            }
        });
        jtListaProductoBusquedaIngProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtListaProductoBusquedaIngProdKeyPressed(evt);
            }
        });
        jScrollPane11.setViewportView(jtListaProductoBusquedaIngProd);

        cboTipoBusquedaProductoIngProd.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Descripcion", "TipoProducto", "Categoria", "Marca", "Codigo" }));

        txtBuscarProductoIngProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarProductoIngProdKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarProductoIngProdKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout dlgBuscarProductoIngProdLayout = new javax.swing.GroupLayout(dlgBuscarProductoIngProd.getContentPane());
        dlgBuscarProductoIngProd.getContentPane().setLayout(dlgBuscarProductoIngProdLayout);
        dlgBuscarProductoIngProdLayout.setHorizontalGroup(
            dlgBuscarProductoIngProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBuscarProductoIngProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBuscarProductoIngProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
                    .addGroup(dlgBuscarProductoIngProdLayout.createSequentialGroup()
                        .addComponent(jLabel55)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarProductoIngProd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTipoBusquedaProductoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgBuscarProductoIngProdLayout.setVerticalGroup(
            dlgBuscarProductoIngProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBuscarProductoIngProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBuscarProductoIngProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel55)
                    .addComponent(cboTipoBusquedaProductoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarProductoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                .addContainerGap())
        );

        dlgClienteBusqVenta.setResizable(false);

        jtListaClienteBusq.setModel(ListaClienteBusqueda);
        jtListaClienteBusq.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtListaClienteBusqMouseClicked(evt);
            }
        });
        jtListaClienteBusq.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtListaClienteBusqKeyPressed(evt);
            }
        });
        jScrollPane13.setViewportView(jtListaClienteBusq);

        jLabel71.setText("Cliente :");

        txtBuscarClienteVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarClienteVentaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout dlgClienteBusqVentaLayout = new javax.swing.GroupLayout(dlgClienteBusqVenta.getContentPane());
        dlgClienteBusqVenta.getContentPane().setLayout(dlgClienteBusqVentaLayout);
        dlgClienteBusqVentaLayout.setHorizontalGroup(
            dlgClienteBusqVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgClienteBusqVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgClienteBusqVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
                    .addGroup(dlgClienteBusqVentaLayout.createSequentialGroup()
                        .addComponent(jLabel71)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarClienteVenta)))
                .addContainerGap())
        );
        dlgClienteBusqVentaLayout.setVerticalGroup(
            dlgClienteBusqVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgClienteBusqVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgClienteBusqVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel71)
                    .addComponent(txtBuscarClienteVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        dlgBuscarProductoVenta.setResizable(false);

        jLabel72.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel72.setText("Producto :");

        jtListaProductoBusquedaVenta.setAutoCreateRowSorter(true);
        jtListaProductoBusquedaVenta.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        jtListaProductoBusquedaVenta.setModel(ListaProductoBusquedaVenta);
        jtListaProductoBusquedaVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtListaProductoBusquedaVentaMouseClicked(evt);
            }
        });
        jtListaProductoBusquedaVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtListaProductoBusquedaVentaKeyPressed(evt);
            }
        });
        jScrollPane14.setViewportView(jtListaProductoBusquedaVenta);

        cboTipoBusquedaProductoVenta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Descripcion", "TipoProducto", "Categoria", "Marca", "Codigo" }));

        txtBuscarProductoVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarProductoVentaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarProductoVentaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout dlgBuscarProductoVentaLayout = new javax.swing.GroupLayout(dlgBuscarProductoVenta.getContentPane());
        dlgBuscarProductoVenta.getContentPane().setLayout(dlgBuscarProductoVentaLayout);
        dlgBuscarProductoVentaLayout.setHorizontalGroup(
            dlgBuscarProductoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBuscarProductoVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBuscarProductoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                    .addGroup(dlgBuscarProductoVentaLayout.createSequentialGroup()
                        .addComponent(jLabel72)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarProductoVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTipoBusquedaProductoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgBuscarProductoVentaLayout.setVerticalGroup(
            dlgBuscarProductoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBuscarProductoVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBuscarProductoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel72)
                    .addComponent(cboTipoBusquedaProductoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarProductoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                .addContainerGap())
        );

        dlgDocumentos.setTitle("CONFIGURAR SERIE Y NUMERO DE IMPRESION");
        dlgDocumentos.setResizable(false);

        jtListaCodigoDocImpresion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Tipo Documento", "Serie", "Numero"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane15.setViewportView(jtListaCodigoDocImpresion);
        if (jtListaCodigoDocImpresion.getColumnModel().getColumnCount() > 0) {
            jtListaCodigoDocImpresion.getColumnModel().getColumn(0).setResizable(false);
            jtListaCodigoDocImpresion.getColumnModel().getColumn(0).setPreferredWidth(20);
            jtListaCodigoDocImpresion.getColumnModel().getColumn(1).setResizable(false);
            jtListaCodigoDocImpresion.getColumnModel().getColumn(1).setPreferredWidth(100);
            jtListaCodigoDocImpresion.getColumnModel().getColumn(2).setResizable(false);
            jtListaCodigoDocImpresion.getColumnModel().getColumn(2).setPreferredWidth(30);
            jtListaCodigoDocImpresion.getColumnModel().getColumn(3).setResizable(false);
            jtListaCodigoDocImpresion.getColumnModel().getColumn(3).setPreferredWidth(50);
        }

        jLabel73.setText("Serie :");

        jLabel74.setText("TIPO DOCUMENTO :");

        jLabel75.setText("Número :");

        btnAgregarDatosDoc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Add-icon-16x16.png"))); // NOI18N
        btnAgregarDatosDoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarDatosDocActionPerformed(evt);
            }
        });

        btnEliminarDatosDoc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Delete-icon-16x16.png"))); // NOI18N

        jLabel76.setBackground(new java.awt.Color(0, 0, 0));
        jLabel76.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel76.setForeground(new java.awt.Color(255, 255, 255));
        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel76.setText("SERIE Y NUMERO INICAL DE LOS DOCUMENTOS DE IMPRESION");
        jLabel76.setOpaque(true);

        txtSerieInicialDoc.setEditable(false);
        txtSerieInicialDoc.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSerieInicialDoc.setText("000");

        txtNumeroInicialDoc.setEditable(false);
        txtNumeroInicialDoc.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumeroInicialDoc.setText("00000");
        txtNumeroInicialDoc.setToolTipText("");

        javax.swing.GroupLayout dlgDocumentosLayout = new javax.swing.GroupLayout(dlgDocumentos.getContentPane());
        dlgDocumentos.getContentPane().setLayout(dlgDocumentosLayout);
        dlgDocumentosLayout.setHorizontalGroup(
            dlgDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgDocumentosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgDocumentosLayout.createSequentialGroup()
                        .addComponent(jLabel73)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSerieInicialDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel75)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroInicialDoc))
                    .addGroup(dlgDocumentosLayout.createSequentialGroup()
                        .addComponent(jLabel74)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboTipoDocumentoDoc, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAgregarDatosDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminarDatosDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(jLabel76, javax.swing.GroupLayout.DEFAULT_SIZE, 411, Short.MAX_VALUE)
        );
        dlgDocumentosLayout.setVerticalGroup(
            dlgDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgDocumentosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel74)
                    .addComponent(cboTipoDocumentoDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlgDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(dlgDocumentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel73)
                        .addComponent(jLabel75)
                        .addComponent(txtSerieInicialDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtNumeroInicialDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnAgregarDatosDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarDatosDoc, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(jLabel76, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        dlgValidarUsuarioVenta.setTitle("INGRESE CONTRASEÑA");
        dlgValidarUsuarioVenta.setResizable(false);

        jpValidacionVenta.setBackground(new java.awt.Color(255, 255, 255));
        jpValidacionVenta.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel78.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel78.setText("Contraseña :");

        txtClaveVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtClaveVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnValidarUsuarioVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnValidarUsuarioVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Key.png"))); // NOI18N
        btnValidarUsuarioVenta.setText("OK");
        btnValidarUsuarioVenta.setIconTextGap(8);
        btnValidarUsuarioVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarUsuarioVentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpValidacionVentaLayout = new javax.swing.GroupLayout(jpValidacionVenta);
        jpValidacionVenta.setLayout(jpValidacionVentaLayout);
        jpValidacionVentaLayout.setHorizontalGroup(
            jpValidacionVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpValidacionVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel78)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpValidacionVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnValidarUsuarioVenta, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                    .addComponent(txtClaveVenta))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpValidacionVentaLayout.setVerticalGroup(
            jpValidacionVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpValidacionVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpValidacionVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel78)
                    .addComponent(txtClaveVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(btnValidarUsuarioVenta)
                .addContainerGap())
        );

        javax.swing.GroupLayout dlgValidarUsuarioVentaLayout = new javax.swing.GroupLayout(dlgValidarUsuarioVenta.getContentPane());
        dlgValidarUsuarioVenta.getContentPane().setLayout(dlgValidarUsuarioVentaLayout);
        dlgValidarUsuarioVentaLayout.setHorizontalGroup(
            dlgValidarUsuarioVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpValidacionVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgValidarUsuarioVentaLayout.setVerticalGroup(
            dlgValidarUsuarioVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpValidacionVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        muEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        muEditar.setText("EDITAR");
        muEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                muEditarActionPerformed(evt);
            }
        });
        popupUsuario.add(muEditar);

        muEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Down.png"))); // NOI18N
        muEliminar.setText("INHABILITAR");
        muEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                muEliminarActionPerformed(evt);
            }
        });
        popupUsuario.add(muEliminar);

        muRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Up.png"))); // NOI18N
        muRestaurar.setText("HABILITAR");
        muRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                muRestaurarActionPerformed(evt);
            }
        });
        popupUsuario.add(muRestaurar);

        mpEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        mpEditar.setText("EDITAR");
        mpEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mpEditarActionPerformed(evt);
            }
        });
        popupProveedor.add(mpEditar);

        mpEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Down.png"))); // NOI18N
        mpEliminar.setText("INHABILITAR");
        mpEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mpEliminarActionPerformed(evt);
            }
        });
        popupProveedor.add(mpEliminar);

        mpRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Up.png"))); // NOI18N
        mpRestaurar.setText("HABILITAR");
        mpRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mpRestaurarActionPerformed(evt);
            }
        });
        popupProveedor.add(mpRestaurar);

        mpExportarExcellAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/excel.png"))); // NOI18N
        mpExportarExcellAll.setText("Exportar a Excel todo");
        popupProveedor.add(mpExportarExcellAll);

        mmEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        mmEditar.setText("EDITAR");
        mmEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mmEditarActionPerformed(evt);
            }
        });
        popupMarca.add(mmEditar);

        mcEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        mcEditar.setText("EDITAR");
        mcEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mcEditarActionPerformed(evt);
            }
        });
        popupCategoria.add(mcEditar);

        mpdEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        mpdEditar.setText("EDITAR");
        mpdEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mpdEditarActionPerformed(evt);
            }
        });
        popupProducto.add(mpdEditar);

        mpdEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Down.png"))); // NOI18N
        mpdEliminar.setText("INHABILITAR");
        mpdEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mpdEliminarActionPerformed(evt);
            }
        });
        popupProducto.add(mpdEliminar);

        mpdExportarExcellAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/excel.png"))); // NOI18N
        mpdExportarExcellAll.setText("Exportar a Excel Todo");
        mpdExportarExcellAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mpdExportarExcellAllActionPerformed(evt);
            }
        });
        popupProducto.add(mpdExportarExcellAll);

        mpdRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Up.png"))); // NOI18N
        mpdRestaurar.setText("HABILITAR");
        mpdRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mpdRestaurarActionPerformed(evt);
            }
        });
        popupProducto.add(mpdRestaurar);

        mtpEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        mtpEditar.setText("EDITAR");
        mtpEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mtpEditarActionPerformed(evt);
            }
        });
        popupTipoProducto.add(mtpEditar);

        dlgBuscarProductoSalidaProd.setResizable(false);

        jLabel84.setText("Producto :");

        jtListaProductoBusquedaSalidaProd.setAutoCreateRowSorter(true);
        jtListaProductoBusquedaSalidaProd.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        jtListaProductoBusquedaSalidaProd.setModel(ListaProductoBusquedaSalProd);
        jtListaProductoBusquedaSalidaProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtListaProductoBusquedaSalidaProdMouseClicked(evt);
            }
        });
        jtListaProductoBusquedaSalidaProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtListaProductoBusquedaSalidaProdKeyPressed(evt);
            }
        });
        jScrollPane19.setViewportView(jtListaProductoBusquedaSalidaProd);

        cboTipoBusquedaProductoSalidaProd.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Descripcion", "TipoProducto", "Categoria", "Marca", "Codigo" }));

        txtBuscarProductoSalidaProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarProductoSalidaProdKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarProductoSalidaProdKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout dlgBuscarProductoSalidaProdLayout = new javax.swing.GroupLayout(dlgBuscarProductoSalidaProd.getContentPane());
        dlgBuscarProductoSalidaProd.getContentPane().setLayout(dlgBuscarProductoSalidaProdLayout);
        dlgBuscarProductoSalidaProdLayout.setHorizontalGroup(
            dlgBuscarProductoSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBuscarProductoSalidaProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBuscarProductoSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane19, javax.swing.GroupLayout.DEFAULT_SIZE, 592, Short.MAX_VALUE)
                    .addGroup(dlgBuscarProductoSalidaProdLayout.createSequentialGroup()
                        .addComponent(jLabel84)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscarProductoSalidaProd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTipoBusquedaProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgBuscarProductoSalidaProdLayout.setVerticalGroup(
            dlgBuscarProductoSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBuscarProductoSalidaProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBuscarProductoSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel84)
                    .addComponent(cboTipoBusquedaProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane19, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                .addContainerGap())
        );

        dlgVentaCredito.setTitle("VENTA CREDITO");
        dlgVentaCredito.setResizable(false);

        jpVentaCredito.setBackground(new java.awt.Color(255, 255, 255));
        jpVentaCredito.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel93.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel93.setText("Nro Cuota :");

        btnCrearVentaCredito.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCrearVentaCredito.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save.png"))); // NOI18N
        btnCrearVentaCredito.setText("CREAR");
        btnCrearVentaCredito.setIconTextGap(8);
        btnCrearVentaCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearVentaCreditoActionPerformed(evt);
            }
        });

        jLabel94.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel94.setText("Monto X Cuota:");

        jLabel95.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel95.setText("Frecuencia Pago :");

        jLabel96.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel96.setText("Monto Total Facturado :");

        jLabel97.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel97.setText("Fecha Primer Pago :");

        txtNroCuotaVentaCredito.setMaximum(50);
        txtNroCuotaVentaCredito.setMinimum(0);
        txtNroCuotaVentaCredito.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                txtNroCuotaVentaCreditoPropertyChange(evt);
            }
        });
        txtNroCuotaVentaCredito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNroCuotaVentaCreditoKeyReleased(evt);
            }
        });

        txtMontoTotalVentaCredito.setEditable(false);
        txtMontoTotalVentaCredito.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtMontoTotalVentaCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoTotalVentaCredito.setText("0.0");

        txtMontoXCuotaVentaCredito.setEditable(false);
        txtMontoXCuotaVentaCredito.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtMontoXCuotaVentaCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoXCuotaVentaCredito.setText("0");

        javax.swing.GroupLayout jpVentaCreditoLayout = new javax.swing.GroupLayout(jpVentaCredito);
        jpVentaCredito.setLayout(jpVentaCreditoLayout);
        jpVentaCreditoLayout.setHorizontalGroup(
            jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpVentaCreditoLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpVentaCreditoLayout.createSequentialGroup()
                        .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel95)
                            .addComponent(jLabel97))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnCrearVentaCredito, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                            .addComponent(cboFrecuenciaPagoVentaCredito, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtFechaPrimerPagoVentaCredito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpVentaCreditoLayout.createSequentialGroup()
                        .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpVentaCreditoLayout.createSequentialGroup()
                                .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel96)
                                    .addComponent(jLabel93))
                                .addGap(0, 10, Short.MAX_VALUE))
                            .addGroup(jpVentaCreditoLayout.createSequentialGroup()
                                .addComponent(jLabel94)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNroCuotaVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMontoTotalVentaCredito, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMontoXCuotaVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(30, 30, 30))
        );
        jpVentaCreditoLayout.setVerticalGroup(
            jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpVentaCreditoLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtMontoTotalVentaCredito)
                    .addComponent(jLabel96, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel93)
                    .addComponent(txtNroCuotaVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel94)
                    .addComponent(txtMontoXCuotaVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel97)
                    .addComponent(txtFechaPrimerPagoVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel95)
                    .addComponent(cboFrecuenciaPagoVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(btnCrearVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout dlgVentaCreditoLayout = new javax.swing.GroupLayout(dlgVentaCredito.getContentPane());
        dlgVentaCredito.getContentPane().setLayout(dlgVentaCreditoLayout);
        dlgVentaCreditoLayout.setHorizontalGroup(
            dlgVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgVentaCreditoLayout.createSequentialGroup()
                .addComponent(jpVentaCredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        dlgVentaCreditoLayout.setVerticalGroup(
            dlgVentaCreditoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpVentaCredito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        midocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Text preview.png"))); // NOI18N
        midocumento.setText("VER DOCUMENTO");
        midocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                midocumentoActionPerformed(evt);
            }
        });
        popupConsultarVenta.add(midocumento);

        micronograma.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/lista.png"))); // NOI18N
        micronograma.setText("VER CRONOGRAMA_PAGO");
        micronograma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                micronogramaActionPerformed(evt);
            }
        });
        popupConsultarVenta.add(micronograma);

        mianularventa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/No.png"))); // NOI18N
        mianularventa.setText("ANULAR VENTA");
        mianularventa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mianularventaActionPerformed(evt);
            }
        });
        popupConsultarVenta.add(mianularventa);

        mimpcronograma.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Print.png"))); // NOI18N
        mimpcronograma.setText("IMPRIMIR CRONOGRAMA PAGO");
        mimpcronograma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mimpcronogramaActionPerformed(evt);
            }
        });
        popupConsultarVenta.add(mimpcronograma);

        miExportarExcelAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/excel.png"))); // NOI18N
        miExportarExcelAll.setText("Exportar a Excel Todo");
        miExportarExcelAll.setToolTipText("");
        miExportarExcelAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExportarExcelAllActionPerformed(evt);
            }
        });
        popupConsultarVenta.add(miExportarExcelAll);

        dlgCronogramaPago.setTitle("VENTA CREDITO");
        dlgCronogramaPago.setResizable(false);

        jpVentaCredito1.setBackground(new java.awt.Color(255, 255, 255));
        jpVentaCredito1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnPagarCronogramaPago.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnPagarCronogramaPago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/money.png"))); // NOI18N
        btnPagarCronogramaPago.setText("PAGAR");
        btnPagarCronogramaPago.setIconTextGap(8);
        btnPagarCronogramaPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagarCronogramaPagoActionPerformed(evt);
            }
        });

        jLabel99.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel99.setText("Monto Abonar :");
        jLabel99.setFocusable(false);

        jLabel101.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel101.setText("Monto Cuota :");
        jLabel101.setFocusable(false);

        jtListaPagosProgramados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Nro Cuota", "Fecha Pago", "Fecha Abono", "Monto a Pagar", "Monto Abonado", "Estado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaPagosProgramados.setFocusable(false);
        jtListaPagosProgramados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtListaPagosProgramadosMouseClicked(evt);
            }
        });
        jScrollPane22.setViewportView(jtListaPagosProgramados);
        if (jtListaPagosProgramados.getColumnModel().getColumnCount() > 0) {
            jtListaPagosProgramados.getColumnModel().getColumn(0).setPreferredWidth(10);
            jtListaPagosProgramados.getColumnModel().getColumn(1).setPreferredWidth(30);
            jtListaPagosProgramados.getColumnModel().getColumn(2).setPreferredWidth(50);
            jtListaPagosProgramados.getColumnModel().getColumn(3).setPreferredWidth(50);
            jtListaPagosProgramados.getColumnModel().getColumn(3).setHeaderValue("Fecha Abono");
            jtListaPagosProgramados.getColumnModel().getColumn(4).setPreferredWidth(70);
            jtListaPagosProgramados.getColumnModel().getColumn(5).setPreferredWidth(70);
            jtListaPagosProgramados.getColumnModel().getColumn(6).setPreferredWidth(20);
        }

        jLabel98.setBackground(new java.awt.Color(0, 0, 0));
        jLabel98.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel98.setForeground(new java.awt.Color(255, 255, 255));
        jLabel98.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel98.setText("LISTA DE PAGOS PROGRAMADOS");
        jLabel98.setFocusable(false);
        jLabel98.setOpaque(true);

        txtMontoAbonarCronogramaP.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMontoAbonarCronogramaP.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoAbonarCronogramaP.setText("0.0");

        txtMontoCuotaCronogramaP.setEditable(false);
        txtMontoCuotaCronogramaP.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMontoCuotaCronogramaP.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoCuotaCronogramaP.setText("0.0");
        txtMontoCuotaCronogramaP.setToolTipText("");

        javax.swing.GroupLayout jpVentaCredito1Layout = new javax.swing.GroupLayout(jpVentaCredito1);
        jpVentaCredito1.setLayout(jpVentaCredito1Layout);
        jpVentaCredito1Layout.setHorizontalGroup(
            jpVentaCredito1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel98, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane22)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpVentaCredito1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel101)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMontoCuotaCronogramaP, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel99)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMontoAbonarCronogramaP, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPagarCronogramaPago, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpVentaCredito1Layout.setVerticalGroup(
            jpVentaCredito1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpVentaCredito1Layout.createSequentialGroup()
                .addComponent(jLabel98, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane22, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpVentaCredito1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel101)
                    .addComponent(jLabel99)
                    .addComponent(btnPagarCronogramaPago, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMontoAbonarCronogramaP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMontoCuotaCronogramaP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout dlgCronogramaPagoLayout = new javax.swing.GroupLayout(dlgCronogramaPago.getContentPane());
        dlgCronogramaPago.getContentPane().setLayout(dlgCronogramaPagoLayout);
        dlgCronogramaPagoLayout.setHorizontalGroup(
            dlgCronogramaPagoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpVentaCredito1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgCronogramaPagoLayout.setVerticalGroup(
            dlgCronogramaPagoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpVentaCredito1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgAnularVenta.setTitle("VENTA CREDITO");
        dlgAnularVenta.setResizable(false);

        jpVentaCredito2.setBackground(new java.awt.Color(255, 255, 255));
        jpVentaCredito2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel103.setBackground(new java.awt.Color(0, 0, 0));
        jLabel103.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel103.setForeground(new java.awt.Color(255, 255, 255));
        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel103.setText("MOTIVO DE ANULACIÓN");
        jLabel103.setFocusable(false);
        jLabel103.setOpaque(true);

        txtMotivoAnulacion.setColumns(20);
        txtMotivoAnulacion.setRows(5);
        txtMotivoAnulacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMotivoAnulacionKeyPressed(evt);
            }
        });
        jScrollPane23.setViewportView(txtMotivoAnulacion);

        jLabel104.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel104.setText("Contraseña :");

        txtClaveVentaAnulacion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtClaveVentaAnulacion.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnValidarUsuarioVentaAnulacion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnValidarUsuarioVentaAnulacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Key.png"))); // NOI18N
        btnValidarUsuarioVentaAnulacion.setText("OK");
        btnValidarUsuarioVentaAnulacion.setIconTextGap(8);
        btnValidarUsuarioVentaAnulacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarUsuarioVentaAnulacionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpVentaCredito2Layout = new javax.swing.GroupLayout(jpVentaCredito2);
        jpVentaCredito2.setLayout(jpVentaCredito2Layout);
        jpVentaCredito2Layout.setHorizontalGroup(
            jpVentaCredito2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel103, javax.swing.GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
            .addComponent(jScrollPane23)
            .addGroup(jpVentaCredito2Layout.createSequentialGroup()
                .addGap(125, 125, 125)
                .addComponent(jLabel104)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtClaveVentaAnulacion, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnValidarUsuarioVentaAnulacion, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpVentaCredito2Layout.setVerticalGroup(
            jpVentaCredito2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpVentaCredito2Layout.createSequentialGroup()
                .addComponent(jLabel103, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jpVentaCredito2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel104)
                    .addComponent(txtClaveVentaAnulacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnValidarUsuarioVentaAnulacion))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dlgAnularVentaLayout = new javax.swing.GroupLayout(dlgAnularVenta.getContentPane());
        dlgAnularVenta.getContentPane().setLayout(dlgAnularVentaLayout);
        dlgAnularVentaLayout.setHorizontalGroup(
            dlgAnularVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpVentaCredito2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgAnularVentaLayout.setVerticalGroup(
            dlgAnularVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpVentaCredito2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        mclEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Down.png"))); // NOI18N
        mclEliminar.setText("INHABILITAR");
        mclEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mclEliminarActionPerformed(evt);
            }
        });
        popupCliente.add(mclEliminar);

        mclRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Up.png"))); // NOI18N
        mclRestaurar.setText("HABILITAR");
        mclRestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mclRestaurarActionPerformed(evt);
            }
        });
        popupCliente.add(mclRestaurar);

        mclExportarExcelAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/excel.png"))); // NOI18N
        mclExportarExcelAll.setText("EXPORTAR A EXCEL TODO");
        mclExportarExcelAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mclExportarExcelAllActionPerformed(evt);
            }
        });
        popupCliente.add(mclExportarExcelAll);

        moiAnular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/No.png"))); // NOI18N
        moiAnular.setText("ANULAR");
        moiAnular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moiAnularActionPerformed(evt);
            }
        });
        popupOrdenIngreso.add(moiAnular);

        moidocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Text preview.png"))); // NOI18N
        moidocumento.setText("VER DOCUMENTO");
        moidocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moidocumentoActionPerformed(evt);
            }
        });
        popupOrdenIngreso.add(moidocumento);

        moiExportarExcelAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/excel.png"))); // NOI18N
        moiExportarExcelAll.setText("Exportar a Excel Todo");
        moiExportarExcelAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moiExportarExcelAllActionPerformed(evt);
            }
        });
        popupOrdenIngreso.add(moiExportarExcelAll);

        mosAnular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/No.png"))); // NOI18N
        mosAnular.setText("ANULAR");
        mosAnular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mosAnularActionPerformed(evt);
            }
        });
        popupOrdenSalida.add(mosAnular);

        mosdocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Text preview.png"))); // NOI18N
        mosdocumento.setText("VER DOCUMENTO");
        mosdocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mosdocumentoActionPerformed(evt);
            }
        });
        popupOrdenSalida.add(mosdocumento);

        mosExportarExcelAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/excel.png"))); // NOI18N
        mosExportarExcelAll.setText("Exportar a Excel Todo");
        mosExportarExcelAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mosExportarExcelAllActionPerformed(evt);
            }
        });
        popupOrdenSalida.add(mosExportarExcelAll);

        mmovTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/excel.png"))); // NOI18N
        mmovTodos.setText("Exportar a Excel Todo");
        mmovTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mmovTodosActionPerformed(evt);
            }
        });
        popupMovInventario.add(mmovTodos);

        dlgDetalleMovimiento.setTitle("Detalle de Movimiento");
        dlgDetalleMovimiento.setResizable(false);

        jpDetalleMovimientoReporte.setBackground(new java.awt.Color(255, 255, 255));

        jLabel120.setBackground(new java.awt.Color(0, 0, 0));
        jLabel120.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel120.setForeground(new java.awt.Color(255, 255, 255));
        jLabel120.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel120.setText("DETALLE DE MOVIMIENTO");
        jLabel120.setFocusable(false);
        jLabel120.setOpaque(true);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jLabel121.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel121.setText("Fecha :");

        txtFechaCajaMovimiento.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnDetalleMovimientoCaja.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnDetalleMovimientoCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-16x16.png"))); // NOI18N
        btnDetalleMovimientoCaja.setText("Mostrar Detalle de Movimientos");
        btnDetalleMovimientoCaja.setIconTextGap(8);
        btnDetalleMovimientoCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleMovimientoCajaActionPerformed(evt);
            }
        });

        btnSalirDetalleMovimientoCaja.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnSalirDetalleMovimientoCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/close_win.png"))); // NOI18N
        btnSalirDetalleMovimientoCaja.setText("Salir");
        btnSalirDetalleMovimientoCaja.setIconTextGap(8);
        btnSalirDetalleMovimientoCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirDetalleMovimientoCajaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel121, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtFechaCajaMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(btnDetalleMovimientoCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 206, Short.MAX_VALUE)
                .addComponent(btnSalirDetalleMovimientoCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel121, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel15Layout.createSequentialGroup()
                        .addComponent(btnSalirDetalleMovimientoCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtFechaCajaMovimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDetalleMovimientoCaja, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jpDetalleMovimientoReporteLayout = new javax.swing.GroupLayout(jpDetalleMovimientoReporte);
        jpDetalleMovimientoReporte.setLayout(jpDetalleMovimientoReporteLayout);
        jpDetalleMovimientoReporteLayout.setHorizontalGroup(
            jpDetalleMovimientoReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel120, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleMovimientoReporteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpDetalleMovimientoReporteLayout.setVerticalGroup(
            jpDetalleMovimientoReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleMovimientoReporteLayout.createSequentialGroup()
                .addComponent(jLabel120, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(533, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dlgDetalleMovimientoLayout = new javax.swing.GroupLayout(dlgDetalleMovimiento.getContentPane());
        dlgDetalleMovimiento.getContentPane().setLayout(dlgDetalleMovimientoLayout);
        dlgDetalleMovimientoLayout.setHorizontalGroup(
            dlgDetalleMovimientoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleMovimientoReporte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgDetalleMovimientoLayout.setVerticalGroup(
            dlgDetalleMovimientoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleMovimientoReporte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgDetalleCaja.setTitle("Detalle de Caja");
        dlgDetalleCaja.setResizable(false);

        jpDetalleMovimientoReporte1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel122.setBackground(new java.awt.Color(0, 0, 0));
        jLabel122.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel122.setForeground(new java.awt.Color(255, 255, 255));
        jLabel122.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel122.setText("DETALLE DE CAJA");
        jLabel122.setFocusable(false);
        jLabel122.setOpaque(true);

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jLabel123.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel123.setText("Fecha :");

        txtFechaCaja.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnDetalleCaja.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnDetalleCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-16x16.png"))); // NOI18N
        btnDetalleCaja.setText("Mostrar Detalle de Caja");
        btnDetalleCaja.setIconTextGap(8);
        btnDetalleCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleCajaActionPerformed(evt);
            }
        });

        btnSalirDetalleCaja.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnSalirDetalleCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/close_win.png"))); // NOI18N
        btnSalirDetalleCaja.setText("Salir");
        btnSalirDetalleCaja.setIconTextGap(8);
        btnSalirDetalleCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirDetalleCajaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel123, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtFechaCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(btnDetalleCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 206, Short.MAX_VALUE)
                .addComponent(btnSalirDetalleCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel123, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel16Layout.createSequentialGroup()
                        .addComponent(btnSalirDetalleCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtFechaCaja, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDetalleCaja, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jpDetalleMovimientoReporte1Layout = new javax.swing.GroupLayout(jpDetalleMovimientoReporte1);
        jpDetalleMovimientoReporte1.setLayout(jpDetalleMovimientoReporte1Layout);
        jpDetalleMovimientoReporte1Layout.setHorizontalGroup(
            jpDetalleMovimientoReporte1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel122, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleMovimientoReporte1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpDetalleMovimientoReporte1Layout.setVerticalGroup(
            jpDetalleMovimientoReporte1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleMovimientoReporte1Layout.createSequentialGroup()
                .addComponent(jLabel122, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(533, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dlgDetalleCajaLayout = new javax.swing.GroupLayout(dlgDetalleCaja.getContentPane());
        dlgDetalleCaja.getContentPane().setLayout(dlgDetalleCajaLayout);
        dlgDetalleCajaLayout.setHorizontalGroup(
            dlgDetalleCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleMovimientoReporte1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgDetalleCajaLayout.setVerticalGroup(
            dlgDetalleCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleMovimientoReporte1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgListaVentaXCliente.setTitle("Detalle de Caja");
        dlgListaVentaXCliente.setResizable(false);

        jpListaVentasXCliente.setBackground(new java.awt.Color(255, 255, 255));

        jLabel124.setBackground(new java.awt.Color(0, 0, 0));
        jLabel124.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel124.setForeground(new java.awt.Color(255, 255, 255));
        jLabel124.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel124.setText("LISTA VENTAS X CLIENTE");
        jLabel124.setFocusable(false);
        jLabel124.setOpaque(true);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jLabel125.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel125.setText("Cliente :");

        btnMostrarListaVentaXCliente.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnMostrarListaVentaXCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-16x16.png"))); // NOI18N
        btnMostrarListaVentaXCliente.setText("VER RESULTADOS");
        btnMostrarListaVentaXCliente.setIconTextGap(8);
        btnMostrarListaVentaXCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarListaVentaXClienteActionPerformed(evt);
            }
        });

        btnSalirListaVentaXCliente.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnSalirListaVentaXCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/close_win.png"))); // NOI18N
        btnSalirListaVentaXCliente.setText("Salir");
        btnSalirListaVentaXCliente.setIconTextGap(8);
        btnSalirListaVentaXCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirListaVentaXClienteActionPerformed(evt);
            }
        });

        cboClienteListaForVenta.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel125, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cboClienteListaForVenta, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnMostrarListaVentaXCliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalirListaVentaXCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboClienteListaForVenta)
                    .addGroup(jPanel17Layout.createSequentialGroup()
                        .addComponent(btnSalirListaVentaXCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel125, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnMostrarListaVentaXCliente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jpListaVentasXClienteLayout = new javax.swing.GroupLayout(jpListaVentasXCliente);
        jpListaVentasXCliente.setLayout(jpListaVentasXClienteLayout);
        jpListaVentasXClienteLayout.setHorizontalGroup(
            jpListaVentasXClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel124, javax.swing.GroupLayout.DEFAULT_SIZE, 981, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpListaVentasXClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpListaVentasXClienteLayout.setVerticalGroup(
            jpListaVentasXClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpListaVentasXClienteLayout.createSequentialGroup()
                .addComponent(jLabel124, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(533, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dlgListaVentaXClienteLayout = new javax.swing.GroupLayout(dlgListaVentaXCliente.getContentPane());
        dlgListaVentaXCliente.getContentPane().setLayout(dlgListaVentaXClienteLayout);
        dlgListaVentaXClienteLayout.setHorizontalGroup(
            dlgListaVentaXClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpListaVentasXCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgListaVentaXClienteLayout.setVerticalGroup(
            dlgListaVentaXClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpListaVentasXCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgListaVentasAnuladas.setTitle("Detalle de Caja");
        dlgListaVentasAnuladas.setResizable(false);

        jpDetalleMovimientoReporte3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel126.setBackground(new java.awt.Color(0, 0, 0));
        jLabel126.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel126.setForeground(new java.awt.Color(255, 255, 255));
        jLabel126.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel126.setText("LISTAS DE VENTAS ANULADAS");
        jLabel126.setFocusable(false);
        jLabel126.setOpaque(true);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jLabel127.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel127.setText("Fecha :");

        txtFechaInicioAlquiler.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnListaVentaAnulada.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnListaVentaAnulada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-16x16.png"))); // NOI18N
        btnListaVentaAnulada.setText("MOSTRAR VENTAS");
        btnListaVentaAnulada.setIconTextGap(8);
        btnListaVentaAnulada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListaVentaAnuladaActionPerformed(evt);
            }
        });

        btnSalirListaVentaAnulada.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnSalirListaVentaAnulada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/close_win.png"))); // NOI18N
        btnSalirListaVentaAnulada.setText("Salir");
        btnSalirListaVentaAnulada.setIconTextGap(8);
        btnSalirListaVentaAnulada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirListaVentaAnuladaActionPerformed(evt);
            }
        });

        txtFechaFinAlquiler.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel128.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel128.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel128.setText("a");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel127, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtFechaInicioAlquiler, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addComponent(jLabel128, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtFechaFinAlquiler, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(btnListaVentaAnulada)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalirListaVentaAnulada, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel127, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaInicioAlquiler, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaFinAlquiler, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalirListaVentaAnulada, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnListaVentaAnulada, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel128, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jpDetalleMovimientoReporte3Layout = new javax.swing.GroupLayout(jpDetalleMovimientoReporte3);
        jpDetalleMovimientoReporte3.setLayout(jpDetalleMovimientoReporte3Layout);
        jpDetalleMovimientoReporte3Layout.setHorizontalGroup(
            jpDetalleMovimientoReporte3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel126, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleMovimientoReporte3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpDetalleMovimientoReporte3Layout.setVerticalGroup(
            jpDetalleMovimientoReporte3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleMovimientoReporte3Layout.createSequentialGroup()
                .addComponent(jLabel126, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(533, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dlgListaVentasAnuladasLayout = new javax.swing.GroupLayout(dlgListaVentasAnuladas.getContentPane());
        dlgListaVentasAnuladas.getContentPane().setLayout(dlgListaVentasAnuladasLayout);
        dlgListaVentasAnuladasLayout.setHorizontalGroup(
            dlgListaVentasAnuladasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleMovimientoReporte3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgListaVentasAnuladasLayout.setVerticalGroup(
            dlgListaVentasAnuladasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleMovimientoReporte3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgListaClientesMorosos.setTitle("Detalle de Caja");
        dlgListaClientesMorosos.setResizable(false);

        jpDetalleClientesMorosos.setBackground(new java.awt.Color(255, 255, 255));

        jLabel129.setBackground(new java.awt.Color(0, 0, 0));
        jLabel129.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel129.setForeground(new java.awt.Color(255, 255, 255));
        jLabel129.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel129.setText("LISTA DE CLIENTES MOROSOS");
        jLabel129.setFocusable(false);
        jLabel129.setOpaque(true);

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jLabel130.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel130.setText("Fecha :");

        txtFechaInicioListaMorosos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnListaClienteMorosos.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnListaClienteMorosos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-16x16.png"))); // NOI18N
        btnListaClienteMorosos.setText("MOSTRAR RESULTADO");
        btnListaClienteMorosos.setIconTextGap(8);
        btnListaClienteMorosos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListaClienteMorososActionPerformed(evt);
            }
        });

        btnSalirListaClienteMoroso.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnSalirListaClienteMoroso.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/close_win.png"))); // NOI18N
        btnSalirListaClienteMoroso.setText("Salir");
        btnSalirListaClienteMoroso.setIconTextGap(8);
        btnSalirListaClienteMoroso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirListaClienteMorosoActionPerformed(evt);
            }
        });

        txtFechaFinListaClienteMorsos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel131.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel131.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel131.setText("a");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel130, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtFechaInicioListaMorosos, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel131, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtFechaFinListaClienteMorsos, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(btnListaClienteMorosos, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalirListaClienteMoroso, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel19Layout.createSequentialGroup()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel130, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaInicioListaMorosos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaFinListaClienteMorsos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalirListaClienteMoroso, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnListaClienteMorosos, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel131, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jpDetalleClientesMorososLayout = new javax.swing.GroupLayout(jpDetalleClientesMorosos);
        jpDetalleClientesMorosos.setLayout(jpDetalleClientesMorososLayout);
        jpDetalleClientesMorososLayout.setHorizontalGroup(
            jpDetalleClientesMorososLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel129, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleClientesMorososLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpDetalleClientesMorososLayout.setVerticalGroup(
            jpDetalleClientesMorososLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleClientesMorososLayout.createSequentialGroup()
                .addComponent(jLabel129, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(533, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dlgListaClientesMorososLayout = new javax.swing.GroupLayout(dlgListaClientesMorosos.getContentPane());
        dlgListaClientesMorosos.getContentPane().setLayout(dlgListaClientesMorososLayout);
        dlgListaClientesMorososLayout.setHorizontalGroup(
            dlgListaClientesMorososLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleClientesMorosos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgListaClientesMorososLayout.setVerticalGroup(
            dlgListaClientesMorososLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleClientesMorosos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgCuadreCaja.setTitle("Detalle de Caja");
        dlgCuadreCaja.setResizable(false);

        jpDetalleClientesMorosos1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel132.setBackground(new java.awt.Color(0, 0, 0));
        jLabel132.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel132.setForeground(new java.awt.Color(255, 255, 255));
        jLabel132.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel132.setText("CUADRE DE CAJA");
        jLabel132.setFocusable(false);
        jLabel132.setOpaque(true);

        jPanel20.setBackground(new java.awt.Color(255, 255, 255));
        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jLabel133.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel133.setText("Fecha :");

        txtFechaCuadreCaja.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnListaCuadreCaja.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnListaCuadreCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-16x16.png"))); // NOI18N
        btnListaCuadreCaja.setText("MOSTRAR RESULTADO");
        btnListaCuadreCaja.setIconTextGap(8);
        btnListaCuadreCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListaCuadreCajaActionPerformed(evt);
            }
        });

        btnSalirCuadreCaja.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        btnSalirCuadreCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/close_win.png"))); // NOI18N
        btnSalirCuadreCaja.setText("Salir");
        btnSalirCuadreCaja.setIconTextGap(8);
        btnSalirCuadreCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirCuadreCajaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addComponent(jLabel133, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtFechaCuadreCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)
                .addComponent(btnListaCuadreCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(btnSalirCuadreCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel133, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaCuadreCaja, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSalirCuadreCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnListaCuadreCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jpDetalleClientesMorosos1Layout = new javax.swing.GroupLayout(jpDetalleClientesMorosos1);
        jpDetalleClientesMorosos1.setLayout(jpDetalleClientesMorosos1Layout);
        jpDetalleClientesMorosos1Layout.setHorizontalGroup(
            jpDetalleClientesMorosos1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel132, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleClientesMorosos1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpDetalleClientesMorosos1Layout.setVerticalGroup(
            jpDetalleClientesMorosos1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpDetalleClientesMorosos1Layout.createSequentialGroup()
                .addComponent(jLabel132, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(527, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dlgCuadreCajaLayout = new javax.swing.GroupLayout(dlgCuadreCaja.getContentPane());
        dlgCuadreCaja.getContentPane().setLayout(dlgCuadreCajaLayout);
        dlgCuadreCajaLayout.setHorizontalGroup(
            dlgCuadreCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleClientesMorosos1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dlgCuadreCajaLayout.setVerticalGroup(
            dlgCuadreCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpDetalleClientesMorosos1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dlgPermisosUsuario.setResizable(false);

        lblUsuario.setText("Usuario:");

        txtCodigoUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCodigoUsuario.setEnabled(false);

        txtNomCompletoUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNomCompletoUsuario.setEnabled(false);

        tbListaPermiso.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbListaPermiso.setColumnSelectionAllowed(true);
        jScrollPane21.setViewportView(tbListaPermiso);

        btnSalirPermisoUsuario.setText("Salir");
        btnSalirPermisoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirPermisoUsuarioActionPerformed(evt);
            }
        });

        btnGuardarPermisoUsuario.setText("Guardar");
        btnGuardarPermisoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarPermisoUsuarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgPermisosUsuarioLayout = new javax.swing.GroupLayout(dlgPermisosUsuario.getContentPane());
        dlgPermisosUsuario.getContentPane().setLayout(dlgPermisosUsuarioLayout);
        dlgPermisosUsuarioLayout.setHorizontalGroup(
            dlgPermisosUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgPermisosUsuarioLayout.createSequentialGroup()
                .addContainerGap(350, Short.MAX_VALUE)
                .addComponent(btnGuardarPermisoUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalirPermisoUsuario)
                .addContainerGap())
            .addGroup(dlgPermisosUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(dlgPermisosUsuarioLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(dlgPermisosUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jScrollPane21, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, dlgPermisosUsuarioLayout.createSequentialGroup()
                            .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtCodigoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtNomCompletoUsuario)))
                    .addContainerGap()))
        );
        dlgPermisosUsuarioLayout.setVerticalGroup(
            dlgPermisosUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgPermisosUsuarioLayout.createSequentialGroup()
                .addContainerGap(326, Short.MAX_VALUE)
                .addGroup(dlgPermisosUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalirPermisoUsuario)
                    .addComponent(btnGuardarPermisoUsuario))
                .addContainerGap())
            .addGroup(dlgPermisosUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(dlgPermisosUsuarioLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(dlgPermisosUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblUsuario)
                        .addComponent(txtCodigoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtNomCompletoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane21, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(42, Short.MAX_VALUE)))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SISTEMA INVENTARIO 1.0");

        jtpPrincipal.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        jtpPrincipal.setToolTipText("");
        jtpPrincipal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jpCliente.setBackground(new java.awt.Color(255, 255, 255));
        jpCliente.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

        btnAgregarNuevoCliente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregarNuevoCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/add_persona.png"))); // NOI18N
        btnAgregarNuevoCliente.setText("AGREGAR CLIENTE");
        btnAgregarNuevoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarNuevoClienteActionPerformed(evt);
            }
        });

        btnEditarNuevoCliente.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnEditarNuevoCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/edit.png"))); // NOI18N
        btnEditarNuevoCliente.setText("EDITAR CLIENTE");
        btnEditarNuevoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarNuevoClienteActionPerformed(evt);
            }
        });

        jtListaCliente.setModel(ListaCliente);
        jtListaCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtListaClienteMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaClienteMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaClienteMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jtListaCliente);

        txtBuscarCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarClienteKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jpClienteLayout = new javax.swing.GroupLayout(jpCliente);
        jpCliente.setLayout(jpClienteLayout);
        jpClienteLayout.setHorizontalGroup(
            jpClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE)
                    .addGroup(jpClienteLayout.createSequentialGroup()
                        .addComponent(txtBuscarCliente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAgregarNuevoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEditarNuevoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jpClienteLayout.setVerticalGroup(
            jpClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarNuevoCliente)
                    .addComponent(btnEditarNuevoCliente)
                    .addComponent(txtBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 541, Short.MAX_VALUE)
                .addContainerGap())
        );

        jtpPrincipal.addTab("CLIENTE", new javax.swing.ImageIcon(getClass().getResource("/recurso/cliente.png")), jpCliente); // NOI18N

        jtbVenta.setBackground(new java.awt.Color(255, 255, 255));
        jtbVenta.setOpaque(true);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Monto Inicial S/.");

        btnAperturarCaja.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnAperturarCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/money.png"))); // NOI18N
        btnAperturarCaja.setText("APERTURAR CAJA");
        btnAperturarCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAperturarCajaActionPerformed(evt);
            }
        });

        btnCierreCaja.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCierreCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Exit.png"))); // NOI18N
        btnCierreCaja.setText("CIERRE CAJA");
        btnCierreCaja.setEnabled(false);
        btnCierreCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCierreCajaActionPerformed(evt);
            }
        });

        txtMontoInicialCaja.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtMontoInicialCaja.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoInicialCaja.setText("0.0");
        txtMontoInicialCaja.setToolTipText("Formato S/. 0.0");
        txtMontoInicialCaja.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMontoInicialCajaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jpCajaLayout = new javax.swing.GroupLayout(jpCaja);
        jpCaja.setLayout(jpCajaLayout);
        jpCajaLayout.setHorizontalGroup(
            jpCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpCajaLayout.createSequentialGroup()
                .addGap(163, 163, 163)
                .addGroup(jpCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAperturarCaja, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                    .addGroup(jpCajaLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtMontoInicialCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnCierreCaja, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(266, Short.MAX_VALUE))
        );
        jpCajaLayout.setVerticalGroup(
            jpCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpCajaLayout.createSequentialGroup()
                .addGap(76, 76, 76)
                .addGroup(jpCajaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtMontoInicialCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpCajaLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(btnAperturarCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCierreCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(329, Short.MAX_VALUE))
        );

        jtbVenta.addTab("  CAJA    ", new javax.swing.ImageIcon(getClass().getResource("/recurso/caja_registradora3.png")), jpCaja); // NOI18N

        jLabel59.setText("CANT. PRODUCT :");

        txtCantProductoVenta.setEditable(false);
        txtCantProductoVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtCantProductoVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantProductoVenta.setText("0");
        txtCantProductoVenta.setToolTipText("");

        jLabel60.setText("SUB. TOTAL :");

        jLabel61.setText("DESCTO. % :");

        txtIGVPorcentaje.setMaximum(100);
        txtIGVPorcentaje.setMinimum(0);
        txtIGVPorcentaje.setValue(18);
        txtIGVPorcentaje.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                txtIGVPorcentajePropertyChange(evt);
            }
        });

        jLabel62.setText("IGV :");

        jLabel67.setText("Fecha Actual :");
        jLabel67.setFocusable(false);

        lblfechaActual.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblfechaActual.setText(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
        lblfechaActual.setFocusable(false);

        jtDetalleProductoVenta.setAutoCreateRowSorter(true);
        jtDetalleProductoVenta.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jtDetalleProductoVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Producto", "Precio Unitario S/.", "Cant. Unid", "Importe S/."
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtDetalleProductoVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtDetalleProductoVentaMouseClicked(evt);
            }
        });
        jScrollPane12.setViewportView(jtDetalleProductoVenta);
        if (jtDetalleProductoVenta.getColumnModel().getColumnCount() > 0) {
            jtDetalleProductoVenta.getColumnModel().getColumn(0).setResizable(false);
            jtDetalleProductoVenta.getColumnModel().getColumn(0).setPreferredWidth(60);
            jtDetalleProductoVenta.getColumnModel().getColumn(1).setResizable(false);
            jtDetalleProductoVenta.getColumnModel().getColumn(1).setPreferredWidth(305);
            jtDetalleProductoVenta.getColumnModel().getColumn(2).setResizable(false);
            jtDetalleProductoVenta.getColumnModel().getColumn(2).setPreferredWidth(90);
            jtDetalleProductoVenta.getColumnModel().getColumn(3).setResizable(false);
            jtDetalleProductoVenta.getColumnModel().getColumn(3).setPreferredWidth(70);
            jtDetalleProductoVenta.getColumnModel().getColumn(4).setResizable(false);
            jtDetalleProductoVenta.getColumnModel().getColumn(4).setPreferredWidth(80);
        }

        jLabel56.setBackground(new java.awt.Color(0, 0, 0));
        jLabel56.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel56.setForeground(new java.awt.Color(255, 255, 255));
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("DETALLE PRODUCTOS");
        jLabel56.setFocusable(false);
        jLabel56.setOpaque(true);

        btnQuitarDetProductoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Delete-icon-16x16.png"))); // NOI18N
        btnQuitarDetProductoVenta.setText("ELIMINAR");
        btnQuitarDetProductoVenta.setEnabled(false);
        btnQuitarDetProductoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarDetProductoVentaActionPerformed(evt);
            }
        });

        btnAgregarDetProdructoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Add-icon-16x16.png"))); // NOI18N
        btnAgregarDetProdructoVenta.setText("AGREGAR");
        btnAgregarDetProdructoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarDetProdructoVentaActionPerformed(evt);
            }
        });

        btnBuscarProductoVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search.png"))); // NOI18N
        btnBuscarProductoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProductoVentaActionPerformed(evt);
            }
        });

        txtNombreProductoVenta.setEditable(false);
        txtNombreProductoVenta.setToolTipText("");
        txtNombreProductoVenta.setFocusable(false);

        jLabel57.setText("Producto :");

        jLabel58.setText("Cliente :");

        txtClienteVenta.setEditable(false);
        txtClienteVenta.setFocusable(false);

        btnBuscarClienteVenta.setText("...");
        btnBuscarClienteVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarClienteVentaActionPerformed(evt);
            }
        });

        txtRucClienteVenta.setEditable(false);
        txtRucClienteVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtRucClienteVenta.setFocusable(false);

        txtDNIClienteVenta.setEditable(false);
        txtDNIClienteVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDNIClienteVenta.setFocusable(false);

        jLabel63.setText("RUC :");

        jLabel64.setText("DNI :");

        jLabel65.setText("A CUENTA :");

        jLabel66.setText("MONTO TOTAL :");

        txtDescuentoVenta.setMaximum(100);
        txtDescuentoVenta.setMinimum(0);
        txtDescuentoVenta.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                txtDescuentoVentaPropertyChange(evt);
            }
        });

        jLabel68.setText("SALDO :");

        btnCancelarVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Cancel-icon.png"))); // NOI18N
        btnCancelarVenta.setText("CANCELAR");
        btnCancelarVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarVentaActionPerformed(evt);
            }
        });

        btnCrearVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save-icon.png"))); // NOI18N
        btnCrearVenta.setText("CREAR VENTA");
        btnCrearVenta.setEnabled(false);
        btnCrearVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearVentaActionPerformed(evt);
            }
        });

        jLabel69.setText("DOCUMENTO :");

        cboTipoDocumentoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoDocumentoVentaActionPerformed(evt);
            }
        });

        jLabel70.setText("FORMA PAGO :");

        cboFormaPagoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFormaPagoVentaActionPerformed(evt);
            }
        });

        jLabel77.setText("Nro Documento :");

        lblNroDocumentoVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblNroDocumentoVenta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNroDocumentoVenta.setText("0000 - 00000000");

        txtCantidadProductoVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtCantidadProductoVenta.setForeground(new java.awt.Color(255, 153, 0));
        txtCantidadProductoVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantidadProductoVenta.setText("0");
        txtCantidadProductoVenta.setToolTipText("Ingreso Cantidad");
        txtCantidadProductoVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadProductoVentaKeyTyped(evt);
            }
        });

        txtPrecioVentaVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtPrecioVentaVenta.setForeground(new java.awt.Color(51, 153, 255));
        txtPrecioVentaVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPrecioVentaVenta.setText("0.0");
        txtPrecioVentaVenta.setToolTipText("Ingreso de Precio Venta");
        txtPrecioVentaVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioVentaVentaKeyTyped(evt);
            }
        });

        txtIGVMontoVenta.setEditable(false);
        txtIGVMontoVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtIGVMontoVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtIGVMontoVenta.setText("0.0");
        txtIGVMontoVenta.setToolTipText("");

        txtSubTotalMonto.setEditable(false);
        txtSubTotalMonto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtSubTotalMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSubTotalMonto.setText("0.0");

        txtMontoTotalVenta.setEditable(false);
        txtMontoTotalVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtMontoTotalVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoTotalVenta.setText("0.0");
        txtMontoTotalVenta.setToolTipText("");

        txtSaldoVenta.setEditable(false);
        txtSaldoVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtSaldoVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSaldoVenta.setText("0.0");

        txtAcuentaVenta.setBackground(new java.awt.Color(255, 255, 204));
        txtAcuentaVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtAcuentaVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAcuentaVenta.setText("0.0");
        txtAcuentaVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtAcuentaVentaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAcuentaVentaKeyTyped(evt);
            }
        });

        jLabel134.setText("VUELTO :");

        txtVueltoVenta.setEditable(false);
        txtVueltoVenta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtVueltoVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtVueltoVenta.setText("0.0");

        jLabel135.setText("TIPO VENTA :");

        cboTipoVentta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Contado", "Credito" }));

        javax.swing.GroupLayout jpGenerarVentaLayout = new javax.swing.GroupLayout(jpGenerarVenta);
        jpGenerarVenta.setLayout(jpGenerarVentaLayout);
        jpGenerarVentaLayout.setHorizontalGroup(
            jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane12)
            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpGenerarVentaLayout.createSequentialGroup()
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel58)
                            .addComponent(jLabel63))
                        .addGap(14, 14, 14)
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                .addComponent(txtRucClienteVenta)
                                .addGap(30, 30, 30)
                                .addComponent(jLabel64)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDNIClienteVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtClienteVenta))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarClienteVenta)
                        .addGap(174, 174, 174))
                    .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel59)
                                    .addComponent(jLabel70))
                                .addGap(9, 9, 9)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtCantProductoVenta, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cboFormaPagoVenta, 0, 121, Short.MAX_VALUE)))
                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel135)
                                    .addComponent(jLabel69, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(23, 23, 23)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cboTipoDocumentoVenta, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboTipoVentta, 0, 121, Short.MAX_VALUE))))
                        .addGap(40, 40, 40)
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel61, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                            .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpGenerarVentaLayout.createSequentialGroup()
                                .addComponent(jLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)
                                .addComponent(txtIGVPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtDescuentoVenta, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                            .addComponent(txtIGVMontoVenta, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSubTotalMonto))
                        .addGap(36, 36, 36)
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel134)
                            .addComponent(jLabel68)
                            .addComponent(jLabel65, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel66))
                        .addGap(6, 6, 6)
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtMontoTotalVenta)
                            .addComponent(txtAcuentaVenta)
                            .addComponent(txtSaldoVenta)
                            .addComponent(txtVueltoVenta))
                        .addContainerGap())
                    .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpGenerarVentaLayout.createSequentialGroup()
                                .addComponent(jLabel57)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNombreProductoVenta, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarProductoVenta)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPrecioVentaVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCantidadProductoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAgregarDetProdructoVenta)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnQuitarDetProductoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpGenerarVentaLayout.createSequentialGroup()
                                .addComponent(jLabel77)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblNroDocumentoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel67)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblfechaActual, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpGenerarVentaLayout.createSequentialGroup()
                                .addComponent(btnCrearVenta)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCancelarVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jpGenerarVentaLayout.setVerticalGroup(
            jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpGenerarVentaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel67)
                            .addComponent(lblfechaActual))
                        .addGap(16, 16, 16))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpGenerarVentaLayout.createSequentialGroup()
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel77)
                            .addComponent(lblNroDocumentoVenta))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel58)
                    .addComponent(txtClienteVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarClienteVenta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRucClienteVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDNIClienteVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel63)
                    .addComponent(jLabel64))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPrecioVentaVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAgregarDetProdructoVenta)
                                .addComponent(btnQuitarDetProductoVenta))
                            .addComponent(btnBuscarProductoVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtNombreProductoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel57))
                            .addComponent(txtCantidadProductoVenta))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtMontoTotalVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(jLabel66, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtAcuentaVenta)
                                    .addComponent(jLabel65, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtSaldoVenta)
                                    .addComponent(jLabel68, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel134)
                                    .addComponent(txtVueltoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(txtSubTotalMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel60))
                                        .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtIGVMontoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addComponent(jLabel62, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                        .addComponent(txtIGVPorcentaje, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(1, 1, 1)))
                                .addGap(7, 7, 7)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDescuentoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel61, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jpGenerarVentaLayout.createSequentialGroup()
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel59)
                                    .addComponent(txtCantProductoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cboTipoVentta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel135))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cboTipoDocumentoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel69))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel70)
                                    .addComponent(cboFormaPagoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpGenerarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnCancelarVenta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCrearVenta, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jtbVenta.addTab("  GENERAR VENTA    ", new javax.swing.ImageIcon(getClass().getResource("/recurso/crear venta.png")), jpGenerarVenta); // NOI18N

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        chkFechasVenta.setSelected(true);
        chkFechasVenta.setText("Fechas :");
        chkFechasVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFechasVentaActionPerformed(evt);
            }
        });

        chkClienteVenta.setText("Cliente :");
        chkClienteVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkClienteVentaActionPerformed(evt);
            }
        });

        chkDocumentoVenta.setText("Documento :");
        chkDocumentoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkDocumentoVentaActionPerformed(evt);
            }
        });

        chkFormaPagoVenta.setText("Forma Pago :");
        chkFormaPagoVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFormaPagoVentaActionPerformed(evt);
            }
        });

        cboBusqFormaPagoVenta.setEnabled(false);

        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel79.setText("al");

        cboClienteVenta.setEnabled(false);

        btnBuscarVenta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBuscarVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-24x24.png"))); // NOI18N
        btnBuscarVenta.setText("BUSCAR");
        btnBuscarVenta.setIconTextGap(10);
        btnBuscarVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarVentaActionPerformed(evt);
            }
        });

        txtSerieDocBusqVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSerieDocBusqVenta.setText("00001");
        txtSerieDocBusqVenta.setEnabled(false);

        txtNumeroDocBusqVenta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumeroDocBusqVenta.setText("00000001");
        txtNumeroDocBusqVenta.setEnabled(false);

        jLabel80.setText("  -  ");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chkFechasVenta)
                            .addComponent(chkClienteVenta))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(txtFechaInicioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel79, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFechaFinVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cboClienteVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(chkDocumentoVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSerieDocBusqVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel80)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroDocBusqVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(chkFormaPagoVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboBusqFormaPagoVenta, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(btnBuscarVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaFinVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(chkFechasVenta)
                        .addComponent(jLabel79))
                    .addComponent(txtFechaInicioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(chkClienteVenta)
                            .addComponent(cboClienteVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboBusqFormaPagoVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkFormaPagoVenta)
                            .addComponent(txtNumeroDocBusqVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel80)
                            .addComponent(txtSerieDocBusqVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkDocumentoVenta)))
                    .addComponent(btnBuscarVenta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jtListaBusquedaVenta.setAutoCreateRowSorter(true);
        jtListaBusquedaVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Fecha", "Documento", "FormaPago", "Total S/.", "A cuenta S/.", "Saldo S/.", "Cliente", "Personal", "Tipo Venta", "Estado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaBusquedaVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaBusquedaVentaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaBusquedaVentaMouseReleased(evt);
            }
        });
        jScrollPane16.setViewportView(jtListaBusquedaVenta);
        if (jtListaBusquedaVenta.getColumnModel().getColumnCount() > 0) {
            jtListaBusquedaVenta.getColumnModel().getColumn(0).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(0).setPreferredWidth(10);
            jtListaBusquedaVenta.getColumnModel().getColumn(1).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(1).setPreferredWidth(25);
            jtListaBusquedaVenta.getColumnModel().getColumn(2).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(2).setPreferredWidth(40);
            jtListaBusquedaVenta.getColumnModel().getColumn(3).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(3).setPreferredWidth(40);
            jtListaBusquedaVenta.getColumnModel().getColumn(4).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(4).setPreferredWidth(35);
            jtListaBusquedaVenta.getColumnModel().getColumn(5).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(5).setPreferredWidth(40);
            jtListaBusquedaVenta.getColumnModel().getColumn(6).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(6).setPreferredWidth(35);
            jtListaBusquedaVenta.getColumnModel().getColumn(7).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(7).setPreferredWidth(50);
            jtListaBusquedaVenta.getColumnModel().getColumn(8).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(8).setPreferredWidth(45);
            jtListaBusquedaVenta.getColumnModel().getColumn(9).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(9).setPreferredWidth(32);
            jtListaBusquedaVenta.getColumnModel().getColumn(10).setResizable(false);
            jtListaBusquedaVenta.getColumnModel().getColumn(10).setPreferredWidth(25);
        }

        javax.swing.GroupLayout jpConsultarVentaLayout = new javax.swing.GroupLayout(jpConsultarVenta);
        jpConsultarVenta.setLayout(jpConsultarVentaLayout);
        jpConsultarVentaLayout.setHorizontalGroup(
            jpConsultarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane16)
        );
        jpConsultarVentaLayout.setVerticalGroup(
            jpConsultarVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpConsultarVentaLayout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE))
        );

        jtbVenta.addTab("  CONSULTAR VENTA    ", new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-24x24.png")), jpConsultarVenta); // NOI18N

        btnLimpiarM.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnLimpiarM.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Delete-icon-16x16.png"))); // NOI18N
        btnLimpiarM.setText("Cancelar");
        btnLimpiarM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarMActionPerformed(evt);
            }
        });

        txtMotivoMovimiento.setColumns(20);
        txtMotivoMovimiento.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtMotivoMovimiento.setRows(5);
        txtMotivoMovimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMotivoMovimientoKeyPressed(evt);
            }
        });
        jScrollPane9.setViewportView(txtMotivoMovimiento);

        jLabel31.setText("Motivo:");

        txtMontoMovimiento.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtMontoMovimiento.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoMovimiento.setText("0.0");
        txtMontoMovimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMontoMovimientoKeyTyped(evt);
            }
        });

        jLabel37.setText("Monto:");

        jLabel38.setText("Forma de Pago:");

        jLabel39.setText("Tipo de Operación:");

        jLabel40.setText("Tipo Documento :");

        cboTipoDocumentoMov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoDocumentoMovActionPerformed(evt);
            }
        });

        jLabel41.setText("Documento :");

        txtDocumentoMov.setEditable(false);
        txtDocumentoMov.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnGuardarMovimiento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnGuardarMovimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save.png"))); // NOI18N
        btnGuardarMovimiento.setText("Guardar");
        btnGuardarMovimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarMovimientoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpMovimientoVentaLayout = new javax.swing.GroupLayout(jpMovimientoVenta);
        jpMovimientoVenta.setLayout(jpMovimientoVentaLayout);
        jpMovimientoVentaLayout.setHorizontalGroup(
            jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpMovimientoVentaLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel31)
                        .addGroup(jpMovimientoVentaLayout.createSequentialGroup()
                            .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpMovimientoVentaLayout.createSequentialGroup()
                                    .addComponent(jLabel38)
                                    .addGap(18, 18, 18)
                                    .addComponent(cboFormaPagoMov, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(jpMovimientoVentaLayout.createSequentialGroup()
                                    .addComponent(jLabel39)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cboTipoOperacionMov, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpMovimientoVentaLayout.createSequentialGroup()
                                    .addComponent(jLabel40)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(cboTipoDocumentoMov, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(35, 35, 35)
                            .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel41)
                                .addComponent(jLabel37))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtDocumentoMov)
                                .addComponent(txtMontoMovimiento, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)))
                        .addComponent(jScrollPane9))
                    .addGroup(jpMovimientoVentaLayout.createSequentialGroup()
                        .addGap(221, 221, 221)
                        .addComponent(btnGuardarMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnLimpiarM, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(200, Short.MAX_VALUE))
        );
        jpMovimientoVentaLayout.setVerticalGroup(
            jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpMovimientoVentaLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel39)
                    .addComponent(cboTipoOperacionMov, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboTipoDocumentoMov, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40)
                    .addComponent(jLabel41)
                    .addComponent(txtDocumentoMov, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(cboFormaPagoMov, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37)
                    .addComponent(txtMontoMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jpMovimientoVentaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLimpiarM, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGuardarMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(237, Short.MAX_VALUE))
        );

        jtbVenta.addTab("  MOVIMIENTO  ", new javax.swing.ImageIcon(getClass().getResource("/recurso/movimiento.png")), jpMovimientoVenta); // NOI18N

        javax.swing.GroupLayout jpVentasLayout = new javax.swing.GroupLayout(jpVentas);
        jpVentas.setLayout(jpVentasLayout);
        jpVentasLayout.setHorizontalGroup(
            jpVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtbVenta)
        );
        jpVentasLayout.setVerticalGroup(
            jpVentasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtbVenta)
        );

        jtpPrincipal.addTab("VENTAS", new javax.swing.ImageIcon(getClass().getResource("/recurso/cart.png")), jpVentas); // NOI18N

        jtbInventario.setBackground(new java.awt.Color(255, 255, 255));
        jtbInventario.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        jtbInventario.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jtbInventario.setOpaque(true);

        jTabbedPane1.setOpaque(true);

        jtDetalleProductoIngProd.setAutoCreateRowSorter(true);
        jtDetalleProductoIngProd.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jtDetalleProductoIngProd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Producto", "Precio Unitario S/.", "Cant. Unid", "Importe S/."
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtDetalleProductoIngProd.setFocusable(false);
        jtDetalleProductoIngProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtDetalleProductoIngProdMouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(jtDetalleProductoIngProd);
        if (jtDetalleProductoIngProd.getColumnModel().getColumnCount() > 0) {
            jtDetalleProductoIngProd.getColumnModel().getColumn(0).setResizable(false);
            jtDetalleProductoIngProd.getColumnModel().getColumn(0).setPreferredWidth(60);
            jtDetalleProductoIngProd.getColumnModel().getColumn(1).setResizable(false);
            jtDetalleProductoIngProd.getColumnModel().getColumn(1).setPreferredWidth(430);
            jtDetalleProductoIngProd.getColumnModel().getColumn(2).setResizable(false);
            jtDetalleProductoIngProd.getColumnModel().getColumn(2).setPreferredWidth(110);
            jtDetalleProductoIngProd.getColumnModel().getColumn(3).setResizable(false);
            jtDetalleProductoIngProd.getColumnModel().getColumn(3).setPreferredWidth(70);
            jtDetalleProductoIngProd.getColumnModel().getColumn(4).setResizable(false);
            jtDetalleProductoIngProd.getColumnModel().getColumn(4).setPreferredWidth(80);
        }

        jLabel46.setBackground(new java.awt.Color(0, 0, 0));
        jLabel46.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel46.setText("DETALLE PRODUCTOS");
        jLabel46.setFocusable(false);
        jLabel46.setOpaque(true);

        jLabel47.setText("Proveedor :");

        btnAgregarDetProdructo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Add-icon-16x16.png"))); // NOI18N
        btnAgregarDetProdructo.setText("AGREGAR");
        btnAgregarDetProdructo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarDetProdructoActionPerformed(evt);
            }
        });

        btnQuitarDetProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Delete-icon-16x16.png"))); // NOI18N
        btnQuitarDetProducto.setText("ELIMINAR");
        btnQuitarDetProducto.setEnabled(false);
        btnQuitarDetProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarDetProductoActionPerformed(evt);
            }
        });

        jLabel48.setText("Motivo :");

        txtSerieDocIngProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSerieDocIngProd.setText("000000");
        txtSerieDocIngProd.setToolTipText("Serie Documento");

        txtNumDocIngProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumDocIngProd.setText("000000000000");
        txtNumDocIngProd.setToolTipText("Número Documento");

        jLabel49.setText("  -  ");

        jLabel51.setText("Tipo Documento :");

        jLabel52.setText("Fecha Emisión:");

        jLabel50.setText("Observación:");

        jLabel53.setText("Producto :");

        txtNombreProductoIngProd.setEditable(false);
        txtNombreProductoIngProd.setToolTipText("");
        txtNombreProductoIngProd.setFocusable(false);

        btnBuscarProductoIngProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search.png"))); // NOI18N
        btnBuscarProductoIngProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProductoIngProdActionPerformed(evt);
            }
        });

        btnGuardarIngProd.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnGuardarIngProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save-icon.png"))); // NOI18N
        btnGuardarIngProd.setText("GUARDAR");
        btnGuardarIngProd.setEnabled(false);
        btnGuardarIngProd.setIconTextGap(5);
        btnGuardarIngProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarIngProdActionPerformed(evt);
            }
        });

        btnCancelarIngProd.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCancelarIngProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Cancel-icon.png"))); // NOI18N
        btnCancelarIngProd.setText("CANCELAR");
        btnCancelarIngProd.setIconTextGap(5);
        btnCancelarIngProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarIngProdActionPerformed(evt);
            }
        });

        txtCantProductoIngProd.setEditable(false);
        txtCantProductoIngProd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtCantProductoIngProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantProductoIngProd.setText("0");

        txtMontoTotalIngProd.setEditable(false);
        txtMontoTotalIngProd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtMontoTotalIngProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoTotalIngProd.setText("0.0");

        txtCantidadProductoingProd.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtCantidadProductoingProd.setForeground(new java.awt.Color(255, 153, 0));
        txtCantidadProductoingProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantidadProductoingProd.setText("0");
        txtCantidadProductoingProd.setToolTipText("Ingrese Cantidad");
        txtCantidadProductoingProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtCantidadProductoingProdMouseClicked(evt);
            }
        });
        txtCantidadProductoingProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadProductoingProdKeyTyped(evt);
            }
        });

        txtPrecioCostoingProd.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtPrecioCostoingProd.setForeground(new java.awt.Color(51, 153, 255));
        txtPrecioCostoingProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPrecioCostoingProd.setText("0.0");
        txtPrecioCostoingProd.setToolTipText("Ingreso de Precio Unitario");
        txtPrecioCostoingProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioCostoingProdKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jpRegistrarIngProductoLayout = new javax.swing.GroupLayout(jpRegistrarIngProducto);
        jpRegistrarIngProducto.setLayout(jpRegistrarIngProductoLayout);
        jpRegistrarIngProductoLayout.setHorizontalGroup(
            jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10)
            .addComponent(jLabel46, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                        .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator4)
                            .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel47)
                                    .addComponent(jLabel48))
                                .addGap(31, 31, 31)
                                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                                        .addComponent(cboMotivoRegIngProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel50)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtComentarioIngProd))
                                    .addComponent(cboProveedorRegIngProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                                .addComponent(jLabel53)
                                .addGap(35, 35, 35)
                                .addComponent(txtNombreProductoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarProductoIngProd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPrecioCostoingProd, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCantidadProductoingProd, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAgregarDetProdructo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnQuitarDetProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 24, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarIngProductoLayout.createSequentialGroup()
                                .addComponent(jLabel51)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cboTipoDocumentoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtSerieDocIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel49)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNumDocIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel52)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFechaEmisionIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarIngProductoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarIngProductoLayout.createSequentialGroup()
                                .addComponent(txtCantProductoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMontoTotalIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarIngProductoLayout.createSequentialGroup()
                                .addComponent(btnGuardarIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnCancelarIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );
        jpRegistrarIngProductoLayout.setVerticalGroup(
            jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel51)
                        .addComponent(cboTipoDocumentoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtNumDocIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel49)
                        .addComponent(txtSerieDocIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel52))
                    .addComponent(txtFechaEmisionIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(cboProveedorRegIngProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel48)
                    .addComponent(cboMotivoRegIngProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel50)
                    .addComponent(txtComentarioIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombreProductoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel53))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jpRegistrarIngProductoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscarProductoIngProd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarIngProductoLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnAgregarDetProdructo)
                                        .addComponent(btnQuitarDetProducto))
                                    .addComponent(txtCantidadProductoingProd, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtPrecioCostoingProd))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCantProductoIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMontoTotalIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpRegistrarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardarIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelarIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(91, 91, 91))
        );

        jTabbedPane1.addTab("REGISTRAR", new javax.swing.ImageIcon(getClass().getResource("/recurso/Clipboard.png")), jpRegistrarIngProducto); // NOI18N

        jtListaBusquedaIngProducto.setAutoCreateRowSorter(true);
        jtListaBusquedaIngProducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Documento", "Serie", "Numero", "Fecha", "Motivo", "Proveedor", "Personal", "Estado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaBusquedaIngProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaBusquedaIngProductoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaBusquedaIngProductoMouseReleased(evt);
            }
        });
        jScrollPane5.setViewportView(jtListaBusquedaIngProducto);
        if (jtListaBusquedaIngProducto.getColumnModel().getColumnCount() > 0) {
            jtListaBusquedaIngProducto.getColumnModel().getColumn(0).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(0).setPreferredWidth(7);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(1).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(1).setPreferredWidth(35);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(1).setHeaderValue("Documento");
            jtListaBusquedaIngProducto.getColumnModel().getColumn(2).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(2).setPreferredWidth(10);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(2).setHeaderValue("Serie");
            jtListaBusquedaIngProducto.getColumnModel().getColumn(3).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(3).setPreferredWidth(20);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(3).setHeaderValue("Numero");
            jtListaBusquedaIngProducto.getColumnModel().getColumn(4).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(4).setPreferredWidth(35);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(5).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(5).setPreferredWidth(30);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(6).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(6).setPreferredWidth(120);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(6).setHeaderValue("Proveedor");
            jtListaBusquedaIngProducto.getColumnModel().getColumn(7).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(7).setPreferredWidth(120);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(8).setResizable(false);
            jtListaBusquedaIngProducto.getColumnModel().getColumn(8).setPreferredWidth(20);
        }

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        chkFechasIngProd.setSelected(true);
        chkFechasIngProd.setText("Fechas :");
        chkFechasIngProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFechasIngProdActionPerformed(evt);
            }
        });

        chkProveedorIngProd.setText("Proveedor :");
        chkProveedorIngProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkProveedorIngProdActionPerformed(evt);
            }
        });

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setText("al");

        cboProveedorIngProveedor.setEnabled(false);

        btnBuscarIngProducto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBuscarIngProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-24x24.png"))); // NOI18N
        btnBuscarIngProducto.setText("BUSCAR");
        btnBuscarIngProducto.setIconTextGap(10);
        btnBuscarIngProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIngProductoActionPerformed(evt);
            }
        });

        cboMotivoBusIngProd.setEnabled(false);

        chkMotivoBusquedaIngProd.setText("Motivo");
        chkMotivoBusquedaIngProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMotivoBusquedaIngProdActionPerformed(evt);
            }
        });

        txtNumeroDocBusquedaIngProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNumeroDocBusquedaIngProd.setText("000000000000");
        txtNumeroDocBusquedaIngProd.setEnabled(false);

        jLabel92.setText("  -  ");

        txtSerieDocBusquedaIngProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSerieDocBusquedaIngProd.setText("000000");
        txtSerieDocBusquedaIngProd.setEnabled(false);

        chkDocumentoIngProd.setText("Documento :");
        chkDocumentoIngProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkDocumentoIngProdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                                .addComponent(chkProveedorIngProd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(chkFechasIngProd)
                                .addGap(22, 22, 22)))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(txtFechaInicioIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFechaFinIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cboProveedorIngProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(chkDocumentoIngProd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSerieDocBusquedaIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel92)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroDocBusquedaIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addComponent(chkMotivoBusquedaIngProd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cboMotivoBusIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addComponent(btnBuscarIngProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaFinIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(chkFechasIngProd)
                        .addComponent(jLabel43))
                    .addComponent(txtFechaInicioIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(chkProveedorIngProd)
                            .addComponent(cboProveedorIngProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboMotivoBusIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkMotivoBusquedaIngProd)
                            .addComponent(txtNumeroDocBusquedaIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel92)
                            .addComponent(txtSerieDocBusquedaIngProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkDocumentoIngProd)))
                    .addComponent(btnBuscarIngProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE))
                .addContainerGap())
        );

        jLabel45.setBackground(new java.awt.Color(0, 0, 0));
        jLabel45.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel45.setForeground(new java.awt.Color(255, 255, 255));
        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel45.setText("LISTA INGRESO DE PRODUCTOS");
        jLabel45.setFocusable(false);
        jLabel45.setOpaque(true);

        javax.swing.GroupLayout jpConsultarIngProductoLayout = new javax.swing.GroupLayout(jpConsultarIngProducto);
        jpConsultarIngProducto.setLayout(jpConsultarIngProductoLayout);
        jpConsultarIngProductoLayout.setHorizontalGroup(
            jpConsultarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel45, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpConsultarIngProductoLayout.setVerticalGroup(
            jpConsultarIngProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpConsultarIngProductoLayout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("CONSULTAR", new javax.swing.ImageIcon(getClass().getResource("/recurso/View.png")), jpConsultarIngProducto); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        jtbInventario.addTab("INGRESO PRODUCTO", new javax.swing.ImageIcon(getClass().getResource("/recurso/checklist.png")), jPanel1); // NOI18N

        jTabbedPane3.setOpaque(true);

        jtDetalleProductoSalidaProd.setAutoCreateRowSorter(true);
        jtDetalleProductoSalidaProd.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jtDetalleProductoSalidaProd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Producto", "Cant. Unid"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtDetalleProductoSalidaProd.setFocusable(false);
        jtDetalleProductoSalidaProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtDetalleProductoSalidaProdMouseClicked(evt);
            }
        });
        jScrollPane17.setViewportView(jtDetalleProductoSalidaProd);
        if (jtDetalleProductoSalidaProd.getColumnModel().getColumnCount() > 0) {
            jtDetalleProductoSalidaProd.getColumnModel().getColumn(0).setResizable(false);
            jtDetalleProductoSalidaProd.getColumnModel().getColumn(0).setPreferredWidth(60);
            jtDetalleProductoSalidaProd.getColumnModel().getColumn(1).setResizable(false);
            jtDetalleProductoSalidaProd.getColumnModel().getColumn(1).setPreferredWidth(450);
            jtDetalleProductoSalidaProd.getColumnModel().getColumn(2).setResizable(false);
            jtDetalleProductoSalidaProd.getColumnModel().getColumn(2).setPreferredWidth(70);
        }

        jLabel81.setBackground(new java.awt.Color(0, 0, 0));
        jLabel81.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel81.setForeground(new java.awt.Color(255, 255, 255));
        jLabel81.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel81.setText("DETALLE PRODUCTOS");
        jLabel81.setFocusable(false);
        jLabel81.setOpaque(true);

        jLabel82.setText("Personal :");

        btnAgregarDetProdructoSalidaProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Add-icon-16x16.png"))); // NOI18N
        btnAgregarDetProdructoSalidaProd.setText("AGREGAR");
        btnAgregarDetProdructoSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarDetProdructoSalidaProdActionPerformed(evt);
            }
        });

        btnQuitarDetProductoSalidaProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Delete-icon-16x16.png"))); // NOI18N
        btnQuitarDetProductoSalidaProd.setText("ELIMINAR");
        btnQuitarDetProductoSalidaProd.setEnabled(false);
        btnQuitarDetProductoSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarDetProductoSalidaProdActionPerformed(evt);
            }
        });

        jLabel83.setText("Motivo :");

        jLabel86.setText("Fecha Emisión:");

        jLabel87.setText("Observación:");

        jLabel88.setText("Producto :");

        txtNombreProductoSalidaProd.setEditable(false);
        txtNombreProductoSalidaProd.setToolTipText("");
        txtNombreProductoSalidaProd.setFocusable(false);

        btnBuscarProductoSalidaProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search.png"))); // NOI18N
        btnBuscarProductoSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProductoSalidaProdActionPerformed(evt);
            }
        });

        btnGuardarSalidaProd.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnGuardarSalidaProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save-icon.png"))); // NOI18N
        btnGuardarSalidaProd.setText("GUARDAR");
        btnGuardarSalidaProd.setEnabled(false);
        btnGuardarSalidaProd.setIconTextGap(5);
        btnGuardarSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarSalidaProdActionPerformed(evt);
            }
        });

        btnCancelarSalidaProd.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCancelarSalidaProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Cancel-icon.png"))); // NOI18N
        btnCancelarSalidaProd.setText("CANCELAR");
        btnCancelarSalidaProd.setIconTextGap(5);
        btnCancelarSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarSalidaProdActionPerformed(evt);
            }
        });

        txtCantProductoSalidaProd.setEditable(false);
        txtCantProductoSalidaProd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtCantProductoSalidaProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantProductoSalidaProd.setText("0");

        txtCantidadProductoSalidaProd.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtCantidadProductoSalidaProd.setForeground(new java.awt.Color(255, 153, 0));
        txtCantidadProductoSalidaProd.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantidadProductoSalidaProd.setText("0");
        txtCantidadProductoSalidaProd.setToolTipText("Ingrese Cantidad");
        txtCantidadProductoSalidaProd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadProductoSalidaProdKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jpRegistrarSalidaProdLayout = new javax.swing.GroupLayout(jpRegistrarSalidaProd);
        jpRegistrarSalidaProd.setLayout(jpRegistrarSalidaProdLayout);
        jpRegistrarSalidaProdLayout.setHorizontalGroup(
            jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane17)
            .addComponent(jLabel81, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jpRegistrarSalidaProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpRegistrarSalidaProdLayout.createSequentialGroup()
                        .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator5)
                            .addGroup(jpRegistrarSalidaProdLayout.createSequentialGroup()
                                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel82)
                                    .addComponent(jLabel83))
                                .addGap(31, 31, 31)
                                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpRegistrarSalidaProdLayout.createSequentialGroup()
                                        .addComponent(cboMotivoRegSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel87)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtComentarioSalidaProd))
                                    .addComponent(cboPersonalRegSalidaProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jpRegistrarSalidaProdLayout.createSequentialGroup()
                                .addComponent(jLabel88)
                                .addGap(28, 28, 28)
                                .addComponent(txtNombreProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarProductoSalidaProd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCantidadProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAgregarDetProdructoSalidaProd)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnQuitarDetProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 24, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarSalidaProdLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel86)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFechaEmisionSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarSalidaProdLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnGuardarSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelarSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarSalidaProdLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(txtCantProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jpRegistrarSalidaProdLayout.setVerticalGroup(
            jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpRegistrarSalidaProdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtFechaEmisionSalidaProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel86, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel82)
                    .addComponent(cboPersonalRegSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel83)
                    .addComponent(cboMotivoRegSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel87)
                    .addComponent(txtComentarioSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpRegistrarSalidaProdLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombreProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel88))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpRegistrarSalidaProdLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAgregarDetProdructoSalidaProd)
                                .addComponent(btnQuitarDetProductoSalidaProd))
                            .addComponent(btnBuscarProductoSalidaProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCantidadProductoSalidaProd))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel81, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane17, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(txtCantProductoSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpRegistrarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnGuardarSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelarSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(91, 91, 91))
        );

        jTabbedPane3.addTab("REGISTRAR", new javax.swing.ImageIcon(getClass().getResource("/recurso/Clipboard.png")), jpRegistrarSalidaProd); // NOI18N

        jtListaBusquedaSalidaProd.setAutoCreateRowSorter(true);
        jtListaBusquedaSalidaProd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Fecha", "Motivo", "Cantidad", "Comentario", "Cliente", "Personal", "Estado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaBusquedaSalidaProd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaBusquedaSalidaProdMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaBusquedaSalidaProdMouseReleased(evt);
            }
        });
        jScrollPane18.setViewportView(jtListaBusquedaSalidaProd);
        if (jtListaBusquedaSalidaProd.getColumnModel().getColumnCount() > 0) {
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(0).setResizable(false);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(0).setPreferredWidth(7);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(1).setPreferredWidth(40);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(2).setResizable(false);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(2).setPreferredWidth(30);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(3).setResizable(false);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(3).setPreferredWidth(50);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(4).setResizable(false);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(4).setPreferredWidth(100);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(6).setResizable(false);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(6).setPreferredWidth(120);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(7).setResizable(false);
            jtListaBusquedaSalidaProd.getColumnModel().getColumn(7).setPreferredWidth(20);
        }

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        chkFechasSalidaProd.setSelected(true);
        chkFechasSalidaProd.setText("Fechas :");
        chkFechasSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFechasSalidaProdActionPerformed(evt);
            }
        });

        chkPersonalSalidaProd.setText("Personal :");
        chkPersonalSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPersonalSalidaProdActionPerformed(evt);
            }
        });

        chkMotivoBusquedaSalidaProd.setText("Motivo");
        chkMotivoBusquedaSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMotivoBusquedaSalidaProdActionPerformed(evt);
            }
        });

        cboMotivoBusSalidaProd.setEnabled(false);

        jLabel89.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel89.setText("al");

        cboPersonalSalidaProd.setEnabled(false);

        btnBuscarSalidaProd.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBuscarSalidaProd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-24x24.png"))); // NOI18N
        btnBuscarSalidaProd.setText("BUSCAR");
        btnBuscarSalidaProd.setIconTextGap(10);
        btnBuscarSalidaProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarSalidaProdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel9Layout.createSequentialGroup()
                        .addComponent(chkMotivoBusquedaSalidaProd)
                        .addGap(32, 32, 32)
                        .addComponent(cboMotivoBusSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chkFechasSalidaProd)
                            .addComponent(chkPersonalSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(2, 2, 2)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(txtFechaInicioSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel89, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFechaFinSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cboPersonalSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addComponent(btnBuscarSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaFinSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(chkFechasSalidaProd)
                        .addComponent(jLabel89))
                    .addComponent(txtFechaInicioSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(chkPersonalSalidaProd)
                            .addComponent(cboPersonalSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboMotivoBusSalidaProd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkMotivoBusquedaSalidaProd)))
                    .addComponent(btnBuscarSalidaProd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jLabel91.setBackground(new java.awt.Color(0, 0, 0));
        jLabel91.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel91.setForeground(new java.awt.Color(255, 255, 255));
        jLabel91.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel91.setText("LISTA SALIDA DE PRODUCTOS");
        jLabel91.setFocusable(false);
        jLabel91.setOpaque(true);

        javax.swing.GroupLayout jpConsultarSalidaProdLayout = new javax.swing.GroupLayout(jpConsultarSalidaProd);
        jpConsultarSalidaProd.setLayout(jpConsultarSalidaProdLayout);
        jpConsultarSalidaProdLayout.setHorizontalGroup(
            jpConsultarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane18)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel91, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpConsultarSalidaProdLayout.setVerticalGroup(
            jpConsultarSalidaProdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpConsultarSalidaProdLayout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel91, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane18, javax.swing.GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE))
        );

        jTabbedPane3.addTab("CONSULTAR", new javax.swing.ImageIcon(getClass().getResource("/recurso/View.png")), jpConsultarSalidaProd); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane3)
        );

        jtbInventario.addTab("SALIDA PRODUCTO", new javax.swing.ImageIcon(getClass().getResource("/recurso/checkout.png")), jPanel2); // NOI18N

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        chkFechasInventario.setSelected(true);
        chkFechasInventario.setText("Fechas :");
        chkFechasInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkFechasInventarioActionPerformed(evt);
            }
        });

        chkPersonalInventario.setText("Personal :");
        chkPersonalInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPersonalInventarioActionPerformed(evt);
            }
        });

        chkMotivoInventario.setText("Movimiento :");
        chkMotivoInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMotivoInventarioActionPerformed(evt);
            }
        });

        cboMotivoInventario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Todos -", "ENTRADA", "SALIDA" }));
        cboMotivoInventario.setEnabled(false);

        jLabel90.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel90.setText("al");

        btnBuscarInventario.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBuscarInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-24x24.png"))); // NOI18N
        btnBuscarInventario.setText("BUSCAR");
        btnBuscarInventario.setIconTextGap(10);
        btnBuscarInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarInventarioActionPerformed(evt);
            }
        });

        cboPersonalInventario.setEnabled(false);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(chkPersonalInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboPersonalInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chkMotivoInventario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboMotivoInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addComponent(btnBuscarInventario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(chkFechasInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtFechaInicioInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel90, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaFinInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chkFechasInventario)
                    .addComponent(txtFechaInicioInventario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaFinInventario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel90, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarInventario)
                    .addComponent(cboMotivoInventario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkMotivoInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkPersonalInventario)
                    .addComponent(cboPersonalInventario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jtListaMovimientoInventario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha - Hora", "Producto", "Tipo", "Cantidad", "Personal"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaMovimientoInventario.setComponentPopupMenu(popupMovInventario);
        jScrollPane20.setViewportView(jtListaMovimientoInventario);
        if (jtListaMovimientoInventario.getColumnModel().getColumnCount() > 0) {
            jtListaMovimientoInventario.getColumnModel().getColumn(0).setPreferredWidth(65);
            jtListaMovimientoInventario.getColumnModel().getColumn(1).setPreferredWidth(240);
            jtListaMovimientoInventario.getColumnModel().getColumn(2).setPreferredWidth(8);
            jtListaMovimientoInventario.getColumnModel().getColumn(3).setPreferredWidth(30);
            jtListaMovimientoInventario.getColumnModel().getColumn(4).setPreferredWidth(85);
        }

        jLabel85.setBackground(new java.awt.Color(0, 0, 0));
        jLabel85.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel85.setForeground(new java.awt.Color(255, 255, 255));
        jLabel85.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel85.setText("MOVIMIENTO DE INVENTARIO");
        jLabel85.setFocusable(false);
        jLabel85.setOpaque(true);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jScrollPane20)
            .addComponent(jLabel85, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel85, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane20, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE))
        );

        jtbInventario.addTab("INVENTARIO", new javax.swing.ImageIcon(getClass().getResource("/recurso/inventory_log.png")), jPanel6); // NOI18N

        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Filtro", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

        chkProductoStockActual.setText("Producto :");
        chkProductoStockActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkProductoStockActualActionPerformed(evt);
            }
        });

        btnBuscarStockActual.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBuscarStockActual.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Search-icon-24x24.png"))); // NOI18N
        btnBuscarStockActual.setText("BUSCAR");
        btnBuscarStockActual.setIconTextGap(10);
        btnBuscarStockActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarStockActualActionPerformed(evt);
            }
        });

        cboProductoInventario.setEnabled(false);

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(chkProductoStockActual, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboProductoInventario, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(btnBuscarStockActual, javax.swing.GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnBuscarStockActual)
                .addComponent(chkProductoStockActual)
                .addComponent(cboProductoInventario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel136.setBackground(new java.awt.Color(0, 0, 0));
        jLabel136.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel136.setForeground(new java.awt.Color(255, 255, 255));
        jLabel136.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel136.setText("MOVIMIENTO DE INVENTARIO");
        jLabel136.setFocusable(false);
        jLabel136.setOpaque(true);

        jtStockActual.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Producto", "Tipo", "Marca", "Categoria", "Stock Final"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtStockActual.setComponentPopupMenu(popupStockActual);
        jScrollPane27.setViewportView(jtStockActual);
        if (jtStockActual.getColumnModel().getColumnCount() > 0) {
            jtStockActual.getColumnModel().getColumn(0).setPreferredWidth(220);
            jtStockActual.getColumnModel().getColumn(1).setPreferredWidth(50);
            jtStockActual.getColumnModel().getColumn(2).setPreferredWidth(50);
            jtStockActual.getColumnModel().getColumn(3).setPreferredWidth(50);
            jtStockActual.getColumnModel().getColumn(4).setPreferredWidth(30);
        }

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel136, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane27)
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel136, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane27, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE))
        );

        jtbInventario.addTab("STOCK ACTUAL", new javax.swing.ImageIcon(getClass().getResource("/recurso/stock.png")), jPanel21); // NOI18N

        javax.swing.GroupLayout jpInventarioLayout = new javax.swing.GroupLayout(jpInventario);
        jpInventario.setLayout(jpInventarioLayout);
        jpInventarioLayout.setHorizontalGroup(
            jpInventarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtbInventario)
        );
        jpInventarioLayout.setVerticalGroup(
            jpInventarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtbInventario)
        );

        jtpPrincipal.addTab("INVENTARIO", new javax.swing.ImageIcon(getClass().getResource("/recurso/inventory.png")), jpInventario); // NOI18N

        jpAdministracion.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

        jtbAdministracion.setBackground(new java.awt.Color(255, 255, 255));
        jtbAdministracion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jtbAdministracion.setOpaque(true);

        jLabel3.setText("Descripción:");

        txtDescripcionProducto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel4.setText("Categoria:");

        jLabel5.setText("Marca:");

        jtListaProducto.setAutoCreateRowSorter(true);
        jtListaProducto.setModel(ListaProducto);
        jtListaProducto.setFocusable(false);
        jtListaProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaProductoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaProductoMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jtListaProducto);

        jLabel7.setText("Precio Venta:");

        txtPrecioVentaProducto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtPrecioVentaProducto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPrecioVentaProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioVentaProductoKeyTyped(evt);
            }
        });

        txtStockMinimo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtStockMinimo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtStockMinimo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockMinimoKeyTyped(evt);
            }
        });

        jLabel8.setText("Stock Mínimo:");

        jLabel9.setText("Stock Máximo:");

        txtStockMaximo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtStockMaximo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtStockMaximo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStockMaximoKeyTyped(evt);
            }
        });

        jLabel10.setText("Tipo Producto :");

        jLabel11.setText("Utilidad %:");

        txtCodigoBarraProducto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtCodigoBarraProducto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCodigoBarraProducto.setToolTipText("Codigo de Barra o Producto");
        txtCodigoBarraProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCodigoBarraProductoKeyTyped(evt);
            }
        });

        jLabel13.setText("Codigo:");

        jLabel14.setBackground(new java.awt.Color(0, 0, 0));
        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("LISTA DE PRODUCTOS");
        jLabel14.setFocusable(false);
        jLabel14.setOpaque(true);

        btnGuardarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save.png"))); // NOI18N
        btnGuardarProducto.setText("GUARDAR");
        btnGuardarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarProductoActionPerformed(evt);
            }
        });

        btnActualizarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        btnActualizarProducto.setText("MODIFICAR");
        btnActualizarProducto.setEnabled(false);
        btnActualizarProducto.setIconTextGap(8);
        btnActualizarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarProductoActionPerformed(evt);
            }
        });

        btnCancelarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/cancelar.png"))); // NOI18N
        btnCancelarProducto.setText("CANCELAR");
        btnCancelarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarProductoActionPerformed(evt);
            }
        });

        txtUtilidadPorcentajeProducto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        cboTipoBusquedaProducto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Descripcion", "TipoProducto", "Categoria", "Marca", "Codigo" }));

        jLabel54.setText("Precio Costo:");

        txtPrecioCostoProducto.setEditable(false);
        txtPrecioCostoProducto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPrecioCostoProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioCostoProductoKeyTyped(evt);
            }
        });

        txtBuscarProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarProductoKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jpProductoLayout = new javax.swing.GroupLayout(jpProducto);
        jpProducto.setLayout(jpProductoLayout);
        jpProductoLayout.setHorizontalGroup(
            jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
            .addGroup(jpProductoLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jpProductoLayout.createSequentialGroup()
                        .addGap(322, 322, 322)
                        .addComponent(txtDescripcionProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpProductoLayout.createSequentialGroup()
                        .addComponent(btnGuardarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnActualizarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnCancelarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpProductoLayout.createSequentialGroup()
                        .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jpProductoLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jpProductoLayout.createSequentialGroup()
                                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel13))
                                .addGap(10, 10, 10)
                                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCodigoBarraProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboTipoProducto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jpProductoLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cboMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(30, 30, 30)
                        .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(jpProductoLayout.createSequentialGroup()
                                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel54))
                                .addGap(26, 26, 26)
                                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtStockMinimo, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                                    .addComponent(txtUtilidadPorcentajeProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                                    .addComponent(txtPrecioCostoProducto))
                                .addGap(29, 29, 29)
                                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel7))
                                .addGap(30, 30, 30)
                                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPrecioVentaProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtStockMaximo, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(57, Short.MAX_VALUE))
            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jpProductoLayout.createSequentialGroup()
                .addComponent(txtBuscarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboTipoBusquedaProducto, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpProductoLayout.setVerticalGroup(
            jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpProductoLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCodigoBarraProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel3)
                    .addComponent(txtDescripcionProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtStockMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtStockMaximo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboTipoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txtPrecioVentaProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel54)
                    .addComponent(txtPrecioCostoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel11)
                    .addComponent(txtUtilidadPorcentajeProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnActualizarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboTipoBusquedaProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(txtBuscarProducto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jtbAdministracion.addTab("PRODUCTO  ", new javax.swing.ImageIcon(getClass().getResource("/recurso/producto.png")), jpProducto); // NOI18N

        txtIDNameUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtIDNameUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel20.setText("ID Name :");

        txtDNIUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtDNIUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDNIUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDNIUsuarioKeyTyped(evt);
            }
        });

        jLabel21.setText("DNI :");

        txtTelefonoUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtTelefonoUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel22.setText("Télefono :");

        txtNomApeUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel23.setText("Nombres y Apellidos :");

        jLabel24.setText("Contraseña :");

        txtClaveUsuario.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtClaveUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnGuardarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save.png"))); // NOI18N
        btnGuardarUsuario.setText("GUARDAR");
        btnGuardarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarUsuarioActionPerformed(evt);
            }
        });

        btnActualizarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        btnActualizarUsuario.setText("MODIFICAR");
        btnActualizarUsuario.setEnabled(false);
        btnActualizarUsuario.setIconTextGap(8);
        btnActualizarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarUsuarioActionPerformed(evt);
            }
        });

        btnCancelarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/cancelar.png"))); // NOI18N
        btnCancelarUsuario.setText("CANCELAR");
        btnCancelarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarUsuarioActionPerformed(evt);
            }
        });

        jLabel25.setBackground(new java.awt.Color(0, 0, 0));
        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("LISTA DE USUARIOS");
        jLabel25.setFocusable(false);
        jLabel25.setOpaque(true);

        jtListaUsuario.setAutoCreateRowSorter(true);
        jtListaUsuario.setModel(ListaUsuario);
        jtListaUsuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtListaUsuarioMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaUsuarioMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaUsuarioMouseReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jtListaUsuario);

        btnPermisoUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/lista.png"))); // NOI18N
        btnPermisoUsuario.setText("PERMISOS");
        btnPermisoUsuario.setEnabled(false);
        btnPermisoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPermisoUsuarioActionPerformed(evt);
            }
        });

        cboTipoBusquedaUsuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "DNI", "Nombre y Apellidos", "ID Name" }));
        cboTipoBusquedaUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoBusquedaUsuarioActionPerformed(evt);
            }
        });

        txtBuscarUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarUsuarioKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jpUsuarioLayout = new javax.swing.GroupLayout(jpUsuario);
        jpUsuario.setLayout(jpUsuarioLayout);
        jpUsuarioLayout.setHorizontalGroup(
            jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane4)
            .addGroup(jpUsuarioLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpUsuarioLayout.createSequentialGroup()
                        .addComponent(btnGuardarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jpUsuarioLayout.createSequentialGroup()
                        .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpUsuarioLayout.createSequentialGroup()
                                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(txtIDNameUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtDNIUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jpUsuarioLayout.createSequentialGroup()
                                        .addGap(32, 32, 32)
                                        .addComponent(btnActualizarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(9, 9, 9)
                                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpUsuarioLayout.createSequentialGroup()
                                        .addComponent(btnPermisoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnCancelarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpUsuarioLayout.createSequentialGroup()
                                            .addComponent(jLabel24)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtClaveUsuario))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpUsuarioLayout.createSequentialGroup()
                                            .addComponent(jLabel22)
                                            .addGap(18, 18, 18)
                                            .addComponent(txtTelefonoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addComponent(txtNomApeUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(80, Short.MAX_VALUE))))
            .addGroup(jpUsuarioLayout.createSequentialGroup()
                .addComponent(txtBuscarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboTipoBusquedaUsuario, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpUsuarioLayout.setVerticalGroup(
            jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpUsuarioLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNomApeUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addGap(18, 18, 18)
                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtTelefonoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel22))
                    .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtDNIUsuario)
                        .addComponent(jLabel21)))
                .addGap(18, 18, 18)
                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIDNameUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24)
                    .addComponent(txtClaveUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnActualizarUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelarUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPermisoUsuario))
                .addGap(18, 18, 18)
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboTipoBusquedaUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jtbAdministracion.addTab("USUARIO  ", new javax.swing.ImageIcon(getClass().getResource("/recurso/user.png")), jpUsuario); // NOI18N

        jLabel15.setText("RUC :");

        txtRucProveedor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtRucProveedor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtRucProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRucProveedorKeyTyped(evt);
            }
        });

        jLabel16.setText("Razón Social :");

        txtRazonSocialProveedor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel17.setText("Télefono :");

        jLabel18.setText("Dirección :");

        txtTelefonoProveedor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtTelefonoProveedor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txtDireccionProveedor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel19.setBackground(new java.awt.Color(0, 0, 0));
        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("LISTA DE PROVEEDORES");
        jLabel19.setFocusable(false);
        jLabel19.setOpaque(true);

        btnGuardarProveedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Save.png"))); // NOI18N
        btnGuardarProveedor.setText("GUARDAR");
        btnGuardarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarProveedorActionPerformed(evt);
            }
        });

        btnActualizarProveedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/editar.png"))); // NOI18N
        btnActualizarProveedor.setText("MODIFICAR");
        btnActualizarProveedor.setEnabled(false);
        btnActualizarProveedor.setIconTextGap(8);
        btnActualizarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarProveedorActionPerformed(evt);
            }
        });

        btnCancelarProveedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/cancelar.png"))); // NOI18N
        btnCancelarProveedor.setText("CANCELAR");
        btnCancelarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarProveedorActionPerformed(evt);
            }
        });

        jtListaProveedor.setAutoCreateRowSorter(true);
        jtListaProveedor.setModel(ListaProveedor);
        jtListaProveedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaProveedorMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaProveedorMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(jtListaProveedor);

        cboTipoBusquedaProveedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ruc", "Razon Social" }));

        txtBuscarProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarProveedorKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jpProveedorLayout = new javax.swing.GroupLayout(jpProveedor);
        jpProveedor.setLayout(jpProveedorLayout);
        jpProveedorLayout.setHorizontalGroup(
            jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jpProveedorLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jpProveedorLayout.createSequentialGroup()
                            .addComponent(jLabel17)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jpProveedorLayout.createSequentialGroup()
                            .addComponent(jLabel15)
                            .addGap(31, 31, 31)
                            .addComponent(txtRucProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnGuardarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpProveedorLayout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtRazonSocialProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
                            .addComponent(txtDireccionProveedor))
                        .addGap(0, 108, Short.MAX_VALUE))
                    .addGroup(jpProveedorLayout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(btnActualizarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(btnCancelarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))))
            .addGroup(jpProveedorLayout.createSequentialGroup()
                .addComponent(txtBuscarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cboTipoBusquedaProveedor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane3)
        );
        jpProveedorLayout.setVerticalGroup(
            jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpProveedorLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRucProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(txtRazonSocialProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnActualizarProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelarProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(23, 23, 23)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpProveedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cboTipoBusquedaProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jtbAdministracion.addTab("PROVEEDOR  ", new javax.swing.ImageIcon(getClass().getResource("/recurso/proveedor.png")), jpProveedor); // NOI18N

        jpTipoProducto.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TIPO PRODUCTO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jtListaTipoProducto.setAutoCreateRowSorter(true);
        jtListaTipoProducto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "#", "Descripción"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaTipoProducto.setFocusable(false);
        jtListaTipoProducto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaTipoProductoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaTipoProductoMouseReleased(evt);
            }
        });
        jScrollPane7.setViewportView(jtListaTipoProducto);
        if (jtListaTipoProducto.getColumnModel().getColumnCount() > 0) {
            jtListaTipoProducto.getColumnModel().getColumn(0).setResizable(false);
            jtListaTipoProducto.getColumnModel().getColumn(0).setPreferredWidth(5);
        }

        jLabel1.setText("Descripción:");

        btnGuardarTipoProducto.setText("GUARDAR");
        btnGuardarTipoProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarTipoProductoActionPerformed(evt);
            }
        });

        btnEditarTipoProducto.setText("EDITAR");
        btnEditarTipoProducto.setEnabled(false);
        btnEditarTipoProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarTipoProductoActionPerformed(evt);
            }
        });

        btnCancelarTipoProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Cancel.png"))); // NOI18N
        btnCancelarTipoProducto.setText("CANCELAR");
        btnCancelarTipoProducto.setIconTextGap(8);

        txtBuscarTipoProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarTipoProductoKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jpTipoProductoLayout = new javax.swing.GroupLayout(jpTipoProducto);
        jpTipoProducto.setLayout(jpTipoProductoLayout);
        jpTipoProductoLayout.setHorizontalGroup(
            jpTipoProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpTipoProductoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpTipoProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDescripcionTipo)
                    .addComponent(btnCancelarTipoProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jpTipoProductoLayout.createSequentialGroup()
                        .addComponent(btnGuardarTipoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditarTipoProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
                    .addGroup(jpTipoProductoLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jSeparator1)
            .addComponent(txtBuscarTipoProducto)
        );
        jpTipoProductoLayout.setVerticalGroup(
            jpTipoProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpTipoProductoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDescripcionTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jpTipoProductoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarTipoProducto)
                    .addComponent(btnEditarTipoProducto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancelarTipoProducto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBuscarTipoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MARCA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jtListaMarca.setAutoCreateRowSorter(true);
        jtListaMarca.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "#", "Descripción"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaMarca.setFocusable(false);
        jtListaMarca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaMarcaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaMarcaMouseReleased(evt);
            }
        });
        jScrollPane8.setViewportView(jtListaMarca);
        if (jtListaMarca.getColumnModel().getColumnCount() > 0) {
            jtListaMarca.getColumnModel().getColumn(0).setResizable(false);
            jtListaMarca.getColumnModel().getColumn(0).setPreferredWidth(5);
        }

        jLabel2.setText("Descripción:");

        btnGuardarMarca.setText("GUARDAR");
        btnGuardarMarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarMarcaActionPerformed(evt);
            }
        });

        btnEditarMarca.setText("EDITAR");
        btnEditarMarca.setEnabled(false);
        btnEditarMarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarMarcaActionPerformed(evt);
            }
        });

        btnCancelarMarca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Cancel.png"))); // NOI18N
        btnCancelarMarca.setText("CANCELAR");
        btnCancelarMarca.setIconTextGap(8);
        btnCancelarMarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarMarcaActionPerformed(evt);
            }
        });

        txtBuscarMarca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarMarcaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDescripcionMarca, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                    .addComponent(btnCancelarMarca, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnGuardarMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditarMarca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jSeparator2)
            .addComponent(txtBuscarMarca, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDescripcionMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditarMarca)
                    .addComponent(btnGuardarMarca))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancelarMarca)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBuscarMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CATEGORIA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jtListaCategoria.setAutoCreateRowSorter(true);
        jtListaCategoria.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "#", "Descripción"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtListaCategoria.setFocusable(false);
        jtListaCategoria.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtListaCategoriaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jtListaCategoriaMouseReleased(evt);
            }
        });
        jScrollPane6.setViewportView(jtListaCategoria);
        if (jtListaCategoria.getColumnModel().getColumnCount() > 0) {
            jtListaCategoria.getColumnModel().getColumn(0).setResizable(false);
            jtListaCategoria.getColumnModel().getColumn(0).setPreferredWidth(5);
        }

        jLabel6.setText("Descripción:");

        btnGuardarCategoria.setText("GUARDAR");
        btnGuardarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarCategoriaActionPerformed(evt);
            }
        });

        btnEditarCategoria.setText("EDITAR");
        btnEditarCategoria.setEnabled(false);
        btnEditarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarCategoriaActionPerformed(evt);
            }
        });

        btnCancelarCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Cancel.png"))); // NOI18N
        btnCancelarCategoria.setText("CANCELAR");
        btnCancelarCategoria.setIconTextGap(8);
        btnCancelarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarCategoriaActionPerformed(evt);
            }
        });

        txtBuscarCategoria.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarCategoriaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDescripcionCategoria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addComponent(btnCancelarCategoria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btnGuardarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditarCategoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jSeparator3)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(txtBuscarCategoria, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDescripcionCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardarCategoria)
                    .addComponent(btnEditarCategoria))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancelarCategoria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBuscarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jpTipoMarcaCategoriaLayout = new javax.swing.GroupLayout(jpTipoMarcaCategoria);
        jpTipoMarcaCategoria.setLayout(jpTipoMarcaCategoriaLayout);
        jpTipoMarcaCategoriaLayout.setHorizontalGroup(
            jpTipoMarcaCategoriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpTipoMarcaCategoriaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpTipoProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(2, 2, 2)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpTipoMarcaCategoriaLayout.setVerticalGroup(
            jpTipoMarcaCategoriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jpTipoProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jtbAdministracion.addTab("TIPO / MARCA / CATEGORIA", new javax.swing.ImageIcon(getClass().getResource("/recurso/Database.png")), jpTipoMarcaCategoria); // NOI18N

        javax.swing.GroupLayout jpAdministracionLayout = new javax.swing.GroupLayout(jpAdministracion);
        jpAdministracion.setLayout(jpAdministracionLayout);
        jpAdministracionLayout.setHorizontalGroup(
            jpAdministracionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtbAdministracion)
        );
        jpAdministracionLayout.setVerticalGroup(
            jpAdministracionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jtbAdministracion)
        );

        jtpPrincipal.addTab("ADMINISTRACION", new javax.swing.ImageIcon(getClass().getResource("/recurso/administracion.png")), jpAdministracion); // NOI18N

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel26.setText("Bienvenido");

        lblNameTrabajador.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblNameTrabajador.setText("Nombre del Trabajador");

        lblFecha.setText("Fecha : " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

        lblHora.setText("Hora");

        miHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Home.png"))); // NOI18N
        miHome.setText("INICIO");
        miHome.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jmiConfiguracionDocImpresion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jmiConfiguracionDocImpresion.setText("Configurar Doc_Impresion");
        jmiConfiguracionDocImpresion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfiguracionDocImpresionActionPerformed(evt);
            }
        });
        miHome.add(jmiConfiguracionDocImpresion);

        jmiSalirSistema.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jmiSalirSistema.setText("Cerrar Session");
        jmiSalirSistema.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSalirSistemaActionPerformed(evt);
            }
        });
        miHome.add(jmiSalirSistema);

        mBarraPrincipal.add(miHome);

        miReporte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Report.png"))); // NOI18N
        miReporte.setText("REPORTE");
        miReporte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jMenu1.setText("Caja");

        miDetalleMovimientoCaja.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        miDetalleMovimientoCaja.setText("Detalle Movimiento");
        miDetalleMovimientoCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miDetalleMovimientoCajaActionPerformed(evt);
            }
        });
        jMenu1.add(miDetalleMovimientoCaja);

        miDetalleCaja.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        miDetalleCaja.setText("Detalle Caja");
        miDetalleCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miDetalleCajaActionPerformed(evt);
            }
        });
        jMenu1.add(miDetalleCaja);

        miCuadreCaja.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        miCuadreCaja.setText("Cuadre Caja");
        miCuadreCaja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miCuadreCajaActionPerformed(evt);
            }
        });
        jMenu1.add(miCuadreCaja);

        miReporte.add(jMenu1);

        jMenu2.setText("Venta");

        miVentaCliente.setText("Ventas por Cliente");
        miVentaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miVentaClienteActionPerformed(evt);
            }
        });
        jMenu2.add(miVentaCliente);

        miVentasAnuladas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        miVentasAnuladas.setText("Ventas Anuladas");
        miVentasAnuladas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miVentasAnuladasActionPerformed(evt);
            }
        });
        jMenu2.add(miVentasAnuladas);

        miReporte.add(jMenu2);

        jMenu3.setText("Clientes");

        miClienteMorosos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        miClienteMorosos.setText("Clientes Morosos / Deudores");
        miClienteMorosos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miClienteMorososActionPerformed(evt);
            }
        });
        jMenu3.add(miClienteMorosos);

        miReporte.add(jMenu3);

        mBarraPrincipal.add(miReporte);

        miAYUDA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recurso/Help.png"))); // NOI18N
        miAYUDA.setText("AYUDA");
        mBarraPrincipal.add(miAYUDA);

        setJMenuBar(mBarraPrincipal);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblNameTrabajador)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFecha)
                .addGap(37, 37, 37)
                .addComponent(lblHora)
                .addGap(48, 48, 48))
            .addComponent(jtpPrincipal)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNameTrabajador)
                    .addComponent(jLabel26)
                    .addComponent(lblFecha)
                    .addComponent(lblHora))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtpPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 603, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtRucProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRucProveedorKeyTyped
        new Funciones().soloNumeros(evt);
        if (txtRucProveedor.getText().length() == 11) {
            evt.consume();
        }
    }//GEN-LAST:event_txtRucProveedorKeyTyped

    private void txtDNIUsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDNIUsuarioKeyTyped
        new Funciones().soloNumeros(evt);
        if (txtDNIUsuario.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtDNIUsuarioKeyTyped

    private void txtStockMinimoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockMinimoKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtStockMinimo.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtStockMinimoKeyTyped

    private void txtStockMaximoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockMaximoKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtStockMaximo.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtStockMaximoKeyTyped

    private void txtPrecioVentaProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioVentaProductoKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtPrecioVentaProducto.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPrecioVentaProductoKeyTyped

    private void txtCodigoBarraProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoBarraProductoKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraProductoKeyTyped

    private void btnGuardarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarProveedorActionPerformed
        int result = 0;
        if (txtRucProveedor.getText().compareTo("") != 0 && txtRazonSocialProveedor.getText().compareTo("") != 0
                && txtTelefonoProveedor.getText().compareTo("") != 0 && txtDireccionProveedor.getText().compareTo("") != 0) {

            result = new BLProveedor().registar_proveedor(txtRucProveedor.getText(), txtRazonSocialProveedor.getText(),
                    txtTelefonoProveedor.getText(), txtDireccionProveedor.getText());
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_proveedor("", 2);
                cargardatos_proveedorIngProd();
                limpiarCajaTexto_proveedor();
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarProveedorActionPerformed

    private void cboTipoBusquedaUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoBusquedaUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboTipoBusquedaUsuarioActionPerformed

    private void btnGuardarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarUsuarioActionPerformed
        int result = 0;
        if (txtNomApeUsuario.getText().compareTo("") != 0 && txtDNIUsuario.getText().compareTo("") != 0
                && txtIDNameUsuario.getText().compareTo("") != 0 && txtClaveUsuario.getText().compareTo("") != 0) {
            result = new BLUsuario().registar_usuario(txtDNIUsuario.getText(), txtNomApeUsuario.getText(),
                    txtIDNameUsuario.getText(), txtClaveUsuario.getText(), txtTelefonoUsuario.getText().compareTo("") != 0 ? txtTelefonoUsuario.getText() : "--");
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_usuario("", 2);
                limpiarCajaTexto_usuario();
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarUsuarioActionPerformed

    private void btnCancelarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarProveedorActionPerformed
        limpiarCajaTexto_proveedor();
    }//GEN-LAST:event_btnCancelarProveedorActionPerformed

    private void btnActualizarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarProveedorActionPerformed
        int result = 0;
        if (txtRucProveedor.getText().compareTo("") != 0 && txtRazonSocialProveedor.getText().compareTo("") != 0
                && txtTelefonoProveedor.getText().compareTo("") != 0 && txtDireccionProveedor.getText().compareTo("") != 0) {
            result = new BLProveedor().actualizar_proveedor(idProveedor, txtRucProveedor.getText(), txtRazonSocialProveedor.getText(),
                    txtTelefonoProveedor.getText(), txtDireccionProveedor.getText(), "1");
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_proveedor("", 2);
                cargardatos_proveedorIngProd();
                limpiarCajaTexto_proveedor();
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarProveedorActionPerformed

    private void btnCancelarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarUsuarioActionPerformed
        limpiarCajaTexto_usuario();
        btnGuardarUsuario.setEnabled(true);
        btnActualizarUsuario.setEnabled(false);
        btnPermisoUsuario.setEnabled(false);
        btnCancelarUsuario.setEnabled(true);
    }//GEN-LAST:event_btnCancelarUsuarioActionPerformed

    private void btnGuardarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarProductoActionPerformed
        int result = 0, tipoProducto = 0, categoria = 0, marca = 0;
        if (txtCodigoBarraProducto.getText().compareTo("") != 0 && txtDescripcionProducto.getText().compareTo("") != 0
                && txtStockMinimo.getText().compareTo("") != 0 && txtStockMaximo.getText().compareTo("") != 0
                && txtPrecioVentaProducto.getText().compareTo("") != 0) {
            tipoProducto = ((Constante) cboTipoProducto.getSelectedItem()).getInt_valor();
            categoria = ((Categoria) cboCategoria.getSelectedItem()).getInt_id();
            marca = ((Marca) cboMarca.getSelectedItem()).getInt_id();
            result = new BLProducto().registar_producto(txtCodigoBarraProducto.getText(),
                    tipoProducto, categoria,
                    Double.parseDouble(txtStockMinimo.getText()),
                    Double.parseDouble(txtStockMaximo.getText()), Double.parseDouble(txtPrecioCostoProducto.getText().equals("") ? "0.0" : txtPrecioCostoProducto.getText()),
                    Double.parseDouble(txtPrecioVentaProducto.getText()), 0.0,
                    Double.parseDouble("" + txtUtilidadPorcentajeProducto.getValue()),
                    txtDescripcionProducto.getText(), marca, 0.0);
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_producto(txtBuscarProducto.getText(), cboTipoBusquedaProducto.getSelectedItem().toString());
                limpiarCajaTexto_Producto();
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarProductoActionPerformed

    private void btnGuardarTipoProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarTipoProductoActionPerformed
        int result = 0;
        if (txtDescripcionTipo.getText().compareTo("") != 0) {
            result = new BLConstante().registar_constante(txtDescripcionTipo.getText(), 1);
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_tipoproducto(txtBuscarTipoProducto.getText(), 0);
                cargardatos_tipoProducto();
                limpiarCajaTexto_TipoProducto();
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarTipoProductoActionPerformed

    private void btnGuardarMarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarMarcaActionPerformed
        int result = 0;
        if (txtDescripcionMarca.getText().compareTo("") != 0) {
            result = new BLMarca().registar_marca(txtDescripcionMarca.getText(), "1");
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_marca(txtBuscarMarca.getText(), 0);
                cargardatos_marca();
                limpiarCajaTexto_Marca();
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarMarcaActionPerformed

    private void btnGuardarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarCategoriaActionPerformed
        int result = 0;
        if (txtDescripcionCategoria.getText().compareTo("") != 0) {
            result = new BLCategoria().registar_categoria(txtDescripcionCategoria.getText(), "1");
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_categoria(txtBuscarCategoria.getText(), 0);
                cargardatos_categoria();
                limpiarCajaTexto_Categoria();
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnGuardarCategoriaActionPerformed

    private void txtApellidoPClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoPClienteKeyTyped
        new Funciones().soloLetras(evt);
    }//GEN-LAST:event_txtApellidoPClienteKeyTyped

    private void txtApellidoPClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoPClienteKeyReleased
        txtApellidoPCliente.setText(txtApellidoPCliente.getText().toUpperCase());
    }//GEN-LAST:event_txtApellidoPClienteKeyReleased

    private void txtNombreClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreClienteKeyTyped
        new Funciones().soloLetras(evt);
    }//GEN-LAST:event_txtNombreClienteKeyTyped

    private void txtNombreClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreClienteKeyReleased
        txtNombreCliente.setText(txtNombreCliente.getText().toUpperCase());
    }//GEN-LAST:event_txtNombreClienteKeyReleased

    private void txtDNIClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDNIClienteKeyTyped
        if (txtDNICliente.getText().length() >= 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtDNIClienteKeyTyped

    private void btnAgregarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarClienteActionPerformed
        try {
            int codigo;
            if (!ckConRUC.isSelected()
                    && (!txtNombreCliente.getText().equals(""))
                    && (!txtApellidoPCliente.getText().equals(""))) {
                String busqueda = txtApellidoPCliente.getText().trim() + " "
                        + txtApellidoMCliente.getText().trim() + " " + txtNombreCliente.getText().trim();
                codigo = new BLCliente().registar_cliente(
                        txtApellidoPCliente.getText().trim(), txtApellidoMCliente.getText().trim(), busqueda,
                        txtDireccionCliente.getText().trim(), txtDNICliente.getText().trim(), "", txtNombreCliente.getText().trim(),
                        txtTelefonoCliente.getText().equals("") ? "" : txtTelefonoCliente.getText()
                );
                if (codigo == 0) {
                    JOptionPane.showMessageDialog(null, "No se puede Registrar", "Mensaje", 1);
                } else {
                    limpiarCajaTexto_cliente();
                    JOptionPane.showMessageDialog(null, "Registrado", "Mensaje", 1);
                    dlgCliente.dispose();
                }
            } else if (ckConRUC.isSelected() && (!txtRazonSocial.getText().equals("")) && txtRUCCliente.getText().equals("")) {
                String busqueda = txtRazonSocial.getText().trim();
                codigo = new BLCliente().registar_cliente(
                        txtApellidoPCliente.getText().trim(), txtApellidoMCliente.getText().trim(), busqueda,
                        txtDireccionCliente.getText().trim(), "", txtRUCCliente.getText().trim(), txtRazonSocial.getText().trim(),
                        txtTelefonoCliente.getText().equals("") ? "" : txtTelefonoCliente.getText()
                );
                if (codigo == 0) {
                    JOptionPane.showMessageDialog(null, "No se puede Registrar", "Mensaje", 1);
                } else {
                    limpiarCajaTexto_cliente();
                    JOptionPane.showMessageDialog(null, "Registrado", "Mensaje", 1);
                    dlgCliente.dispose();
                }
            }
            cargardatos_cliente("", 0);
            cargardatos_cliente_busqueda_morosos("", 0);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_btnAgregarClienteActionPerformed

    private void btnSalirClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirClienteActionPerformed
        dlgCliente.dispose();
    }//GEN-LAST:event_btnSalirClienteActionPerformed

    private void txtApellidoMClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoMClienteKeyTyped
        new Funciones().soloLetras(evt);
    }//GEN-LAST:event_txtApellidoMClienteKeyTyped

    private void txtApellidoMClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoMClienteKeyReleased
        txtApellidoMCliente.setText(txtApellidoMCliente.getText().toUpperCase());
    }//GEN-LAST:event_txtApellidoMClienteKeyReleased

    private void txtTelefonoClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoClienteKeyTyped

    }//GEN-LAST:event_txtTelefonoClienteKeyTyped

    private void txtRUCClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRUCClienteKeyTyped
        if (txtRUCCliente.getText().length() >= 11) {
            evt.consume();
        }
        new Funciones().soloNumeros(evt);
    }//GEN-LAST:event_txtRUCClienteKeyTyped

    private void ckConRUCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ckConRUCActionPerformed
        if (ckConRUC.isSelected()) {
            txtRUCCliente.setEnabled(true);
            txtRazonSocial.setEnabled(true);
        } else if (!ckConRUC.isSelected()) {
            txtRUCCliente.setEnabled(false);
            txtRazonSocial.setEnabled(false);
        }
    }//GEN-LAST:event_ckConRUCActionPerformed

    private void btnEditarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarClienteActionPerformed
        try {
            int codigo;
            if (!ckConRUC.isSelected()
                    && (!txtNombreCliente.getText().equals(""))
                    && (!txtApellidoPCliente.getText().equals(""))) {
                String busqueda = txtApellidoPCliente.getText().trim() + " "
                        + txtApellidoMCliente.getText().trim() + " " + txtNombreCliente.getText().trim();
                codigo = new BLCliente().actualizar_cliente(idCliente,
                        txtApellidoPCliente.getText().trim(), txtApellidoMCliente.getText().trim(), busqueda,
                        txtDireccionCliente.getText().trim(), txtDNICliente.getText().trim(), "", txtNombreCliente.getText().trim(),
                        txtTelefonoCliente.getText().equals("") ? "" : txtTelefonoCliente.getText()
                );
                if (codigo == 0) {
                    JOptionPane.showMessageDialog(null, "No se puede Actualizar", "Mensaje", 1);
                } else {
                    limpiarCajaTexto_cliente();
                    JOptionPane.showMessageDialog(null, "Actualizar", "Mensaje", 1);
                    dlgCliente.dispose();
                }
            } else if (ckConRUC.isSelected() && (!txtRazonSocial.getText().equals("")) && txtRUCCliente.getText().equals("")) {
                String busqueda = txtRazonSocial.getText().trim();
                codigo = new BLCliente().actualizar_cliente(idCliente,
                        txtApellidoPCliente.getText().trim(), txtApellidoMCliente.getText().trim(), busqueda,
                        txtDireccionCliente.getText().trim(), "", txtRUCCliente.getText().trim(), txtRazonSocial.getText().trim(),
                        txtTelefonoCliente.getText().equals("") ? "" : txtTelefonoCliente.getText()
                );
                if (codigo == 0) {
                    JOptionPane.showMessageDialog(null, "No se puede Actualizar", "Mensaje", 1);
                } else {
                    limpiarCajaTexto_cliente();
                    JOptionPane.showMessageDialog(null, "Actualizar", "Mensaje", 1);
                    dlgCliente.dispose();
                }
            }
            cargardatos_cliente("", 0);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_btnEditarClienteActionPerformed

    private void btnAgregarNuevoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarNuevoClienteActionPerformed
        pnlCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Agregar Cliente"));
        btnEditarCliente.setEnabled(false);
        txtRazonSocial.setEnabled(false);
        txtRUCCliente.setEnabled(false);

        dlgCliente.setEnabled(true);
        dlgCliente.pack();
        dlgCliente.setModal(true);
        dlgCliente.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgCliente), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgCliente.setLocationRelativeTo(null);
        dlgCliente.setVisible(true);
    }//GEN-LAST:event_btnAgregarNuevoClienteActionPerformed

    private void btnEditarNuevoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarNuevoClienteActionPerformed
        limpiarCajaTexto_cliente();
        try {
            if (jtListaCliente.getSelectedRow() != -1) {
                int codigo = Integer.parseInt(String.valueOf(jtListaCliente.getModel().getValueAt(jtListaCliente.getSelectedRow(), 0)));
                idCliente = codigo;
                Cliente c = new BLCliente().get_cliente_byId(codigo);
                if (c != null) {
                    if (!c.getVar_ruc().equals("")) {
                        ckConRUC.setSelected(true);
                        txtRazonSocial.setEnabled(true);
                        txtRUCCliente.setEnabled(true);
                        txtRazonSocial.setText(c.getVar_nombreM());
                        txtRUCCliente.setText(c.getVar_ruc());
                    } else {
                        txtNombreCliente.setText(c.getVar_nombreM());
                        txtApellidoPCliente.setText(c.getVar_apepaterno());
                        txtApellidoMCliente.setText(c.getVar_apematerno());
                        txtDNICliente.setText(c.getVar_dni());
                        txtRazonSocial.setEnabled(false);
                        txtRUCCliente.setEnabled(false);
                    }
                    txtDireccionCliente.setText(c.getVar_direccion());
                    txtTelefonoCliente.setText(c.getVar_telefono());
                    txtNombreCliente.setEnabled(true);
                    txtApellidoPCliente.setEnabled(true);
                    txtApellidoMCliente.setEnabled(true);
                    txtDNICliente.setEnabled(true);
                }
                pnlCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Editar Cliente"));
                btnAgregarCliente.setEnabled(false);
                btnEditarCliente.setEnabled(true);
                dlgCliente.pack();
                dlgCliente.setDefaultCloseOperation(0);
                dlgCliente.setModal(true);
                dlgCliente.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgCliente), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
                dlgCliente.setLocationRelativeTo(null);
                dlgCliente.setVisible(true);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnEditarNuevoClienteActionPerformed

    private void btnLimpiarMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarMActionPerformed
        limpiarCajaTexto_movimiento();
    }//GEN-LAST:event_btnLimpiarMActionPerformed

    private void txtMotivoMovimientoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMotivoMovimientoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            btnGuardarMovimiento.requestFocus();
        }
    }//GEN-LAST:event_txtMotivoMovimientoKeyPressed

    private void txtMontoMovimientoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoMovimientoKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtMontoMovimiento.getText().length() == 6) {
            evt.consume();
        }
    }//GEN-LAST:event_txtMontoMovimientoKeyTyped

    private void btnValidarUsuarioMovActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarUsuarioMovActionPerformed
        userDefault = null;
        String clave = new String(txtClaveMovimiento.getPassword());
        if (clave.compareTo("") != 0) {
            userDefault = new BLUsuario().validarPassword(clave);
            if (userDefault == null) {
                txtClaveMovimiento.requestFocus(true);
                JOptionPane.showMessageDialog(null, "[ Contraseña Invalida ]");
            } else {
                registrar_movimiento(Double.parseDouble(txtMontoMovimiento.getText()),
                        0, ((Constante) cboTipoOperacionMov.getSelectedItem()).getInt_valor(), txtMotivoMovimiento.getText(),
                        ((Constante) cboTipoDocumentoMov.getSelectedItem()).getInt_valor(),
                        txtDocumentoMov.getText().equals("") ? "000 - 00000" : txtDocumentoMov.getText(),
                        ((Constante) cboFormaPagoMov.getSelectedItem()).getInt_valor(), userDefault.getInt_id());
                dlgValidarUsuarioMov.dispose();
            }
        } else {
            JOptionPane.showMessageDialog(null, "[ No se Admite Campos Vacios ]");
        }
    }//GEN-LAST:event_btnValidarUsuarioMovActionPerformed

    private void btnAgregarDetProdructoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarDetProdructoActionPerformed
        try {
            if (idProductoIngProd != 0) {
                double precioCosto = Double.parseDouble(txtPrecioCostoingProd.getText());
                int cantidad = Integer.parseInt(txtCantidadProductoingProd.getText());
                DefaultTableModel tempIngProd = (DefaultTableModel) jtDetalleProductoIngProd.getModel();
                if (cantidad != 0 && precioCosto != 0) {
                    Object datos[] = {
                        idProductoIngProd,
                        txtNombreProductoIngProd.getText(),
                        precioCosto,
                        cantidad,
                        new Double(precioCosto * cantidad)
                    };
                    tempIngProd.addRow(datos);
                    calcular_cantidad_total_ingprod();
                    idProductoIngProd = 0;
                    txtPrecioCostoingProd.setText("");
                    txtCantidadProductoingProd.setText("0");
                    btnBuscarProductoIngProd.requestFocus();
                    txtNombreProductoIngProd.setText("");
                    btnQuitarDetProducto.setEnabled(false);
                    btnGuardarIngProd.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(null, "[ Precio Unitario > 0 y Cantidad > 0 ]", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "[ Debe seleccionar Producto ]", "Mensaje", 1);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnAgregarDetProdructoActionPerformed

    private void btnQuitarDetProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarDetProductoActionPerformed
        int fila = jtDetalleProductoIngProd.getSelectedRow();
        DefaultTableModel tempIngProd = (DefaultTableModel) jtDetalleProductoIngProd.getModel();
        if (fila > 0) {
            tempIngProd.removeRow(fila);
        } else if (fila == 0) {
            tempIngProd.removeRow(fila);
            txtMontoTotalIngProd.setText("");
            txtCantProductoIngProd.setText("0");
        }
        calcular_cantidad_total_ingprod();
        btnQuitarDetProducto.setEnabled(false);
    }//GEN-LAST:event_btnQuitarDetProductoActionPerformed

    private void txtPrecioCostoProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioCostoProductoKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtPrecioCostoProducto.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPrecioCostoProductoKeyTyped

    private void jtListaProductoBusquedaIngProdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtListaProductoBusquedaIngProdKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                idProductoIngProd = Integer.parseInt(String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 0)));
                txtNombreProductoIngProd.setText(String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 2))
                        + " - " + String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 3))
                        + " - " + String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 4)));
                txtNombreProductoIngProd.setCaretPosition(0);
                dlgBuscarProductoIngProd.dispose();
            } catch (Exception e) {
                System.out.println("" + e.getMessage());
            }
        }
    }//GEN-LAST:event_jtListaProductoBusquedaIngProdKeyPressed

    private void jtListaProductoBusquedaIngProdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaProductoBusquedaIngProdMouseClicked
        try {
            idProductoIngProd = Integer.parseInt(String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 0)));
            txtNombreProductoIngProd.setText(String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 2))
                    + " - " + String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 3))
                    + " - " + String.valueOf(jtListaProductoBusquedaIngProd.getModel().getValueAt(jtListaProductoBusquedaIngProd.getSelectedRow(), 4)));
            txtNombreProductoIngProd.setCaretPosition(0);
            dlgBuscarProductoIngProd.dispose();
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_jtListaProductoBusquedaIngProdMouseClicked

    private void btnBuscarProductoIngProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProductoIngProdActionPerformed
        cargardatos_busqueda_ingproducto("", "Descripcion");
        dlgBuscarProductoIngProd.setSize(this.getToolkit().getScreenSize());
        dlgBuscarProductoIngProd.pack();
        dlgBuscarProductoIngProd.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgBuscarProductoIngProd), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgBuscarProductoIngProd.setLocationRelativeTo(null);
        dlgBuscarProductoIngProd.setModal(true);
        dlgBuscarProductoIngProd.setVisible(true);
    }//GEN-LAST:event_btnBuscarProductoIngProdActionPerformed

    private void chkFechasIngProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFechasIngProdActionPerformed
        if (chkFechasIngProd.isSelected()) {
            txtFechaInicioIngProd.setEnabled(true);
            txtFechaFinIngProd.setEnabled(true);
        } else if (!chkFechasIngProd.isSelected()) {
            txtFechaInicioIngProd.setEnabled(false);
            txtFechaFinIngProd.setEnabled(false);
        }
    }//GEN-LAST:event_chkFechasIngProdActionPerformed

    private void chkProveedorIngProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkProveedorIngProdActionPerformed
        if (chkProveedorIngProd.isSelected()) {
            cboProveedorIngProveedor.setEnabled(true);
        } else if (!chkProveedorIngProd.isSelected()) {
            cboProveedorIngProveedor.setEnabled(false);
        }
    }//GEN-LAST:event_chkProveedorIngProdActionPerformed

    private void btnCancelarIngProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarIngProdActionPerformed
        limpiarCajaTexto_IngProd();
        btnGuardarIngProd.setEnabled(false);
    }//GEN-LAST:event_btnCancelarIngProdActionPerformed

    private void btnGuardarIngProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarIngProdActionPerformed
        int result = 0;
        try {
            int id_proveedor = ((Proveedor) cboProveedorRegIngProveedor.getSelectedItem()).getInt_id();
            int id_tipodocumento = ((Constante) cboTipoDocumentoIngProd.getSelectedItem()).getInt_valor();
            int id_motivo = ((Constante) cboMotivoRegIngProducto.getSelectedItem()).getInt_valor();
            String comentario = txtComentarioIngProd.getText().equalsIgnoreCase("") ? "-" : txtComentarioIngProd.getText();
            String seriedoc = txtSerieDocIngProd.getText();
            String numerodoc = txtNumDocIngProd.getText();
            Timestamp fechaEmision = new java.sql.Timestamp(txtFechaEmisionIngProd.getDate().getTime());
            if (txtFechaEmisionIngProd.getDate().toString().compareTo("") != 0) {
                //Armar Arreglo Tabla Detalle
                ArrayList<Detalle_OrdenIngreso> lista_doi = new ArrayList<Detalle_OrdenIngreso>();
                Detalle_OrdenIngreso doi = null;
                int nroFilas = ((DefaultTableModel) jtDetalleProductoIngProd.getModel()).getRowCount();
                for (int f = 0; f < nroFilas; f++) {
                    doi = new Detalle_OrdenIngreso();
                    doi.setDec_cantidad(Double.parseDouble(jtDetalleProductoIngProd.getModel().getValueAt(f, 3).toString()));
                    doi.setDec_importe(Double.parseDouble(jtDetalleProductoIngProd.getModel().getValueAt(f, 4).toString()));
                    doi.setDec_preciounitario(Double.parseDouble(jtDetalleProductoIngProd.getModel().getValueAt(f, 2).toString()));
                    doi.setInt_producto(Integer.parseInt(jtDetalleProductoIngProd.getModel().getValueAt(f, 0).toString()));
                    doi.setVar_estado("1");
                    lista_doi.add(doi);
                }
                result = new BLOrden_Ingreso().registar_ordenIngreso(id_motivo, id_proveedor, id_tipodocumento, 1, comentario,
                        seriedoc, numerodoc, fechaEmision, Double.parseDouble(txtMontoTotalIngProd.getText()), lista_doi);

                if (result != 0) {
                    JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    cargardatos_producto("", "Descripcion");
                    limpiarCajaTexto_IngProd();
                } else {
                    JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, " No se admiten campos vacios ", "Mensaje", 1);
            }
        } catch (Exception e) {
            System.out.println("Error al registrar Orden Compra" + e.toString());
        }
    }//GEN-LAST:event_btnGuardarIngProdActionPerformed

    private void jtDetalleProductoIngProdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtDetalleProductoIngProdMouseClicked
        try {
            btnQuitarDetProducto.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_jtDetalleProductoIngProdMouseClicked

    private void jtDetalleProductoVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtDetalleProductoVentaMouseClicked
        try {
            btnQuitarDetProductoVenta.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_jtDetalleProductoVentaMouseClicked

    private void btnQuitarDetProductoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarDetProductoVentaActionPerformed
        int fila = jtDetalleProductoVenta.getSelectedRow();
        DefaultTableModel tempVenta = (DefaultTableModel) jtDetalleProductoVenta.getModel();
        if (fila > 0) {
            tempVenta.removeRow(fila);
        } else if (fila == 0) {
            tempVenta.removeRow(fila);
            txtCantProductoVenta.setText("0");
            txtSubTotalMonto.setText("");
            txtIGVMontoVenta.setText("");
            txtMontoTotalVenta.setText("");
            txtAcuentaVenta.setText("");
            txtSaldoVenta.setText("");
            txtIGVPorcentaje.setValue(18);
            txtDescuentoVenta.setValue(0);
        }
        calcular_montos_ventas();
        btnQuitarDetProductoVenta.setEnabled(false);
    }//GEN-LAST:event_btnQuitarDetProductoVentaActionPerformed

    private void btnAgregarDetProdructoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarDetProdructoVentaActionPerformed
        try {
            DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
            simbolos.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.##", simbolos);
            if (idProductoVenta != 0) {
                double precioVenta = Double.parseDouble(txtPrecioVentaVenta.getText());
                double cantidad = Double.parseDouble(txtCantidadProductoVenta.getText().equals("") ? "0.0" : txtCantidadProductoVenta.getText());
                DefaultTableModel tempIngProd = (DefaultTableModel) jtDetalleProductoVenta.getModel();
                if (cantidad != 0 && precioVenta != 0) {
                    Object datos[] = {
                        idProductoVenta,
                        txtNombreProductoVenta.getText(),
                        precioVenta,
                        cantidad,
                        new Double(df.format(precioVenta * cantidad))
                    };
                    tempIngProd.addRow(datos);
                    calcular_montos_ventas();
                    idProductoVenta = 0;
                    txtPrecioVentaVenta.setText("0.0");
                    txtCantidadProductoVenta.setText("0");
                    btnBuscarProductoVenta.requestFocus();
                    txtNombreProductoVenta.setText("");
                    btnQuitarDetProductoVenta.setEnabled(false);
                    btnCrearVenta.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(null, "[ Precio Unitario > 0 y Cantidad > 0 ]", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "[ Debe seleccionar Producto ]", "Mensaje", 1);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_btnAgregarDetProdructoVentaActionPerformed

    private void btnBuscarProductoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProductoVentaActionPerformed
        cargardatos_busqueda_venta("", "Descripcion");
        dlgBuscarProductoVenta.setSize(this.getToolkit().getScreenSize());
        dlgBuscarProductoVenta.pack();
        dlgBuscarProductoVenta.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgBuscarProductoVenta), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgBuscarProductoVenta.setLocationRelativeTo(null);
        dlgBuscarProductoVenta.setModal(true);
        dlgBuscarProductoVenta.setVisible(true);
    }//GEN-LAST:event_btnBuscarProductoVentaActionPerformed

    private void btnBuscarClienteVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarClienteVentaActionPerformed
        cargardatos_clientebusqueda("", 0);
        dlgClienteBusqVenta.setSize(this.getToolkit().getScreenSize());
        dlgClienteBusqVenta.pack();
        dlgClienteBusqVenta.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgClienteBusqVenta), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgClienteBusqVenta.setLocationRelativeTo(null);
        dlgClienteBusqVenta.setModal(true);
        dlgClienteBusqVenta.setVisible(true);
    }//GEN-LAST:event_btnBuscarClienteVentaActionPerformed

    private void jtListaProductoBusquedaVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaProductoBusquedaVentaMouseClicked
        try {
            idProductoVenta = Integer.parseInt(String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 0)));
            txtNombreProductoVenta.setText(String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 2))
                    + " - " + String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 3))
                    + " - " + String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 4)));
            txtNombreProductoVenta.setCaretPosition(0);
            txtPrecioVentaVenta.setText(String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 6)));
            dlgBuscarProductoVenta.dispose();
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_jtListaProductoBusquedaVentaMouseClicked

    private void jtListaProductoBusquedaVentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtListaProductoBusquedaVentaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                idProductoVenta = Integer.parseInt(String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 0)));
                txtNombreProductoVenta.setText(String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 2))
                        + " - " + String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 3))
                        + " - " + String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 4)));
                txtNombreProductoVenta.setCaretPosition(0);
                txtPrecioVentaVenta.setText(String.valueOf(jtListaProductoBusquedaVenta.getModel().getValueAt(jtListaProductoBusquedaVenta.getSelectedRow(), 6)));
                dlgBuscarProductoVenta.dispose();
            } catch (Exception e) {
                System.out.println("" + e.getMessage());
            }
        }
    }//GEN-LAST:event_jtListaProductoBusquedaVentaKeyPressed

    private void txtIGVPorcentajePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_txtIGVPorcentajePropertyChange
        try {
            double totalFacturado = 0.0, subTotal = 0.0, montoigv = 0.0, acuenta = 0.0, descuento = 0.0;
            DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
            simbolos.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.##", simbolos);
            int igvporc = txtIGVPorcentaje.getValue();
            descuento = temp_totalFacturado * (txtDescuentoVenta.getValue() * 0.01);
            totalFacturado = temp_totalFacturado - descuento;
            montoigv = Double.parseDouble("" + ((igvporc * 0.01) * totalFacturado));
            txtIGVMontoVenta.setText("" + df.format(montoigv));
            subTotal = (totalFacturado - montoigv);
            txtSubTotalMonto.setText("" + df.format(subTotal));
            txtMontoTotalVenta.setText("" + df.format(totalFacturado));
            acuenta = Double.parseDouble(txtAcuentaVenta.getText().equals("") ? "0.0" : txtAcuentaVenta.getText());
            txtSaldoVenta.setText("" + df.format(totalFacturado - acuenta));
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_txtIGVPorcentajePropertyChange

    private void txtDescuentoVentaPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_txtDescuentoVentaPropertyChange
        try {
            double totalFacturado = 0.0, subTotal = 0.0, montoigv = 0.0, acuenta = 0.0, descuento = 0.0;
            DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
            simbolos.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.##", simbolos);
            int igvporc = txtIGVPorcentaje.getValue();
            descuento = temp_totalFacturado * (txtDescuentoVenta.getValue() * 0.01);
            totalFacturado = temp_totalFacturado - descuento;
            montoigv = Double.parseDouble("" + ((igvporc * 0.01) * totalFacturado));
            txtIGVMontoVenta.setText("" + df.format(montoigv));
            subTotal = (totalFacturado - montoigv);
            txtSubTotalMonto.setText("" + df.format(subTotal));
            txtMontoTotalVenta.setText("" + df.format(totalFacturado));
            acuenta = Double.parseDouble(txtAcuentaVenta.getText().equals("") ? "0.0" : txtAcuentaVenta.getText());
            txtSaldoVenta.setText("" + df.format(totalFacturado - acuenta));
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_txtDescuentoVentaPropertyChange

    private void jmiConfiguracionDocImpresionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfiguracionDocImpresionActionPerformed
        dlgDocumentos.setSize(this.getToolkit().getScreenSize());
        dlgDocumentos.pack();
        dlgDocumentos.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgBuscarProductoVenta), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgDocumentos.setLocationRelativeTo(null);
        dlgDocumentos.setModal(true);
        dlgDocumentos.setVisible(true);
    }//GEN-LAST:event_jmiConfiguracionDocImpresionActionPerformed

    private void btnCancelarVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarVentaActionPerformed
        limpiarCajaTexto_Venta();
    }//GEN-LAST:event_btnCancelarVentaActionPerformed

    private void cboTipoDocumentoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoDocumentoVentaActionPerformed
        lblNroDocumentoVenta.setText("");
        obtener_serienumero_venta(((Constante) cboTipoDocumentoVenta.getSelectedItem()).getInt_valor());
    }//GEN-LAST:event_cboTipoDocumentoVentaActionPerformed

    private void btnCrearVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearVentaActionPerformed
        int tipVenta = cboTipoVentta.getSelectedItem().toString().equalsIgnoreCase("Credito") ? 2 : 1;
        if (tipVenta == 2) {
            txtMontoTotalVentaCredito.setText(txtSaldoVenta.getText());
            txtNroCuotaVentaCredito.setValue(0);
            txtMontoXCuotaVentaCredito.setText("0.0");
            txtFechaPrimerPagoVentaCredito.setDate(new Date());
            cboFrecuenciaPagoVentaCredito.setSelectedIndex(0);
            dlgVentaCredito.pack();
            dlgVentaCredito.setLocationRelativeTo(null);
            dlgVentaCredito.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgValidarUsuarioVenta), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
            dlgVentaCredito.setModal(true);
            dlgVentaCredito.setVisible(true);
        } else {
            double acuenta = Double.parseDouble(txtAcuentaVenta.getText().equals("") ? "0.0" : txtAcuentaVenta.getText());
            double montofact = Double.parseDouble(txtMontoTotalVenta.getText());
            if (acuenta >= montofact) {
                txtClaveVenta.setText("");
                dlgValidarUsuarioVenta.pack();
                dlgValidarUsuarioVenta.setLocationRelativeTo(null);
                dlgValidarUsuarioVenta.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgValidarUsuarioVenta), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
                dlgValidarUsuarioVenta.setModal(true);
                dlgValidarUsuarioVenta.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Se admite pagos menor al Facturado, cuando es VENTA al CREDITO", "Alerta", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnCrearVentaActionPerformed

    private void jtListaClienteBusqKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtListaClienteBusqKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                idClienteVenta = Integer.parseInt(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 0)));
                txtClienteVenta.setText(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 1)));
                txtDNIClienteVenta.setText(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 2)));
                txtRucClienteVenta.setText(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 3)));
                dlgClienteBusqVenta.dispose();
            } catch (Exception e) {
                System.out.println("" + e.getMessage());
            }
        }
    }//GEN-LAST:event_jtListaClienteBusqKeyPressed

    private void jtListaClienteBusqMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaClienteBusqMouseClicked
        try {
            idClienteVenta = Integer.parseInt(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 0)));
            txtClienteVenta.setText(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 1)));
            txtDNIClienteVenta.setText(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 2)));
            txtRucClienteVenta.setText(String.valueOf(jtListaClienteBusq.getModel().getValueAt(jtListaClienteBusq.getSelectedRow(), 3)));
            dlgClienteBusqVenta.dispose();
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_jtListaClienteBusqMouseClicked

    private void cboFormaPagoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboFormaPagoVentaActionPerformed

    }//GEN-LAST:event_cboFormaPagoVentaActionPerformed

    private void jtListaClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaClienteMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jtListaClienteMouseClicked

    private void jtListaUsuarioMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaUsuarioMousePressed
        if (evt.isPopupTrigger() && jtListaUsuario.getModel().getRowCount() != 0
                && jtListaUsuario.getSelectedRow() != -1) {
            popupUsuario.show(jtListaUsuario, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaUsuarioMousePressed

    private void jtListaUsuarioMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaUsuarioMouseReleased
        if (evt.isPopupTrigger() && jtListaUsuario.getModel().getRowCount() != 0
                && jtListaUsuario.getSelectedRow() != -1) {
            popupUsuario.show(jtListaUsuario, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaUsuarioMouseReleased

    private void jtListaUsuarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaUsuarioMouseClicked
        try {
            if (jtListaUsuario.getSelectedRow() != -1) {
                idUsuario = Integer.parseInt(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 0)));
                btnPermisoUsuario.setEnabled(true);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_jtListaUsuarioMouseClicked

    private void btnActualizarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarUsuarioActionPerformed
        int result = 0;
        if (txtNomApeUsuario.getText().compareTo("") != 0 && txtDNIUsuario.getText().compareTo("") != 0
                && txtIDNameUsuario.getText().compareTo("") != 0 && txtClaveUsuario.getText().compareTo("") != 0) {
            result = new BLUsuario().actualizar_usuario(idUsuario, txtDNIUsuario.getText(), txtNomApeUsuario.getText(),
                    txtIDNameUsuario.getText(), txtClaveUsuario.getText(), txtTelefonoUsuario.getText().compareTo("") != 0 ? txtTelefonoUsuario.getText() : "--",
                    "1");
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_usuario("", 2);
                limpiarCajaTexto_usuario();
                btnGuardarUsuario.setEnabled(true);
                btnActualizarUsuario.setEnabled(false);
                btnPermisoUsuario.setEnabled(true);
                btnCancelarUsuario.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarUsuarioActionPerformed

    private void jtListaProveedorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaProveedorMousePressed
        if (evt.isPopupTrigger() && jtListaProveedor.getModel().getRowCount() != 0
                && jtListaProveedor.getSelectedRow() != -1) {
            popupProveedor.show(jtListaProveedor, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaProveedorMousePressed

    private void jtListaProveedorMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaProveedorMouseReleased
        if (evt.isPopupTrigger() && jtListaProveedor.getModel().getRowCount() != 0
                && jtListaProveedor.getSelectedRow() != -1) {
            popupProveedor.show(jtListaProveedor, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaProveedorMouseReleased

    private void btnCancelarMarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarMarcaActionPerformed
        txtDescripcionMarca.setText("");
        btnEditarMarca.setEnabled(false);
        btnGuardarMarca.setEnabled(true);
    }//GEN-LAST:event_btnCancelarMarcaActionPerformed

    private void btnCancelarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarCategoriaActionPerformed
        txtDescripcionCategoria.setText("");
        btnEditarCategoria.setEnabled(false);
        btnGuardarCategoria.setEnabled(true);
    }//GEN-LAST:event_btnCancelarCategoriaActionPerformed

    private void btnEditarMarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarMarcaActionPerformed
        int result = 0;
        if (txtDescripcionMarca.getText().compareTo("") != 0) {
            result = new BLMarca().actualizar_marca(idMarca, txtDescripcionMarca.getText(), "1");
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Actualizacion Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_marca(txtBuscarMarca.getText(), 0);
                cargardatos_marca();
                limpiarCajaTexto_Marca();
            } else {
                JOptionPane.showMessageDialog(null, "Actualizacion Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
        btnEditarMarca.setEnabled(false);
        btnGuardarMarca.setEnabled(true);
    }//GEN-LAST:event_btnEditarMarcaActionPerformed

    private void btnEditarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarCategoriaActionPerformed
        int result = 0;
        if (txtDescripcionCategoria.getText().compareTo("") != 0) {
            result = new BLCategoria().actualizar_categoria(idCategoria, txtDescripcionCategoria.getText(), "1");
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Actualizacion Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_categoria(txtBuscarCategoria.getText(), 0);
                cargardatos_categoria();
                limpiarCajaTexto_Categoria();
            } else {
                JOptionPane.showMessageDialog(null, "Actualizacion Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
        btnEditarCategoria.setEnabled(false);
        btnGuardarCategoria.setEnabled(true);
    }//GEN-LAST:event_btnEditarCategoriaActionPerformed

    private void jtListaMarcaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaMarcaMousePressed
        if (evt.isPopupTrigger() && jtListaMarca.getModel().getRowCount() != 0
                && jtListaMarca.getSelectedRow() != -1) {
            popupMarca.show(jtListaMarca, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaMarcaMousePressed

    private void jtListaMarcaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaMarcaMouseReleased
        if (evt.isPopupTrigger() && jtListaMarca.getModel().getRowCount() != 0
                && jtListaMarca.getSelectedRow() != -1) {
            popupMarca.show(jtListaMarca, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaMarcaMouseReleased

    private void jtListaCategoriaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaCategoriaMousePressed
        if (evt.isPopupTrigger() && jtListaCategoria.getModel().getRowCount() != 0
                && jtListaCategoria.getSelectedRow() != -1) {
            popupCategoria.show(jtListaCategoria, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaCategoriaMousePressed

    private void jtListaCategoriaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaCategoriaMouseReleased
        if (evt.isPopupTrigger() && jtListaCategoria.getModel().getRowCount() != 0
                && jtListaCategoria.getSelectedRow() != -1) {
            popupCategoria.show(jtListaCategoria, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaCategoriaMouseReleased

    private void jtListaProductoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaProductoMousePressed
        if (evt.isPopupTrigger() && jtListaProducto.getModel().getRowCount() != 0
                && jtListaProducto.getSelectedRow() != -1) {
            popupProducto.show(jtListaProducto, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaProductoMousePressed

    private void jtListaProductoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaProductoMouseReleased
        if (evt.isPopupTrigger() && jtListaProducto.getModel().getRowCount() != 0
                && jtListaProducto.getSelectedRow() != -1) {
            popupProducto.show(jtListaProducto, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaProductoMouseReleased

    private void btnActualizarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarProductoActionPerformed
        int result = 0, tipoProducto = 0, categoria = 0, marca = 0;
        if (txtCodigoBarraProducto.getText().compareTo("") != 0 && txtDescripcionProducto.getText().compareTo("") != 0
                && txtStockMinimo.getText().compareTo("") != 0 && txtStockMaximo.getText().compareTo("") != 0
                && txtPrecioVentaProducto.getText().compareTo("") != 0) {
            tipoProducto = ((Constante) cboTipoProducto.getSelectedItem()).getInt_valor();
            categoria = ((Categoria) cboCategoria.getSelectedItem()).getInt_id();
            marca = ((Marca) cboMarca.getSelectedItem()).getInt_id();

            result = new BLProducto().actualizar_producto(idProducto, txtCodigoBarraProducto.getText(),
                    tipoProducto, categoria,
                    Double.parseDouble(txtStockMinimo.getText()),
                    Double.parseDouble(txtStockMaximo.getText()), Double.parseDouble(txtPrecioCostoProducto.getText().equals("") ? "0.0" : txtPrecioCostoProducto.getText()),
                    Double.parseDouble(txtPrecioVentaProducto.getText()), 0.0,
                    Double.parseDouble("" + txtUtilidadPorcentajeProducto.getValue()), txtDescripcionProducto.getText(), marca, 0.0);
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Actualizacion Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_producto(txtBuscarProducto.getText(), cboTipoBusquedaProducto.getSelectedItem().toString());
                limpiarCajaTexto_Producto();
            } else {
                JOptionPane.showMessageDialog(null, "Actualizacion Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnActualizarProductoActionPerformed

    private void btnCancelarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarProductoActionPerformed
        limpiarCajaTexto_Producto();
        btnGuardarProducto.setEnabled(true);
        btnActualizarProducto.setEnabled(false);
        btnCancelarProducto.setEnabled(true);
    }//GEN-LAST:event_btnCancelarProductoActionPerformed

    private void btnBuscarIngProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIngProductoActionPerformed
        cargadatos_consultar_ordeningreso();
    }//GEN-LAST:event_btnBuscarIngProductoActionPerformed

    private void chkFechasVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFechasVentaActionPerformed
        if (chkFechasVenta.isSelected()) {
            txtFechaInicioVenta.setEnabled(true);
            txtFechaFinVenta.setEnabled(true);
        } else if (!chkFechasVenta.isSelected()) {
            txtFechaInicioVenta.setEnabled(false);
            txtFechaFinVenta.setEnabled(false);
        }
    }//GEN-LAST:event_chkFechasVentaActionPerformed

    private void chkClienteVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkClienteVentaActionPerformed
        if (chkClienteVenta.isSelected()) {
            cboClienteVenta.setEnabled(true);
        } else if (!chkClienteVenta.isSelected()) {
            cboClienteVenta.setEnabled(false);
        }
    }//GEN-LAST:event_chkClienteVentaActionPerformed

    private void chkDocumentoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkDocumentoVentaActionPerformed
        if (chkDocumentoVenta.isSelected()) {
            txtSerieDocBusqVenta.setEnabled(true);
            txtNumeroDocBusqVenta.setEnabled(true);
        } else if (!chkDocumentoVenta.isSelected()) {
            txtSerieDocBusqVenta.setEnabled(false);
            txtNumeroDocBusqVenta.setEnabled(false);
        }
    }//GEN-LAST:event_chkDocumentoVentaActionPerformed

    private void chkFormaPagoVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFormaPagoVentaActionPerformed
        if (chkFormaPagoVenta.isSelected()) {
            cboBusqFormaPagoVenta.setEnabled(true);
        } else if (!chkFormaPagoVenta.isSelected()) {
            cboBusqFormaPagoVenta.setEnabled(false);
        }
    }//GEN-LAST:event_chkFormaPagoVentaActionPerformed

    private void btnBuscarVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarVentaActionPerformed
        cargadatos_consultar_venta();
    }//GEN-LAST:event_btnBuscarVentaActionPerformed

    private void jtDetalleProductoSalidaProdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtDetalleProductoSalidaProdMouseClicked
        try {
            btnQuitarDetProductoSalidaProd.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_jtDetalleProductoSalidaProdMouseClicked

    private void btnAgregarDetProdructoSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarDetProdructoSalidaProdActionPerformed
        try {
            if (idProductoSalProd != 0) {
                int cantidad = Integer.parseInt(txtCantidadProductoSalidaProd.getText());
                DefaultTableModel tempIngProd = (DefaultTableModel) jtDetalleProductoSalidaProd.getModel();
                if (cantidad <= Producto_stockactual) {
                    Object datos[] = {
                        idProductoSalProd,
                        txtNombreProductoSalidaProd.getText(),
                        cantidad,};
                    tempIngProd.addRow(datos);
                    calcular_cantidad_total_salidaprod();
                    idProductoSalProd = 0;
                    txtCantidadProductoSalidaProd.setText("0");
                    btnBuscarProductoSalidaProd.requestFocus();
                    txtNombreProductoSalidaProd.setText("");
                    btnQuitarDetProductoSalidaProd.setEnabled(false);
                    btnGuardarSalidaProd.setEnabled(true);
                } else {
                    JOptionPane.showMessageDialog(null, "[ Cantidad < = Stock Actual ]", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "[ Debe seleccionar Producto ]", "Mensaje", 1);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnAgregarDetProdructoSalidaProdActionPerformed

    private void btnQuitarDetProductoSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarDetProductoSalidaProdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnQuitarDetProductoSalidaProdActionPerformed

    private void btnBuscarProductoSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProductoSalidaProdActionPerformed
        cargardatos_busqueda_salidaproducto("", "Descripcion");
        dlgBuscarProductoSalidaProd.setSize(this.getToolkit().getScreenSize());
        dlgBuscarProductoSalidaProd.pack();
        dlgBuscarProductoSalidaProd.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgBuscarProductoSalidaProd), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgBuscarProductoSalidaProd.setLocationRelativeTo(null);
        dlgBuscarProductoSalidaProd.setModal(true);
        dlgBuscarProductoSalidaProd.setVisible(true);
    }//GEN-LAST:event_btnBuscarProductoSalidaProdActionPerformed

    private void btnGuardarSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarSalidaProdActionPerformed
        int result = 0;
        try {
            int id_usuario = ((Usuario) cboPersonalRegSalidaProd.getSelectedItem()).getInt_id();
            int id_motivo = ((Constante) cboMotivoRegSalidaProd.getSelectedItem()).getInt_valor();
            String comentario = txtComentarioSalidaProd.getText();
            Timestamp fechaEmision = new java.sql.Timestamp(txtFechaEmisionSalidaProd.getDate().getTime());
            if (txtFechaEmisionSalidaProd.getDate().toString().compareTo("") != 0) {
                //Armar Arreglo Tabla Detalle
                ArrayList<Detalle_OrdenSalida> lista_dos = new ArrayList<Detalle_OrdenSalida>();
                Detalle_OrdenSalida dos = null;
                int nroFilas = ((DefaultTableModel) jtDetalleProductoSalidaProd.getModel()).getRowCount();
                for (int f = 0; f < nroFilas; f++) {
                    dos = new Detalle_OrdenSalida();
                    dos.setInt_producto(Integer.parseInt(jtDetalleProductoSalidaProd.getModel().getValueAt(f, 0).toString()));
                    dos.setDec_cantidad(Double.parseDouble(jtDetalleProductoSalidaProd.getModel().getValueAt(f, 2).toString()));
                    dos.setVar_estado("1");
                    lista_dos.add(dos);
                }
                result = new BLOrden_Salida().registar_ordenSalida(id_motivo, id_usuario, comentario, fechaEmision, 0, lista_dos);
                if (result != 0) {
                    JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    limpiarCajaTexto_SalidaProd();
                } else {
                    JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, " No se admiten campos vacios ", "Mensaje", 1);
            }
        } catch (Exception e) {
            System.out.println("Error al registrar Orden Compra" + e.toString());
        }
    }//GEN-LAST:event_btnGuardarSalidaProdActionPerformed

    private void btnCancelarSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarSalidaProdActionPerformed
        limpiarCajaTexto_SalidaProd();
        btnGuardarSalidaProd.setEnabled(false);
    }//GEN-LAST:event_btnCancelarSalidaProdActionPerformed

    private void chkFechasSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFechasSalidaProdActionPerformed
        if (chkFechasSalidaProd.isSelected()) {
            txtFechaInicioSalidaProd.setEnabled(true);
            txtFechaFinSalidaProd.setEnabled(true);
        } else if (!chkFechasSalidaProd.isSelected()) {
            txtFechaInicioSalidaProd.setEnabled(false);
            txtFechaFinSalidaProd.setEnabled(false);
        }
    }//GEN-LAST:event_chkFechasSalidaProdActionPerformed

    private void chkPersonalSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkPersonalSalidaProdActionPerformed
        if (chkPersonalSalidaProd.isSelected()) {
            cboPersonalSalidaProd.setEnabled(true);
        } else if (!chkPersonalSalidaProd.isSelected()) {
            cboPersonalSalidaProd.setEnabled(false);
        }
    }//GEN-LAST:event_chkPersonalSalidaProdActionPerformed

    private void chkMotivoBusquedaSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMotivoBusquedaSalidaProdActionPerformed
        if (chkMotivoBusquedaSalidaProd.isSelected()) {
            cboMotivoBusSalidaProd.setEnabled(true);
        } else if (!chkMotivoBusquedaSalidaProd.isSelected()) {
            cboMotivoBusSalidaProd.setEnabled(false);
        }
    }//GEN-LAST:event_chkMotivoBusquedaSalidaProdActionPerformed

    private void btnBuscarSalidaProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarSalidaProdActionPerformed
        cargadatos_consultar_ordensalida();
    }//GEN-LAST:event_btnBuscarSalidaProdActionPerformed

    private void chkMotivoBusquedaIngProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMotivoBusquedaIngProdActionPerformed
        if (chkMotivoBusquedaIngProd.isSelected()) {
            cboMotivoBusIngProd.setEnabled(true);
        } else if (!chkMotivoBusquedaIngProd.isSelected()) {
            cboMotivoBusIngProd.setEnabled(false);
        }
    }//GEN-LAST:event_chkMotivoBusquedaIngProdActionPerformed

    private void chkDocumentoIngProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkDocumentoIngProdActionPerformed
        if (chkDocumentoIngProd.isSelected()) {
            txtSerieDocBusquedaIngProd.setEnabled(true);
            txtNumeroDocBusquedaIngProd.setEnabled(true);
        } else if (!chkDocumentoIngProd.isSelected()) {
            txtSerieDocBusquedaIngProd.setEnabled(false);
            txtNumeroDocBusquedaIngProd.setEnabled(false);
        }
    }//GEN-LAST:event_chkDocumentoIngProdActionPerformed

    private void chkFechasInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkFechasInventarioActionPerformed
        if (chkFechasInventario.isSelected()) {
            txtFechaInicioInventario.setEnabled(true);
            txtFechaFinInventario.setEnabled(true);
        } else if (!chkFechasInventario.isSelected()) {
            txtFechaInicioInventario.setEnabled(false);
            txtFechaFinInventario.setEnabled(false);
        }
    }//GEN-LAST:event_chkFechasInventarioActionPerformed

    private void chkPersonalInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkPersonalInventarioActionPerformed
        if (chkPersonalInventario.isSelected()) {
            cboPersonalInventario.setEnabled(true);
        } else if (!chkPersonalInventario.isSelected()) {
            cboPersonalInventario.setEnabled(false);
        }
    }//GEN-LAST:event_chkPersonalInventarioActionPerformed

    private void chkMotivoInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMotivoInventarioActionPerformed
        if (chkMotivoInventario.isSelected()) {
            cboMotivoInventario.setEnabled(true);
        } else if (!chkMotivoInventario.isSelected()) {
            cboMotivoInventario.setEnabled(false);
        }
    }//GEN-LAST:event_chkMotivoInventarioActionPerformed

    private void btnBuscarInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarInventarioActionPerformed
        int contador = 0;
        ArrayList<String> lista = new ArrayList();
        String condicionFinal = "", tipo_motivo = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        boolean fecha = chkFechasInventario.isSelected();
        boolean personal = chkPersonalInventario.isSelected();
        boolean motivo = chkMotivoInventario.isSelected();
        if (fecha == true) {
            lista.add(" ( date(fechaBusqueda) between '" + sdf.format(txtFechaInicioInventario.getDate()) + "' and '" + sdf.format(txtFechaFinInventario.getDate()) + "' ) ");
            contador++;
        }
        if (personal == true) {
            lista.add(" ( int_usuario =" + ((Usuario) cboPersonalInventario.getSelectedItem()).getInt_id() + " )");
            contador++;
        }
        if (motivo == true) {
            tipo_motivo = cboMotivoInventario.getSelectedItem().toString().equalsIgnoreCase("- Todos -") ? " <> '' " : (" = " + "'" + cboMotivoInventario.getSelectedItem().toString() + "'");
            lista.add(" ( tipo " + tipo_motivo + " )");
            contador++;
        }

        switch (contador) {
            case 1:
                condicionFinal = lista.get(0);
                break;
            case 2:
                condicionFinal = lista.get(0) + " and " + lista.get(1);
                break;
            case 3:
                condicionFinal = lista.get(0) + " and " + lista.get(1) + " and " + lista.get(2);
                break;
        }

        DefaultTableModel tempIngProd = (DefaultTableModel) jtListaMovimientoInventario.getModel();
        tempIngProd.setRowCount(0);

        for (ListaMovimiento_Stock lms : new BLMovimiento().get_lista_movimiento_stock_all(condicionFinal)) {
            Object datos[] = {lms.getDat_fecharegistro().toString(), lms.getProducto(), lms.getTipo(), String.valueOf(lms.getDec_cantidad()), lms.getPersonal()};
            tempIngProd.addRow(datos);
        }
    }//GEN-LAST:event_btnBuscarInventarioActionPerformed

    private void btnPermisoUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPermisoUsuarioActionPerformed
        try {
            if (idUsuario != 0) {
                txtCodigoUsuario.setText("" + idUsuario);
                txtNomCompletoUsuario.setText(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 1)));
                //habilitarOpcioneMenu(Integer.parseInt(String.valueOf(jtblUsuario.getModel().getValueAt(jtblUsuario.getSelectedRow(), 0))));
                dlgPermisosUsuario.pack();
                dlgPermisosUsuario.setLocationRelativeTo(null);
                dlgPermisosUsuario.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgPermisosUsuario), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
                dlgPermisosUsuario.setModal(true);
                dlgPermisosUsuario.setVisible(true);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnPermisoUsuarioActionPerformed

    private void jtListaTipoProductoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaTipoProductoMousePressed
        if (evt.isPopupTrigger() && jtListaTipoProducto.getModel().getRowCount() != 0
                && jtListaTipoProducto.getSelectedRow() != -1) {
            popupTipoProducto.show(jtListaTipoProducto, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaTipoProductoMousePressed

    private void jtListaTipoProductoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaTipoProductoMouseReleased
        if (evt.isPopupTrigger() && jtListaTipoProducto.getModel().getRowCount() != 0
                && jtListaTipoProducto.getSelectedRow() != -1) {
            popupTipoProducto.show(jtListaTipoProducto, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaTipoProductoMouseReleased

    private void btnEditarTipoProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarTipoProductoActionPerformed
        int result = 0;
        if (txtDescripcionTipo.getText().compareTo("") != 0) {
            result = new BLConstante().actualizar_constante(idTipoProducto, txtDescripcionTipo.getText(), 1);
            if (result != 0) {
                JOptionPane.showMessageDialog(null, "Actualizacion Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                cargardatos_tipoproducto(txtBuscarTipoProducto.getText(), 0);
                cargardatos_tipoProducto();
                limpiarCajaTexto_TipoProducto();
            } else {
                JOptionPane.showMessageDialog(null, "Actualizacion Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnEditarTipoProductoActionPerformed

    private void jtListaBusquedaVentaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaBusquedaVentaMousePressed
        if (evt.isPopupTrigger() && jtListaBusquedaVenta.getModel().getRowCount() != 0
                && jtListaBusquedaVenta.getSelectedRow() != -1) {
            popupConsultarVenta.show(jtListaBusquedaVenta, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaBusquedaVentaMousePressed

    private void jtListaBusquedaVentaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaBusquedaVentaMouseReleased
        if (evt.isPopupTrigger() && jtListaBusquedaVenta.getModel().getRowCount() != 0
                && jtListaBusquedaVenta.getSelectedRow() != -1) {
            popupConsultarVenta.show(jtListaBusquedaVenta, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaBusquedaVentaMouseReleased

    private void jtListaClienteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaClienteMousePressed
        if (evt.isPopupTrigger() && jtListaCliente.getModel().getRowCount() != 0
                && jtListaCliente.getSelectedRow() != -1) {
            popupCliente.show(jtListaCliente, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaClienteMousePressed

    private void jtListaClienteMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaClienteMouseReleased
        if (evt.isPopupTrigger() && jtListaCliente.getModel().getRowCount() != 0
                && jtListaCliente.getSelectedRow() != -1) {
            popupCliente.show(jtListaCliente, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaClienteMouseReleased

    private void jtListaBusquedaIngProductoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaBusquedaIngProductoMousePressed
        if (evt.isPopupTrigger() && jtListaBusquedaIngProducto.getModel().getRowCount() != 0
                && jtListaBusquedaIngProducto.getSelectedRow() != -1) {
            popupOrdenIngreso.show(jtListaBusquedaIngProducto, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaBusquedaIngProductoMousePressed

    private void jtListaBusquedaIngProductoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaBusquedaIngProductoMouseReleased
        if (evt.isPopupTrigger() && jtListaBusquedaIngProducto.getModel().getRowCount() != 0
                && jtListaBusquedaIngProducto.getSelectedRow() != -1) {
            popupOrdenIngreso.show(jtListaBusquedaIngProducto, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaBusquedaIngProductoMouseReleased

    private void jtListaBusquedaSalidaProdMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaBusquedaSalidaProdMousePressed
        if (evt.isPopupTrigger() && jtListaBusquedaSalidaProd.getModel().getRowCount() != 0
                && jtListaBusquedaSalidaProd.getSelectedRow() != -1) {
            popupOrdenSalida.show(jtListaBusquedaSalidaProd, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaBusquedaSalidaProdMousePressed

    private void btnAperturarCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAperturarCajaActionPerformed
        int result = 0;
        if (txtMontoInicialCaja.getText().compareTo("") != 0) {
            result = new BLCaja().registar_caja(1, Double.parseDouble(txtMontoInicialCaja.getText()));
            if (result != 0) {
                btnAperturarCaja.setEnabled(false);
                btnCierreCaja.setEnabled(true);
                JOptionPane.showMessageDialog(null, "Caja se Aperturo Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error al Aperturar Caja", "Mensaje", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAperturarCajaActionPerformed

    private void jtListaBusquedaSalidaProdMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaBusquedaSalidaProdMouseReleased
        if (evt.isPopupTrigger() && jtListaBusquedaSalidaProd.getModel().getRowCount() != 0
                && jtListaBusquedaSalidaProd.getSelectedRow() != -1) {
            popupOrdenSalida.show(jtListaBusquedaSalidaProd, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtListaBusquedaSalidaProdMouseReleased

    private void jmiSalirSistemaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSalirSistemaActionPerformed
        new ControlLogin().cerrarApp();
    }//GEN-LAST:event_jmiSalirSistemaActionPerformed

    private void miDetalleMovimientoCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miDetalleMovimientoCajaActionPerformed
        dlgDetalleMovimiento.setSize(this.getToolkit().getScreenSize());
        dlgDetalleMovimiento.pack();
        dlgDetalleMovimiento.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgDetalleMovimiento), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgDetalleMovimiento.setLocationRelativeTo(null);
        dlgDetalleMovimiento.setModal(true);
        dlgDetalleMovimiento.setVisible(true);
    }//GEN-LAST:event_miDetalleMovimientoCajaActionPerformed

    private void btnDetalleCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleCajaActionPerformed
        try {
            if (jvdlg_detallecaja != null) {
                dlgDetalleCaja.remove(jvdlg_detallecaja);
            }
            if (!txtFechaCaja.getDate().equals(null)) {
                dlg_dcaja = new dlg_detallecaja();
                jvdlg_detallecaja = dlg_dcaja.runDetalleCaja(txtFechaCaja.getDate(), dlgDetalleCaja);
                if (jvdlg_detallecaja != null) {
                    jvdlg_detallecaja.setVisible(true);
                } else {
                    dlgDetalleCaja.repaint();
                    JOptionPane.showMessageDialog(null, " No Hay Registro en Caja", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese Fecha a Buscar",
                        "Aviso", 0);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_btnDetalleCajaActionPerformed

    private void btnSalirDetalleCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirDetalleCajaActionPerformed
        dlgDetalleCaja.dispose();
    }//GEN-LAST:event_btnSalirDetalleCajaActionPerformed

    private void miDetalleCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miDetalleCajaActionPerformed
        dlgDetalleCaja.setSize(this.getToolkit().getScreenSize());
        dlgDetalleCaja.pack();
        dlgDetalleCaja.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgDetalleCaja), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgDetalleCaja.setLocationRelativeTo(null);
        dlgDetalleCaja.setModal(true);
        dlgDetalleCaja.setVisible(true);
    }//GEN-LAST:event_miDetalleCajaActionPerformed

    private void btnCierreCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCierreCajaActionPerformed
        int result = 0;
        result = new BLCaja().realizar_cierre_caja(idCaja);
        if (result != 0) {
            JOptionPane.showMessageDialog(null, "Caja cerro exitosamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Error al cerrar Caja", "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnCierreCajaActionPerformed

    private void btnMostrarListaVentaXClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarListaVentaXClienteActionPerformed
        try {
            if (jvdlg_vencliente != null) {
                dlgListaVentaXCliente.remove(jvdlg_vencliente);
            }
            dlg_ventacliente = new dlg_ventaxcliente();
            jvdlg_vencliente = dlg_ventacliente.runVenta_Cliente(((ListaCliente) cboClienteListaForVenta.getSelectedItem()).getId(), dlgListaVentaXCliente);
            if (jvdlg_vencliente != null) {
                jvdlg_vencliente.setVisible(true);
            } else {
                dlgListaVentaXCliente.repaint();
                JOptionPane.showMessageDialog(null, " No Tiene Ventas Pendientes", "Mensaje", 1);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnMostrarListaVentaXClienteActionPerformed

    private void btnSalirListaVentaXClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirListaVentaXClienteActionPerformed
        dlgListaVentaXCliente.dispose();
    }//GEN-LAST:event_btnSalirListaVentaXClienteActionPerformed

    private void miClienteMorososActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miClienteMorososActionPerformed
        cargardatos_cliente_busqueda_morosos("", 0);
        dlgListaClientesMorosos.setSize(this.getToolkit().getScreenSize());
        dlgListaClientesMorosos.pack();
        dlgListaClientesMorosos.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgListaClientesMorosos), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgListaClientesMorosos.setLocationRelativeTo(null);
        dlgListaClientesMorosos.setModal(true);
        dlgListaClientesMorosos.setVisible(true);
    }//GEN-LAST:event_miClienteMorososActionPerformed

    private void btnListaVentaAnuladaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListaVentaAnuladaActionPerformed
        try {
            if (jvdlg_ventanulacion != null) {
                dlgListaVentasAnuladas.remove(jvdlg_ventanulacion);
            }
            if (!txtFechaInicioAlquiler.getDate().equals(null) && !txtFechaFinAlquiler.getDate().equals(null)) {
                dlg_ventanulada = new dlg_venta_anulada();
                jvdlg_ventanulacion = dlg_ventanulada.runVenta_Anulada(
                        txtFechaInicioAlquiler.getDate(),
                        txtFechaFinAlquiler.getDate(),
                        dlgListaVentasAnuladas);
                if (jvdlg_ventanulacion != null) {
                    jvdlg_ventanulacion.setVisible(true);
                } else {
                    dlgListaVentasAnuladas.repaint();
                    JOptionPane.showMessageDialog(null, " No Hay Ventas Anuladas", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese Fecha a Buscar",
                        "Aviso", 0);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnListaVentaAnuladaActionPerformed

    private void btnSalirListaVentaAnuladaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirListaVentaAnuladaActionPerformed
        dlgListaVentasAnuladas.dispose();
    }//GEN-LAST:event_btnSalirListaVentaAnuladaActionPerformed

    private void btnListaClienteMorososActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListaClienteMorososActionPerformed
        try {
            if (jvdlg_clientem != null) {
                dlgListaClientesMorosos.remove(jvdlg_clientem);
            }
            if (!txtFechaInicioListaMorosos.getDate().equals(null) && !txtFechaFinListaClienteMorsos.getDate().equals(null)) {
                dlg_clientem = new dlg_clientemorsos();
                jvdlg_clientem = dlg_clientem.runLista_Morosos(
                        txtFechaInicioListaMorosos.getDate(),
                        txtFechaFinListaClienteMorsos.getDate(),
                        dlgListaClientesMorosos);
                if (jvdlg_clientem != null) {
                    jvdlg_clientem.setVisible(true);
                } else {
                    dlgListaClientesMorosos.repaint();
                    JOptionPane.showMessageDialog(null, " No Existen Deudas Pendientes", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese Fecha a Buscar",
                        "Aviso", 0);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnListaClienteMorososActionPerformed

    private void btnSalirListaClienteMorosoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirListaClienteMorosoActionPerformed
        dlgListaClientesMorosos.dispose();
    }//GEN-LAST:event_btnSalirListaClienteMorosoActionPerformed

    private void miVentasAnuladasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miVentasAnuladasActionPerformed
        dlgListaVentasAnuladas.setSize(this.getToolkit().getScreenSize());
        dlgListaVentasAnuladas.pack();
        dlgListaVentasAnuladas.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgListaVentasAnuladas), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgListaVentasAnuladas.setLocationRelativeTo(null);
        dlgListaVentasAnuladas.setModal(true);
        dlgListaVentasAnuladas.setVisible(true);
    }//GEN-LAST:event_miVentasAnuladasActionPerformed

    private void miVentaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miVentaClienteActionPerformed
        dlgListaVentaXCliente.setSize(this.getToolkit().getScreenSize());
        dlgListaVentaXCliente.pack();
        dlgListaVentaXCliente.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgListaVentaXCliente), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgListaVentaXCliente.setLocationRelativeTo(null);
        dlgListaVentaXCliente.setModal(true);
        dlgListaVentaXCliente.setVisible(true);
    }//GEN-LAST:event_miVentaClienteActionPerformed

    private void miCuadreCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miCuadreCajaActionPerformed
        dlgCuadreCaja.setSize(this.getToolkit().getScreenSize());
        dlgCuadreCaja.pack();
        dlgCuadreCaja.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgCuadreCaja), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgCuadreCaja.setLocationRelativeTo(null);
        dlgCuadreCaja.setModal(true);
        dlgCuadreCaja.setVisible(true);
    }//GEN-LAST:event_miCuadreCajaActionPerformed

    private void btnListaCuadreCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListaCuadreCajaActionPerformed
        try {
            if (jvdlg_cuadre != null) {
                dlgCuadreCaja.remove(jvdlg_cuadre);
            }
            if (!txtFechaCuadreCaja.getDate().equals(null)) {
                dlg_cuadre = new dlg_cuadrecaja();
                jvdlg_cuadre = dlg_cuadre.runCuadreCaja(
                        txtFechaCuadreCaja.getDate(),
                        dlgCuadreCaja);
                if (jvdlg_cuadre != null) {
                    jvdlg_cuadre.setVisible(true);
                } else {
                    dlgCuadreCaja.repaint();
                    JOptionPane.showMessageDialog(null, " No Existen Cuadre Caja", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese Fecha a Buscar",
                        "Aviso", 0);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnListaCuadreCajaActionPerformed

    private void btnSalirCuadreCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirCuadreCajaActionPerformed
        dlgCuadreCaja.dispose();
    }//GEN-LAST:event_btnSalirCuadreCajaActionPerformed

    private void txtBuscarClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarClienteKeyReleased
        cargardatos_cliente(txtBuscarCliente.getText(), 0);
    }//GEN-LAST:event_txtBuscarClienteKeyReleased

    private void txtBuscarProductoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProductoKeyReleased
        cargardatos_producto(txtBuscarProducto.getText(), cboTipoBusquedaProducto.getSelectedItem().toString());
    }//GEN-LAST:event_txtBuscarProductoKeyReleased

    private void txtBuscarTipoProductoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarTipoProductoKeyReleased
        cargardatos_tipoproducto(txtBuscarTipoProducto.getText(), 0);
    }//GEN-LAST:event_txtBuscarTipoProductoKeyReleased

    private void txtBuscarMarcaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarMarcaKeyReleased
        cargardatos_marca(txtBuscarMarca.getText(), 0);
    }//GEN-LAST:event_txtBuscarMarcaKeyReleased

    private void txtBuscarCategoriaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarCategoriaKeyReleased
        cargardatos_categoria(txtBuscarCategoria.getText(), 0);
    }//GEN-LAST:event_txtBuscarCategoriaKeyReleased

    private void txtBuscarProductoIngProdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProductoIngProdKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            jtListaProductoBusquedaIngProd.setFocusable(true);
        }
    }//GEN-LAST:event_txtBuscarProductoIngProdKeyPressed

    private void txtBuscarProductoIngProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProductoIngProdKeyReleased
        cargardatos_busqueda_ingproducto(txtBuscarProductoIngProd.getText(),
                cboTipoBusquedaProductoIngProd.getSelectedItem().toString());
    }//GEN-LAST:event_txtBuscarProductoIngProdKeyReleased

    private void txtBuscarClienteVentaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarClienteVentaKeyReleased
        cargardatos_clientebusqueda(txtBuscarClienteVenta.getText(), 0);
    }//GEN-LAST:event_txtBuscarClienteVentaKeyReleased

    private void txtBuscarProductoVentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProductoVentaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            jtListaProductoBusquedaVenta.setFocusable(true);
        }
    }//GEN-LAST:event_txtBuscarProductoVentaKeyPressed

    private void txtBuscarProductoVentaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProductoVentaKeyReleased
        cargardatos_busqueda_venta(txtBuscarProductoVenta.getText(),
                cboTipoBusquedaProductoVenta.getSelectedItem().toString());
    }//GEN-LAST:event_txtBuscarProductoVentaKeyReleased

    private void txtMontoInicialCajaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoInicialCajaKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtMontoInicialCaja.getText().length() == 18) {
            evt.consume();
        }
    }//GEN-LAST:event_txtMontoInicialCajaKeyTyped

    private void txtCantidadProductoVentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadProductoVentaKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtCantidadProductoVenta.getText().length() == 12) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCantidadProductoVentaKeyTyped

    private void txtPrecioVentaVentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioVentaVentaKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtPrecioVentaVenta.getText().length() == 12) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPrecioVentaVentaKeyTyped

    private void txtAcuentaVentaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAcuentaVentaKeyReleased
        try {
            DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
            simbolos.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.##", simbolos);
            if (!txtAcuentaVenta.getText().equals("")) {
                double totalFacturado = 0.0, acuenta = 0.0;
                totalFacturado = Double.parseDouble(txtMontoTotalVenta.getText());
                acuenta = Double.parseDouble(txtAcuentaVenta.getText());
                if ((totalFacturado - acuenta) <= 0) {
                    txtSaldoVenta.setText("0.0");
                    txtVueltoVenta.setText("" + Double.parseDouble(df.format(acuenta - totalFacturado)));
                } else {
                    txtSaldoVenta.setText("" + Double.parseDouble(df.format(totalFacturado - acuenta)));
                }
            }
        } catch (Exception e) {
            System.out.println("error al ingresar a cuenta venta:" + e.getMessage());
        }
    }//GEN-LAST:event_txtAcuentaVentaKeyReleased

    private void txtAcuentaVentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAcuentaVentaKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtAcuentaVenta.getText().length() == 12) {
            evt.consume();
        }
    }//GEN-LAST:event_txtAcuentaVentaKeyTyped

    private void txtCantidadProductoSalidaProdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadProductoSalidaProdKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtCantidadProductoingProd.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCantidadProductoSalidaProdKeyTyped

    private void txtCantidadProductoingProdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadProductoingProdKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtCantidadProductoingProd.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCantidadProductoingProdKeyTyped

    private void txtPrecioCostoingProdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioCostoingProdKeyTyped
        new Funciones().soloDecimales(evt);
        if (txtPrecioCostoingProd.getText().length() == 8) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPrecioCostoingProdKeyTyped

    private void btnSalirDetalleMovimientoCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirDetalleMovimientoCajaActionPerformed
        dlgDetalleMovimiento.dispose();
    }//GEN-LAST:event_btnSalirDetalleMovimientoCajaActionPerformed

    private void btnDetalleMovimientoCajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleMovimientoCajaActionPerformed
        try {
            if (jvdlg_movimiento != null) {
                dlgDetalleMovimiento.remove(jvdlg_movimiento);
            }
            if (!txtFechaCajaMovimiento.getDate().equals(null)) {
                dlg_mov = new dlg_movimiento();
                jvdlg_movimiento = dlg_mov.runDetalleMovimiento(txtFechaCajaMovimiento.getDate(), dlgDetalleMovimiento);
                if (jvdlg_movimiento != null) {
                    jvdlg_movimiento.setVisible(true);
                } else {
                    dlgDetalleMovimiento.repaint();
                    JOptionPane.showMessageDialog(null, " No Hay Movimientos Registrados", "Mensaje", 1);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese Fecha a Buscar",
                        "Aviso", 0);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_btnDetalleMovimientoCajaActionPerformed

    private void mmovTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mmovTodosActionPerformed
        try {
            List<JTable> tb = new ArrayList<JTable>();
            tb.add(jtListaMovimientoInventario);
            exportar_excel excelExporter = new exportar_excel(jtListaMovimientoInventario, new File("STOCK_ACTUAL.xls"));
            if (excelExporter.export()) {
                JOptionPane.showMessageDialog(null, "DATOS EXPORTADOS CON EXITO!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        llama_excel_movimiento_stock();
    }//GEN-LAST:event_mmovTodosActionPerformed

    private void mosExportarExcelAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mosExportarExcelAllActionPerformed
        try {
            List<JTable> tb = new ArrayList<JTable>();
            tb.add(jtListaBusquedaSalidaProd);
            exportar_excel excelExporter = new exportar_excel(jtListaBusquedaSalidaProd, new File("LISTA_ORDEN_SALIDA.xls"));
            if (excelExporter.export()) {
                JOptionPane.showMessageDialog(null, "DATOS EXPORTADOS CON EXITO!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        llama_excel_salida_producto();
    }//GEN-LAST:event_mosExportarExcelAllActionPerformed

    private void mosdocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mosdocumentoActionPerformed
        try {
            doc_ordensalida = new documento_ordensalida();
            doc_ordensalida.run_viewdocumento_ordensalida(Integer.parseInt(String.valueOf(jtListaBusquedaSalidaProd.getModel().getValueAt(jtListaBusquedaSalidaProd.getSelectedRow(), 0))));
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mosdocumentoActionPerformed

    private void mosAnularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mosAnularActionPerformed
        try {
            int result = 0;
            result = new BLOrden_Salida().anular_orden_salida(Integer.parseInt(String.valueOf(jtListaBusquedaSalidaProd.getModel().getValueAt(jtListaBusquedaSalidaProd.getSelectedRow(), 0))),
                    "0");
            if (result != 0) {
                cargadatos_consultar_ordensalida();
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mosAnularActionPerformed

    private void moiExportarExcelAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moiExportarExcelAllActionPerformed
        try {
            List<JTable> tb = new ArrayList<JTable>();
            tb.add(jtListaBusquedaIngProducto);
            exportar_excel excelExporter = new exportar_excel(jtListaBusquedaIngProducto, new File("LISTA_ORDEN_INGRESO.xls"));
            if (excelExporter.export()) {
                JOptionPane.showMessageDialog(null, "DATOS EXPORTADOS CON EXITO!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        llama_excel_ingreso_producto();
    }//GEN-LAST:event_moiExportarExcelAllActionPerformed

    private void moidocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moidocumentoActionPerformed
        try {
            doc_ordeningreso = new documento_ordeningreso();
            doc_ordeningreso.run_viewdocumento_ordeningreso(Integer.parseInt(String.valueOf(jtListaBusquedaIngProducto.getModel().getValueAt(jtListaBusquedaIngProducto.getSelectedRow(), 0))));
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_moidocumentoActionPerformed

    private void moiAnularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moiAnularActionPerformed
        try {
            int result = 0;
            result = new BLOrden_Ingreso().anular_orden_ingreso(Integer.parseInt(String.valueOf(jtListaBusquedaIngProducto.getModel().getValueAt(jtListaBusquedaIngProducto.getSelectedRow(), 0))),
                    "0");
            if (result != 0) {
                cargadatos_consultar_ordeningreso();
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_moiAnularActionPerformed

    private void mclExportarExcelAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mclExportarExcelAllActionPerformed
        try {
            exportar_excel excelExporter = new exportar_excel(jtListaCliente, new File("LISTA_CLIENTE.xls"));
            if (excelExporter.export()) {
                JOptionPane.showMessageDialog(null, "DATOS EXPORTADOS CON EXITO!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        llama_excel_cliente();
    }//GEN-LAST:event_mclExportarExcelAllActionPerformed

    private void mclRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mclRestaurarActionPerformed
        try {
            int result = 0;
            result = new BLCliente().activar_restablecer_cliente(Integer.parseInt(String.valueOf(jtListaCliente.getModel().getValueAt(jtListaCliente.getSelectedRow(), 0))),
                    "1");
            if (result != 0) {
                cargardatos_cliente("", 0);
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mclRestaurarActionPerformed

    private void mclEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mclEliminarActionPerformed
        try {
            int result = 0;
            result = new BLCliente().activar_restablecer_cliente(Integer.parseInt(String.valueOf(jtListaCliente.getModel().getValueAt(jtListaCliente.getSelectedRow(), 0))),
                    "0");
            if (result != 0) {
                cargardatos_cliente("", 0);
                JOptionPane.showMessageDialog(null, "Se habilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mclEliminarActionPerformed

    private void btnValidarUsuarioVentaAnulacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarUsuarioVentaAnulacionActionPerformed
        if (txtMotivoAnulacion.getText().compareTo("") != 0) {
            userDefault = null;
            String clave = new String(txtClaveVentaAnulacion.getPassword());
            if (clave.compareTo("") != 0) {
                userDefault = new BLUsuario().validarPassword(clave);
                if (userDefault == null) {
                    txtClaveVentaAnulacion.requestFocus(true);
                    JOptionPane.showMessageDialog(null, "[ Contraseña Invalida ]");
                } else {
                    int result = 0;
                    result = new BLVenta_Anulada().registar_venta_anulada(idVenta, txtMotivoAnulacion.getText(), userDefault.getInt_id());
                    if (result != 0) {
                        cargadatos_consultar_venta();
                        dlgAnularVenta.dispose();
                        JOptionPane.showMessageDialog(null, "Anulación Éxito", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Anulación Fallida", "Alerta", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "[ Ingrese su Clave ]");
            }
        } else {
            JOptionPane.showMessageDialog(null, "[ Porfavor escriba el motivo de la anulacion ]");
        }
    }//GEN-LAST:event_btnValidarUsuarioVentaAnulacionActionPerformed

    private void txtMotivoAnulacionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMotivoAnulacionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtClaveVentaAnulacion.requestFocus();
        }
    }//GEN-LAST:event_txtMotivoAnulacionKeyPressed

    private void jtListaPagosProgramadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaPagosProgramadosMouseClicked
        try {
            String estado = String.valueOf(jtListaPagosProgramados.getModel().getValueAt(jtListaPagosProgramados.getSelectedRow(), 6));
            if (!estado.equals("Cancelada")) {
                idCronoPago = Integer.parseInt(String.valueOf(jtListaPagosProgramados.getModel().getValueAt(jtListaPagosProgramados.getSelectedRow(), 0)));
                acuenta_anterior_cp = Double.parseDouble(String.valueOf(jtListaPagosProgramados.getModel().getValueAt(jtListaPagosProgramados.getSelectedRow(), 5)));
                txtMontoCuotaCronogramaP.setText(String.valueOf(jtListaPagosProgramados.getModel().getValueAt(jtListaPagosProgramados.getSelectedRow(), 4)));
            } else {
                JOptionPane.showMessageDialog(null, "Cuota ya fue pagada", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_jtListaPagosProgramadosMouseClicked

    private void btnPagarCronogramaPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagarCronogramaPagoActionPerformed
        double acuenta_actual = Double.parseDouble(txtMontoAbonarCronogramaP.getText().equals("") ? "0.0" : txtMontoAbonarCronogramaP.getText());
        double monto_cuota = Double.parseDouble(txtMontoCuotaCronogramaP.getText().equals("") ? "0.0" : txtMontoCuotaCronogramaP.getText());
        String estado = (acuenta_actual + acuenta_anterior_cp) >= monto_cuota ? "2" : "1";
        int result = 0;
        result = new BLVenta_Credito().actualizar_cronogramapago(idCronoPago, acuenta_anterior_cp, acuenta_actual, estado, idVenta);
        if (result != 0) {
            cargardatos_listar_cronogramapago(idVenta);
            cargadatos_consultar_venta();
            txtMontoAbonarCronogramaP.setText("");
            txtMontoCuotaCronogramaP.setText("");
            JOptionPane.showMessageDialog(null, "Pago Realizado tuvo Éxito", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Pago Realizado no tuvo Éxito", "Alerta", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPagarCronogramaPagoActionPerformed

    private void miExportarExcelAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExportarExcelAllActionPerformed
        try {
            List<JTable> tb = new ArrayList<JTable>();
            tb.add(jtListaBusquedaVenta);
            exportar_excel excelExporter = new exportar_excel(jtListaBusquedaVenta, new File("LISTA_VENTAS.xls"));
            if (excelExporter.export()) {
                JOptionPane.showMessageDialog(null, "DATOS EXPORTADOS CON EXITO!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        llama_excel_venta();
    }//GEN-LAST:event_miExportarExcelAllActionPerformed

    private void mimpcronogramaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mimpcronogramaActionPerformed
        try {
            doc_cronograma = new documento_cronogramapago();
            doc_cronograma.run_viewdocumento_cronogramapago(Integer.parseInt(String.valueOf(jtListaBusquedaVenta.getModel().getValueAt(jtListaBusquedaVenta.getSelectedRow(), 0))));
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mimpcronogramaActionPerformed

    private void mianularventaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mianularventaActionPerformed
        try {
            idVenta = Integer.parseInt(String.valueOf(jtListaBusquedaVenta.getModel().getValueAt(jtListaBusquedaVenta.getSelectedRow(), 0)));
            txtMotivoAnulacion.setText("");
            txtClaveVentaAnulacion.setText("");
            dlgAnularVenta.pack();
            dlgAnularVenta.setLocationRelativeTo(null);
            dlgAnularVenta.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgAnularVenta), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
            dlgAnularVenta.setModal(true);
            dlgAnularVenta.setVisible(true);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mianularventaActionPerformed

    private void micronogramaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_micronogramaActionPerformed
        try {
            idVenta = Integer.parseInt(String.valueOf(jtListaBusquedaVenta.getModel().getValueAt(jtListaBusquedaVenta.getSelectedRow(), 0)));
            String formapago = String.valueOf(jtListaBusquedaVenta.getModel().getValueAt(jtListaBusquedaVenta.getSelectedRow(), 9));
            if (formapago.equals("Credito")) {
                // buscar en venta_credito obtener el id ventacredito a traves del id venta
                cargardatos_listar_cronogramapago(idVenta);
                dlgCronogramaPago.pack();
                dlgCronogramaPago.setLocationRelativeTo(null);
                dlgCronogramaPago.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgCronogramaPago), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
                dlgCronogramaPago.setModal(true);
                dlgCronogramaPago.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Valido Solo para Ventas al Credito", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_micronogramaActionPerformed

    private void midocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_midocumentoActionPerformed
        try {
            idVenta = Integer.parseInt(String.valueOf(jtListaBusquedaVenta.getModel().getValueAt(jtListaBusquedaVenta.getSelectedRow(), 0)));
            doc_venta = new documento_venta();
            doc_venta.run_viewdocumento_venta(idVenta);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_midocumentoActionPerformed

    private void txtNroCuotaVentaCreditoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNroCuotaVentaCreditoKeyReleased
        try {
            double totalFacturado = 0.0, montocuota = 0.0;
            DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
            simbolos.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.##", simbolos);
            totalFacturado = Double.parseDouble(txtMontoTotalVentaCredito.getText().equals("") ? "0.0" : txtMontoTotalVentaCredito.getText());
            int nrocuota = txtNroCuotaVentaCredito.getValue();
            montocuota = (totalFacturado / nrocuota);
            txtMontoXCuotaVentaCredito.setText("" + df.format(montocuota));
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_txtNroCuotaVentaCreditoKeyReleased

    private void txtNroCuotaVentaCreditoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_txtNroCuotaVentaCreditoPropertyChange
        try {
            double totalFacturado = 0.0, montocuota = 0.0, acuenta = 0.0, descuento = 0.0;
            DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
            simbolos.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.##", simbolos);
            totalFacturado = Double.parseDouble(txtMontoTotalVentaCredito.getText().equals("") ? "0.0" : txtMontoTotalVentaCredito.getText());
            int nrocuota = txtNroCuotaVentaCredito.getValue();
            montocuota = (totalFacturado / nrocuota);
            txtMontoXCuotaVentaCredito.setText("" + df.format(montocuota));
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_txtNroCuotaVentaCreditoPropertyChange

    private void btnCrearVentaCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearVentaCreditoActionPerformed
        txtClaveVenta.setText("");
        dlgValidarUsuarioVenta.pack();
        dlgValidarUsuarioVenta.setLocationRelativeTo(null);
        dlgValidarUsuarioVenta.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgValidarUsuarioVenta), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgValidarUsuarioVenta.setModal(true);
        dlgValidarUsuarioVenta.setVisible(true);
    }//GEN-LAST:event_btnCrearVentaCreditoActionPerformed

    private void txtBuscarProductoSalidaProdKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProductoSalidaProdKeyReleased
        cargardatos_busqueda_salidaproducto(txtBuscarProductoSalidaProd.getText(),
                cboTipoBusquedaProductoSalidaProd.getSelectedItem().toString());
    }//GEN-LAST:event_txtBuscarProductoSalidaProdKeyReleased

    private void txtBuscarProductoSalidaProdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProductoSalidaProdKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            jtListaProductoBusquedaSalidaProd.setFocusable(true);
        }
    }//GEN-LAST:event_txtBuscarProductoSalidaProdKeyPressed

    private void jtListaProductoBusquedaSalidaProdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtListaProductoBusquedaSalidaProdKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                idProductoSalProd = Integer.parseInt(String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 0)));
                Producto_stockactual = Double.parseDouble(String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 6)));
                txtNombreProductoSalidaProd.setText(String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 2))
                        + " - " + String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 3))
                        + " - " + String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 4)));
                txtNombreProductoSalidaProd.setCaretPosition(0);
                dlgBuscarProductoSalidaProd.dispose();
            } catch (Exception e) {
                System.out.println("" + e.getMessage());
            }
        }
    }//GEN-LAST:event_jtListaProductoBusquedaSalidaProdKeyPressed

    private void jtListaProductoBusquedaSalidaProdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtListaProductoBusquedaSalidaProdMouseClicked
        try {
            idProductoSalProd = Integer.parseInt(String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 0)));
            Producto_stockactual = Double.parseDouble(String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 6)));
            txtNombreProductoSalidaProd.setText(String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 2))
                    + " - " + String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 3))
                    + " - " + String.valueOf(jtListaProductoBusquedaSalidaProd.getModel().getValueAt(jtListaProductoBusquedaSalidaProd.getSelectedRow(), 4)));
            txtNombreProductoSalidaProd.setCaretPosition(0);
            dlgBuscarProductoSalidaProd.dispose();
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }//GEN-LAST:event_jtListaProductoBusquedaSalidaProdMouseClicked

    private void mtpEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mtpEditarActionPerformed
        try {
            idTipoProducto = Integer.parseInt(String.valueOf(jtListaTipoProducto.getModel().getValueAt(jtListaTipoProducto.getSelectedRow(), 0)));
            String descripcion = String.valueOf(jtListaTipoProducto.getModel().getValueAt(jtListaTipoProducto.getSelectedRow(), 1));
            txtDescripcionTipo.setText(descripcion);
            btnGuardarTipoProducto.setEnabled(false);
            btnEditarTipoProducto.setEnabled(true);
            btnCancelarTipoProducto.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mtpEditarActionPerformed

    private void mpdExportarExcellAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mpdExportarExcellAllActionPerformed
        try {
            exportar_excel excelExporter = new exportar_excel(jtListaProducto, new File("LISTA_PRODUCTOS.xls"));
            if (excelExporter.export()) {
                JOptionPane.showMessageDialog(null, "DATOS EXPORTADOS CON EXITO!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        llama_excel_producto();
    }//GEN-LAST:event_mpdExportarExcellAllActionPerformed

    private void mpdEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mpdEditarActionPerformed
        try {
            String codigoBarra = String.valueOf(jtListaProducto.getModel().getValueAt(jtListaProducto.getSelectedRow(), 0));
            txtCodigoBarraProducto.setText(codigoBarra);
            txtDescripcionProducto.setText(String.valueOf(jtListaProducto.getModel().getValueAt(jtListaProducto.getSelectedRow(), 1)));
            txtPrecioCostoProducto.setText(String.valueOf(jtListaProducto.getModel().getValueAt(jtListaProducto.getSelectedRow(), 5)));
            txtPrecioVentaProducto.setText(String.valueOf(jtListaProducto.getModel().getValueAt(jtListaProducto.getSelectedRow(), 6)));
            Producto p = new BLProducto().getProducto_byCodigoBarra(codigoBarra);
            idProducto = p.getInt_id();
            txtStockMinimo.setText("" + p.getDec_stockMinimo());
            txtStockMaximo.setText("" + p.getDec_stockMaximo());
            txtUtilidadPorcentajeProducto.setValue(p.getDec_utilidadbrutaporcentaje());
            seleccionar_tipoproducto_byNombre(new BLConstante().get_constante_byvalor_clase(p.getInt_tipo(), 1).getVar_descripcion());
            seleccionar_marca_byNombre(new BLMarca().get_marca_byId(p.getInt_marca()).getVar_descripcion());
            seleccionar_categoria_byNombre("" + new BLCategoria().get_categoria_byId(p.getInt_categoria()).getVar_descripcion());
            btnGuardarProducto.setEnabled(false);
            btnActualizarProducto.setEnabled(true);
            btnCancelarProducto.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mpdEditarActionPerformed

    private void mcEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mcEditarActionPerformed
        try {
            idCategoria = Integer.parseInt(String.valueOf(jtListaCategoria.getModel().getValueAt(jtListaCategoria.getSelectedRow(), 0)));
            txtDescripcionCategoria.setText(String.valueOf(jtListaCategoria.getModel().getValueAt(jtListaCategoria.getSelectedRow(), 1)));
            btnGuardarCategoria.setEnabled(false);
            btnEditarCategoria.setEnabled(true);
            btnCancelarCategoria.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mcEditarActionPerformed

    private void mmEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mmEditarActionPerformed
        try {
            idMarca = Integer.parseInt(String.valueOf(jtListaMarca.getModel().getValueAt(jtListaMarca.getSelectedRow(), 0)));
            txtDescripcionMarca.setText(String.valueOf(jtListaMarca.getModel().getValueAt(jtListaMarca.getSelectedRow(), 1)));
            btnGuardarMarca.setEnabled(false);
            btnEditarMarca.setEnabled(true);
            btnCancelarMarca.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mmEditarActionPerformed

    private void mpRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mpRestaurarActionPerformed
        try {
            int result = 0;
            result = new BLProveedor().activar_restablecer_proveedor(Integer.parseInt(String.valueOf(jtListaProveedor.getModel().getValueAt(jtListaProveedor.getSelectedRow(), 0))),
                    "1");
            if (result != 0) {
                cargardatos_proveedor("", 1);
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mpRestaurarActionPerformed

    private void mpEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mpEliminarActionPerformed
        try {
            int result = 0;
            result = new BLProveedor().activar_restablecer_proveedor(Integer.parseInt(String.valueOf(jtListaProveedor.getModel().getValueAt(jtListaProveedor.getSelectedRow(), 0))),
                    "0");
            if (result != 0) {
                cargardatos_proveedor("", 1);
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mpEliminarActionPerformed

    private void mpEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mpEditarActionPerformed
        try {
            idProveedor = Integer.parseInt(String.valueOf(jtListaProveedor.getModel().getValueAt(jtListaProveedor.getSelectedRow(), 0)));
            txtRucProveedor.setText(String.valueOf(jtListaProveedor.getModel().getValueAt(jtListaProveedor.getSelectedRow(), 1)));
            txtRazonSocialProveedor.setText(String.valueOf(jtListaProveedor.getModel().getValueAt(jtListaProveedor.getSelectedRow(), 2)));
            txtTelefonoProveedor.setText(String.valueOf(jtListaProveedor.getModel().getValueAt(jtListaProveedor.getSelectedRow(), 4)));
            txtDireccionProveedor.setText(String.valueOf(jtListaProveedor.getModel().getValueAt(jtListaProveedor.getSelectedRow(), 3)));
            btnGuardarProveedor.setEnabled(false);
            btnActualizarProveedor.setEnabled(true);
            btnCancelarProveedor.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mpEditarActionPerformed

    private void muRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_muRestaurarActionPerformed
        try {
            int result = 0;
            result = new BLUsuario().activar_restablecer_usuario(Integer.parseInt(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 0))),
                    "1");
            if (result != 0) {
                cargardatos_usuario("", 2);
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_muRestaurarActionPerformed

    private void muEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_muEliminarActionPerformed
        try {
            int result = 0;
            result = new BLUsuario().activar_restablecer_usuario(Integer.parseInt(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 0))),
                    "0");
            if (result != 0) {
                cargardatos_usuario("", 2);
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_muEliminarActionPerformed

    private void muEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_muEditarActionPerformed
        try {
            idUsuario = Integer.parseInt(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 0)));
            txtNomApeUsuario.setText(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 1)));
            txtDNIUsuario.setText(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 2)));
            txtIDNameUsuario.setText(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 3)));
            txtTelefonoUsuario.setText(String.valueOf(jtListaUsuario.getModel().getValueAt(jtListaUsuario.getSelectedRow(), 4)));
            btnGuardarUsuario.setEnabled(false);
            btnActualizarUsuario.setEnabled(true);
            btnPermisoUsuario.setEnabled(false);
            btnCancelarUsuario.setEnabled(true);
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_muEditarActionPerformed

    private void btnValidarUsuarioVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarUsuarioVentaActionPerformed
        userDefault = null;
        String clave = new String(txtClaveVenta.getPassword());
        if (clave.compareTo("") != 0) {
            userDefault = new BLUsuario().validarPassword(clave);
            if (userDefault == null) {
                txtClaveVenta.requestFocus(true);
                JOptionPane.showMessageDialog(null, "[ Contraseña Invalida ]");
            } else {
                dlgVentaCredito.dispose();
                btnValidarUsuarioVenta.setEnabled(false);
                btnValidarUsuarioVenta.setBackground(Color.yellow);
                dlgValidarUsuarioVenta.dispose();
                crearVenta();
                btnValidarUsuarioVenta.setEnabled(true);
                btnValidarUsuarioVenta.setBackground(Color.DARK_GRAY);
            }
        } else {
            JOptionPane.showMessageDialog(null, "[ No se Admite Campos Vacios ]");
        }
    }//GEN-LAST:event_btnValidarUsuarioVentaActionPerformed

    private void btnAgregarDatosDocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarDatosDocActionPerformed
        int result = 0, tipoDocumento = 0;
        boolean condicion = true;
        if (txtSerieInicialDoc.getText().compareTo("") != 0 && txtNumeroInicialDoc.getText().compareTo("") != 0) {
            tipoDocumento = ((Constante) cboTipoDocumentoDoc.getSelectedItem()).getInt_valor();
            DefaultTableModel listadetalle = (DefaultTableModel) jtListaCodigoDocImpresion.getModel();
            int fila = listadetalle.getRowCount();
            for (int f = 0; f < fila; f++) {
                if (String.valueOf(jtListaCodigoDocImpresion.getModel().getValueAt(f, 1))
                        .equalsIgnoreCase(cboTipoDocumentoDoc.getSelectedItem().toString())) {
                    condicion = false;
                }
            }
            if (condicion) {
                result = new BLCodigo_Documento().registar_codigodocumento(tipoDocumento, txtSerieInicialDoc.getText(), txtNumeroInicialDoc.getText());
                if (result != 0) {
                    cargardatos_codigo_documento_impresion();
                    JOptionPane.showMessageDialog(null, "Registro Exitoso", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    txtSerieInicialDoc.setText("");
                    txtNumeroInicialDoc.setText("");
                    cboTipoDocumentoDoc.setSelectedIndex(0);
                } else {
                    JOptionPane.showMessageDialog(null, "Registro Fallido", "Mensaje", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se admite campos vacios!", "Mensaje", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnAgregarDatosDocActionPerformed

    private void btnSalirPermisoUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirPermisoUsuarioActionPerformed
        dlgPermisosUsuario.dispose();
    }//GEN-LAST:event_btnSalirPermisoUsuarioActionPerformed

    private void btnGuardarPermisoUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarPermisoUsuarioActionPerformed
        /*if (registrarPermisos(Integer.parseInt(txtCodigoUsuario.getText())) != 0) {
         JOptionPane.showMessageDialog(null, "Para ver los Cambios en el Sistema, termine sesion y vuelva inciar",
         "Aviso", 1);
         } else {
         JOptionPane.showMessageDialog(null, "No se puedo asignar persmiso al sistema", "Aviso", 1);
         }*/
    }//GEN-LAST:event_btnGuardarPermisoUsuarioActionPerformed

    private void txtBuscarUsuarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarUsuarioKeyReleased
        String filtro = cboTipoBusquedaUsuario.getSelectedItem().toString();
        int condicion = 0;
        switch (filtro) {
            case "DNI":
                condicion = 2;
                break;
            case "Nombre y Apellidos":
                condicion = 1;
                break;
            case "ID Name":
                condicion = 3;
                break;
        }
        cargardatos_usuario(txtBuscarUsuario.getText(), condicion);
    }//GEN-LAST:event_txtBuscarUsuarioKeyReleased

    private void txtBuscarProveedorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarProveedorKeyReleased
        String filtro = cboTipoBusquedaProveedor.getSelectedItem().toString();
        int condicion = 0;
        switch (filtro) {
            case "Ruc":
                condicion = 1;
                break;
            case "Razon Social":
                condicion = 2;
                break;
        }
        cargardatos_usuario(txtBuscarUsuario.getText(), condicion);
    }//GEN-LAST:event_txtBuscarProveedorKeyReleased

    private void btnGuardarMovimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarMovimientoActionPerformed
        txtClaveMovimiento.setText("");
        dlgValidarUsuarioMov.pack();
        dlgValidarUsuarioMov.setLocationRelativeTo(null);
        dlgValidarUsuarioMov.getRootPane().registerKeyboardAction(new CloseDialogEscape(dlgValidarUsuarioMov), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        dlgValidarUsuarioMov.setModal(true);
        dlgValidarUsuarioMov.setVisible(true);
    }//GEN-LAST:event_btnGuardarMovimientoActionPerformed

    private void cboTipoDocumentoMovActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoDocumentoMovActionPerformed
        Constante c = (Constante) (cboTipoDocumentoMov.getSelectedItem());
        switch (c.getInt_valor()) {
            case 2:
                txtDocumentoMov.setEditable(true);
                break;
            case 3:
                txtDocumentoMov.setEditable(true);
                break;
            case 4:
                txtDocumentoMov.setEditable(true);
                break;
            default:
                txtDocumentoMov.setEditable(false);
                break;
        }
    }//GEN-LAST:event_cboTipoDocumentoMovActionPerformed

    private void chkProductoStockActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkProductoStockActualActionPerformed
        if (chkProductoStockActual.isSelected()) {
            cboProductoInventario.setEnabled(true);
        } else if (!chkProductoStockActual.isSelected()) {
            cboProductoInventario.setEnabled(false);
        }
    }//GEN-LAST:event_chkProductoStockActualActionPerformed

    private void btnBuscarStockActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarStockActualActionPerformed
        if (chkProductoStockActual.isSelected()) {
            cargar_stock_actual(cboProductoInventario.getSelectedItem().toString());
        } else if (!chkProductoStockActual.isSelected()) {
            cargar_stock_actual("");
        }
    }//GEN-LAST:event_btnBuscarStockActualActionPerformed

    private void mpdEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mpdEliminarActionPerformed
        try {
            int result = 0;
            Producto p = new BLProducto().getProducto_byCodigoBarra(String.valueOf(jtListaProducto.getModel().getValueAt(jtListaProducto.getSelectedRow(), 0)));
            result = new BLProducto().activar_restablecer_producto(p.getInt_id(), "0");
            if (result != 0) {
                cargardatos_producto("", "Descripcion");
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mpdEliminarActionPerformed

    private void mpdRestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mpdRestaurarActionPerformed
        try {
            int result = 0;
            Producto p = new BLProducto().getProducto_byCodigoBarra(String.valueOf(jtListaProducto.getModel().getValueAt(jtListaProducto.getSelectedRow(), 0)));
            result = new BLProducto().activar_restablecer_producto(p.getInt_id(), "1");
            if (result != 0) {
                cargardatos_producto("", "Descripcion");
                JOptionPane.showMessageDialog(null, "Se Inhabilito Correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Fallado Inhabilitacion", "Alerta", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            System.out.println("" + e.toString());
        }
    }//GEN-LAST:event_mpdRestaurarActionPerformed

    private void txtCantidadProductoingProdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtCantidadProductoingProdMouseClicked
        txtCantidadProductoingProd.setText("");
    }//GEN-LAST:event_txtCantidadProductoingProdMouseClicked

    public void run() {
        Thread ct = Thread.currentThread();
        while (ct == h1) {
            calcula();
            lblHora.setText("Hora: " + hora + ":" + minutos + ":" + segundos + " " + ampm);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void calcula() {
        Calendar calendario = new GregorianCalendar();
        Date fechaHoraActual = new Date();

        calendario.setTime(fechaHoraActual);
        ampm = calendario.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";
        if (ampm.equals("PM")) {
            int h = calendario.get(Calendar.HOUR_OF_DAY) - 12;
            if (h == 0) {
                h = 12;
            }

            hora = h > 9 ? "" + h : "0" + h;
        } else {
            hora = calendario.get(Calendar.HOUR_OF_DAY) > 9 ? "" + calendario.get(Calendar.HOUR_OF_DAY) : "0" + calendario.get(Calendar.HOUR_OF_DAY);
        }
        minutos = calendario.get(Calendar.MINUTE) > 9 ? "" + calendario.get(Calendar.MINUTE) : "0" + calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND) > 9 ? "" + calendario.get(Calendar.SECOND) : "0" + calendario.get(Calendar.SECOND);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            /*for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }*/
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarProducto;
    private javax.swing.JButton btnActualizarProveedor;
    private javax.swing.JButton btnActualizarUsuario;
    private javax.swing.JButton btnAgregarCliente;
    private javax.swing.JButton btnAgregarDatosDoc;
    private javax.swing.JButton btnAgregarDetProdructo;
    private javax.swing.JButton btnAgregarDetProdructoSalidaProd;
    private javax.swing.JButton btnAgregarDetProdructoVenta;
    private javax.swing.JButton btnAgregarNuevoCliente;
    public javax.swing.JButton btnAperturarCaja;
    private javax.swing.JButton btnBuscarClienteVenta;
    private javax.swing.JButton btnBuscarIngProducto;
    private javax.swing.JButton btnBuscarInventario;
    private javax.swing.JButton btnBuscarProductoIngProd;
    private javax.swing.JButton btnBuscarProductoSalidaProd;
    private javax.swing.JButton btnBuscarProductoVenta;
    private javax.swing.JButton btnBuscarSalidaProd;
    private javax.swing.JButton btnBuscarStockActual;
    private javax.swing.JButton btnBuscarVenta;
    private javax.swing.JButton btnCancelarCategoria;
    private javax.swing.JButton btnCancelarIngProd;
    private javax.swing.JButton btnCancelarMarca;
    private javax.swing.JButton btnCancelarProducto;
    private javax.swing.JButton btnCancelarProveedor;
    private javax.swing.JButton btnCancelarSalidaProd;
    private javax.swing.JButton btnCancelarTipoProducto;
    private javax.swing.JButton btnCancelarUsuario;
    private javax.swing.JButton btnCancelarVenta;
    public javax.swing.JButton btnCierreCaja;
    private javax.swing.JButton btnCrearVenta;
    private javax.swing.JButton btnCrearVentaCredito;
    private javax.swing.JButton btnDetalleCaja;
    private javax.swing.JButton btnDetalleMovimientoCaja;
    private javax.swing.JButton btnEditarCategoria;
    private javax.swing.JButton btnEditarCliente;
    private javax.swing.JButton btnEditarMarca;
    private javax.swing.JButton btnEditarNuevoCliente;
    private javax.swing.JButton btnEditarTipoProducto;
    private javax.swing.JButton btnEliminarDatosDoc;
    private javax.swing.JButton btnGuardarCategoria;
    private javax.swing.JButton btnGuardarIngProd;
    private javax.swing.JButton btnGuardarMarca;
    private javax.swing.JButton btnGuardarMovimiento;
    private javax.swing.JButton btnGuardarPermisoUsuario;
    private javax.swing.JButton btnGuardarProducto;
    private javax.swing.JButton btnGuardarProveedor;
    private javax.swing.JButton btnGuardarSalidaProd;
    private javax.swing.JButton btnGuardarTipoProducto;
    private javax.swing.JButton btnGuardarUsuario;
    private javax.swing.JButton btnLimpiarM;
    private javax.swing.JButton btnListaClienteMorosos;
    private javax.swing.JButton btnListaCuadreCaja;
    private javax.swing.JButton btnListaVentaAnulada;
    private javax.swing.JButton btnMostrarListaVentaXCliente;
    private javax.swing.JButton btnPagarCronogramaPago;
    private javax.swing.JButton btnPermisoUsuario;
    private javax.swing.JButton btnQuitarDetProducto;
    private javax.swing.JButton btnQuitarDetProductoSalidaProd;
    private javax.swing.JButton btnQuitarDetProductoVenta;
    private javax.swing.JButton btnSalirCliente;
    private javax.swing.JButton btnSalirCuadreCaja;
    private javax.swing.JButton btnSalirDetalleCaja;
    private javax.swing.JButton btnSalirDetalleMovimientoCaja;
    private javax.swing.JButton btnSalirListaClienteMoroso;
    private javax.swing.JButton btnSalirListaVentaAnulada;
    private javax.swing.JButton btnSalirListaVentaXCliente;
    private javax.swing.JButton btnSalirPermisoUsuario;
    private javax.swing.JButton btnValidarUsuarioMov;
    private javax.swing.JButton btnValidarUsuarioVenta;
    private javax.swing.JButton btnValidarUsuarioVentaAnulacion;
    private javax.swing.JComboBox cboBusqFormaPagoVenta;
    private org.jdesktop.swingx.JXComboBox cboCategoria;
    private javax.swing.JComboBox cboClienteListaForVenta;
    private org.jdesktop.swingx.JXComboBox cboClienteVenta;
    private javax.swing.JComboBox cboFormaPagoMov;
    private javax.swing.JComboBox cboFormaPagoVenta;
    private javax.swing.JComboBox cboFrecuenciaPagoVentaCredito;
    private org.jdesktop.swingx.JXComboBox cboMarca;
    private javax.swing.JComboBox cboMotivoBusIngProd;
    private javax.swing.JComboBox cboMotivoBusSalidaProd;
    private javax.swing.JComboBox cboMotivoInventario;
    private javax.swing.JComboBox cboMotivoRegIngProducto;
    private javax.swing.JComboBox cboMotivoRegSalidaProd;
    private org.jdesktop.swingx.JXComboBox cboPersonalInventario;
    private org.jdesktop.swingx.JXComboBox cboPersonalRegSalidaProd;
    private org.jdesktop.swingx.JXComboBox cboPersonalSalidaProd;
    private org.jdesktop.swingx.JXComboBox cboProductoInventario;
    private org.jdesktop.swingx.JXComboBox cboProveedorIngProveedor;
    private org.jdesktop.swingx.JXComboBox cboProveedorRegIngProveedor;
    private javax.swing.JComboBox cboTipoBusquedaProducto;
    private javax.swing.JComboBox cboTipoBusquedaProductoIngProd;
    private javax.swing.JComboBox cboTipoBusquedaProductoSalidaProd;
    private javax.swing.JComboBox cboTipoBusquedaProductoVenta;
    private javax.swing.JComboBox cboTipoBusquedaProveedor;
    private javax.swing.JComboBox cboTipoBusquedaUsuario;
    private javax.swing.JComboBox cboTipoDocumentoDoc;
    private org.jdesktop.swingx.JXComboBox cboTipoDocumentoIngProd;
    private javax.swing.JComboBox cboTipoDocumentoMov;
    private javax.swing.JComboBox cboTipoDocumentoVenta;
    private javax.swing.JComboBox cboTipoOperacionMov;
    private org.jdesktop.swingx.JXComboBox cboTipoProducto;
    private javax.swing.JComboBox cboTipoVentta;
    private javax.swing.JCheckBox chkClienteVenta;
    private javax.swing.JCheckBox chkDocumentoIngProd;
    private javax.swing.JCheckBox chkDocumentoVenta;
    private javax.swing.JCheckBox chkFechasIngProd;
    private javax.swing.JCheckBox chkFechasInventario;
    private javax.swing.JCheckBox chkFechasSalidaProd;
    private javax.swing.JCheckBox chkFechasVenta;
    private javax.swing.JCheckBox chkFormaPagoVenta;
    private javax.swing.JCheckBox chkMotivoBusquedaIngProd;
    private javax.swing.JCheckBox chkMotivoBusquedaSalidaProd;
    private javax.swing.JCheckBox chkMotivoInventario;
    private javax.swing.JCheckBox chkPersonalInventario;
    private javax.swing.JCheckBox chkPersonalSalidaProd;
    private javax.swing.JCheckBox chkProductoStockActual;
    private javax.swing.JCheckBox chkProveedorIngProd;
    private javax.swing.JCheckBox ckConRUC;
    private javax.swing.JDialog dlgAnularVenta;
    private javax.swing.JDialog dlgBuscarProductoIngProd;
    private javax.swing.JDialog dlgBuscarProductoSalidaProd;
    private javax.swing.JDialog dlgBuscarProductoVenta;
    private javax.swing.JDialog dlgCliente;
    private javax.swing.JDialog dlgClienteBusqVenta;
    private javax.swing.JDialog dlgCronogramaPago;
    private javax.swing.JDialog dlgCuadreCaja;
    private javax.swing.JDialog dlgDetalleCaja;
    private javax.swing.JDialog dlgDetalleMovimiento;
    private javax.swing.JDialog dlgDocumentos;
    private javax.swing.JDialog dlgListaClientesMorosos;
    private javax.swing.JDialog dlgListaVentaXCliente;
    private javax.swing.JDialog dlgListaVentasAnuladas;
    private javax.swing.JDialog dlgPermisosUsuario;
    private javax.swing.JDialog dlgValidarUsuarioMov;
    private javax.swing.JDialog dlgValidarUsuarioVenta;
    private javax.swing.JDialog dlgVentaCredito;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel120;
    private javax.swing.JLabel jLabel121;
    private javax.swing.JLabel jLabel122;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel124;
    private javax.swing.JLabel jLabel125;
    private javax.swing.JLabel jLabel126;
    private javax.swing.JLabel jLabel127;
    private javax.swing.JLabel jLabel128;
    private javax.swing.JLabel jLabel129;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel130;
    private javax.swing.JLabel jLabel131;
    private javax.swing.JLabel jLabel132;
    private javax.swing.JLabel jLabel133;
    private javax.swing.JLabel jLabel134;
    private javax.swing.JLabel jLabel135;
    private javax.swing.JLabel jLabel136;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane17;
    private javax.swing.JScrollPane jScrollPane18;
    private javax.swing.JScrollPane jScrollPane19;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane20;
    private javax.swing.JScrollPane jScrollPane21;
    private javax.swing.JScrollPane jScrollPane22;
    private javax.swing.JScrollPane jScrollPane23;
    private javax.swing.JScrollPane jScrollPane27;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JMenuItem jmiConfiguracionDocImpresion;
    private javax.swing.JMenuItem jmiSalirSistema;
    private javax.swing.JPanel jpAdministracion;
    private javax.swing.JPanel jpCaja;
    private javax.swing.JPanel jpCliente;
    private javax.swing.JPanel jpConsultarIngProducto;
    private javax.swing.JPanel jpConsultarSalidaProd;
    private javax.swing.JPanel jpConsultarVenta;
    private javax.swing.JPanel jpDetalleClientesMorosos;
    private javax.swing.JPanel jpDetalleClientesMorosos1;
    private javax.swing.JPanel jpDetalleMovimientoReporte;
    private javax.swing.JPanel jpDetalleMovimientoReporte1;
    private javax.swing.JPanel jpDetalleMovimientoReporte3;
    private javax.swing.JPanel jpGenerarVenta;
    private javax.swing.JPanel jpInventario;
    private javax.swing.JPanel jpListaVentasXCliente;
    private javax.swing.JPanel jpMovimientoVenta;
    private javax.swing.JPanel jpProducto;
    private javax.swing.JPanel jpProveedor;
    private javax.swing.JPanel jpRegistrarIngProducto;
    private javax.swing.JPanel jpRegistrarSalidaProd;
    private javax.swing.JPanel jpTipoMarcaCategoria;
    private javax.swing.JPanel jpTipoProducto;
    private javax.swing.JPanel jpUsuario;
    private javax.swing.JPanel jpValidacion;
    private javax.swing.JPanel jpValidacionVenta;
    private javax.swing.JPanel jpVentaCredito;
    private javax.swing.JPanel jpVentaCredito1;
    private javax.swing.JPanel jpVentaCredito2;
    private javax.swing.JPanel jpVentas;
    private javax.swing.JTable jtDetalleProductoIngProd;
    private javax.swing.JTable jtDetalleProductoSalidaProd;
    private javax.swing.JTable jtDetalleProductoVenta;
    private javax.swing.JTable jtListaBusquedaIngProducto;
    private javax.swing.JTable jtListaBusquedaSalidaProd;
    private javax.swing.JTable jtListaBusquedaVenta;
    private javax.swing.JTable jtListaCategoria;
    private javax.swing.JTable jtListaCliente;
    private javax.swing.JTable jtListaClienteBusq;
    private javax.swing.JTable jtListaCodigoDocImpresion;
    private javax.swing.JTable jtListaMarca;
    private javax.swing.JTable jtListaMovimientoInventario;
    private javax.swing.JTable jtListaPagosProgramados;
    private javax.swing.JTable jtListaProducto;
    private javax.swing.JTable jtListaProductoBusquedaIngProd;
    private javax.swing.JTable jtListaProductoBusquedaSalidaProd;
    private javax.swing.JTable jtListaProductoBusquedaVenta;
    private javax.swing.JTable jtListaProveedor;
    private javax.swing.JTable jtListaTipoProducto;
    private javax.swing.JTable jtListaUsuario;
    private javax.swing.JTable jtStockActual;
    private javax.swing.JTabbedPane jtbAdministracion;
    private javax.swing.JTabbedPane jtbInventario;
    private javax.swing.JTabbedPane jtbVenta;
    private javax.swing.JTabbedPane jtpPrincipal;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblHora;
    public javax.swing.JLabel lblNameTrabajador;
    private javax.swing.JLabel lblNroDocumentoVenta;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JLabel lblfechaActual;
    private javax.swing.JMenuBar mBarraPrincipal;
    private javax.swing.JMenuItem mcEditar;
    private javax.swing.JMenuItem mclEliminar;
    private javax.swing.JMenuItem mclExportarExcelAll;
    private javax.swing.JMenuItem mclRestaurar;
    private javax.swing.JMenu miAYUDA;
    private javax.swing.JMenuItem miClienteMorosos;
    private javax.swing.JMenuItem miCuadreCaja;
    private javax.swing.JMenuItem miDetalleCaja;
    private javax.swing.JMenuItem miDetalleMovimientoCaja;
    private javax.swing.JMenuItem miExportarExcelAll;
    private javax.swing.JMenu miHome;
    private javax.swing.JMenu miReporte;
    private javax.swing.JMenuItem miVentaCliente;
    private javax.swing.JMenuItem miVentasAnuladas;
    private javax.swing.JMenuItem mianularventa;
    private javax.swing.JMenuItem micronograma;
    private javax.swing.JMenuItem midocumento;
    private javax.swing.JMenuItem mimpcronograma;
    private javax.swing.JMenuItem mmEditar;
    private javax.swing.JMenuItem mmovTodos;
    private javax.swing.JMenuItem moiAnular;
    private javax.swing.JMenuItem moiExportarExcelAll;
    private javax.swing.JMenuItem moidocumento;
    private javax.swing.JMenuItem mosAnular;
    private javax.swing.JMenuItem mosExportarExcelAll;
    private javax.swing.JMenuItem mosdocumento;
    private javax.swing.JMenuItem mpEditar;
    private javax.swing.JMenuItem mpEliminar;
    private javax.swing.JMenuItem mpExportarExcellAll;
    private javax.swing.JMenuItem mpRestaurar;
    private javax.swing.JMenuItem mpdEditar;
    private javax.swing.JMenuItem mpdEliminar;
    private javax.swing.JMenuItem mpdExportarExcellAll;
    private javax.swing.JMenuItem mpdRestaurar;
    private javax.swing.JMenuItem mtpEditar;
    private javax.swing.JMenuItem muEditar;
    private javax.swing.JMenuItem muEliminar;
    private javax.swing.JMenuItem muRestaurar;
    private javax.swing.JPanel pnlCliente;
    private javax.swing.JPopupMenu popupCategoria;
    private javax.swing.JPopupMenu popupCliente;
    private javax.swing.JPopupMenu popupConsultarVenta;
    private javax.swing.JPopupMenu popupMarca;
    private javax.swing.JPopupMenu popupMovInventario;
    private javax.swing.JPopupMenu popupOrdenIngreso;
    private javax.swing.JPopupMenu popupOrdenSalida;
    private javax.swing.JPopupMenu popupProducto;
    private javax.swing.JPopupMenu popupProveedor;
    private javax.swing.JPopupMenu popupStockActual;
    private javax.swing.JPopupMenu popupTipoProducto;
    private javax.swing.JPopupMenu popupUsuario;
    private javax.swing.JTable tbListaPermiso;
    private javax.swing.JTextField txtAcuentaVenta;
    private javax.swing.JTextField txtApellidoMCliente;
    private javax.swing.JTextField txtApellidoPCliente;
    private javax.swing.JTextField txtBuscarCategoria;
    private javax.swing.JTextField txtBuscarCliente;
    private javax.swing.JTextField txtBuscarClienteVenta;
    private javax.swing.JTextField txtBuscarMarca;
    private javax.swing.JTextField txtBuscarProducto;
    private javax.swing.JTextField txtBuscarProductoIngProd;
    private javax.swing.JTextField txtBuscarProductoSalidaProd;
    private javax.swing.JTextField txtBuscarProductoVenta;
    private javax.swing.JTextField txtBuscarProveedor;
    private javax.swing.JTextField txtBuscarTipoProducto;
    private javax.swing.JTextField txtBuscarUsuario;
    private javax.swing.JTextField txtCantProductoIngProd;
    private javax.swing.JTextField txtCantProductoSalidaProd;
    private javax.swing.JTextField txtCantProductoVenta;
    private javax.swing.JTextField txtCantidadProductoSalidaProd;
    private javax.swing.JTextField txtCantidadProductoVenta;
    private javax.swing.JTextField txtCantidadProductoingProd;
    private javax.swing.JPasswordField txtClaveMovimiento;
    private javax.swing.JPasswordField txtClaveUsuario;
    private javax.swing.JPasswordField txtClaveVenta;
    private javax.swing.JPasswordField txtClaveVentaAnulacion;
    private javax.swing.JTextField txtClienteVenta;
    private javax.swing.JTextField txtCodigoBarraProducto;
    private javax.swing.JTextField txtCodigoUsuario;
    private javax.swing.JTextField txtComentarioIngProd;
    private javax.swing.JTextField txtComentarioSalidaProd;
    private javax.swing.JTextField txtDNICliente;
    private javax.swing.JTextField txtDNIClienteVenta;
    private javax.swing.JTextField txtDNIUsuario;
    private javax.swing.JTextField txtDescripcionCategoria;
    private javax.swing.JTextField txtDescripcionMarca;
    private javax.swing.JTextField txtDescripcionProducto;
    private javax.swing.JTextField txtDescripcionTipo;
    private com.toedter.components.JSpinField txtDescuentoVenta;
    private javax.swing.JTextField txtDireccionCliente;
    private javax.swing.JTextField txtDireccionProveedor;
    private javax.swing.JTextField txtDocumentoMov;
    private com.toedter.calendar.JDateChooser txtFechaCaja;
    private com.toedter.calendar.JDateChooser txtFechaCajaMovimiento;
    private com.toedter.calendar.JDateChooser txtFechaCuadreCaja;
    private com.toedter.calendar.JDateChooser txtFechaEmisionIngProd;
    private com.toedter.calendar.JDateChooser txtFechaEmisionSalidaProd;
    private com.toedter.calendar.JDateChooser txtFechaFinAlquiler;
    private com.toedter.calendar.JDateChooser txtFechaFinIngProd;
    private com.toedter.calendar.JDateChooser txtFechaFinInventario;
    private com.toedter.calendar.JDateChooser txtFechaFinListaClienteMorsos;
    private com.toedter.calendar.JDateChooser txtFechaFinSalidaProd;
    private com.toedter.calendar.JDateChooser txtFechaFinVenta;
    private com.toedter.calendar.JDateChooser txtFechaInicioAlquiler;
    private com.toedter.calendar.JDateChooser txtFechaInicioIngProd;
    private com.toedter.calendar.JDateChooser txtFechaInicioInventario;
    private com.toedter.calendar.JDateChooser txtFechaInicioListaMorosos;
    private com.toedter.calendar.JDateChooser txtFechaInicioSalidaProd;
    private com.toedter.calendar.JDateChooser txtFechaInicioVenta;
    private com.toedter.calendar.JDateChooser txtFechaPrimerPagoVentaCredito;
    private javax.swing.JTextField txtIDNameUsuario;
    private javax.swing.JTextField txtIGVMontoVenta;
    private com.toedter.components.JSpinField txtIGVPorcentaje;
    private javax.swing.JTextField txtMontoAbonarCronogramaP;
    private javax.swing.JTextField txtMontoCuotaCronogramaP;
    public javax.swing.JTextField txtMontoInicialCaja;
    private javax.swing.JTextField txtMontoMovimiento;
    private javax.swing.JTextField txtMontoTotalIngProd;
    private javax.swing.JTextField txtMontoTotalVenta;
    private javax.swing.JTextField txtMontoTotalVentaCredito;
    private javax.swing.JTextField txtMontoXCuotaVentaCredito;
    private javax.swing.JTextArea txtMotivoAnulacion;
    private javax.swing.JTextArea txtMotivoMovimiento;
    private javax.swing.JTextField txtNomApeUsuario;
    private javax.swing.JTextField txtNomCompletoUsuario;
    private javax.swing.JTextField txtNombreCliente;
    private javax.swing.JTextField txtNombreProductoIngProd;
    private javax.swing.JTextField txtNombreProductoSalidaProd;
    private javax.swing.JTextField txtNombreProductoVenta;
    private com.toedter.components.JSpinField txtNroCuotaVentaCredito;
    private javax.swing.JTextField txtNumDocIngProd;
    private javax.swing.JTextField txtNumeroDocBusqVenta;
    private javax.swing.JTextField txtNumeroDocBusquedaIngProd;
    private javax.swing.JTextField txtNumeroInicialDoc;
    private javax.swing.JTextField txtPrecioCostoProducto;
    private javax.swing.JTextField txtPrecioCostoingProd;
    private javax.swing.JTextField txtPrecioVentaProducto;
    private javax.swing.JTextField txtPrecioVentaVenta;
    private javax.swing.JTextField txtRUCCliente;
    private javax.swing.JTextField txtRazonSocial;
    private javax.swing.JTextField txtRazonSocialProveedor;
    private javax.swing.JTextField txtRucClienteVenta;
    private javax.swing.JTextField txtRucProveedor;
    private javax.swing.JTextField txtSaldoVenta;
    private javax.swing.JTextField txtSerieDocBusqVenta;
    private javax.swing.JTextField txtSerieDocBusquedaIngProd;
    private javax.swing.JTextField txtSerieDocIngProd;
    private javax.swing.JTextField txtSerieInicialDoc;
    private javax.swing.JTextField txtStockMaximo;
    private javax.swing.JTextField txtStockMinimo;
    private javax.swing.JTextField txtSubTotalMonto;
    private javax.swing.JTextField txtTelefonoCliente;
    private javax.swing.JTextField txtTelefonoProveedor;
    private javax.swing.JTextField txtTelefonoUsuario;
    private javax.swing.JSpinner txtUtilidadPorcentajeProducto;
    private javax.swing.JTextField txtVueltoVenta;
    // End of variables declaration//GEN-END:variables
    private documento_venta doc_venta;
    private documento_cronogramapago doc_cronograma;
    private documento_ordeningreso doc_ordeningreso;
    private documento_ordensalida doc_ordensalida;
    private dlg_movimiento dlg_mov;
    private dlg_detallecaja dlg_dcaja;
    private dlg_ventaxcliente dlg_ventacliente;
    private dlg_venta_anulada dlg_ventanulada;
    private dlg_clientemorsos dlg_clientem;
    private dlg_cuadrecaja dlg_cuadre;
    private JRViewer jvdlg_movimiento, jvdlg_detallecaja, jvdlg_vencliente,
            jvdlg_clientem, jvdlg_ventanulacion, jvdlg_cuadre;

    //-----------------Lista Defaul Model For JTable-------------------------
    DefaultTableModel ListaProveedor = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    DefaultTableModel ListaUsuario = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    DefaultTableModel ListaProducto = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    DefaultTableModel ListaProductoBusquedaIngProd = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    DefaultTableModel ListaProductoBusquedaSalProd = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    DefaultTableModel ListaProductoBusquedaVenta = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    DefaultTableModel ListaCliente = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    DefaultTableModel ListaClienteBusqueda = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

}
