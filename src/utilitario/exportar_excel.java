/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.io.*;
import java.util.List;
import javax.swing.*;
import javax.swing.table.TableModel;
import jxl.*;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;

public class exportar_excel {

    private File archi;
    //private List<JTable> tabla;
    private JTable tabla;
    private List<String> nom_hoja;
    private WritableCellFormat fomato_fila;
    private WritableCellFormat fomato_columna;

    /*public exportar_excel(List<JTable> tab, File ar) throws Exception {
     this.archi = ar;
     this.tabla = tab;
     if (tab.size() < 0) {
     throw new Exception("ERROR");
     }
     }*/
    public exportar_excel(JTable tab, File ar) throws Exception {
        this.archi = ar;
        this.tabla = tab;
        if (tab.getRowCount() < 0) {
            throw new Exception("ERROR");
        }
    }

    public boolean export() {
        /*try {
         DataOutputStream out = new DataOutputStream(new FileOutputStream(archi));
         WritableWorkbook w = Workbook.createWorkbook(out);
         w.createSheet("Reporte Sistema", 0);

         for (int index = 0; index < tabla.size(); index++) {
         JTable table = tabla.get(index);

         WritableSheet s = w.getSheet(0);

         for (int i = 0; i < table.getColumnCount(); i++) {
         for (int j = 0; j < table.getRowCount(); j++) {
         Object objeto = table.getValueAt(j, i);
         s.addCell(new Label(i,j,String.valueOf(objeto)));
         //createColumna(s, table.getColumnName(i), i);//crea la columna
         //createFilas(s, i, j, String.valueOf(objeto));//crea las filas
         }
         }
         }
         w.write();
         w.close();
         out.close();
         return true;

         } catch (IOException ex) {
         ex.printStackTrace();
         } catch (WriteException ex) {
         ex.printStackTrace();
         }*/
        try {

            DataOutputStream out = new DataOutputStream(new FileOutputStream(archi));
            WritableWorkbook w = Workbook.createWorkbook(out);
            WritableSheet s = w.createSheet("First Sheet", 0);

            TableModel model = tabla.getModel();

            for (int i = 0; i < model.getColumnCount(); i++) {
                Label column = new Label(i, 0, model.getColumnName(i));
                s.addCell(column);
            }
            int j = 0;
            for (int i = 0; i < model.getRowCount(); i++) {
                for (j = 0; j < model.getColumnCount(); j++) {
                    Label row = new Label(j, i + 1,
                            model.getValueAt(i, j).toString());
                    s.addCell(row);
                }
            }
            w.write();
            w.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private void createColumna(WritableSheet sheet, String columna, int number_columna) throws WriteException {
        //creamos el tipo de letra
        WritableFont times10pt = new WritableFont(WritableFont.TAHOMA, 14);
        // definimos el formato d ela celda
        WritableCellFormat times = new WritableCellFormat(times10pt);
        // Permite si se ajusta automáticamente a las células
        //times.setWrap(true);
        // crea una negrita con subrayado
        WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TAHOMA, 10, WritableFont.BOLD, false, UnderlineStyle.SINGLE);
        fomato_columna = new WritableCellFormat(times10ptBoldUnderline);
        // Permite que se ajusta automáticamente a las células
        //fomato_columna.setWrap(true);
        CellView cv = new CellView();
        cv.setSize(220);
        cv.setDimension(70);
        cv.setFormat(times);
        cv.setFormat(fomato_columna);
        //cv.setAutosize(true);
        // escribimos las columnas
        addColumna(sheet, number_columna, 0, columna, fomato_columna);//numero de columna , 0 es la fila
    }

    /**
     * *************************************
     */
    private void createFilas(WritableSheet sheet, int number_columna, int filas, String name_filas) throws WriteException {
        //creamos el tipo de letra
        WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 12);
        times10pt.setColour(Colour.GOLD);
        // definimos el formato d ela celda
        WritableCellFormat times = new WritableCellFormat(times10pt);
        times.setBorder(Border.TOP, BorderLineStyle.MEDIUM, Colour.GOLD);
        // Permite si se ajusta automáticamente a las células
        //times.setWrap(true);
        // crea una negrita con subrayado
        WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 12, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE);
        fomato_fila = new WritableCellFormat(times10ptBoldUnderline);
        // Permite que se ajusta automáticamente a las células
        //fomato_fila.setWrap(true);
        CellView cv = new CellView();
        cv.setDimension(70);
        cv.setFormat(times);
        cv.setFormat(fomato_fila);
        //cv.setAutosize(true);
        // escribimos las columnas
        addFilas(sheet, number_columna, filas, name_filas, fomato_fila);
    }

    /**
     * ********************************
     */
    private void addColumna(WritableSheet sheet, int column, int row, String s, WritableCellFormat format) throws RowsExceededException, WriteException {
        Label label;
        label = new Label(column, row, s, format);
        sheet.addCell(label);
    }

    private void addFilas(WritableSheet sheet, int column, int row, String s, WritableCellFormat format) throws WriteException, RowsExceededException {
        Label label;
        label = new Label(column, row, s, format);
        sheet.addCell(label);
    }

}
