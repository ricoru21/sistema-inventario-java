/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @web http://www.jc-mouse.net
 * @author Mouse
 */
public class FormatoTabla extends DefaultTableCellRenderer {

    private int columna_patron;

    public FormatoTabla(int Colpatron) {
        this.columna_patron = Colpatron;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        setBackground(Color.white);//color de fondo
        table.setForeground(Color.black);//color de texto

        // if (column == 7) {
            if (table.getValueAt(row, columna_patron).equals("0.0")) {
                setBackground(Color.red);
            }
        // }
        
        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        return this;
    }

}
