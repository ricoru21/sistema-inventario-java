package utilitario;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
/**
 * @web http://www.jc-mouse.net/
 * @author Mouse
 */
public class Worker extends SwingWorker<Void, Void>{

    private final JButton boton ;    
    private final JLabel estado;
    //private database db = new database();
    private String nombre="";
            
    public Worker( JButton boton, JLabel estado )
    {
        this.boton = boton;        
        this.estado = estado;
    }
    
    /**
     * Metodo que realiza la tarea pesada
     */
    @Override
    protected Void doInBackground() throws Exception {
        estado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/bolivia/example/wait.gif")));
        this.estado.setText("Estado: Buscando, por favor espere...");
        this.boton.setEnabled(false);        
        //nombre = db.getRandRegistro();
        Thread.sleep(5000);                       
        return null;
    }

    /**
     * Resultado en pantalla
     */
    @Override
    protected void done(){      
        estado.setIcon(null);
        this.boton.setEnabled(true);
        this.estado.setText("Estado:");
        JOptionPane.showMessageDialog( null , nombre );
    }
    
}
