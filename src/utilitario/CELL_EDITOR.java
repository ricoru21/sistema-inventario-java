/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

/**
 *
 * @author Richard
 */
public class CELL_EDITOR extends DefaultCellEditor {

    private static final long serialVersionUID = 1L;

    public CELL_EDITOR(JCheckBox checkBox) {
        super(checkBox);
        checkBox.setHorizontalAlignment(JLabel.CENTER);
    }
}