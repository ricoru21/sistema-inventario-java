/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitario;

import java.awt.*;
import java.awt.print.*;
import javax.swing.JOptionPane;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.Doc;
import javax.print.ServiceUI;
import javax.print.attribute.*;

/**
 *
 * @author Richard
 */
public class ImpresionTicket {

    //Ticket attribute content
    public String contentTicket = "   CLEAN SERVICE S.A.C\n"
            + "Esquina Av Mansiche y America\n"
            + "Oeste Int. H101 Sector SODIMAC\n"
            + "RUC:20481550816    S:FFFG17608\n"
            + "EL CISNE\n"
            + "{{fechaRegistro}}\n"
            + "-----------------------------\n"
            + "Ticket # {{ticket}}\n"
            + "Cli: {{cliente}}\n"
            + "-----------------------------\n"
            + "{{items}}\n"
            + "-----------------------------\n"
            + "SUBTOTAL: S/. {{subTotal}}\n"
            + "CORTESIA: {{cortesia}}\n"
            + "SERV. RAPIDO: {{sevrap}}\n"
            + "----------------\n"
            + "TOTAL: {{total}}\n"
            + "A CUENTA: {{acuenta}}\n"
            + "----------------\n"
            + "SALDO FINAL: {{saldofinal}}\n\n"
            + "-----------------------------\n"
            + "GRACIAS POR SU PREFERENCIA!!!\n"
            + "{{usuario}}\n"
            + "\n"
            + "\n";

    //El constructor que setea los valores a la instancia
    public ImpresionTicket(String fechaRegistro, String ticket, String cliente, String items, String subTotal, String cortesia, String sevrap, String total, String acuenta, String saldofinal, String usuario) {
        this.contentTicket = this.contentTicket.replace("{{fechaRegistro}}", fechaRegistro);
        this.contentTicket = this.contentTicket.replace("{{ticket}}", ticket);
        this.contentTicket = this.contentTicket.replace("{{cliente}}", cliente);
        this.contentTicket = this.contentTicket.replace("{{items}}", items);
        this.contentTicket = this.contentTicket.replace("{{subTotal}}", subTotal);
        this.contentTicket = this.contentTicket.replace("{{cortesia}}", cortesia);
        this.contentTicket = this.contentTicket.replace("{{sevrap}}", sevrap);
        this.contentTicket = this.contentTicket.replace("{{total}}", total);
        this.contentTicket = this.contentTicket.replace("{{acuenta}}", acuenta);
        this.contentTicket = this.contentTicket.replace("{{saldofinal}}", saldofinal);
        this.contentTicket = this.contentTicket.replace("{{usuario}}", usuario);
    }

    public void print(String contentido) {
    //Especificamos el tipo de dato a imprimir
        //Tipo: bytes; Subtipo: autodetectado
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;

    //Aca obtenemos el servicio de impresion por defatul
        //Si no quieres ver el dialogo de seleccionar impresora usa esto
        //PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
    //Con esto mostramos el dialogo para seleccionar impresora
        //Si quieres ver el dialogo de seleccionar impresora usalo
        //Solo mostrara las impresoras que soporte arreglo de bits
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor, pras);
        PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
        PrintService service = ServiceUI.printDialog(null, 700, 200, printService, defaultService, flavor, pras);

        //Creamos un arreglo de tipo byte
        byte[] bytes;

    //Aca convertimos el string(cuerpo del ticket) a bytes tal como
        //lo maneja la impresora(mas bien ticketera :p)
        //bytes = this.contentTicket.getBytes();
        bytes = contentido.getBytes();
    //Creamos un documento a imprimir, a el se le appendeara
        //el arreglo de bytes
        Doc doc = new SimpleDoc(bytes, flavor, null);

        try {
            //Creamos un trabajo de impresión
            DocPrintJob job = service.createPrintJob();
      //Imprimimos dentro de un try de a huevo
            //El metodo print imprime
            job.print(doc, null);
        } catch (Exception er) {
            JOptionPane.showMessageDialog(null, "Error al imprimir: " + er.getMessage());
        }

    }

    public void print_direct() {
      //Especificamos el tipo de dato a imprimir
        //Tipo: bytes; Subtipo: autodetectado
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        
        //Aca obtenemos el servicio de impresion por defatul
        //Si no quieres ver el dialogo de seleccionar impresora usa esto
        //PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
        //Con esto mostramos el dialogo para seleccionar impresora
        //Si quieres ver el dialogo de seleccionar impresora usalo
        //Solo mostrara las impresoras que soporte arreglo de bits
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor, pras);
        PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
        PrintService service = ServiceUI.printDialog(null, 700, 200, printService, defaultService, flavor, pras);

        //Creamos un arreglo de tipo byte
        byte[] bytes;

        //Aca convertimos el string(cuerpo del ticket) a bytes tal como
        //lo maneja la impresora(mas bien ticketera :p)
        bytes = this.contentTicket.getBytes();
        //Creamos un documento a imprimir, a el se le appendeara
        //el arreglo de bytes
        Doc doc = new SimpleDoc(bytes, flavor, null);
        
        try {
            //Creamos un trabajo de impresión
            DocPrintJob job = service.createPrintJob();
            //Imprimimos dentro de un try de a huevo
            //El metodo print imprime
            job.print(doc, null);
        } catch (Exception er) {
            System.out.println("Error al imprimir: " + er.getMessage());
        }
    }

}
