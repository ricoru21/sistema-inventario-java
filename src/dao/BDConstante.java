/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDConstante {

    public int insertarConstante(Constante c) {
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_constante (?,?,?)}");
            cstmt.setString(1, c.getVar_descripcion());
            cstmt.setInt(2, c.getInt_clase());
            cstmt.setInt(3, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            cstmt.executeUpdate();
            cnn.commit();
            valor = cstmt.getInt("codigo");
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
    public int updateConstante(Constante c) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_constante (?,?,?)}");
            cstmt.setInt(1,c.getInt_id());
            cstmt.setString(2, c.getVar_descripcion());
            cstmt.setInt(3, c.getInt_clase());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
    public ArrayList<Constante> selectConstante_byClase(int clase) {
        ArrayList<Constante> lista = new ArrayList<Constante>();
        Constante c = null;
        try {
            PreparedStatement ps;
            try (Connection cnn = BD.getConnection()) {
                ps = null;
                ps = cnn.prepareStatement("select * from constante where int_valor<>0 and int_clase=" + clase);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    c = new Constante();
                    c.setInt_id(rs.getInt("int_id"));
                    c.setInt_valor(rs.getInt("int_valor"));
                    c.setVar_descripcion(rs.getString("var_descripcion"));
                    c.setInt_clase(clase);
                    lista.add(c);
                }
            }
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        }
        return lista;
    }

    public ArrayList<Constante> get_lista_constante_tipoproducto(String condicion) {
        ArrayList<Constante> lista = new ArrayList<Constante>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from constante where int_valor<>0 and int_clase=1 and " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Constante c = new Constante();
                c.setInt_id(rs.getInt("int_id"));
                c.setInt_valor(rs.getInt("int_valor"));
                c.setVar_descripcion(rs.getString("var_descripcion"));
                c.setInt_clase(rs.getInt("int_clase"));
                lista.add(c);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

    public Constante get_constante_byvalor_clase(int valor, int clase) {
        Constante c = null;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from constante where int_valor = " + valor + " and int_clase=" + clase);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                c = new Constante();
                c.setInt_id(rs.getInt("int_id"));
                c.setInt_valor(rs.getInt("int_valor"));
                c.setVar_descripcion(rs.getString("var_descripcion"));
                c.setInt_clase(rs.getInt("int_clase"));
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return c;
    }

}
