/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDMarca {
    
    public int insertarMarca(Marca m) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_marca (?,?)}");
            cstmt.setString(1, m.getVar_descripcion() );
            cstmt.setString(2, m.getVar_estado());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally{
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
    public int updateMarca(Marca m) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_marca (?,?,?)}");
            cstmt.setInt(1, m.getInt_id() );
            cstmt.setString(2, m.getVar_descripcion() );
            cstmt.setString(3, m.getVar_estado());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally{
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
    public ArrayList<Marca> selectMarca_all() {
        ArrayList<Marca> lista = new ArrayList<Marca>();
        Marca m = null;
        try {
            PreparedStatement ps;
            try (Connection cnn = BD.getConnection()) {
                ps = null;
                ps = cnn.prepareStatement("select * from marca");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    m = new Marca();
                    m.setInt_id(rs.getInt("int_id"));
                    m.setVar_descripcion(rs.getString("var_descripcion"));
                    m.setVar_estado(rs.getString("var_estado"));
                    lista.add(m);
                }
            }
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        }
        return lista;
    }
    
    public ArrayList<Marca> get_lista_marca_all(String condicion) {
        ArrayList<Marca> lista = new ArrayList<Marca>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from marca where "+condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Marca m = new Marca();
                m.setInt_id(rs.getInt("int_id"));
                m.setVar_descripcion(rs.getString("var_descripcion"));
                m.setVar_estado(rs.getString("var_estado"));
                lista.add(m);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }
    
    public Marca get_marca_byId(int id) {
        Marca m = null;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from marca where int_id ="+id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                m = new Marca();
                m.setInt_id(rs.getInt("int_id"));
                m.setVar_descripcion(rs.getString("var_descripcion"));
                m.setVar_estado(rs.getString("var_estado"));
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return m;
    }
    
}
