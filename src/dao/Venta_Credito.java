/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Venta_Credito {

    private int int_id;
    private int int_nrocuota;
    private double dec_montocuota;
    private int int_formapago;
    private int int_frecuenciapago;
    private String var_estado;
    private int int_venta;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public int getInt_nrocuota() {
        return int_nrocuota;
    }

    public void setInt_nrocuota(int int_nrocuota) {
        this.int_nrocuota = int_nrocuota;
    }

    public double getDec_montocuota() {
        return dec_montocuota;
    }

    public void setDec_montocuota(double dec_montocuota) {
        this.dec_montocuota = dec_montocuota;
    }

    public int getInt_formapago() {
        return int_formapago;
    }

    public void setInt_formapago(int int_formapago) {
        this.int_formapago = int_formapago;
    }

    public int getInt_frecuenciapago() {
        return int_frecuenciapago;
    }

    public void setInt_frecuenciapago(int int_frecuenciapago) {
        this.int_frecuenciapago = int_frecuenciapago;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

    public int getInt_venta() {
        return int_venta;
    }

    public void setInt_venta(int int_venta) {
        this.int_venta = int_venta;
    }

}
