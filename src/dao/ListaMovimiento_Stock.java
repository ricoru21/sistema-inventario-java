/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class ListaMovimiento_Stock {

    //private Timestamp dat_fecharegistro;
    private String dat_fecharegistro;
    private int int_producto;
    private String producto;
    private String tipo;
    private double dec_cantidad;
    private int int_usuario;
    private String personal;

    public String getDat_fecharegistro() {
        return dat_fecharegistro;
    }

    public void setDat_fecharegistro(String dat_fecharegistro) {
        this.dat_fecharegistro = dat_fecharegistro;
    }

    public int getInt_producto() {
        return int_producto;
    }

    public void setInt_producto(int int_producto) {
        this.int_producto = int_producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getInt_usuario() {
        return int_usuario;
    }

    public void setInt_usuario(int int_usuario) {
        this.int_usuario = int_usuario;
    }

    public String getPersonal() {
        return personal;
    }

    public void setPersonal(String personal) {
        this.personal = personal;
    }

    public double getDec_cantidad() {
        return dec_cantidad;
    }

    public void setDec_cantidad(double dec_cantidad) {
        this.dec_cantidad = dec_cantidad;
    }

}
