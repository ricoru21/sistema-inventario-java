/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class Orden_Salida {

    private int int_id;
    private String var_comentario;
    private int int_motivo;
    private String var_estado;
    private int int_usuario;
    private Timestamp dat_fecharegistro;
    private int int_venta;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public String getVar_comentario() {
        return var_comentario;
    }

    public void setVar_comentario(String var_comentario) {
        this.var_comentario = var_comentario;
    }

    public int getInt_motivo() {
        return int_motivo;
    }

    public void setInt_motivo(int int_motivo) {
        this.int_motivo = int_motivo;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

    public int getInt_usuario() {
        return int_usuario;
    }

    public void setInt_usuario(int int_usuario) {
        this.int_usuario = int_usuario;
    }

    public Timestamp getDat_fecharegistro() {
        return dat_fecharegistro;
    }

    public void setDat_fecharegistro(Timestamp dat_fecharegistro) {
        this.dat_fecharegistro = dat_fecharegistro;
    }

    public int getInt_venta() {
        return int_venta;
    }

    public void setInt_venta(int int_venta) {
        this.int_venta = int_venta;
    }

}
