/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDProveedor {
    
    public int insertarProveedor(Proveedor p){
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_proveedor (?,?,?,?,?)}");
            cstmt.setString(1, p.getVar_ruc());
            cstmt.setString(2, p.getVar_razonsocial());
            cstmt.setString(3, p.getVar_telefono());
            cstmt.setString(4, p.getVar_direccion());
            cstmt.setInt(5, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            cstmt.executeUpdate();
            cnn.commit();
            valor = cstmt.getInt("codigo");
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println(""+b.getMessage());
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println(""+ex.getMessage());
            }
        }
        return valor;
    }
    
    public int updateProveedor(Proveedor p){
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_proveedor (?,?,?,?,?,?)}");
            cstmt.setInt(1, p.getInt_id());
            cstmt.setString(2, p.getVar_ruc());
            cstmt.setString(3, p.getVar_razonsocial());
            cstmt.setString(4, p.getVar_telefono());
            cstmt.setString(5, p.getVar_direccion());
            cstmt.setString(6, p.getVar_estado());
            valor = cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println(""+b.getMessage());
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println(""+ex.getMessage());
            }
        }
        return valor;
    }
    
    public ArrayList<Proveedor> selectProveedor_All(String palabra,int condicion){
        
        ArrayList<Proveedor> lista = new ArrayList<Proveedor>();
        try {
            Connection cnn = null;
            CallableStatement cstmt = null;
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_lista_proveedor_byCondicion (?,?)}");
            cstmt.setString(1, palabra);
            cstmt.setInt(2, condicion);
            ResultSet rs = cstmt.executeQuery();
            while (rs.next()) {
                Proveedor p = new Proveedor();
                p.setVar_direccion(rs.getString("var_direccion"));
                p.setVar_ruc(rs.getString("var_ruc"));
                p.setVar_razonsocial(rs.getString("var_razonsocial"));
                p.setVar_telefono(rs.getString("var_telefono"));
                p.setInt_id(rs.getInt("int_id"));
                p.setVar_estado(rs.getString("var_estado"));
                lista.add(p);
            }
            cnn.close();
            cstmt.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }
    
    public int activar_restablecer_proveedor(int id, String estado) {
        int valor=1;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("UPDATE `proveedor` SET `var_estado` = '"+ estado +"', WHERE `int_id` ="+id);
            ps.executeUpdate();
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            valor=0;
            System.out.println("" + a);
        }
        return valor;
    }
    
}