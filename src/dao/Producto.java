/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Producto {

    private int int_id;
    private String var_codigoBarra;
    private int int_tipo;
    private int int_categoria;
    private double dec_stockMinimo;
    private double dec_stockMaximo;
    private String var_estado;
    private double dec_preciounitario;
    private double dec_utilidadbrutamonetaria;
    private double dec_utilidadbrutaporcentaje;
    private String var_descripcion;
    private int int_marca;
    private double dec_precioventa;
    private double dec_stock;
    private int int_unidadMedida;

    public String getVar_codigoBarra() {
        return var_codigoBarra;
    }

    public void setVar_codigoBarra(String var_codigoBarra) {
        this.var_codigoBarra = var_codigoBarra;
    }

    public int getInt_tipo() {
        return int_tipo;
    }

    public void setInt_tipo(int int_tipo) {
        this.int_tipo = int_tipo;
    }

    public double getDec_stockMinimo() {
        return dec_stockMinimo;
    }

    public void setDec_stockMinimo(double dec_stockMinimo) {
        this.dec_stockMinimo = dec_stockMinimo;
    }

    public double getDec_stockMaximo() {
        return dec_stockMaximo;
    }

    public void setDec_stockMaximo(double dec_stockMaximo) {
        this.dec_stockMaximo = dec_stockMaximo;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String int_estado) {
        this.var_estado = int_estado;
    }

    public double getDec_preciounitario() {
        return dec_preciounitario;
    }

    public void setDec_preciounitario(double dec_preciounitario) {
        this.dec_preciounitario = dec_preciounitario;
    }

    public double getDec_utilidadbrutamonetaria() {
        return dec_utilidadbrutamonetaria;
    }

    public void setDec_utilidadbrutamonetaria(double dec_utilidadbrutamonetaria) {
        this.dec_utilidadbrutamonetaria = dec_utilidadbrutamonetaria;
    }

    public double getDec_utilidadbrutaporcentaje() {
        return dec_utilidadbrutaporcentaje;
    }

    public void setDec_utilidadbrutaporcentaje(double dec_utilidadbrutaporcentaje) {
        this.dec_utilidadbrutaporcentaje = dec_utilidadbrutaporcentaje;
    }

    public String getVar_descripcion() {
        return var_descripcion;
    }

    public void setVar_descripcion(String var_descripcion) {
        this.var_descripcion = var_descripcion;
    }

    public int getInt_marca() {
        return int_marca;
    }

    public void setInt_marca(int int_marca) {
        this.int_marca = int_marca;
    }

    public double getDec_precioventa() {
        return dec_precioventa;
    }

    public void setDec_precioventa(double dec_precioventa) {
        this.dec_precioventa = dec_precioventa;
    }

    public double getDec_stock() {
        return dec_stock;
    }

    public void setDec_stock(double dec_stock) {
        this.dec_stock = dec_stock;
    }

    public int getInt_categoria() {
        return int_categoria;
    }

    public void setInt_categoria(int int_categoria) {
        this.int_categoria = int_categoria;
    }

    public int getInt_unidadMedida() {
        return int_unidadMedida;
    }

    public void setInt_unidadMedida(int int_unidadMedida) {
        this.int_unidadMedida = int_unidadMedida;
    }

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

}
