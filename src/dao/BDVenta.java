/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDVenta {

    public int insertarVenta(Venta v, ArrayList<Transaccion> lsttra, Documento d) {
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        CallableStatement cstmt2 = null;
        CallableStatement cstmt3 = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_venta (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setInt(1, v.getCliente());
            cstmt.setDouble(2, v.getDec_totalfacturado());
            cstmt.setDouble(3, v.getDec_subtotal());
            cstmt.setDouble(4, v.getDec_descuentoporcentaje());
            cstmt.setDouble(5, v.getDec_descuentomonetario());
            cstmt.setDouble(6, v.getDec_acuenta());
            cstmt.setDouble(7, v.getDec_saldo());
            cstmt.setInt(8, v.getInt_tipoventa());
            cstmt.setInt(9, v.getInt_usuario());
            cstmt.setDouble(10, v.getIgv());
            cstmt.setDouble(11, v.getIgvmonto());
            cstmt.setString(12, v.getVar_estado());
            cstmt.setInt(13, v.getInt_formapago());
            cstmt.setInt(14, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            cstmt.executeUpdate();
            valor = cstmt.getInt("codigo");
            // Recorrer el Array
            for (Transaccion tra : lsttra) {
                tra.setInt_venta(valor);
                cstmt2 = cnn.prepareCall("{call sp_insert_transaccion (?,?,?,?,?)}");
                cstmt2.setDouble(1, tra.getInt_venta());
                cstmt2.setDouble(2, tra.getInt_producto());
                cstmt2.setDouble(3, tra.getDec_preciounitario());
                cstmt2.setDouble(4, tra.getDec_cantidad());
                cstmt2.setDouble(5, tra.getDec_importe());
                cstmt2.executeUpdate();
            }
            d.setInt_numerooperacionInterna(valor);
            cstmt3 = cnn.prepareCall("{call sp_insert_documento (?,?,?,?,?)}");
            cstmt3.setInt(1, d.getInt_tipodocumento());
            cstmt3.setString(2, d.getVar_serie());
            cstmt3.setString(3, d.getVar_numero());
            cstmt3.setInt(4, d.getInt_tipooperacionInterna());
            cstmt3.setInt(5, d.getInt_numerooperacionInterna());
            cstmt3.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cstmt2.close();
                cstmt3.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public String[] get_serienumero_documento(int tipoDocumento) {
        String[] codigo_documento = new String[2];
        try {
            Connection cnn = BD.getConnection();
            CallableStatement cstmt = null;
            cstmt = cnn.prepareCall("{call sp_get_serienumero_documento (?)}");
            cstmt.setInt(1, tipoDocumento);
            ResultSet rs = cstmt.executeQuery();
            if (rs.next()) {
                codigo_documento[0] = rs.getString("serie");
                codigo_documento[1] = rs.getString("numero");
            }
            cnn.close();
            cstmt.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return codigo_documento;
    }

    public ArrayList<ListaVenta> get_lista_venta_all(String condicion) {
        ArrayList<ListaVenta> lista = new ArrayList<ListaVenta>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_venta_all where " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaVenta lv = new ListaVenta();
                lv.setId(rs.getInt("id"));
                //lv.setFecha(rs.getTimestamp("fecha"));
                lv.setFecha(rs.getString("fecha"));
                lv.setSerie(rs.getString("serie"));
                lv.setNumero(rs.getString("numero"));
                lv.setCliente(rs.getString("cliente"));
                lv.setSubtotal(rs.getDouble("subtotal"));
                lv.setDescuento(rs.getDouble("descuento"));
                lv.setIgv(rs.getDouble("igv"));
                lv.setMontoFacturado(rs.getDouble("montoFacturado"));
                lv.setMontoPagado(rs.getDouble("montoPagado"));
                lv.setSaldo(rs.getDouble("saldo"));
                lv.setPersonal(rs.getString("Personal"));
                lv.setId_formapago(rs.getInt("id_formapago"));
                lv.setFormaPago(rs.getString("formapago"));
                lv.setId_tipodocumento(rs.getInt("id_tipodocumento"));
                lv.setDocumento(rs.getString("documento"));
                lv.setSerie(rs.getString("serie"));
                lv.setNumero(rs.getString("numero"));
                lv.setEstado(rs.getString("estado"));
                lv.setTipoPago(rs.getString("tipo_venta"));
                lista.add(lv);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

}
