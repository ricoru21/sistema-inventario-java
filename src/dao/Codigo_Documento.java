/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Codigo_Documento {

    private int int_id;
    private int int_tipodocumento;
    private String var_tipodocumento;
    private String var_serie;
    private String var_numero;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public int getInt_tipodocumento() {
        return int_tipodocumento;
    }

    public void setInt_tipodocumento(int int_tipodocumento) {
        this.int_tipodocumento = int_tipodocumento;
    }

    public String getVar_serie() {
        return var_serie;
    }

    public void setVar_serie(String var_serie) {
        this.var_serie = var_serie;
    }

    public String getVar_numero() {
        return var_numero;
    }

    public void setVar_numero(String var_numero) {
        this.var_numero = var_numero;
    }

    public String getVar_tipodocumento() {
        return var_tipodocumento;
    }

    public void setVar_tipodocumento(String var_tipodocumento) {
        this.var_tipodocumento = var_tipodocumento;
    }

}
