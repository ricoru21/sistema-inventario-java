/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDVenta_Credito {

    public int insertarVenta_Credito(Venta_Credito vc, ArrayList<CronogramaPago> lista_cp) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        CallableStatement cstmt2 = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_venta_credito (?,?,?,?,?,?,?)}");
            cstmt.setInt(1, vc.getInt_nrocuota());
            cstmt.setDouble(2, vc.getDec_montocuota());
            cstmt.setDouble(3, vc.getInt_formapago());
            cstmt.setDouble(4, vc.getInt_frecuenciapago());
            cstmt.setString(5, vc.getVar_estado());
            cstmt.setInt(6, vc.getInt_venta());
            cstmt.setInt(7, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            cstmt.executeUpdate();
            valor = cstmt.getInt("codigo");
            // Recorrer el Array
            for (CronogramaPago cp : lista_cp) {
                cp.setInt_ventacredito(valor);
                cstmt2 = cnn.prepareCall("{call sp_insert_cronogramapago (?,?,?,?,?,?)}");
                cstmt2.setDouble(1, cp.getInt_numerocuota());
                cstmt2.setTimestamp(2, cp.getDat_fechapago());
                cstmt2.setDouble(3, cp.getDec_monto());
                cstmt2.setDouble(4, cp.getDec_acuenta());
                cstmt2.setString(5, cp.getVar_estado());
                cstmt2.setInt(6, cp.getInt_ventacredito());
                cstmt2.executeUpdate();
            }
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cstmt2.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public int updateCronogramaPago(CronogramaPago cp) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_cronogramapago (?,?,?,?,?)}");
            cstmt.setInt(1, cp.getInt_id());
            cstmt.setDouble(2, cp.getDec_monto()); // esta remplazando por el acuenta anterior
            cstmt.setDouble(3, cp.getDec_acuenta());
            cstmt.setString(4, cp.getVar_estado());
            cstmt.setInt(5, cp.getInt_venta());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public Venta_Credito get_ventacredito_byId_Venta(int idventa) {
        Venta_Credito vc = null;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from venta_credito where int_venta=" + idventa);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                vc = new Venta_Credito();
                vc.setInt_id(rs.getInt("int_id"));
                vc.setInt_nrocuota(rs.getInt("int_nrocuota"));
                vc.setDec_montocuota(rs.getDouble("dec_montocuota"));
                vc.setInt_formapago(rs.getInt("int_formapago"));
                vc.setInt_frecuenciapago(rs.getInt("int_frecuenciapago"));
                vc.setVar_estado(rs.getString("var_estado"));
                vc.setInt_venta(rs.getInt("int_venta"));
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return vc;
    }

    public ArrayList<ListaCronogramaPago> get_lista_cronogramapago_byId_Venta(int idventa) {
        ArrayList<ListaCronogramaPago> lista = new ArrayList<ListaCronogramaPago>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_cronogramapago_all where id_ventacredito=" + idventa);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaCronogramaPago lcp = new ListaCronogramaPago();
                lcp.setInt_id(rs.getInt("int_id"));
                lcp.setInt_numerocuota(rs.getInt("int_numerocuota"));
                lcp.setDat_fechapago(rs.getDate("dat_fechapago"));
                lcp.setDat_fechapagorealizado(rs.getDate("dat_fechapagorealizado"));
                lcp.setDec_monto(rs.getDouble("dec_monto"));
                lcp.setDec_acuenta(rs.getDouble("dec_acuenta"));
                lcp.setEstado(rs.getString("estado"));
                lista.add(lcp);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

}
