package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author BADY RICHARD
 */
public abstract class BD {

    public static Connection getConnection() {
        Connection cn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/bd_inventario";
            String user = "root";
            String password = "1234";
            cn = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            cn = null;
            System.out.println("Error no se puede cargar el driver:" + e.getMessage());
        } catch (SQLException e) {
            cn = null;
            System.out.println("Error no se establecer la conexion:" + e.getMessage());
        }
        return cn;
    }

}
