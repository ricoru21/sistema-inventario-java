/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Detalle_OrdenIngreso {

    private int int_id;
    private double dec_preciounitario;
    private double dec_cantidad;
    private double dec_importe;
    private int int_producto;
    private int int_ordeningreso;
    private String var_estado;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public double getDec_preciounitario() {
        return dec_preciounitario;
    }

    public void setDec_preciounitario(double dec_preciounitario) {
        this.dec_preciounitario = dec_preciounitario;
    }

    public double getDec_cantidad() {
        return dec_cantidad;
    }

    public void setDec_cantidad(double dec_cantidad) {
        this.dec_cantidad = dec_cantidad;
    }

    public double getDec_importe() {
        return dec_importe;
    }

    public void setDec_importe(double dec_importe) {
        this.dec_importe = dec_importe;
    }

    public int getInt_producto() {
        return int_producto;
    }

    public void setInt_producto(int int_producto) {
        this.int_producto = int_producto;
    }

    public int getInt_ordeningreso() {
        return int_ordeningreso;
    }

    public void setInt_ordeningreso(int int_ordeningreso) {
        this.int_ordeningreso = int_ordeningreso;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

}
