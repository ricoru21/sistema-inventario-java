/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDUsuario {

    public int insertarUsuario(Usuario u) {
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_usuario (?,?,?,?,?,?)}");
            cstmt.setString(1, u.getVar_dni());
            cstmt.setString(2, u.getVar_nombreapellido());
            cstmt.setString(3, u.getVar_idname());
            cstmt.setString(4, u.getVar_password());
            cstmt.setString(5, u.getVar_telefono());
            cstmt.setInt(6, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            cstmt.executeUpdate();
            cnn.commit();
            valor = cstmt.getInt("codigo");
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public int updateUsuario(Usuario u) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_usuario (?,?,?,?,?,?,?)}");
            cstmt.setInt(1, u.getInt_id());
            cstmt.setString(2, u.getVar_idname());
            cstmt.setString(3, u.getVar_password());
            cstmt.setString(4, u.getVar_nombreapellido());
            cstmt.setString(5, u.getVar_dni());
            cstmt.setString(6, u.getVar_telefono());
            cstmt.setString(7, u.getVar_estado());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public ArrayList<Usuario> selectUsuario_All(String palabra, int condicion) {

        ArrayList<Usuario> lista = new ArrayList<Usuario>();
        try {
            Connection cnn = null;
            CallableStatement cstmt = null;
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_lista_usuario_byCondicion (?,?)}");
            cstmt.setString(1, palabra);
            cstmt.setInt(2, condicion);
            ResultSet rs = cstmt.executeQuery();
            while (rs.next()) {
                Usuario u = new Usuario();
                u.setInt_id(rs.getInt("int_id"));
                u.setVar_dni(rs.getString("var_dni"));
                u.setVar_idname(rs.getString("var_idname"));
                u.setVar_password(rs.getString("var_password"));
                u.setVar_nombreapellido(rs.getString("var_nombreapellido"));
                u.setVar_telefono(rs.getString("var_telefono"));
                u.setVar_estado(rs.getString("var_estado"));
                lista.add(u);
            }
            cnn.close();
            cstmt.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

    public Usuario getUsuario_validatePassword(String clave) {
        Usuario u = null;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select *  from usuario where var_password='" + clave + "' limit 0,1");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                u = new Usuario();
                u.setInt_id(rs.getInt("int_id"));
                u.setVar_dni(rs.getString("var_idname"));
                u.setVar_password(rs.getString("var_password"));
                u.setVar_nombreapellido(rs.getString("var_nombreapellido"));
                u.setVar_dni(rs.getString("var_dni"));
                u.setVar_telefono(rs.getString("var_telefono"));
                u.setVar_estado(rs.getString("var_estado"));
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        }
        return u;
    }

    public int activar_restablecer_usuario(int id, String estado) {
        int valor=1;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("UPDATE `usuario` SET `var_estado` = '"+ estado +"', WHERE `int_id` ="+id);
            //ps.executeQuery();
            ps.executeUpdate();
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            valor=0;
            System.out.println("" + a);
        }
        return valor;
    }

}
