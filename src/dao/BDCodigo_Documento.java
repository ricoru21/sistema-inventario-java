/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDCodigo_Documento {

    public int insertarCodigoDocumento(Codigo_Documento cd) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_codigo_documento (?,?,?)}");
            cstmt.setInt(1, cd.getInt_tipodocumento());
            cstmt.setString(2, cd.getVar_serie());
            cstmt.setString(3, cd.getVar_numero());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public ArrayList<Codigo_Documento> selectCodigoDocumento_all() {
        ArrayList<Codigo_Documento> lista = new ArrayList<Codigo_Documento>();
        Codigo_Documento m = null;
        try {
            PreparedStatement ps;
            try (Connection cnn = BD.getConnection()) {
                ps = null;
                ps = cnn.prepareStatement("select cd.int_id, cd.var_serie as var_serie, \n"
                        + "cd.var_numero as var_numero , c.var_descripcion as documento\n"
                        + "from codigo_documento cd\n"
                        + "inner join Constante c on c.int_valor = cd.int_tipo and c.int_clase=6\n"
                        + "order by 1 desc");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    m = new Codigo_Documento();
                    m.setInt_id(rs.getInt("int_id"));
                    m.setVar_serie(rs.getString("var_serie"));
                    m.setVar_numero(rs.getString("var_numero"));
                    m.setVar_tipodocumento(rs.getString("documento"));
                    lista.add(m);
                }
            }
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        }
        return lista;
    }

}
