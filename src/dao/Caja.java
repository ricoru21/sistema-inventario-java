/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class Caja {

    private int int_id;
    private Timestamp dat_fechaInicio;
    private Timestamp dat_fechaCierre;
    private int int_usuario;
    private double dec_saldoinicial;
    private double dec_ingreso;
    private double dec_egreso;
    private String var_estado;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public Timestamp getDat_fechaInicio() {
        return dat_fechaInicio;
    }

    public void setDat_fechaInicio(Timestamp dat_fechaInicio) {
        this.dat_fechaInicio = dat_fechaInicio;
    }

    public Timestamp getDat_fechaCierre() {
        return dat_fechaCierre;
    }

    public void setDat_fechaCierre(Timestamp dat_fechaCierre) {
        this.dat_fechaCierre = dat_fechaCierre;
    }

    public int getInt_usuario() {
        return int_usuario;
    }

    public void setInt_usuario(int int_usuario) {
        this.int_usuario = int_usuario;
    }

    public double getDec_saldoinicial() {
        return dec_saldoinicial;
    }

    public void setDec_saldoinicial(double dec_saldoinicial) {
        this.dec_saldoinicial = dec_saldoinicial;
    }

    public double getDec_ingreso() {
        return dec_ingreso;
    }

    public void setDec_ingreso(double dec_ingreso) {
        this.dec_ingreso = dec_ingreso;
    }

    public double getDec_egreso() {
        return dec_egreso;
    }

    public void setDec_egreso(double dec_egreso) {
        this.dec_egreso = dec_egreso;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

}
