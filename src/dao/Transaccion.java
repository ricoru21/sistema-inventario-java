/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Transaccion {

    private int int_id;
    private int int_venta;
    private int int_producto;
    private double dec_preciounitario;
    private double dec_cantidad;
    private double dec_importe;

    public int getInt_venta() {
        return int_venta;
    }

    public void setInt_venta(int int_venta) {
        this.int_venta = int_venta;
    }

    public int getInt_producto() {
        return int_producto;
    }

    public void setInt_producto(int int_producto) {
        this.int_producto = int_producto;
    }

    public double getDec_preciounitario() {
        return dec_preciounitario;
    }

    public void setDec_preciounitario(double dec_preciounitario) {
        this.dec_preciounitario = dec_preciounitario;
    }

    public double getDec_cantidad() {
        return dec_cantidad;
    }

    public void setDec_cantidad(double dec_cantidad) {
        this.dec_cantidad = dec_cantidad;
    }

    public double getDec_importe() {
        return dec_importe;
    }

    public void setDec_importe(double dec_importe) {
        this.dec_importe = dec_importe;
    }

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

}