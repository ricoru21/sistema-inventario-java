/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class Venta_Anulada {

    private int int_id;
    private int int_venta;
    private Timestamp dat_fecharegistro;
    private String var_motivo;
    private int int_usuario;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public Timestamp getDat_fecharegistro() {
        return dat_fecharegistro;
    }

    public void setDat_fecharegistro(Timestamp dat_fecharegistro) {
        this.dat_fecharegistro = dat_fecharegistro;
    }

    public String getVar_motivo() {
        return var_motivo;
    }

    public void setVar_motivo(String var_motivo) {
        this.var_motivo = var_motivo;
    }

    public int getInt_usuario() {
        return int_usuario;
    }

    public void setInt_usuario(int int_usuario) {
        this.int_usuario = int_usuario;
    }

    public int getInt_venta() {
        return int_venta;
    }

    public void setInt_venta(int int_venta) {
        this.int_venta = int_venta;
    }

}
