/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDCategoria {

    public int insertarCategoria(Categoria c) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_categoria (?,?)}");
            cstmt.setString(1, c.getVar_descripcion() );
            cstmt.setString(2, c.getVar_estado());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally{
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
    public int updateCategoria(Categoria c) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_categoria (?,?,?)}");
            cstmt.setInt(1, c.getInt_id() );
            cstmt.setString(2, c.getVar_descripcion() );
            cstmt.setString(3, c.getVar_estado());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally{
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
    public ArrayList<Categoria> selectCategoria_all() {
        ArrayList<Categoria> lista = new ArrayList<Categoria>();
        Categoria c = null;
        try {
            PreparedStatement ps;
            try (Connection cnn = BD.getConnection()) {
                ps = null;
                ps = cnn.prepareStatement("select * from categoria");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    c = new Categoria();
                    c.setInt_id(rs.getInt("int_id"));
                    c.setVar_descripcion(rs.getString("var_descripcion"));
                    c.setVar_estado(rs.getString("var_estado"));
                    lista.add(c);
                }
            }
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        }
        return lista;
    }

    public ArrayList<Categoria> get_lista_categoria_all(String condicion) {
        ArrayList<Categoria> lista = new ArrayList<Categoria>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from categoria where " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Categoria c = new Categoria();
                c.setInt_id(rs.getInt("int_id"));
                c.setVar_descripcion(rs.getString("var_descripcion"));
                c.setVar_estado(rs.getString("var_estado"));
                lista.add(c);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }
    
    public Categoria get_categoria_byId(int id) {
        Categoria c = null;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from categoria where int_id =" + id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                c = new Categoria();
                c.setInt_id(rs.getInt("int_id"));
                c.setVar_descripcion(rs.getString("var_descripcion"));
                c.setVar_estado(rs.getString("var_estado"));
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return c;
    }

}
