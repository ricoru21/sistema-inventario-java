/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class Venta {

    private int int_id;
    private Timestamp dat_fecharegistro;
    private int cliente;
    private double dec_totalfacturado;
    private double dec_subtotal;
    private double dec_descuentomonetario;
    private double dec_descuentoporcentaje;
    private double dec_acuenta;
    private double dec_saldo;
    private String var_estado;
    private int int_tipoventa;
    private double igv;
    private double igvmonto;
    private int int_usuario;
    private int int_formapago;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public Timestamp getDat_fecharegistro() {
        return dat_fecharegistro;
    }

    public void setDat_fecharegistro(Timestamp dat_fecharegistro) {
        this.dat_fecharegistro = dat_fecharegistro;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public double getDec_totalfacturado() {
        return dec_totalfacturado;
    }

    public void setDec_totalfacturado(double dec_totalfacturado) {
        this.dec_totalfacturado = dec_totalfacturado;
    }

    public double getDec_subtotal() {
        return dec_subtotal;
    }

    public void setDec_subtotal(double dec_subtotal) {
        this.dec_subtotal = dec_subtotal;
    }

    public double getDec_descuentomonetario() {
        return dec_descuentomonetario;
    }

    public void setDec_descuentomonetario(double dec_descuentomonetario) {
        this.dec_descuentomonetario = dec_descuentomonetario;
    }

    public double getDec_descuentoporcentaje() {
        return dec_descuentoporcentaje;
    }

    public void setDec_descuentoporcentaje(double dec_descuentoporcentaje) {
        this.dec_descuentoporcentaje = dec_descuentoporcentaje;
    }

    public double getDec_acuenta() {
        return dec_acuenta;
    }

    public void setDec_acuenta(double dec_acuenta) {
        this.dec_acuenta = dec_acuenta;
    }

    public double getDec_saldo() {
        return dec_saldo;
    }

    public void setDec_saldo(double dec_saldo) {
        this.dec_saldo = dec_saldo;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

    public int getInt_tipoventa() {
        return int_tipoventa;
    }

    public void setInt_tipoventa(int int_tipoventa) {
        this.int_tipoventa = int_tipoventa;
    }

    public int getInt_usuario() {
        return int_usuario;
    }

    public void setInt_usuario(int int_usuario) {
        this.int_usuario = int_usuario;
    }

    public double getIgv() {
        return igv;
    }

    public void setIgv(double igv) {
        this.igv = igv;
    }

    public double getIgvmonto() {
        return igvmonto;
    }

    public void setIgvmonto(double igvmonto) {
        this.igvmonto = igvmonto;
    }

    public int getInt_formapago() {
        return int_formapago;
    }

    public void setInt_formapago(int int_formapago) {
        this.int_formapago = int_formapago;
    }

}