/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Proveedor {

    private int int_id;
    private String var_ruc;
    private String var_razonsocial;
    private String var_telefono;
    private String var_direccion;
    private String var_estado;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public String getVar_ruc() {
        return var_ruc;
    }

    public void setVar_ruc(String var_ruc) {
        this.var_ruc = var_ruc;
    }

    public String getVar_razonsocial() {
        return var_razonsocial;
    }

    public void setVar_razonsocial(String var_razonsocial) {
        this.var_razonsocial = var_razonsocial;
    }

    public String getVar_telefono() {
        return var_telefono;
    }

    public void setVar_telefono(String var_telefono) {
        this.var_telefono = var_telefono;
    }

    public String getVar_direccion() {
        return var_direccion;
    }

    public void setVar_direccion(String var_direccion) {
        this.var_direccion = var_direccion;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }
    
    public String toString(){
        return this.var_razonsocial;
    }

}
