/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Detalle_OrdenSalida {

    private int int_id;
    private double dec_cantidad;
    private int int_producto;
    private int int_ordensalida;
    private String var_estado;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public double getDec_cantidad() {
        return dec_cantidad;
    }

    public void setDec_cantidad(double dec_cantidad) {
        this.dec_cantidad = dec_cantidad;
    }

    public int getInt_producto() {
        return int_producto;
    }

    public void setInt_producto(int int_producto) {
        this.int_producto = int_producto;
    }

    public int getInt_ordensalida() {
        return int_ordensalida;
    }

    public void setInt_ordensalida(int int_ordensalida) {
        this.int_ordensalida = int_ordensalida;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

}
