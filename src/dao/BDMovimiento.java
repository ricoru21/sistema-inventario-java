/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDMovimiento {

    public int insertarMovimiento(Movimiento m) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_movimiento (?,?,?,?,?,?,?,?)}");
            cstmt.setDouble(1, m.getDec_monto());
            cstmt.setInt(2, m.getInt_venta());
            cstmt.setInt(3, m.getInt_tipooperacion());
            cstmt.setString(4, m.getVar_motivo());
            cstmt.setInt(5, m.getInt_tipodocumento());
            cstmt.setString(6, m.getVar_documento());
            cstmt.setInt(7, m.getInt_tipopago());
            cstmt.setInt(8, m.getInt_usuario());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    //Movimiento Stock
    public ArrayList<ListaMovimiento_Stock> get_lista_movimiento_stock_all(String condicion) {
        ArrayList<ListaMovimiento_Stock> lista = new ArrayList<ListaMovimiento_Stock>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lsita_movimiento_stock_all where " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaMovimiento_Stock lms = new ListaMovimiento_Stock();
                //lms.setDat_fecharegistro(rs.getTimestamp("dat_fecharegistro"));
                lms.setDat_fecharegistro(rs.getString("dat_fecharegistro"));
                lms.setInt_producto(rs.getInt("int_producto"));
                lms.setProducto(rs.getString("producto"));
                lms.setTipo(rs.getString("tipo"));
                lms.setDec_cantidad(rs.getDouble("dec_cantidad"));
                lms.setInt_usuario(rs.getInt("int_usuario"));
                lms.setPersonal(rs.getString("personal"));
                lista.add(lms);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }
}
