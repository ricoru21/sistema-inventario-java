/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Movimiento {

    private double dec_monto;
    private int int_venta;
    private int int_tipooperacion;
    private String var_motivo;
    private int int_tipodocumento;
    private String var_documento;
    private int int_tipopago;
    private int int_usuario;

    public double getDec_monto() {
        return dec_monto;
    }

    public void setDec_monto(double dec_monto) {
        this.dec_monto = dec_monto;
    }

    public int getInt_venta() {
        return int_venta;
    }

    public void setInt_venta(int int_venta) {
        this.int_venta = int_venta;
    }

    public int getInt_tipooperacion() {
        return int_tipooperacion;
    }

    public void setInt_tipooperacion(int int_tipooperacion) {
        this.int_tipooperacion = int_tipooperacion;
    }

    public String getVar_motivo() {
        return var_motivo;
    }

    public void setVar_motivo(String var_motivo) {
        this.var_motivo = var_motivo;
    }

    public int getInt_tipodocumento() {
        return int_tipodocumento;
    }

    public void setInt_tipodocumento(int int_tipodocumento) {
        this.int_tipodocumento = int_tipodocumento;
    }

    public String getVar_documento() {
        return var_documento;
    }

    public void setVar_documento(String var_documento) {
        this.var_documento = var_documento;
    }

    public int getInt_tipopago() {
        return int_tipopago;
    }

    public void setInt_tipopago(int int_tipopago) {
        this.int_tipopago = int_tipopago;
    }

    public int getInt_usuario() {
        return int_usuario;
    }

    public void setInt_usuario(int int_usuario) {
        this.int_usuario = int_usuario;
    }

}
