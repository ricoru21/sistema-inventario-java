/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class Orden_Ingreso {

    private int int_id;
    private String var_documentoserie;
    private String var_documentonumero;
    private Timestamp dat_fecharegistro;
    private String var_comentario;
    private int int_motivo;
    private int int_proveedor;
    private String var_estado;
    private int int_usuario;
    private int int_tipodocumento;
    private double monto_total;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public String getVar_documentoserie() {
        return var_documentoserie;
    }

    public void setVar_documentoserie(String var_documentoserie) {
        this.var_documentoserie = var_documentoserie;
    }

    public String getVar_documentonumero() {
        return var_documentonumero;
    }

    public void setVar_documentonumero(String var_documentonumero) {
        this.var_documentonumero = var_documentonumero;
    }

    public Timestamp getDat_fecharegistro() {
        return dat_fecharegistro;
    }

    public void setDat_fecharegistro(Timestamp dat_fecharegistro) {
        this.dat_fecharegistro = dat_fecharegistro;
    }

    public String getVar_comentario() {
        return var_comentario;
    }

    public void setVar_comentario(String var_comentario) {
        this.var_comentario = var_comentario;
    }

    public int getInt_motivo() {
        return int_motivo;
    }

    public void setInt_motivo(int int_motivo) {
        this.int_motivo = int_motivo;
    }

    public int getInt_proveedor() {
        return int_proveedor;
    }

    public void setInt_proveedor(int int_proveedor) {
        this.int_proveedor = int_proveedor;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

    public int getInt_usuario() {
        return int_usuario;
    }

    public void setInt_usuario(int int_usuario) {
        this.int_usuario = int_usuario;
    }

    public int getInt_tipodocumento() {
        return int_tipodocumento;
    }

    public void setInt_tipodocumento(int int_tipodocumento) {
        this.int_tipodocumento = int_tipodocumento;
    }

    public double getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(double monto_total) {
        this.monto_total = monto_total;
    }

}
