/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Cliente {

    private int int_id;
    private String var_nombreM;
    private String var_dni;
    private String var_ruc;
    private String var_apepaterno;
    private String var_apematerno;
    private String var_direccion;
    private String var_telefono;
    private String var_busqueda;
    private String var_estado;

    public String getVar_nombreM() {
        return var_nombreM;
    }

    public void setVar_nombreM(String var_nombreM) {
        this.var_nombreM = var_nombreM;
    }

    public String getVar_dni() {
        return var_dni;
    }

    public void setVar_dni(String var_dni) {
        this.var_dni = var_dni;
    }

    public String getVar_apepaterno() {
        return var_apepaterno;
    }

    public void setVar_apepaterno(String var_apepaterno) {
        this.var_apepaterno = var_apepaterno;
    }

    public String getVar_apematerno() {
        return var_apematerno;
    }

    public void setVar_apematerno(String var_apematerno) {
        this.var_apematerno = var_apematerno;
    }

    public String getVar_direccion() {
        return var_direccion;
    }

    public void setVar_direccion(String var_direccion) {
        this.var_direccion = var_direccion;
    }

    public String getVar_telefono() {
        return var_telefono;
    }

    public void setVar_telefono(String var_telefono) {
        this.var_telefono = var_telefono;
    }

    public String getVar_busqueda() {
        return var_busqueda;
    }

    public void setVar_busqueda(String var_busqueda) {
        this.var_busqueda = var_busqueda;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

    public String getVar_ruc() {
        return var_ruc;
    }

    public void setVar_ruc(String var_ruc) {
        this.var_ruc = var_ruc;
    }

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public String toString() {
        return this.var_busqueda;
    }
}
