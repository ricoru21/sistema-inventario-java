/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Richard
 */
public class Usuario {

    private int int_id;
    private String var_idname;
    private String var_password;
    private String var_nombreapellido;
    private String var_dni;
    private String var_telefono;
    private String var_estado;

    public String getVar_idname() {
        return var_idname;
    }

    public void setVar_idname(String var_idname) {
        this.var_idname = var_idname;
    }

    public String getVar_password() {
        return var_password;
    }

    public void setVar_password(String var_password) {
        this.var_password = var_password;
    }

    public String getVar_nombreapellido() {
        return var_nombreapellido;
    }

    public void setVar_nombreapellido(String var_nombreapellido) {
        this.var_nombreapellido = var_nombreapellido;
    }

    public String getVar_dni() {
        return var_dni;
    }

    public void setVar_dni(String var_dni) {
        this.var_dni = var_dni;
    }

    public String getVar_telefono() {
        return var_telefono;
    }

    public void setVar_telefono(String var_telefono) {
        this.var_telefono = var_telefono;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }
    
    public String toString(){
        return this.getVar_nombreapellido();
    }

}