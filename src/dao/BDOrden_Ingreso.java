/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDOrden_Ingreso {

    public int insertarOrdenIngreso(Orden_Ingreso oi, ArrayList<Detalle_OrdenIngreso> lstdoi) {
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        CallableStatement cstmt2 = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_ordeningreso (?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, oi.getVar_documentoserie());
            cstmt.setString(2, oi.getVar_documentonumero());
            cstmt.setString(3, oi.getVar_comentario());
            cstmt.setInt(4, oi.getInt_motivo());
            cstmt.setInt(5, oi.getInt_proveedor());
            cstmt.setInt(6, oi.getInt_usuario());
            cstmt.setInt(7, oi.getInt_tipodocumento());
            cstmt.setTimestamp(8, oi.getDat_fecharegistro());
            cstmt.setDouble(9, oi.getMonto_total());
            cstmt.setInt(10, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            //cstmt.execute();
            cstmt.executeUpdate();
            valor = cstmt.getInt("codigo");
            // Recorrer el Array
            for (Detalle_OrdenIngreso doi : lstdoi) {
                doi.setInt_ordeningreso(valor);
                cstmt2 = cnn.prepareCall("{call sp_insert_detalle_ordeningreso (?,?,?,?,?)}");
                cstmt2.setDouble(1, doi.getDec_preciounitario());
                cstmt2.setDouble(2, doi.getDec_cantidad());
                cstmt2.setDouble(3, doi.getDec_importe());
                cstmt2.setInt(4, doi.getInt_producto());
                cstmt2.setInt(5, doi.getInt_ordeningreso());
                cstmt2.executeUpdate();
            }       
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cstmt2.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public ArrayList<ListaOrdenIngreso> get_lista_ordeningreso_all(String condicion) {
        ArrayList<ListaOrdenIngreso> lista = new ArrayList<ListaOrdenIngreso>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_ordeningreso_all where " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaOrdenIngreso loi = new ListaOrdenIngreso();
                loi.setId(rs.getInt("id"));
                loi.setSerie(rs.getString("serie"));
                loi.setNumero(rs.getString("numero"));
                loi.setFecha(rs.getTimestamp("fecha"));
                loi.setComentario(rs.getString("comentario"));
                loi.setId_proveedor(rs.getInt("id_proveedor"));
                loi.setProveedor(rs.getString("proveedor"));
                loi.setId_motivo(rs.getInt("id_motivo"));
                loi.setMotivo(rs.getString("motivo"));
                loi.setId_documento(rs.getInt("id_documento"));
                loi.setDocumento(rs.getString("Documento"));
                loi.setPersonal(rs.getString("Personal"));
                loi.setEstado(rs.getString("Estado"));
                lista.add(loi);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

    public int anular_orden_ingreso(int id, String estado) {
        int valor=1;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("UPDATE `orden_ingreso` SET `var_estado` = '"+ estado +"' WHERE `int_id` ="+id);
            //ps.executeQuery();
            ps.executeUpdate();
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            valor=0;
            System.out.println("" + a);
        }
        return valor;
    }
    
}
