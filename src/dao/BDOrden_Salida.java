/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDOrden_Salida {

    public int insertarOrdenSalida(Orden_Salida os, ArrayList<Detalle_OrdenSalida> lstdos) {
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        CallableStatement cstmt2 = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_ordensalida (?,?,?,?,?,?,?)}");
            cstmt.setString(1, os.getVar_comentario());
            cstmt.setTimestamp(2, os.getDat_fecharegistro());
            cstmt.setInt(3, os.getInt_motivo());
            cstmt.setString(4, os.getVar_estado());
            cstmt.setInt(5, os.getInt_usuario());
            cstmt.setInt(6, os.getInt_venta());
            cstmt.setInt(7, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            cstmt.executeUpdate();
            valor = cstmt.getInt("codigo");
            // Recorrer el Array
            for (Detalle_OrdenSalida dos : lstdos) {
                dos.setInt_ordensalida(valor);
                cstmt2 = cnn.prepareCall("{call sp_insert_detalle_ordensalida (?,?,?,?)}");
                cstmt2.setDouble(1, dos.getDec_cantidad());
                cstmt2.setInt(2, dos.getInt_producto());
                cstmt2.setInt(3, dos.getInt_ordensalida());
                cstmt2.setString(4, dos.getVar_estado());
                cstmt2.executeUpdate();
            }
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            }finally{
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cstmt2.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public ArrayList<ListaOrdenSalida> get_lista_ordensalida_all(String condicion) {
        ArrayList<ListaOrdenSalida> lista = new ArrayList<ListaOrdenSalida>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_ordensalida_all where " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaOrdenSalida los = new ListaOrdenSalida();
                los.setId(rs.getInt("id"));
                los.setFecha(rs.getTimestamp("fecha"));
                los.setComentario(rs.getString("comentario"));
                los.setCliente(rs.getString("Cliente"));
                los.setId_personal(rs.getInt("id_personal"));
                los.setPersonal(rs.getString("personal"));
                los.setId_motivo(rs.getInt("id_motivo"));
                los.setMotivo(rs.getString("motivo"));
                los.setCantidad(rs.getDouble("cantidad"));
                los.setEstado(rs.getString("estado"));
                lista.add(los);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }
    
    public int anular_orden_salida(int id, String estado) {
        int valor=1;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("UPDATE `orden_ingreso` SET `var_estado` = '"+ estado +"' WHERE `int_id` ="+id);
            //ps.executeQuery();
            ps.executeUpdate();
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            valor=0;
            System.out.println("" + a);
        }
        return valor;
    }

}
