/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDVenta_Anulada {

    public int insertarVenta_Anulada(Venta_Anulada va) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_venta_anulado (?,?,?)}");
            cstmt.setInt(1, va.getInt_venta());
            cstmt.setString(2, va.getVar_motivo());
            cstmt.setInt(3, va.getInt_usuario());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

}
