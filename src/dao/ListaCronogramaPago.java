/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Date;

/**
 *
 * @author Richard
 */
public class ListaCronogramaPago {

    private int int_id;
    private int int_numerocuota;
    private Date dat_fechapago;
    private Date dat_fechapagorealizado;
    private double dec_monto;
    private double dec_acuenta;
    private String estado;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public int getInt_numerocuota() {
        return int_numerocuota;
    }

    public void setInt_numerocuota(int int_numerocuota) {
        this.int_numerocuota = int_numerocuota;
    }

    public Date getDat_fechapago() {
        return dat_fechapago;
    }

    public void setDat_fechapago(Date dat_fechapago) {
        this.dat_fechapago = dat_fechapago;
    }

    public Date getDat_fechapagorealizado() {
        return dat_fechapagorealizado;
    }

    public void setDat_fechapagorealizado(Date dat_fechapagorealizado) {
        this.dat_fechapagorealizado = dat_fechapagorealizado;
    }

    public double getDec_monto() {
        return dec_monto;
    }

    public void setDec_monto(double dec_monto) {
        this.dec_monto = dec_monto;
    }

    public double getDec_acuenta() {
        return dec_acuenta;
    }

    public void setDec_acuenta(double dec_acuenta) {
        this.dec_acuenta = dec_acuenta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
