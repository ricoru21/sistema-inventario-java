/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class Documento {

    private int int_id;
    private Timestamp dat_fechaRegistrom;
    private int int_tipodocumento;
    private String var_serie;
    private String var_numero;
    private int int_tipooperacionInterna;
    private int int_numerooperacionInterna;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public Timestamp getDat_fechaRegistrom() {
        return dat_fechaRegistrom;
    }

    public void setDat_fechaRegistrom(Timestamp dat_fechaRegistrom) {
        this.dat_fechaRegistrom = dat_fechaRegistrom;
    }

    public int getInt_tipodocumento() {
        return int_tipodocumento;
    }

    public void setInt_tipodocumento(int int_tipodocumento) {
        this.int_tipodocumento = int_tipodocumento;
    }

    public String getVar_serie() {
        return var_serie;
    }

    public void setVar_serie(String var_serie) {
        this.var_serie = var_serie;
    }

    public String getVar_numero() {
        return var_numero;
    }

    public void setVar_numero(String var_numero) {
        this.var_numero = var_numero;
    }

    public int getInt_tipooperacionInterna() {
        return int_tipooperacionInterna;
    }

    public void setInt_tipooperacionInterna(int int_tipooperacionInterna) {
        this.int_tipooperacionInterna = int_tipooperacionInterna;
    }

    public int getInt_numerooperacionInterna() {
        return int_numerooperacionInterna;
    }

    public void setInt_numerooperacionInterna(int int_numerooperacionInterna) {
        this.int_numerooperacionInterna = int_numerooperacionInterna;
    }

}
