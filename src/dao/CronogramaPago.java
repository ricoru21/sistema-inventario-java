/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Timestamp;

/**
 *
 * @author Richard
 */
public class CronogramaPago {

    private int int_id;
    private int int_numerocuota;
    private Timestamp dat_fecharegistro;
    private Timestamp dat_fechapago;
    private Timestamp dat_fechapagorealizado;
    private double dec_monto;
    private double dec_acuenta;
    private String var_estado;
    private int int_ventacredito;
    private int int_venta;

    public int getInt_id() {
        return int_id;
    }

    public void setInt_id(int int_id) {
        this.int_id = int_id;
    }

    public int getInt_numerocuota() {
        return int_numerocuota;
    }

    public void setInt_numerocuota(int int_numerocuota) {
        this.int_numerocuota = int_numerocuota;
    }

    public Timestamp getDat_fecharegistro() {
        return dat_fecharegistro;
    }

    public void setDat_fecharegistro(Timestamp dat_fecharegistro) {
        this.dat_fecharegistro = dat_fecharegistro;
    }

    public Timestamp getDat_fechapago() {
        return dat_fechapago;
    }

    public void setDat_fechapago(Timestamp dat_fechapago) {
        this.dat_fechapago = dat_fechapago;
    }

    public Timestamp getDat_fechapagorealizado() {
        return dat_fechapagorealizado;
    }

    public void setDat_fechapagorealizado(Timestamp dat_fechapagorealizado) {
        this.dat_fechapagorealizado = dat_fechapagorealizado;
    }

    public double getDec_monto() {
        return dec_monto;
    }

    public void setDec_monto(double dec_monto) {
        this.dec_monto = dec_monto;
    }

    public double getDec_acuenta() {
        return dec_acuenta;
    }

    public void setDec_acuenta(double dec_acuenta) {
        this.dec_acuenta = dec_acuenta;
    }

    public String getVar_estado() {
        return var_estado;
    }

    public void setVar_estado(String var_estado) {
        this.var_estado = var_estado;
    }

    public int getInt_ventacredito() {
        return int_ventacredito;
    }

    public void setInt_ventacredito(int int_ventacredito) {
        this.int_ventacredito = int_ventacredito;
    }

    public int getInt_venta() {
        return int_venta;
    }

    public void setInt_venta(int int_venta) {
        this.int_venta = int_venta;
    }

}
