/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDCliente {

    public int insertarCliente(Cliente c) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_cliente (?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, c.getVar_nombreM());
            cstmt.setString(2, c.getVar_dni());
            cstmt.setString(3, c.getVar_apepaterno());
            cstmt.setString(4, c.getVar_apematerno());
            cstmt.setString(5, c.getVar_direccion());
            cstmt.setString(6, c.getVar_telefono());
            cstmt.setString(7, c.getVar_busqueda());
            cstmt.setString(8, c.getVar_ruc());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    //
    public int updateCliente(Cliente c) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_cliente (?,?,?,?,?,?,?,?,?)}");
            cstmt.setInt(1, c.getInt_id());
            cstmt.setString(2, c.getVar_nombreM());
            cstmt.setString(3, c.getVar_dni());
            cstmt.setString(4, c.getVar_apepaterno());
            cstmt.setString(5, c.getVar_apematerno());
            cstmt.setString(6, c.getVar_direccion());
            cstmt.setString(7, c.getVar_telefono());
            cstmt.setString(8, c.getVar_busqueda());
            cstmt.setString(9, c.getVar_ruc());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
    public ArrayList<ListaCliente> get_lista_cliente_all(String condicion) {
        ArrayList<ListaCliente> lista = new ArrayList<ListaCliente>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_cliente_all where " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaCliente lc = new ListaCliente();
                lc.setId(rs.getInt("id"));
                lc.setCliente(rs.getString("cliente"));
                lc.setDireccion(rs.getString("direccion"));
                lc.setDni(rs.getString("dni"));
                lc.setRuc(rs.getString("ruc"));
                lc.setTelefono(rs.getString("telefono"));
                lc.setEstado(rs.getString("estado"));
                lc.setFechaRegistro(rs.getTimestamp("fecharegistro"));
                lista.add(lc);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

    public ArrayList<ListaCliente> get_lista_cliente_all_activos(String condicion) {
        ArrayList<ListaCliente> lista = new ArrayList<ListaCliente>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_cliente_all where estado='1' and " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaCliente lc = new ListaCliente();
                lc.setId(rs.getInt("id"));
                lc.setCliente(rs.getString("cliente"));
                lc.setDireccion(rs.getString("direccion"));
                lc.setDni(rs.getString("dni"));
                lc.setRuc(rs.getString("ruc"));
                lc.setTelefono(rs.getString("telefono"));
                lc.setEstado(rs.getString("estado"));
                lc.setFechaRegistro(rs.getTimestamp("fecharegistro"));
                lista.add(lc);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }
    
    public Cliente get_cliente_byId(int id) {
        Cliente c = null;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from cliente where int_id =" + id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                c = new Cliente();
                c.setVar_nombreM(rs.getString("var_nombre"));
                c.setVar_apematerno(rs.getString("var_apematerno"));
                c.setVar_apepaterno(rs.getString("var_apepaterno"));
                c.setVar_busqueda(rs.getString("var_busqueda"));
                c.setVar_direccion(rs.getString("var_direccion"));
                c.setVar_dni(rs.getString("var_dni"));
                c.setVar_ruc(rs.getString("var_ruc"));
                c.setVar_telefono(rs.getString("var_telefono"));
                c.setVar_estado(rs.getString("var_estado"));
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return c;
    }

    public int activar_restablecer_cliente(int id, String estado) {
        int valor=1;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("UPDATE `cliente` SET `var_estado` = '"+ estado +"' WHERE `int_id` ="+id);
            ps.executeUpdate();
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            valor=0;
            System.out.println("" + a);
        }
        return valor;
    }
    
}
