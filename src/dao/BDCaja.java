/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Richard
 */
public class BDCaja {

    public int insertarCaja(Caja c) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_caja (?,?)}");
            cstmt.setInt(1, c.getInt_usuario());
            cstmt.setDouble(2, c.getDec_saldoinicial());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
        
    public Caja verificar_inicio_caja() {
        Caja c = null;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from caja c where DATE(c.dat_fechaInicio) = curdate()");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                c = new Caja();
                c.setInt_id(rs.getInt("int_id"));
                c.setDat_fechaInicio(new java.sql.Timestamp(rs.getDate("dat_fechaInicio").getTime()));
                c.setDec_saldoinicial(rs.getDouble("dec_saldoinicial"));
                c.setDec_ingreso(rs.getDouble("dec_ingreso"));
                c.setDec_egreso(rs.getDouble("dec_egreso"));
                c.setVar_estado(rs.getString("var_estado"));
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            c = null;
            System.out.println("" + a);
        }
        return c;
    }
    
    public int realizar_cierre_caja(Caja c){
         int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_cierre_caja (?)}");
            cstmt.setInt(1, c.getInt_id() );
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }
    
}
