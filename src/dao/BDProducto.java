/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BDProducto {

    public int insertarProducto(Producto p) {
        int valor = 0;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_insert_producto (?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, p.getVar_codigoBarra());
            cstmt.setInt(2, p.getInt_tipo());
            cstmt.setInt(3, p.getInt_categoria());
            cstmt.setDouble(4, p.getDec_stockMinimo());
            cstmt.setDouble(5, p.getDec_stockMaximo());
            cstmt.setDouble(6, p.getDec_preciounitario());
            cstmt.setDouble(7, p.getDec_utilidadbrutamonetaria());
            cstmt.setDouble(8, p.getDec_utilidadbrutaporcentaje());
            cstmt.setString(9, p.getVar_descripcion());
            cstmt.setInt(10, p.getInt_marca());
            cstmt.setDouble(11, p.getDec_precioventa());
            cstmt.setDouble(12, p.getDec_stock());
            cstmt.setInt(13, 0);
            cstmt.registerOutParameter("codigo", Types.INTEGER);
            cstmt.executeUpdate();
            cnn.commit();
            valor = cstmt.getInt("codigo");
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public int updateProducto(Producto p) {
        int valor = 1;
        Connection cnn = null;
        CallableStatement cstmt = null;
        try {
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_update_producto (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setInt(1, p.getInt_id());
            cstmt.setString(2, p.getVar_codigoBarra());
            cstmt.setInt(3, p.getInt_tipo());
            cstmt.setInt(4, p.getInt_categoria());
            cstmt.setDouble(5, p.getDec_stockMinimo());
            cstmt.setDouble(6, p.getDec_stockMaximo());
            cstmt.setString(7, p.getVar_estado());
            cstmt.setDouble(8, p.getDec_preciounitario());
            cstmt.setDouble(9, p.getDec_utilidadbrutamonetaria());
            cstmt.setDouble(10, p.getDec_utilidadbrutaporcentaje());
            cstmt.setString(11, p.getVar_descripcion());
            cstmt.setInt(12, p.getInt_marca());
            cstmt.setDouble(13, p.getDec_precioventa());
            cstmt.setInt(14, p.getInt_unidadMedida());
            cstmt.executeUpdate();
            cnn.commit();
        } catch (SQLException a) {
            try {
                cnn.rollback();
            } catch (SQLException b) {
                System.out.println("" + b.getMessage());
            } finally {
                valor = 0;
            }
            System.out.println("" + a);
        } finally {
            try {
                cstmt.close();
                cnn.close();
            } catch (SQLException ex) {
                System.out.println("" + ex.getMessage());
            }
        }
        return valor;
    }

    public int activar_restablecer_producto(int id, String estado) {
        int valor = 1;
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("UPDATE `producto` SET `int_estado` = '" + estado + "' WHERE `int_id` =" + id);
            ps.executeUpdate();
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            valor = 0;
            System.out.println("" + a);
        }
        return valor;
    }

    public ArrayList<Producto> selectProducto_All(String palabra, int condicion) {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        try {
            Connection cnn = null;
            CallableStatement cstmt = null;
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_lista_producto_byCondicion (?,?)}");
            cstmt.setString(1, palabra);
            cstmt.setInt(2, condicion);
            ResultSet rs = cstmt.executeQuery();
            while (rs.next()) {
                Producto p = new Producto();
                p.setVar_codigoBarra(rs.getString("var_codigoBarra"));
                p.setInt_tipo(rs.getInt("int_tipo"));
                p.setDec_stockMinimo(rs.getDouble("dec_stockMinimo"));
                p.setDec_stockMaximo(rs.getDouble("dec_stockMaximo"));
                p.setDec_precioventa(rs.getDouble("dec_precioventa"));
                p.setDec_preciounitario(rs.getDouble("dec_preciounitario"));
                p.setDec_stock(0.0);
                p.setDec_utilidadbrutamonetaria(rs.getDouble("dec_utilidadbrutamonetaria"));
                p.setDec_utilidadbrutaporcentaje(rs.getDouble("dec_utilidadbrutaporcentaje"));
                p.setVar_descripcion(rs.getString("var_descripcion"));
                p.setDec_precioventa(rs.getDouble("dec_precioventa"));
                lista.add(p);
            }
            cnn.close();
            cstmt.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

    public Producto getProducto_byCodigoBarra(String codigobarra) {

        Producto p = null;
        try {
            Connection cnn = null;
            CallableStatement cstmt = null;
            cnn = BD.getConnection();
            cnn.setAutoCommit(false);
            cstmt = cnn.prepareCall("{call sp_get_producto_byCodigoBarra (?)}");
            cstmt.setString(1, codigobarra);
            ResultSet rs = cstmt.executeQuery();
            if (rs.next()) {
                p = new Producto();
                p.setInt_id(rs.getInt("int_id"));
                p.setVar_codigoBarra(rs.getString("var_codigoBarra"));
                p.setInt_tipo(rs.getInt("int_tipo"));
                p.setInt_marca(rs.getInt("int_marca"));
                p.setInt_categoria(rs.getInt("int_categoria"));
                p.setDec_stockMinimo(rs.getDouble("dec_stockMinimo"));
                p.setDec_stockMaximo(rs.getDouble("dec_stockMaximo"));
                p.setDec_precioventa(rs.getDouble("dec_precioventa"));
                p.setDec_preciounitario(rs.getDouble("dec_preciounitario"));
                p.setDec_stock(rs.getDouble("dec_stock"));
                p.setDec_utilidadbrutamonetaria(rs.getDouble("dec_utilidadbrutamonetaria"));
                p.setDec_utilidadbrutaporcentaje(rs.getDouble("dec_utilidadbrutaporcentaje"));
                p.setVar_descripcion(rs.getString("var_descripcion"));
                p.setDec_precioventa(rs.getDouble("dec_precioventa"));
            }
            cnn.close();
            cstmt.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return p;
    }

    public String generar_codigobarra_serie() {
        String serie = "";
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from v_generar_codigobarra_serie");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                serie = rs.getString("serie");
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return serie;
    }

    public ArrayList<ListaProducto> get_lista_productos_all(String condicion) {
        ArrayList<ListaProducto> lista = new ArrayList<ListaProducto>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_producto_all where " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaProducto lp = new ListaProducto();
                lp.setInt_id(rs.getInt("id"));
                lp.setCodigobarra(rs.getString("codigobarra"));
                lp.setProducto(rs.getString("producto"));
                lp.setPreciounitario(rs.getDouble("preciounitario"));
                lp.setPrecioventa(rs.getDouble("precioventa"));
                lp.setTipoProducto(rs.getString("TipoProducto"));
                lp.setStock_minimo(rs.getDouble("stock_minimo"));
                lp.setStock_actual(rs.getDouble("stock_actual"));
                lp.setCategoria(rs.getString("Categoria"));
                lp.setMarca(rs.getString("Marca"));
                lp.setInt_estado(rs.getInt("int_estado"));
                lp.setEstado(rs.getString("estado"));
                lista.add(lp);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }
        
    public ArrayList<ListaProducto> get_lista_productos_all_activo(String condicion) {
        ArrayList<ListaProducto> lista = new ArrayList<ListaProducto>();
        try {
            Connection cnn = BD.getConnection();
            PreparedStatement ps = null;
            ps = cnn.prepareStatement("select * from get_lista_producto_all where int_estado=1 and " + condicion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ListaProducto lp = new ListaProducto();
                lp.setInt_id(rs.getInt("id"));
                lp.setCodigobarra(rs.getString("codigobarra"));
                lp.setProducto(rs.getString("producto"));
                lp.setPreciounitario(rs.getDouble("preciounitario"));
                lp.setPrecioventa(rs.getDouble("precioventa"));
                lp.setTipoProducto(rs.getString("TipoProducto"));
                lp.setStock_minimo(rs.getDouble("stock_minimo"));
                lp.setStock_actual(rs.getDouble("stock_actual"));
                lp.setCategoria(rs.getString("Categoria"));
                lp.setMarca(rs.getString("Marca"));
                lp.setInt_estado(rs.getInt("int_estado"));
                lp.setEstado(rs.getString("estado"));
                lista.add(lp);
            }
            cnn.close();
            ps.close();
        } catch (SQLException a) {
            System.out.println("" + a);
        } finally {
        }
        return lista;
    }

}
