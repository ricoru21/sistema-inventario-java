/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import dao.BDCliente;
import dao.Cliente;
import dao.ListaCliente;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLCliente {
    
    public int registar_cliente(String apePaterno, String apeMaterno,String busqueda,String direccion,
            String dni,String ruc,String nombre,String telefono) {
        int valor = 0;
        try {
            Cliente c = new Cliente();
            c.setVar_apepaterno(apePaterno);
            c.setVar_apematerno(apeMaterno);
            c.setVar_busqueda(busqueda);
            c.setVar_direccion(direccion);
            c.setVar_dni(dni);
            c.setVar_ruc(ruc);
            c.setVar_nombreM(nombre);
            c.setVar_telefono(telefono);
            valor = new BDCliente().insertarCliente(c);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }
    
    public int actualizar_cliente(int cliente,String apePaterno, String apeMaterno,String busqueda,String direccion,
            String dni,String ruc,String nombre,String telefono) {
        int valor = 0;
        try {
            Cliente c = new Cliente();
            c.setInt_id(cliente);
            c.setVar_apepaterno(apePaterno);
            c.setVar_apematerno(apeMaterno);
            c.setVar_busqueda(busqueda);
            c.setVar_direccion(direccion);
            c.setVar_dni(dni);
            c.setVar_ruc(ruc);
            c.setVar_nombreM(nombre);
            c.setVar_telefono(telefono);
            valor = new BDCliente().updateCliente(c);
        } catch (Exception e) {
            System.out.println("Error al actualizar :" + e.getMessage());
        }
        return valor;
    }
    
    public ArrayList<ListaCliente> get_lista_cliente_all(String palabra, int condicion) {
        ArrayList<ListaCliente> lista = null;
        try {
            //lista = new ArrayList<ListaCliente>();
            switch (condicion) {
                case 0: // Cliente
                    lista = new BDCliente().get_lista_cliente_all("cliente like '%" + palabra + "%'");
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }
    
    public ArrayList<ListaCliente> get_lista_cliente_all_activos(String palabra, int condicion) {
        ArrayList<ListaCliente> lista = null;
        try {
            //lista = new ArrayList<ListaCliente>();
            switch (condicion) {
                case 0: // Cliente
                    lista = new BDCliente().get_lista_cliente_all_activos("cliente like '%" + palabra + "%'");
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }
    
    public Cliente get_cliente_byId(int id){
        Cliente lista = null;
        try {
            lista = new BDCliente().get_cliente_byId(id);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }
    
    public int activar_restablecer_cliente(int id, String estado) {
        int valor = 0;
        try {
            valor =new BDCliente().activar_restablecer_cliente(id,estado);
        } catch (Exception e) {
            System.out.println("Error al activar / retablecer :" + e.getMessage());
        }
        return valor;
    }
    
}
