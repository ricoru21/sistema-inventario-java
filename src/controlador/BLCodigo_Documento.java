/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDCodigo_Documento;
import dao.Codigo_Documento;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLCodigo_Documento {

    public int registar_codigodocumento(int tipodocumento, String seriedoc, String numerodoc) {
        int valor = 0;
        try {
            Codigo_Documento cd = new Codigo_Documento();
            cd.setInt_tipodocumento(tipodocumento);
            cd.setVar_serie(seriedoc);
            cd.setVar_numero(numerodoc);
            valor = new BDCodigo_Documento().insertarCodigoDocumento(cd);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }

    public ArrayList<Codigo_Documento> selectCodigoDocumento_all() {
        ArrayList<Codigo_Documento> lista = null;
        try {
            lista = new ArrayList<Codigo_Documento>();
            lista = new BDCodigo_Documento().selectCodigoDocumento_all();
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }
}
