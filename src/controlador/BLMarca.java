/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDMarca;
import dao.Marca;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLMarca {

    public int registar_marca(String descripcion, String estado) {
        int valor = 0;
        try {
            Marca m = new Marca();
            m.setVar_descripcion(descripcion);
            m.setVar_estado(estado);
            valor = new BDMarca().insertarMarca(m);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }

    public int actualizar_marca(int id, String descripcion, String estado) {
        int valor = 0;
        try {
            Marca m = new Marca();
            m.setInt_id(id);
            m.setVar_descripcion(descripcion);
            m.setVar_estado(estado);
            valor = new BDMarca().updateMarca(m);
        } catch (Exception e) {
            System.out.println("Error al actualizar :" + e.getMessage());
        }
        return valor;
    }

    public ArrayList<Marca> selectMarca_all() {
        ArrayList<Marca> lista = null;
        try {
            lista = new ArrayList<Marca>();
            lista = new BDMarca().selectMarca_all();
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

    public ArrayList<Marca> get_lista_marca_all(String palabra, int condicion) {
        ArrayList<Marca> lista = null;
        try {
            lista = new ArrayList<Marca>();
            switch (condicion) {
                case 0: // Descripcion
                    lista = new BDMarca().get_lista_marca_all("var_descripcion like '%" + palabra + "%'");
                    break;
                case 1: // ID
                    lista = new BDMarca().get_lista_marca_all("int_id =" + Integer.parseInt(palabra));
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

    public Marca get_marca_byId(int id) {
        Marca m = null;
        try {
            m = new BDMarca().get_marca_byId(id);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return m;
    }

}
