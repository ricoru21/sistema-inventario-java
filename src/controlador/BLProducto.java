/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDProducto;
import dao.ListaProducto;
import dao.Producto;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLProducto {

    public int registar_producto(String codigoBarra, int tipo, int categoria, double stockminimo,
            double stockmaximo, double precioUnitario, double precioVenta,
            double utilmonetaria, double utilporcentaje, String descripcion, int marca, double sotck) {
        int valor = 0;
        try {
            Producto p = new Producto();
            p.setVar_codigoBarra(codigoBarra);
            p.setInt_tipo(tipo);
            p.setInt_categoria(categoria);
            p.setDec_stockMinimo(stockminimo);
            p.setDec_stockMaximo(stockmaximo);
            p.setDec_preciounitario(precioUnitario);
            p.setDec_precioventa(precioVenta);
            p.setVar_descripcion(descripcion);
            p.setDec_utilidadbrutamonetaria(utilmonetaria);
            p.setDec_utilidadbrutaporcentaje(utilporcentaje);
            p.setInt_marca(marca);
            p.setDec_stock(sotck);
            valor = new BDProducto().insertarProducto(p);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.toString());
        }
        return valor;
    }

    public int actualizar_producto(int id,String codigoBarra, int tipo, int categoria, double stockminimo,
            double stockmaximo, double precioUnitario, double precioVenta,
            double utilmonetaria, double utilporcemtaje, String descripcion, int marca, double sotck) {
        int valor = 0;
        try {
            Producto p = new Producto();
            p.setInt_id(id);
            p.setVar_codigoBarra(codigoBarra);
            p.setInt_tipo(tipo);
            p.setInt_categoria(categoria);
            p.setDec_stockMinimo(stockminimo);
            p.setDec_stockMaximo(stockmaximo);
            p.setDec_preciounitario(precioUnitario);
            p.setDec_precioventa(precioVenta);
            p.setVar_descripcion(descripcion);
            p.setDec_utilidadbrutamonetaria(utilmonetaria);
            p.setDec_utilidadbrutaporcentaje(utilporcemtaje);
            p.setInt_marca(marca);
            p.setDec_stock(sotck);
            p.setVar_estado("1");
            valor = new BDProducto().updateProducto(p);
        } catch (Exception e) {
            System.out.println("Error al actualizar :" + e.toString());
        }
        return valor;
    }

    public ArrayList<Producto> listar_prroducto(String palabra, int condicion) {
        ArrayList<Producto> listaClientes = null;
        try {
            listaClientes = new BDProducto().selectProducto_All(palabra, condicion);
        } catch (Exception e) {
            System.out.println("Error al consultar producto :" + e.toString());
        }
        return listaClientes;
    }

    public Producto getProducto_byCodigoBarra(String codigobarra) {
        Producto p = null;
        try {
            p = new BDProducto().getProducto_byCodigoBarra(codigobarra);
        } catch (Exception e) {
            System.out.println("Error al obtener producto :" + e.toString());
        }
        return p;
    }

    public String generar_codigobarra_serie() {
        String codigoBarra = "";
        try {
            codigoBarra = new BDProducto().generar_codigobarra_serie();
        } catch (Exception e) {
            System.out.println("Error al obtener CodigoBarra :" + e.toString());
        }
        return codigoBarra;
    }

    public ArrayList<ListaProducto> get_lista_productos_all(String palabra, String condicion) {
        ArrayList<ListaProducto> lista = null;
        try {
            switch (condicion) {
                case "Descripcion":
                    lista = new BDProducto().get_lista_productos_all("producto like '%" + palabra + "%'");
                    break;
                case "Tipo Producto":
                    lista = new BDProducto().get_lista_productos_all("TipoProducto like '%" + palabra + "%'");
                    break;
                case "Categoria":
                    lista = new BDProducto().get_lista_productos_all("Categoria like '%" + palabra + "%'");
                    break;
                case "Marca":
                    lista = new BDProducto().get_lista_productos_all("Marca like '%" + palabra + "%'");
                    break;
                case "Codigo":
                    lista = new BDProducto().get_lista_productos_all("codigobarra like '%" + palabra + "%'");
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error al consultar producto :" + e.toString());
        }
        return lista;
    }
    
    public ArrayList<ListaProducto> get_lista_productos_all_activo(String palabra, String condicion) {
        ArrayList<ListaProducto> lista = null;
        try {
            switch (condicion) {
                case "Descripcion":
                    lista = new BDProducto().get_lista_productos_all_activo("producto like '%" + palabra + "%'");
                    break;
                case "Tipo Producto":
                    lista = new BDProducto().get_lista_productos_all_activo("TipoProducto like '%" + palabra + "%'");
                    break;
                case "Categoria":
                    lista = new BDProducto().get_lista_productos_all_activo("Categoria like '%" + palabra + "%'");
                    break;
                case "Marca":
                    lista = new BDProducto().get_lista_productos_all_activo("Marca like '%" + palabra + "%'");
                    break;
                case "Codigo":
                    lista = new BDProducto().get_lista_productos_all_activo("codigobarra like '%" + palabra + "%'");
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error al consultar producto :" + e.toString());
        }
        return lista;
    }
    
    public int activar_restablecer_producto(int id, String estado) {
        int valor = 0;
        try {
            valor =new BDProducto().activar_restablecer_producto(id, estado);
        } catch (Exception e) {
            System.out.println("Error al activar / retablecer :" + e.getMessage());
        }
        return valor;
    }

}
