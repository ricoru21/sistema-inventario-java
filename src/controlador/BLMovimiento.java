/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDMovimiento;
import dao.ListaMovimiento_Stock;
import dao.Movimiento;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLMovimiento {

    public int registar_moviento(double monto, int venta, int tipooperacion, String motivo, 
            int tipodocumento,String documento, int tipopago, int usuario) {
        int valor = 0;
        try {
            Movimiento m = new Movimiento();
            m.setDec_monto(monto);
            m.setInt_venta(venta);
            m.setInt_tipodocumento(tipodocumento);
            m.setVar_motivo(motivo);
            m.setInt_usuario(usuario);
            m.setInt_tipopago(tipopago);
            m.setVar_documento(documento);
            m.setInt_tipooperacion(tipooperacion);
            valor = new BDMovimiento().insertarMovimiento(m);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }
    
    public ArrayList<ListaMovimiento_Stock> get_lista_movimiento_stock_all(String condicion) {
        ArrayList<ListaMovimiento_Stock> lista = null;
        try {
            lista = new BDMovimiento().get_lista_movimiento_stock_all(condicion);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
     }

}
