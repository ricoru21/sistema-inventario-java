/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDCategoria;
import dao.Categoria;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLCategoria {

    public int registar_categoria(String descripcion, String estado) {
        int valor = 0;
        try {
            Categoria c = new Categoria();
            c.setVar_descripcion(descripcion);
            c.setVar_estado(estado);
            valor = new BDCategoria().insertarCategoria(c);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }

    public int actualizar_categoria(int id, String descripcion, String estado) {
        int valor = 0;
        try {
            Categoria c = new Categoria();
            c.setInt_id(id);
            c.setVar_descripcion(descripcion);
            c.setVar_estado(estado);
            valor = new BDCategoria().updateCategoria(c);
        } catch (Exception e) {
            System.out.println("Error al actualizar :" + e.getMessage());
        }
        return valor;
    }

    public ArrayList<Categoria> selectCategoria_all() {
        ArrayList<Categoria> lista = null;
        try {
            lista = new ArrayList<Categoria>();
            lista = new BDCategoria().selectCategoria_all();
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

    public ArrayList<Categoria> get_lista_categoria_all(String palabra, int condicion) {
        ArrayList<Categoria> lista = null;
        try {
            lista = new ArrayList<Categoria>();
            switch (condicion) {
                case 0: // Descripcion
                    lista = new BDCategoria().get_lista_categoria_all("var_descripcion like '%" + palabra + "%'");
                    break;
                case 1: // ID
                    lista = new BDCategoria().get_lista_categoria_all("int_id =" + Integer.parseInt(palabra));
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

    public Categoria get_categoria_byId(int id) {
        Categoria c = null;
        try {
            c = new BDCategoria().get_categoria_byId(id);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return c;
    }

}
