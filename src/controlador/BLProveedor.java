/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDProveedor;
import dao.Proveedor;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLProveedor {

    public int registar_proveedor(String ruc, String razonsocial, String telefono, String direccion) {
        int valor = 0;
        try {
            Proveedor p = new Proveedor();
            p.setVar_direccion(direccion);
            p.setVar_razonsocial(razonsocial);
            p.setVar_ruc(ruc);
            p.setVar_telefono(telefono);
            valor = new BDProveedor().insertarProveedor(p);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }

    public int actualizar_proveedor(int id, String ruc, String razonsocial, String telefono, String direccion,
            String estado) {
        int valor = 0;
        try {
            Proveedor p = new Proveedor();
            p.setInt_id(id);
            p.setVar_direccion(direccion);
            p.setVar_razonsocial(razonsocial);
            p.setVar_ruc(ruc);
            p.setVar_telefono(telefono);
            p.setVar_estado(estado);
            valor = new BDProveedor().updateProveedor(p);
        } catch (Exception e) {
            System.out.println("Error al actualizar :" + e.getMessage());
        }
        return valor;
    }
    
    public int activar_restablecer_proveedor(int id, String estado) {
        int valor = 0;
        try {
            valor =new BDProveedor().activar_restablecer_proveedor(id,estado);
        } catch (Exception e) {
            System.out.println("Error al activar / retablecer :" + e.getMessage());
        }
        return valor;
    }
    
    public ArrayList<Proveedor> listar_proveedor(String palabra, int condicion) {
        ArrayList<Proveedor> listaClientes = null;
        try {
            listaClientes = new BDProveedor().selectProveedor_All(palabra, condicion);
        } catch (Exception e) {
            throw e;
        }
        return listaClientes;
    }

}
