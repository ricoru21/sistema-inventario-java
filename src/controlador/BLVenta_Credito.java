/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDVenta_Credito;
import dao.CronogramaPago;
import dao.ListaCronogramaPago;
import dao.Venta_Credito;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLVenta_Credito {

    public int registar_venta_credito(int nrocuota, double montocuota, int formapago,
            int frecuenciapago, String estado, int idventa, ArrayList<CronogramaPago> lista_cp) {
        int valor = 0;
        try {
            Venta_Credito vc = new Venta_Credito();
            vc.setInt_nrocuota(nrocuota);
            vc.setDec_montocuota(montocuota);
            vc.setInt_formapago(formapago);
            vc.setInt_frecuenciapago(frecuenciapago);
            vc.setInt_venta(idventa);
            vc.setVar_estado(estado);
            valor = new BDVenta_Credito().insertarVenta_Credito(vc, lista_cp);
        } catch (Exception e) {
            System.out.println("Error al registrar venta_credito :" + e.getMessage());
        }
        return valor;
    }

    public int actualizar_cronogramapago(int id_cp, double acuenta_anterior,double acuenta, String estado,
               int id_venta) {
        int valor = 0;
        try {
            CronogramaPago cp = new CronogramaPago();
            cp.setInt_id(id_cp);
            cp.setDec_monto(acuenta_anterior);
            cp.setDec_acuenta(acuenta);
            cp.setVar_estado(estado);
            cp.setInt_venta(id_venta);
            valor = new BDVenta_Credito().updateCronogramaPago(cp);
        } catch (Exception e) {
            System.out.println("Error al actualizar cronograma pago :" + e.getMessage());
        }
        return valor;
    }

    public Venta_Credito get_ventacredito_byId_Venta(int idventa) {
        Venta_Credito vc = null;
        try {
            vc = new BDVenta_Credito().get_ventacredito_byId_Venta(idventa);
        } catch (Exception e) {
            System.out.println("Error al consultar cronograma pago :" + e.getMessage());
        }
        return vc;
    }

    public ArrayList<ListaCronogramaPago> get_lista_cronogramapago_byId_VentaCredito(int idventa) {
        ArrayList<ListaCronogramaPago> lista = null;
        try {
            lista = new BDVenta_Credito().get_lista_cronogramapago_byId_Venta(idventa);
        } catch (Exception e) {
            System.out.println("Error al consultar cronograma pago :" + e.getMessage());
        }
        return lista;
    }

}
