/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDUsuario;
import dao.Usuario;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLUsuario {

    public int registar_usuario(String dni, String nomape, String idname, String pass, String telefono) {
        int valor = 0;
        try {
            Usuario u = new Usuario();
            u.setVar_dni(dni);
            u.setVar_nombreapellido(nomape);
            u.setVar_idname(idname);
            u.setVar_password(pass);
            u.setVar_telefono(telefono);
            valor = new BDUsuario().insertarUsuario(u);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }

    public int actualizar_usuario(int id, String dni, String nomape, String idname, String pass, String telefono,
            String estado) {
        int valor = 0;
        try {
            Usuario u = new Usuario();
            u.setInt_id(id);
            u.setVar_dni(dni);
            u.setVar_nombreapellido(nomape);
            u.setVar_idname(idname);
            u.setVar_password(pass);
            u.setVar_telefono(telefono);
            u.setVar_estado(estado);
            valor = new BDUsuario().updateUsuario(u);
        } catch (Exception e) {
            System.out.println("Error al actualizar :" + e.getMessage());
        }
        return valor;
    }

    public int activar_restablecer_usuario(int id, String estado) {
        int valor = 0;
        try {
            valor = new BDUsuario().activar_restablecer_usuario(id, estado);
        } catch (Exception e) {
            System.out.println("Error al activar / retablecer " + e.getMessage());
        }
        return valor;
    }

    public ArrayList<Usuario> listar_usuario(String palabra, int condicion) {
        ArrayList<Usuario> lista_usuario = null;
        try {
            lista_usuario = new BDUsuario().selectUsuario_All(palabra, condicion);
        } catch (Exception e) {
            System.out.println("Error al Listar Usuarios :" + e.getMessage());
        }
        return lista_usuario;
    }

    public Usuario validarPassword(String clave) {
        Usuario u = null;
        try {
            u = new BDUsuario().getUsuario_validatePassword(clave);
        } catch (Exception e) {
            System.out.println("Error al validar Usuario :" + e.getMessage());
        }
        return u;
    }

}
