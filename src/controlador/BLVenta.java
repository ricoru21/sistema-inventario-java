/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDVenta;
import dao.CronogramaPago;
import dao.Detalle_OrdenSalida;
import dao.Documento;
import dao.ListaVenta;
import dao.Transaccion;
import dao.Venta;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Richard
 */
public class BLVenta {

    public int registar_venta(int cliente, double totalfacturado, double subtotal, double descuentoporcentaje,
            double descuentomonetario, double acuenta, double saldo, int tipoventa, double igv, double igvmonto,
            int usuario, String estado, ArrayList<Transaccion> tra, Documento d, ArrayList<Detalle_OrdenSalida> dos,
            int nrocuota, double montocuota, int formapagoventacredito, int nrofrecuenciapago, int formapago,
            ArrayList<CronogramaPago> listacp) {
        int idventa = 0;
        try {
            Venta v = new Venta();
            v.setCliente(cliente);
            v.setDec_totalfacturado(totalfacturado);
            v.setDec_subtotal(subtotal);
            v.setDec_descuentoporcentaje(descuentoporcentaje);
            v.setDec_descuentomonetario(descuentomonetario);
            v.setDec_acuenta(acuenta);
            v.setDec_saldo(saldo);
            v.setInt_tipoventa(tipoventa);
            v.setIgv(igv);
            v.setIgvmonto(igvmonto);
            v.setInt_usuario(usuario);
            v.setVar_estado(estado);
            v.setInt_formapago(formapago);
            idventa = new BDVenta().insertarVenta(v, tra, d);
            if (idventa != 0) {
                //Registrar Orden_Salida
                new BLOrden_Salida().registar_ordenSalida(1, usuario, "venta realizado", new Timestamp(new Date().getTime()), idventa, dos);
                //Registrar Movimiento
                new BLMovimiento().registar_moviento(acuenta, idventa, 2, "Venta Producto", d.getInt_tipodocumento(),
                        d.getVar_serie() + " - " + d.getVar_numero(), tipoventa, v.getInt_usuario());
                if (estado.equals("3")) {
                    new BLVenta_Credito().registar_venta_credito(nrocuota, montocuota, formapagoventacredito, nrofrecuenciapago, "1", idventa, listacp);
                }
            }
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return idventa;
    }

    public String[] get_serienumero_documento(int tipoDocumento) {
        String[] serienumero = null;
        try {
            serienumero = new BDVenta().get_serienumero_documento(tipoDocumento);
        } catch (Exception e) {
            System.out.println("Error al obtener serie y numero documento :" + e.getMessage());
        }
        return serienumero;
    }

    public ArrayList<ListaVenta> get_lista_venta_all(String condicion) {
        ArrayList<ListaVenta> lista = null;
        try {
            lista = new BDVenta().get_lista_venta_all(condicion);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

}
