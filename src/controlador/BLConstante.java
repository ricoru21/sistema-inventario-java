/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDConstante;
import dao.Constante;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLConstante {

    public int registar_constante(String descripcion, int clase) {
        int valor = 0;
        try {
            Constante c = new Constante();
            c.setVar_descripcion(descripcion);
            c.setInt_clase(clase);
            valor = new BDConstante().insertarConstante(c);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }
    
    public int actualizar_constante(int id,String descripcion, int clase) {
        int valor = 0;
        try {
            Constante c = new Constante();
            c.setInt_id(id);
            c.setVar_descripcion(descripcion);
            c.setInt_clase(clase);
            valor = new BDConstante().updateConstante(c);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }
    
    public ArrayList<Constante> selectConstante_byClase(int clase) {
        ArrayList<Constante> lista = null;
        try {
            lista = new ArrayList<Constante>();
            lista = new BDConstante().selectConstante_byClase(clase);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

    public ArrayList<Constante> get_lista_constante_tipoproducto(String palabra, int condicion) {
        ArrayList<Constante> lista = null;
        try {
            lista = new ArrayList<Constante>();
            switch (condicion) {
                case 0: // Descripcion
                    lista = new BDConstante().get_lista_constante_tipoproducto("var_descripcion like '%" + palabra + "%'");
                    break;
                case 1: // ID
                    lista = new BDConstante().get_lista_constante_tipoproducto("int_id =" + Integer.parseInt(palabra));
                    break;
            }
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

    public Constante get_constante_byvalor_clase(int valor, int clase) {
        Constante c = null;
        try {
            c = new BDConstante().get_constante_byvalor_clase(valor, clase);
        } catch (Exception e) {
            System.out.println("Error al obtener una constante :" + e.getMessage());
        }
        return c;
    }

}
