/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDOrden_Ingreso;
import dao.Detalle_OrdenIngreso;
import dao.ListaOrdenIngreso;
import dao.Movimiento;
import dao.Orden_Ingreso;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLOrden_Ingreso {

    public int registar_ordenIngreso(int motivo, int proveedor, int tipodocumento, int usuario, String comentario,
            String seriedoc, String numerodoc, Timestamp fechaEmision, double monto_total,ArrayList<Detalle_OrdenIngreso> doi) {
        int valor = 0;
        try {
            Orden_Ingreso oi = new Orden_Ingreso();
            oi.setInt_motivo(motivo);
            oi.setInt_proveedor(proveedor);
            oi.setInt_tipodocumento(tipodocumento);
            oi.setInt_usuario(usuario);
            oi.setVar_comentario(comentario);
            oi.setVar_documentonumero(numerodoc);
            oi.setVar_documentoserie(seriedoc);
            oi.setDat_fecharegistro(fechaEmision);
            oi.setMonto_total(monto_total);
            valor = new BDOrden_Ingreso().insertarOrdenIngreso(oi, doi);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.toString());
        }
        return valor;
    }

    public ArrayList<ListaOrdenIngreso> get_lista_ordeningreso_all(String condicion) {
        ArrayList<ListaOrdenIngreso> lista = null;
        try {
            lista = new BDOrden_Ingreso().get_lista_ordeningreso_all(condicion);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }

    public int anular_orden_ingreso(int id, String estado) {
        int valor = 0;
        try {
            valor = new BDOrden_Ingreso().anular_orden_ingreso(id, estado);
        } catch (Exception e) {
            System.out.println("Error al anular orden ingreso " + e.getMessage());
        }
        return valor;
    }

}
