/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDVenta_Anulada;
import dao.Venta_Anulada;

/**
 *
 * @author Richard
 */
public class BLVenta_Anulada {

    public int registar_venta_anulada(int id_venta, String motivo, int usuario) {
        int idventa = 0;
        try {
            Venta_Anulada va = new Venta_Anulada();
            va.setInt_venta(id_venta);
            va.setVar_motivo(motivo);
            va.setInt_usuario(usuario);
            idventa = new BDVenta_Anulada().insertarVenta_Anulada(va);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return idventa;
    }
}
