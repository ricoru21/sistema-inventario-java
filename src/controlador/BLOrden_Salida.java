/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDOrden_Salida;
import dao.Detalle_OrdenSalida;
import dao.ListaOrdenSalida;
import dao.Orden_Salida;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Richard
 */
public class BLOrden_Salida {

    public int registar_ordenSalida(int motivo, int usuario, String comentario,
            Timestamp fechaEmision,int idventa , ArrayList<Detalle_OrdenSalida> dos) {
        int valor = 0;
        try {
            Orden_Salida os = new Orden_Salida();
            os.setInt_motivo(motivo);
            os.setInt_usuario(usuario);
            os.setVar_comentario(comentario);
            os.setDat_fecharegistro(fechaEmision);
            os.setVar_estado("1");
            os.setInt_venta(idventa);
            valor = new BDOrden_Salida().insertarOrdenSalida(os, dos);
        } catch (Exception e) {
            System.out.println("Error al registrar :" + e.getMessage());
        }
        return valor;
    }

    public ArrayList<ListaOrdenSalida> get_lista_ordensalida_all(String condicion) {
        ArrayList<ListaOrdenSalida> lista = null;
        try {
            lista = new BDOrden_Salida().get_lista_ordensalida_all(condicion);
        } catch (Exception e) {
            System.out.println("Error al consultar :" + e.getMessage());
        }
        return lista;
    }
    
    public int anular_orden_salida(int id, String estado) {
        int valor = 0;
        try {
            valor = new BDOrden_Salida().anular_orden_salida(id, estado);
        } catch (Exception e) {
            System.out.println("Error al anular orden ingreso " + e.getMessage());
        }
        return valor;
    }

}
