/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import dao.BDCaja;
import dao.Caja;

/**
 *
 * @author Richard
 */
public class BLCaja {

    public int registar_caja(int usuario, double saldoInicial) {
        int valor = 0;
        try {
            Caja c = new Caja();
            c.setInt_usuario(usuario);
            c.setDec_saldoinicial(saldoInicial);
            valor = new BDCaja().insertarCaja(c);
        } catch (Exception e) {
            System.out.println("Error al registrar caja :" + e.getMessage());
        }
        return valor;
    }

    public Caja verificar_inicio_caja() {
        Caja c = null;
        try {
            c = new BDCaja().verificar_inicio_caja();
        } catch (Exception a) {
            System.out.println("" + a);
        }
        return c;
    }
    
    public int realizar_cierre_caja(int id_caja){
        int valor = 0;
        try {
            Caja c = new Caja();
            c.setInt_id(id_caja);
            valor = new BDCaja().realizar_cierre_caja(c);
        } catch (Exception e) {
            System.out.println("Error al cerrrar caja :" + e.getMessage());
        }
        return valor;
    }
}
