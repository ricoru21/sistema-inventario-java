CREATE DATABASE  IF NOT EXISTS `bd_inventario` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bd_inventario`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: bd_inventario
-- ------------------------------------------------------
-- Server version	5.5.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `caja`
--

DROP TABLE IF EXISTS `caja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caja` (
  `int_id` int(11) NOT NULL,
  `dat_fechaInicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dat_fechaCierre` timestamp NULL DEFAULT NULL,
  `int_usuario` int(11) NOT NULL,
  `dec_saldoinicial` decimal(9,2) NOT NULL,
  `dec_ingreso` decimal(9,2) NOT NULL,
  `dec_egreso` decimal(9,2) NOT NULL,
  `var_estado` char(1) NOT NULL,
  PRIMARY KEY (`int_id`),
  UNIQUE KEY `dat_fechaInicio_UNIQUE` (`dat_fechaInicio`),
  KEY `CajaFKUsuario_idx` (`int_usuario`),
  CONSTRAINT `CajaFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caja`
--

LOCK TABLES `caja` WRITE;
/*!40000 ALTER TABLE `caja` DISABLE KEYS */;
INSERT INTO `caja` VALUES (1,'2014-04-25 00:02:34',NULL,1,100.00,0.00,0.00,'1'),(2,'2014-04-25 07:50:48','2014-04-25 05:00:00',1,100.00,1036.77,20.00,'2'),(3,'2014-05-10 03:21:03',NULL,1,100.00,0.00,0.00,'1'),(4,'2014-05-10 19:07:00',NULL,1,100.00,0.00,0.00,'1'),(5,'2014-05-25 18:46:21',NULL,1,100.00,0.00,0.00,'1'),(6,'2014-06-09 05:02:30',NULL,1,60.00,0.00,0.00,'1'),(7,'2014-06-11 18:20:49',NULL,1,60.00,0.00,0.00,'1'),(8,'2014-06-12 23:27:02',NULL,1,60.00,0.00,0.00,'1');
/*!40000 ALTER TABLE `caja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `int_id` int(11) NOT NULL,
  `var_descripcion` varchar(200) NOT NULL,
  `var_estado` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'categoria #1','1'),(2,'categoria #2','1'),(3,'categoria #3','1'),(4,'BEBIDAS','1'),(5,'categoria #modificado','1');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `int_id` int(11) NOT NULL,
  `var_nombre` varchar(45) NOT NULL,
  `var_dni` char(8) NOT NULL,
  `var_apepaterno` varchar(45) NOT NULL,
  `var_apematerno` varchar(45) NOT NULL,
  `var_direccion` varchar(45) DEFAULT NULL,
  `var_telefono` varchar(20) DEFAULT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `var_busqueda` varchar(200) NOT NULL,
  `var_estado` char(1) NOT NULL,
  `var_ruc` char(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `ClienteBusqueda_idx` (`var_busqueda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'BUDDY','47197204','ORUNA','RODRIGUEZ','Sin Direccion','#980031860','2014-04-12 21:02:37','ORUNA RODRIGUEZ BUDDY','1',''),(2,'ANONIMO','01548723','CLIENTE','EEJMPLO','','','2014-04-19 19:33:25','CLIENTE EEJMPLO ANONIMO','1',''),(3,'MANUEL','54875689','CEGARRA','VALDIVIA','','568954','2014-04-21 21:50:20','CEGARRA VALDIVIA MANUEL','1',''),(4,'EJEMPLO','','CLIENTE','PRUEBA','Sin direccion','','2014-05-10 21:08:32','CLIENTE PRUEBA EJEMPLO','1',''),(5,'ERASMO','00000000','RODRIGUEZ','RUIZ','Sin Direccion','','2014-05-10 21:12:40','RODRIGUEZ RUIZ ERASMO','1','');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codigo_documento`
--

DROP TABLE IF EXISTS `codigo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigo_documento` (
  `int_id` int(11) NOT NULL,
  `int_tipo` int(11) NOT NULL,
  `var_serie` varchar(8) NOT NULL,
  `var_numero` varchar(15) NOT NULL,
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codigo_documento`
--

LOCK TABLES `codigo_documento` WRITE;
/*!40000 ALTER TABLE `codigo_documento` DISABLE KEYS */;
INSERT INTO `codigo_documento` VALUES (1,1,'001','00001'),(2,2,'0001','00000001'),(3,3,'0001','00000001');
/*!40000 ALTER TABLE `codigo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `constante`
--

DROP TABLE IF EXISTS `constante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `constante` (
  `int_id` int(11) NOT NULL,
  `var_descripcion` varchar(500) NOT NULL,
  `int_clase` int(11) NOT NULL,
  `int_valor` int(11) NOT NULL,
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `constante`
--

LOCK TABLES `constante` WRITE;
/*!40000 ALTER TABLE `constante` DISABLE KEYS */;
INSERT INTO `constante` VALUES (1,'Tipo Producto',1,0),(2,'Tipo Modificado',1,1),(3,'Tipo 2',1,2),(4,'Tipo #3',1,3),(5,'Tipo #4',1,4),(6,'Tipo Operacion',2,0),(7,'Retiro',2,1),(8,'Deposito',2,2),(9,'Tipo Documento Mov',3,0),(10,'Sin Documento',3,1),(11,'Factura',3,2),(12,'Boleta',3,3),(13,'Nota Venta',3,4),(14,'Forma Pago',4,0),(15,'Efectivo',4,1),(16,'Tarjeta Visa',4,2),(17,'Tarjeta Marstercad',4,3),(18,'Motivo IngProd',5,0),(19,'Compra',5,1),(20,'Devolucion',5,2),(21,'Otros',5,3),(22,'Tipo Documento Venta',6,0),(23,'Ticket',6,1),(24,'Boleta',6,2),(25,'Factura',6,3),(26,'Motivo SalidaProd',7,0),(27,'Venta',7,1),(28,'Traslado',7,2),(29,'Otros',7,3),(31,'Frecuencia Pago - VentaCredito',8,0),(32,'Semanal',8,2),(33,'Quincenal',8,3),(34,'Mensual',8,4),(35,'ABARROTE',1,5),(36,'Diario',8,1);
/*!40000 ALTER TABLE `constante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronograma_pago`
--

DROP TABLE IF EXISTS `cronograma_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronograma_pago` (
  `int_id` int(11) NOT NULL,
  `int_numerocuota` int(11) NOT NULL,
  `dat_fecharegistro` datetime NOT NULL COMMENT 'fecha que las cuotas fue creadas',
  `dat_fechapago` datetime NOT NULL COMMENT 'Fecha que se tiene que pagar',
  `dat_fechapagorealizado` datetime DEFAULT NULL COMMENT 'cuando se realiza el pago',
  `dec_monto` decimal(9,2) NOT NULL,
  `dec_acuenta` decimal(9,2) NOT NULL,
  `var_estado` char(1) NOT NULL COMMENT '1 PROGRAMADA- 2 CANCELADA - 3 DEUDA',
  `int_ventacredito` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `cronograma_pagoFKventacredito_idx` (`int_ventacredito`),
  CONSTRAINT `cronograma_pagoFKventacredito` FOREIGN KEY (`int_ventacredito`) REFERENCES `venta_credito` (`int_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronograma_pago`
--

LOCK TABLES `cronograma_pago` WRITE;
/*!40000 ALTER TABLE `cronograma_pago` DISABLE KEYS */;
INSERT INTO `cronograma_pago` VALUES (1,0,'2014-04-20 13:37:01','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(2,1,'2014-04-20 13:37:01','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(3,2,'2014-04-20 13:37:01','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(4,3,'2014-04-20 13:37:02','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(5,4,'2014-04-20 13:37:02','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(6,5,'2014-04-20 13:37:02','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(7,6,'2014-04-20 13:37:02','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(8,7,'2014-04-20 13:37:02','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(9,8,'2014-04-20 13:37:02','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(10,9,'2014-04-20 13:37:02','2014-05-05 00:00:00',NULL,21.66,0.00,'1',1),(11,0,'2014-04-20 13:42:44','2014-04-27 00:00:00','2014-04-21 01:38:41',14.44,14.44,'2',2),(12,1,'2014-04-20 13:42:44','2014-05-04 00:00:00','2014-04-24 19:28:21',14.44,14.44,'2',2),(13,2,'2014-04-20 13:42:44','2014-05-11 00:00:00',NULL,14.44,0.00,'1',2),(14,3,'2014-04-20 13:42:44','2014-05-18 00:00:00',NULL,14.44,0.00,'1',2),(15,4,'2014-04-20 13:42:44','2014-05-25 00:00:00',NULL,14.44,0.00,'1',2);
/*!40000 ALTER TABLE `cronograma_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_ordeningreso`
--

DROP TABLE IF EXISTS `detalle_ordeningreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_ordeningreso` (
  `int_id` int(11) NOT NULL,
  `dec_preciounitario` decimal(9,2) NOT NULL,
  `dec_cantidad` decimal(9,2) NOT NULL,
  `dec_importe` decimal(9,2) NOT NULL,
  `int_producto` int(11) NOT NULL,
  `int_ordeningreso` int(11) NOT NULL,
  `var_estado` char(1) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `Detalle_OrdenIngresoFKProducto_idx` (`int_producto`),
  KEY `Detalle_OrdenIngresoFKOrdenIngreso_idx` (`int_ordeningreso`),
  CONSTRAINT `Detalle_OrdenIngresoFKOrdenIngreso` FOREIGN KEY (`int_ordeningreso`) REFERENCES `orden_ingreso` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Detalle_OrdenIngresoFKProducto` FOREIGN KEY (`int_producto`) REFERENCES `producto` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_ordeningreso`
--

LOCK TABLES `detalle_ordeningreso` WRITE;
/*!40000 ALTER TABLE `detalle_ordeningreso` DISABLE KEYS */;
INSERT INTO `detalle_ordeningreso` VALUES (2,12.00,10.00,120.00,1,1,'1'),(3,14.50,10.00,145.00,1,2,'1'),(4,17.20,10.00,172.00,2,2,'1'),(5,7.30,100.00,730.00,1,3,'1'),(6,7.28,20.00,145.60,1,4,'1'),(7,2.50,10.00,25.00,5,5,'1'),(8,12.00,32.00,384.00,4,5,'1'),(9,3.00,4.00,12.00,5,6,'1');
/*!40000 ALTER TABLE `detalle_ordeningreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_ordensalida`
--

DROP TABLE IF EXISTS `detalle_ordensalida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_ordensalida` (
  `int_id` int(11) NOT NULL,
  `dec_cantidad` decimal(9,2) NOT NULL,
  `int_producto` int(11) NOT NULL,
  `int_ordensalida` int(11) NOT NULL,
  `var_estado` char(1) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `DetOrdenSalidaFKOrdenSalida_idx` (`int_ordensalida`),
  KEY `DetOrdenSalidaFKProducto_idx` (`int_producto`),
  CONSTRAINT `DetOrdenSalidaFKOrdenSalida` FOREIGN KEY (`int_ordensalida`) REFERENCES `orden_salida` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `DetOrdenSalidaFKProducto` FOREIGN KEY (`int_producto`) REFERENCES `producto` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_ordensalida`
--

LOCK TABLES `detalle_ordensalida` WRITE;
/*!40000 ALTER TABLE `detalle_ordensalida` DISABLE KEYS */;
INSERT INTO `detalle_ordensalida` VALUES (1,10.00,2,1,'1'),(2,5.00,2,2,'1'),(3,12.00,3,3,'1'),(4,4.00,3,4,'1'),(5,5.00,2,5,'1'),(6,2.00,4,6,'1'),(7,2.00,5,7,'1'),(8,4.00,4,8,'1'),(9,10.00,3,9,'1'),(10,2.00,4,9,'1'),(11,4.00,4,10,'1'),(12,10.00,5,11,'1'),(13,1.00,4,12,'1');
/*!40000 ALTER TABLE `detalle_ordensalida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_prestamo`
--

DROP TABLE IF EXISTS `detalle_prestamo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_prestamo` (
  `int_id` int(11) NOT NULL,
  `int_prestamo` int(11) NOT NULL,
  `dat_fechamaxpago` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dec_montocuota` decimal(9,2) NOT NULL,
  `dec_montointeres` decimal(9,2) NOT NULL,
  `dec_montopagado` decimal(9,2) NOT NULL,
  `dec_interespagado` decimal(9,2) NOT NULL,
  `var_estado` char(1) NOT NULL COMMENT '1 pendiente, 2 cancelada, 0 anulada',
  PRIMARY KEY (`int_id`),
  KEY `DetallePrestamoFKPrestamo_idx` (`int_prestamo`),
  CONSTRAINT `DetallePrestamoFKPrestamo` FOREIGN KEY (`int_prestamo`) REFERENCES `prestamo` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_prestamo`
--

LOCK TABLES `detalle_prestamo` WRITE;
/*!40000 ALTER TABLE `detalle_prestamo` DISABLE KEYS */;
INSERT INTO `detalle_prestamo` VALUES (1,1,'2014-04-30 05:00:00',500.00,250.00,500.00,250.00,'2'),(2,2,'2014-04-30 05:00:00',500.00,250.00,0.00,0.00,'1'),(3,3,'2014-04-30 05:00:00',300.00,50.00,300.00,0.00,'1'),(4,4,'2014-06-30 05:00:00',0.00,0.00,0.00,0.00,'1'),(5,5,'2014-06-30 05:00:00',600.00,200.00,100.00,40.00,'1'),(6,1,'2014-06-30 05:00:00',500.00,225.00,100.00,225.00,'1'),(7,2,'2014-06-30 05:00:00',500.00,250.00,0.00,0.00,'1'),(8,3,'2014-06-30 05:00:00',300.00,35.00,0.00,0.00,'1');
/*!40000 ALTER TABLE `detalle_prestamo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `int_id` int(11) NOT NULL,
  `dat_fechaRegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `int_tipodocumento` int(11) NOT NULL COMMENT 'FACTURA,BOLETA,TICKET',
  `var_serie` varchar(8) NOT NULL,
  `var_numero` varchar(16) NOT NULL,
  `int_tipooperacionInterna` int(11) NOT NULL COMMENT '1 VENTA , 2 PAGO POR CREDITO, 3 OTROS',
  `int_numerooperacionInterna` int(11) NOT NULL DEFAULT '0' COMMENT 'Numero de Operacion Interna, se refiere a el codigo de la venta',
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
INSERT INTO `documento` VALUES (1,'2014-04-19 19:35:31',1,'001','00001',1,3),(2,'2014-04-19 23:09:14',1,'001','00002',1,4),(3,'2014-04-20 18:37:01',1,'001','00003',1,5),(4,'2014-04-20 18:42:43',1,'001','00004',1,6),(5,'2014-04-25 08:18:49',1,'001','00005',1,7),(6,'2014-05-10 21:25:42',2,'0001','00000001',1,8),(7,'2014-05-10 23:25:11',2,'0001','00000002',1,9),(8,'2014-05-11 00:02:22',3,'0001','00000001',1,10),(9,'2014-05-25 18:47:36',2,'0001','00000003',1,11),(10,'2014-06-09 06:34:11',3,'0001','00000002',1,12),(11,'2014-06-09 06:49:36',2,'0001','00000004',1,13),(12,'2014-06-11 20:18:03',2,'0001','00000005',1,14);
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `get_lista_cliente_all`
--

DROP TABLE IF EXISTS `get_lista_cliente_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_cliente_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_cliente_all` (
  `id` tinyint NOT NULL,
  `cliente` tinyint NOT NULL,
  `direccion` tinyint NOT NULL,
  `dni` tinyint NOT NULL,
  `estado` tinyint NOT NULL,
  `telefono` tinyint NOT NULL,
  `ruc` tinyint NOT NULL,
  `fecharegistro` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lista_cronogramapago_all`
--

DROP TABLE IF EXISTS `get_lista_cronogramapago_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_cronogramapago_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_cronogramapago_all` (
  `int_id` tinyint NOT NULL,
  `id_ventacredito` tinyint NOT NULL,
  `int_numerocuota` tinyint NOT NULL,
  `dat_fechapago` tinyint NOT NULL,
  `dat_fechapagorealizado` tinyint NOT NULL,
  `dec_monto` tinyint NOT NULL,
  `dec_acuenta` tinyint NOT NULL,
  `estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lista_detalleprestamo_all`
--

DROP TABLE IF EXISTS `get_lista_detalleprestamo_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_detalleprestamo_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_detalleprestamo_all` (
  `int_id` tinyint NOT NULL,
  `mes` tinyint NOT NULL,
  `int_prestamo` tinyint NOT NULL,
  `dat_fechamaxpago` tinyint NOT NULL,
  `dec_montototal` tinyint NOT NULL,
  `dec_montocuota` tinyint NOT NULL,
  `dec_montointeres` tinyint NOT NULL,
  `dec_montopagado` tinyint NOT NULL,
  `dec_interespagado` tinyint NOT NULL,
  `estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lista_ordeningreso_all`
--

DROP TABLE IF EXISTS `get_lista_ordeningreso_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_ordeningreso_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_ordeningreso_all` (
  `id` tinyint NOT NULL,
  `serie` tinyint NOT NULL,
  `numero` tinyint NOT NULL,
  `fecha` tinyint NOT NULL,
  `comentario` tinyint NOT NULL,
  `id_proveedor` tinyint NOT NULL,
  `proveedor` tinyint NOT NULL,
  `id_motivo` tinyint NOT NULL,
  `Motivo` tinyint NOT NULL,
  `id_documento` tinyint NOT NULL,
  `Documento` tinyint NOT NULL,
  `Personal` tinyint NOT NULL,
  `Estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lista_ordensalida_all`
--

DROP TABLE IF EXISTS `get_lista_ordensalida_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_ordensalida_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_ordensalida_all` (
  `id` tinyint NOT NULL,
  `fecha` tinyint NOT NULL,
  `comentario` tinyint NOT NULL,
  `id_motivo` tinyint NOT NULL,
  `motivo` tinyint NOT NULL,
  `Cliente` tinyint NOT NULL,
  `id_personal` tinyint NOT NULL,
  `personal` tinyint NOT NULL,
  `cantidad` tinyint NOT NULL,
  `estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lista_prestamo_all`
--

DROP TABLE IF EXISTS `get_lista_prestamo_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_prestamo_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_prestamo_all` (
  `int_id` tinyint NOT NULL,
  `dat_fechacreacion` tinyint NOT NULL,
  `dec_montototal` tinyint NOT NULL,
  `int_cliente` tinyint NOT NULL,
  `cliente` tinyint NOT NULL,
  `int_tasa` tinyint NOT NULL,
  `dec_cuotames` tinyint NOT NULL,
  `int_modalidadpago` tinyint NOT NULL,
  `garantia` tinyint NOT NULL,
  `modalidad` tinyint NOT NULL,
  `estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lista_producto_all`
--

DROP TABLE IF EXISTS `get_lista_producto_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_producto_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_producto_all` (
  `id` tinyint NOT NULL,
  `codigobarra` tinyint NOT NULL,
  `producto` tinyint NOT NULL,
  `preciounitario` tinyint NOT NULL,
  `precioventa` tinyint NOT NULL,
  `Marca` tinyint NOT NULL,
  `Categoria` tinyint NOT NULL,
  `TipoProducto` tinyint NOT NULL,
  `stock_actual` tinyint NOT NULL,
  `stock_minimo` tinyint NOT NULL,
  `int_estado` tinyint NOT NULL,
  `estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lista_venta_all`
--

DROP TABLE IF EXISTS `get_lista_venta_all`;
/*!50001 DROP VIEW IF EXISTS `get_lista_venta_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lista_venta_all` (
  `id` tinyint NOT NULL,
  `fechaBusq` tinyint NOT NULL,
  `fecha` tinyint NOT NULL,
  `id_cliente` tinyint NOT NULL,
  `cliente` tinyint NOT NULL,
  `subtotal` tinyint NOT NULL,
  `descuento` tinyint NOT NULL,
  `igv` tinyint NOT NULL,
  `montoFacturado` tinyint NOT NULL,
  `montoPagado` tinyint NOT NULL,
  `saldo` tinyint NOT NULL,
  `personal` tinyint NOT NULL,
  `id_formapago` tinyint NOT NULL,
  `formaPago` tinyint NOT NULL,
  `id_tipodocumento` tinyint NOT NULL,
  `documento` tinyint NOT NULL,
  `serie` tinyint NOT NULL,
  `numero` tinyint NOT NULL,
  `var_estado` tinyint NOT NULL,
  `tipo_venta` tinyint NOT NULL,
  `estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_lsita_movimiento_stock_all`
--

DROP TABLE IF EXISTS `get_lsita_movimiento_stock_all`;
/*!50001 DROP VIEW IF EXISTS `get_lsita_movimiento_stock_all`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_lsita_movimiento_stock_all` (
  `fechaBusqueda` tinyint NOT NULL,
  `dat_fecharegistro` tinyint NOT NULL,
  `int_producto` tinyint NOT NULL,
  `producto` tinyint NOT NULL,
  `dec_cantidad` tinyint NOT NULL,
  `tipo` tinyint NOT NULL,
  `int_usuario` tinyint NOT NULL,
  `personal` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_cuadre_caja`
--

DROP TABLE IF EXISTS `get_reporte_cuadre_caja`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_cuadre_caja`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_cuadre_caja` (
  `fecha` tinyint NOT NULL,
  `Efectivo` tinyint NOT NULL,
  `Tarjeta` tinyint NOT NULL,
  `TipoOperacion` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_documento_cronogrampago`
--

DROP TABLE IF EXISTS `get_reporte_documento_cronogrampago`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_cronogrampago`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_documento_cronogrampago` (
  `id_cp` tinyint NOT NULL,
  `id_venta` tinyint NOT NULL,
  `dec_totalfacturado` tinyint NOT NULL,
  `dec_montocuota` tinyint NOT NULL,
  `int_nrocuota` tinyint NOT NULL,
  `cantidadprod` tinyint NOT NULL,
  `cliente` tinyint NOT NULL,
  `int_numerocuota` tinyint NOT NULL,
  `dat_fechapago` tinyint NOT NULL,
  `dat_fechapagorealizado` tinyint NOT NULL,
  `dec_monto` tinyint NOT NULL,
  `dec_acuenta` tinyint NOT NULL,
  `var_nombreapellido` tinyint NOT NULL,
  `FormaPago` tinyint NOT NULL,
  `estado` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_documento_ordeningreso`
--

DROP TABLE IF EXISTS `get_reporte_documento_ordeningreso`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_ordeningreso`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_documento_ordeningreso` (
  `int_id` tinyint NOT NULL,
  `TipoDocumento` tinyint NOT NULL,
  `documento` tinyint NOT NULL,
  `fechaEmision` tinyint NOT NULL,
  `Motivo` tinyint NOT NULL,
  `var_comentario` tinyint NOT NULL,
  `proveedor` tinyint NOT NULL,
  `ruc` tinyint NOT NULL,
  `responsable` tinyint NOT NULL,
  `producto` tinyint NOT NULL,
  `Marca` tinyint NOT NULL,
  `Categoria` tinyint NOT NULL,
  `TipoProducto` tinyint NOT NULL,
  `dec_cantidad` tinyint NOT NULL,
  `dec_preciounitario` tinyint NOT NULL,
  `dec_importe` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_documento_ordensalida`
--

DROP TABLE IF EXISTS `get_reporte_documento_ordensalida`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_ordensalida`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_documento_ordensalida` (
  `int_id` tinyint NOT NULL,
  `fechaEmision` tinyint NOT NULL,
  `Motivo` tinyint NOT NULL,
  `var_comentario` tinyint NOT NULL,
  `responsable` tinyint NOT NULL,
  `producto` tinyint NOT NULL,
  `Marca` tinyint NOT NULL,
  `Categoria` tinyint NOT NULL,
  `TipoProducto` tinyint NOT NULL,
  `dec_cantidad` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_documento_prestamo`
--

DROP TABLE IF EXISTS `get_reporte_documento_prestamo`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_prestamo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_documento_prestamo` (
  `int_prestamo` tinyint NOT NULL,
  `dat_fechacreacion` tinyint NOT NULL,
  `int_cliente` tinyint NOT NULL,
  `cliente` tinyint NOT NULL,
  `dec_montototal` tinyint NOT NULL,
  `int_tasa` tinyint NOT NULL,
  `dec_cuotames` tinyint NOT NULL,
  `int_modalidadpago` tinyint NOT NULL,
  `trabajador` tinyint NOT NULL,
  `modalidad` tinyint NOT NULL,
  `estado_prestamo` tinyint NOT NULL,
  `int_id` tinyint NOT NULL,
  `mes` tinyint NOT NULL,
  `dat_fechamaxpago` tinyint NOT NULL,
  `dec_montocuota` tinyint NOT NULL,
  `dec_montointeres` tinyint NOT NULL,
  `dec_montopagado` tinyint NOT NULL,
  `dec_interespagado` tinyint NOT NULL,
  `estado_detalle` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_documento_venta`
--

DROP TABLE IF EXISTS `get_reporte_documento_venta`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_venta`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_documento_venta` (
  `int_id` tinyint NOT NULL,
  `dec_subtotal` tinyint NOT NULL,
  `dec_igv` tinyint NOT NULL,
  `dec_igvmonto` tinyint NOT NULL,
  `dec_descuentoporcentaje` tinyint NOT NULL,
  `descuentoMonetario` tinyint NOT NULL,
  `dec_totalfacturado` tinyint NOT NULL,
  `dec_acuenta` tinyint NOT NULL,
  `dec_saldo` tinyint NOT NULL,
  `var_nombreapellido` tinyint NOT NULL,
  `fechaEmision` tinyint NOT NULL,
  `cliente` tinyint NOT NULL,
  `direccion` tinyint NOT NULL,
  `var_ruc` tinyint NOT NULL,
  `var_dni` tinyint NOT NULL,
  `FormaPago` tinyint NOT NULL,
  `documento` tinyint NOT NULL,
  `TipoDocumento` tinyint NOT NULL,
  `producto` tinyint NOT NULL,
  `Marca` tinyint NOT NULL,
  `Categoria` tinyint NOT NULL,
  `TipoProducto` tinyint NOT NULL,
  `dec_preciounitario` tinyint NOT NULL,
  `dec_cantidad` tinyint NOT NULL,
  `dec_importe` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_movimiento_caja`
--

DROP TABLE IF EXISTS `get_reporte_movimiento_caja`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_movimiento_caja`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_movimiento_caja` (
  `FechaRegistro` tinyint NOT NULL,
  `TipoOperacion` tinyint NOT NULL,
  `TipoPago` tinyint NOT NULL,
  `Usuario` tinyint NOT NULL,
  `motivo` tinyint NOT NULL,
  `Monto` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `get_reporte_venta_detallejcaja`
--

DROP TABLE IF EXISTS `get_reporte_venta_detallejcaja`;
/*!50001 DROP VIEW IF EXISTS `get_reporte_venta_detallejcaja`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `get_reporte_venta_detallejcaja` (
  `FechaRegistro` tinyint NOT NULL,
  `TipoOperacion` tinyint NOT NULL,
  `FormaPago` tinyint NOT NULL,
  `var_busqueda` tinyint NOT NULL,
  `MontoTotal` tinyint NOT NULL,
  `Acuenta` tinyint NOT NULL,
  `Saldo` tinyint NOT NULL,
  `Documento` tinyint NOT NULL,
  `TipoDocumento` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `lista_pagoprestamo`
--

DROP TABLE IF EXISTS `lista_pagoprestamo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista_pagoprestamo` (
  `int_id` int(11) NOT NULL,
  `int_detalleprestamo` int(11) NOT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dec_monto` decimal(9,2) NOT NULL,
  `int_usuario` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `Lista_PagoPrestamoFKDetallePrestamo_idx` (`int_detalleprestamo`),
  KEY `Lista_PagoPrestamoFKUsuario_idx` (`int_usuario`),
  CONSTRAINT `Lista_PagoPrestamoFKDetallePrestamo` FOREIGN KEY (`int_detalleprestamo`) REFERENCES `detalle_prestamo` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Lista_PagoPrestamoFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_pagoprestamo`
--

LOCK TABLES `lista_pagoprestamo` WRITE;
/*!40000 ALTER TABLE `lista_pagoprestamo` DISABLE KEYS */;
INSERT INTO `lista_pagoprestamo` VALUES (1,1,'2014-04-30 20:26:19',750.00,1),(2,3,'2014-04-30 22:52:33',300.00,1),(3,6,'2014-06-30 20:26:19',225.00,1),(4,6,'2014-06-12 20:26:19',100.00,1),(5,5,'2014-06-12 23:28:12',140.00,1);
/*!40000 ALTER TABLE `lista_pagoprestamo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `int_id` int(11) NOT NULL,
  `var_descripcion` varchar(200) NOT NULL,
  `var_estado` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (1,'marca #1','1'),(2,'marca #2','1'),(3,'marca #3','1'),(4,'marca #modificado','1');
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `int_id` int(11) NOT NULL,
  `int_clase` int(11) NOT NULL,
  `var_descripcion` varchar(500) DEFAULT NULL,
  `int_valor` int(11) NOT NULL,
  `var_estado` char(1) NOT NULL,
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimiento`
--

DROP TABLE IF EXISTS `movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimiento` (
  `int_id` int(11) NOT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dec_monto` decimal(9,2) NOT NULL,
  `int_venta` int(11) DEFAULT '0',
  `int_tipooperacion` int(11) NOT NULL,
  `var_motivo` varchar(500) NOT NULL,
  `int_tipodocumento` int(11) NOT NULL COMMENT 'FACTURA,BOLETA,TICKET',
  `var_documento` varchar(30) DEFAULT NULL,
  `int_tipopago` int(11) NOT NULL COMMENT 'EFECTIVO, TARJETA VISA, MASTER CARD',
  `var_estado` char(1) NOT NULL,
  `int_usuario` int(11) NOT NULL,
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimiento`
--

LOCK TABLES `movimiento` WRITE;
/*!40000 ALTER TABLE `movimiento` DISABLE KEYS */;
INSERT INTO `movimiento` VALUES (1,'2014-04-13 15:09:40',10.30,0,1,'Prueba de ingreso de Dinero	',1,'000 - 00000',1,'1',1),(2,'2014-04-19 00:52:28',0.00,1,2,'Venta Producto',1,'001 - 00001',1,'1',1),(3,'2014-04-19 19:26:27',0.00,2,2,'Venta Producto',1,'001 - 00001',1,'1',1),(4,'2014-04-19 19:34:57',36.11,3,2,'Venta Producto',1,'001 - 00001',1,'1',1),(5,'2014-04-19 23:09:15',886.77,4,2,'Venta Producto',1,'001 - 00002',1,'1',1),(6,'2014-04-20 18:37:01',0.00,5,2,'Venta Producto',1,'001 - 00003',1,'1',1),(7,'2014-04-20 18:42:43',0.00,6,2,'Venta Producto',1,'001 - 00004',1,'1',1),(8,'2014-04-25 00:28:21',14.44,6,2,'Pago de Cuota Nro id - 12',1,'000 - 00000',1,'1',1),(9,'2014-04-25 08:18:49',886.77,7,2,'Venta Producto',1,'001 - 00005',2,'1',1),(10,'2014-04-25 08:29:59',100.00,0,2,'ingreso de dinero por prueba ',1,'000 - 00000',1,'1',1),(11,'2014-04-25 08:30:36',50.00,0,2,'ingreso por prueba de tarjeta de',1,'000 - 00000',2,'1',1),(12,'2014-04-25 08:42:15',20.00,0,1,'saque de dinero para pago',1,'000 - 00000',1,'1',1),(13,'2014-05-10 19:37:13',20.00,0,1,'Compra de agua para la tienda',1,'000 - 00000',1,'1',1),(14,'2014-05-10 19:38:08',20.00,0,2,'Devolucion de un dinero para la caja',1,'000 - 00000',1,'1',1),(15,'2014-05-10 19:39:11',10.00,0,1,'Almuerzo de Richard',1,'000 - 00000',1,'1',1),(16,'2014-05-10 19:43:40',2.00,0,2,'Devolucion del gasto para la comida de Richard',1,'000 - 00000',1,'1',1),(17,'2014-05-10 21:25:42',24.00,8,2,'Venta Producto',2,'0001 - 00000001',1,'1',1),(18,'2014-05-10 21:26:52',5.00,9,2,'Venta Producto',2,'001 - 00006',2,'1',1),(19,'2014-05-11 00:02:22',48.00,10,2,'Venta Producto',3,'0001 - 00000001',2,'1',1),(20,'2014-05-25 18:47:37',177.00,11,2,'Venta Producto',2,'0001 - 00000003',1,'1',1),(21,'2014-06-09 05:08:24',100.00,0,2,'Ingreso de Dinero 100',1,'000 - 00000',1,'1',1),(22,'2014-06-09 06:34:12',48.00,12,2,'Venta Producto',3,'0001 - 00000002',1,'1',1),(23,'2014-06-09 06:49:37',25.00,13,2,'Venta Producto',2,'0001 - 00000004',1,'0',1),(24,'2014-06-11 20:18:04',12.00,14,2,'Venta Producto',2,'0001 - 00000005',1,'1',1),(25,'2014-06-11 22:36:47',12.00,0,1,'Retiro de Dinero para pago de compras de Productos a Proveedor',1,'000 - 00000',1,'1',1);
/*!40000 ALTER TABLE `movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orden_ingreso`
--

DROP TABLE IF EXISTS `orden_ingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orden_ingreso` (
  `int_id` int(11) NOT NULL,
  `var_documentoserie` varchar(6) DEFAULT '000000',
  `var_documentonumero` varchar(12) DEFAULT '000000000000',
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `var_comentario` varchar(500) NOT NULL,
  `int_motivo` int(11) NOT NULL,
  `int_proveedor` int(11) NOT NULL,
  `var_estado` char(1) NOT NULL,
  `int_usuario` int(11) NOT NULL,
  `int_tipodocumento` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `Orden_IngresoFKProveedor_idx` (`int_proveedor`),
  KEY `fk_Orden_Ingreso_Usuario2_idx` (`int_usuario`),
  CONSTRAINT `Orden_IngresoFKProveedor` FOREIGN KEY (`int_proveedor`) REFERENCES `proveedor` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Orden_IngresoFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orden_ingreso`
--

LOCK TABLES `orden_ingreso` WRITE;
/*!40000 ALTER TABLE `orden_ingreso` DISABLE KEYS */;
INSERT INTO `orden_ingreso` VALUES (1,'000000','000000000000','2014-04-14 18:17:27','',1,1,'1',1,1),(2,'000000','000000000000','2014-04-20 16:50:22','compra producto',1,2,'1',1,1),(3,'00001','000047588','2014-04-25 16:25:58','',1,1,'1',1,1),(4,'00002','000000458756','2014-04-25 16:38:03','compra',1,1,'1',1,1),(5,'000000','000000000000','2014-05-10 23:42:45','',1,1,'1',1,1),(6,'000000','000000000000','2014-06-11 22:24:51','-',1,1,'1',1,1);
/*!40000 ALTER TABLE `orden_ingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orden_salida`
--

DROP TABLE IF EXISTS `orden_salida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orden_salida` (
  `int_id` int(11) NOT NULL,
  `var_comentario` varchar(500) DEFAULT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `int_motivo` int(11) NOT NULL,
  `var_estado` char(1) NOT NULL,
  `int_usuario` int(11) NOT NULL,
  `int_venta` int(11) DEFAULT '0',
  PRIMARY KEY (`int_id`),
  KEY `OrdenSalidaFKUsuario_idx` (`int_usuario`),
  CONSTRAINT `OrdenSalidaFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orden_salida`
--

LOCK TABLES `orden_salida` WRITE;
/*!40000 ALTER TABLE `orden_salida` DISABLE KEYS */;
INSERT INTO `orden_salida` VALUES (1,'Traslado de Productos','2014-04-19 22:51:41',2,'1',1,0),(2,'venta realizado','2014-04-19 23:09:15',1,'1',1,4),(3,'venta realizado','2014-04-20 18:37:02',1,'1',1,5),(4,'venta realizado','2014-04-20 18:42:43',1,'1',1,6),(5,'venta realizado','2014-04-25 08:18:49',1,'1',1,7),(6,'venta realizado','2014-05-10 21:25:42',1,'1',1,8),(7,'venta realizado','2014-05-10 21:26:52',1,'1',1,9),(8,'venta realizado','2014-05-11 00:02:22',1,'1',1,10),(9,'venta realizado','2014-05-25 18:47:36',1,'1',1,11),(10,'venta realizado','2014-06-09 06:34:11',1,'1',1,12),(11,'venta realizado','2014-06-09 06:49:36',1,'1',1,13),(12,'venta realizado','2014-06-11 20:18:03',1,'1',1,14);
/*!40000 ALTER TABLE `orden_salida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestamo`
--

DROP TABLE IF EXISTS `prestamo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestamo` (
  `int_id` int(11) NOT NULL,
  `dat_fechacreacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `int_cliente` int(11) NOT NULL,
  `dec_montototal` decimal(9,2) NOT NULL,
  `int_tasa` int(11) NOT NULL,
  `dec_cuotames` decimal(9,2) NOT NULL,
  `int_modalidadpago` int(11) NOT NULL,
  `var_estado` char(1) NOT NULL,
  `int_usuario` int(11) NOT NULL,
  `var_garantia` varchar(300) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `PrestamoFKUsuario_idx` (`int_usuario`),
  KEY `PrestamoFKCliente_idx` (`int_cliente`),
  CONSTRAINT `PrestamoFKCliente` FOREIGN KEY (`int_cliente`) REFERENCES `cliente` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `PrestamoFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestamo`
--

LOCK TABLES `prestamo` WRITE;
/*!40000 ALTER TABLE `prestamo` DISABLE KEYS */;
INSERT INTO `prestamo` VALUES (1,'2014-04-21 17:09:31',2,5000.00,5,500.00,3,'1',1,''),(2,'2014-04-21 21:46:44',1,5000.00,5,500.00,1,'1',1,''),(3,'2014-04-21 21:51:07',3,1000.00,5,300.00,1,'1',1,''),(4,'2014-06-11 23:18:14',5,0.00,5,0.00,3,'1',1,'Deja de Garantia una Televisor de 4 pulgadas'),(5,'2014-06-12 00:57:34',5,4000.00,5,600.00,3,'1',1,'Dejo como garantia un tv de 21 pulgadas');
/*!40000 ALTER TABLE `prestamo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `int_id` int(11) NOT NULL,
  `var_codigoBarra` varchar(11) DEFAULT NULL,
  `int_tipo` int(11) NOT NULL,
  `int_categoria` int(11) NOT NULL,
  `dec_stockMinimo` decimal(9,2) NOT NULL,
  `dec_stockMaximo` decimal(9,2) NOT NULL,
  `int_estado` char(1) NOT NULL,
  `dec_preciounitario` decimal(9,2) NOT NULL,
  `dec_utilidadbrutamonetaria` decimal(9,2) DEFAULT NULL,
  `dec_utilidadbrutaporcentaje` decimal(9,1) DEFAULT NULL,
  `var_descripcion` varchar(500) NOT NULL,
  `int_marca` int(11) NOT NULL,
  `dec_precioventa` decimal(9,2) NOT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dec_stock` decimal(9,2) NOT NULL,
  `int_unidadMedida` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `ProductoCategoria_idx` (`int_categoria`),
  KEY `MarcaFKProducto_idx` (`int_marca`),
  CONSTRAINT `CategoriaFKProducto` FOREIGN KEY (`int_categoria`) REFERENCES `categoria` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `MarcaFKProducto` FOREIGN KEY (`int_marca`) REFERENCES `marca` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'P0000000003',1,1,10.00,150.00,'1',7.28,0.00,40.0,'producto #B',1,10.19,'2014-04-12 03:36:24',140.00,0),(2,'P0000000002',1,1,150.00,10.00,'1',8.60,0.00,10.0,'Producto #2',1,150.30,'2014-04-19 19:24:46',15.00,0),(3,'P0000000001',1,2,10.00,140.00,'1',0.00,0.00,0.0,'producto #A',3,15.30,'2014-04-20 15:24:17',124.00,0),(4,'P0000000004',5,2,30.00,200.00,'1',12.00,0.00,0.0,'Producto B',1,12.00,'2014-05-10 20:22:46',19.00,0),(5,'P0000000005',5,4,20.00,50.00,'1',2.75,0.00,0.0,'Producto 5',3,2.50,'2014-05-10 20:23:24',2.00,0);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `int_id` int(11) NOT NULL,
  `var_ruc` varchar(15) NOT NULL,
  `var_razonsocial` varchar(500) NOT NULL,
  `var_telefono` varchar(20) DEFAULT NULL,
  `var_direccion` varchar(500) DEFAULT NULL,
  `var_estado` char(1) NOT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`int_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'43001234565','Proveedor 1 S. A','980031860','Sin Direccion','1','2014-03-25 21:05:59'),(2,'03458719823','Proveedor 3 S.A','#980031860','Sin Dirección','1','2014-03-25 21:33:23');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaccion`
--

DROP TABLE IF EXISTS `transaccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaccion` (
  `int_venta` int(11) NOT NULL,
  `int_producto` int(11) NOT NULL,
  `dec_preciounitario` decimal(9,2) NOT NULL,
  `dec_cantidad` decimal(9,2) NOT NULL,
  `dec_importe` decimal(9,2) NOT NULL,
  `int_id` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `fk_transaccion_Venta1_idx` (`int_venta`),
  KEY `fk_transaccion_Producto1_idx` (`int_producto`),
  CONSTRAINT `fk_transaccion_Producto1` FOREIGN KEY (`int_producto`) REFERENCES `producto` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_transaccion_Venta1` FOREIGN KEY (`int_venta`) REFERENCES `venta` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaccion`
--

LOCK TABLES `transaccion` WRITE;
/*!40000 ALTER TABLE `transaccion` DISABLE KEYS */;
INSERT INTO `transaccion` VALUES (1,1,15.30,10.00,153.00,1),(2,2,150.30,1.00,150.30,2),(2,1,15.30,1.00,15.30,3),(3,1,15.30,2.00,30.60,4),(4,2,150.30,5.00,751.50,5),(5,3,15.30,12.00,183.60,6),(6,3,15.30,4.00,61.20,7),(7,2,150.30,5.00,751.50,8),(8,4,12.00,2.00,24.00,9),(9,5,2.50,2.00,5.00,10),(10,4,12.00,4.00,48.00,11),(11,3,15.30,10.00,153.00,12),(11,4,12.00,2.00,24.00,13),(12,4,12.00,4.00,48.00,14),(13,5,2.50,10.00,25.00,15),(14,4,12.00,1.00,12.00,16);
/*!40000 ALTER TABLE `transaccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `int_id` int(11) NOT NULL,
  `var_idname` varchar(30) NOT NULL,
  `var_password` varchar(20) NOT NULL,
  `var_nombreapellido` varchar(60) NOT NULL,
  `var_dni` varchar(8) NOT NULL,
  `var_telefono` varchar(15) NOT NULL,
  `var_estado` char(1) NOT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`int_id`),
  UNIQUE KEY `var_dni_UNIQUE` (`var_dni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'ADMINISTRADOR','1234','PERCY PLASENCIA','02369854','9563258741','1','2014-03-26 02:26:48');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_menu`
--

DROP TABLE IF EXISTS `usuario_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_menu` (
  `int_id` int(11) NOT NULL,
  `int_menu` int(11) NOT NULL,
  `var_estado` char(1) NOT NULL,
  `int_usuario` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `Usuario_MenuFKMenu_idx` (`int_menu`),
  KEY `Usuario_MenuFKUsuario_idx` (`int_usuario`),
  CONSTRAINT `Usuario_MenuFKMenu` FOREIGN KEY (`int_menu`) REFERENCES `menu` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Usuario_MenuFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_menu`
--

LOCK TABLES `usuario_menu` WRITE;
/*!40000 ALTER TABLE `usuario_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_generar_codigobarra_serie`
--

DROP TABLE IF EXISTS `v_generar_codigobarra_serie`;
/*!50001 DROP VIEW IF EXISTS `v_generar_codigobarra_serie`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_generar_codigobarra_serie` (
  `serie` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `int_id` int(11) NOT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `int_cliente` int(11) NOT NULL,
  `dec_totalfacturado` decimal(9,2) NOT NULL,
  `dec_subtotal` decimal(9,2) NOT NULL,
  `dec_descuentomonetario` decimal(9,2) NOT NULL,
  `dec_descuentoporcentaje` decimal(9,1) NOT NULL,
  `dec_acuenta` decimal(9,2) NOT NULL,
  `dec_saldo` decimal(9,2) NOT NULL,
  `var_estado` char(1) NOT NULL COMMENT '1 pagada 2 pendiente 0 anulada',
  `int_tipoventa` int(11) NOT NULL COMMENT 'CONTADO / CREDITO',
  `int_usuario` int(11) NOT NULL,
  `dec_igv` decimal(9,2) NOT NULL,
  `dec_igvmonto` decimal(9,2) NOT NULL,
  `int_formapago` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`int_id`),
  KEY `VentaFKCliente_idx` (`int_cliente`),
  KEY `fk_Venta_Usuario1_idx` (`int_usuario`),
  CONSTRAINT `VentaFKCliente` FOREIGN KEY (`int_cliente`) REFERENCES `cliente` (`int_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `VentaFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (1,'2014-04-19 00:52:28',1,180.54,0.00,0.00,0.0,0.00,180.54,'2',1,1,18.00,27.54,1),(2,'2014-04-19 19:26:27',1,195.41,165.60,0.00,0.0,0.00,195.41,'2',1,1,18.00,29.81,1),(3,'2014-04-19 19:34:57',2,36.11,30.60,0.00,0.0,36.11,0.00,'1',1,1,18.00,5.51,1),(4,'2014-04-19 23:09:14',1,886.77,751.50,0.00,0.0,886.77,0.00,'1',1,1,18.00,135.27,2),(5,'2014-04-20 18:37:01',2,216.65,183.60,0.00,0.0,0.00,216.65,'2',2,1,18.00,33.05,1),(6,'2014-04-20 18:42:42',2,72.22,61.20,0.00,0.0,14.44,57.78,'2',2,1,18.00,11.02,1),(7,'2014-04-25 08:18:49',1,886.77,751.50,0.00,0.0,886.77,0.00,'1',1,1,18.00,135.27,2),(8,'2014-05-10 21:25:42',5,24.00,24.00,0.00,0.0,24.00,0.00,'1',1,1,0.00,0.00,1),(9,'2014-05-10 21:26:52',4,5.00,5.00,0.00,0.0,5.00,0.00,'1',1,1,0.00,0.00,2),(10,'2014-05-11 00:02:22',5,48.00,48.00,0.00,0.0,48.00,0.00,'1',1,1,0.00,0.00,2),(11,'2014-05-25 18:47:36',5,177.00,177.00,0.00,0.0,177.00,0.00,'1',1,1,0.00,0.00,1),(12,'2014-06-09 06:34:11',2,48.00,39.36,0.00,0.0,48.00,0.00,'1',1,1,18.00,8.64,1),(13,'2014-06-09 06:49:36',4,25.00,25.00,0.00,0.0,25.00,0.00,'0',1,1,0.00,0.00,1),(14,'2014-06-11 20:18:03',5,12.00,12.00,0.00,0.0,12.00,0.00,'1',1,1,0.00,0.00,1);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta_anulado`
--

DROP TABLE IF EXISTS `venta_anulado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta_anulado` (
  `int_id` int(11) NOT NULL,
  `int_venta` int(11) NOT NULL,
  `dat_fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `var_motivo` varchar(500) NOT NULL,
  `int_usuario` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `VentaAnuladoFKVenta_idx` (`int_venta`),
  KEY `fk_Venta_Anulado_Usuario1_idx` (`int_usuario`),
  CONSTRAINT `Venta_AnuladoFKUsuario` FOREIGN KEY (`int_usuario`) REFERENCES `usuario` (`int_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Venta_AnuladoFKVenta` FOREIGN KEY (`int_venta`) REFERENCES `venta` (`int_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta_anulado`
--

LOCK TABLES `venta_anulado` WRITE;
/*!40000 ALTER TABLE `venta_anulado` DISABLE KEYS */;
INSERT INTO `venta_anulado` VALUES (1,13,'2014-06-09 06:53:27','prueba sistema',1);
/*!40000 ALTER TABLE `venta_anulado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta_credito`
--

DROP TABLE IF EXISTS `venta_credito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta_credito` (
  `int_id` int(11) NOT NULL,
  `int_nrocuota` int(11) NOT NULL,
  `dec_montocuota` decimal(9,2) NOT NULL,
  `int_formapago` int(11) NOT NULL COMMENT 'semanal,quincenal,mensual,otros',
  `int_frecuenciapago` int(11) NOT NULL COMMENT 'cada que tiempo, 15 o 30 o 45 días',
  `var_estado` char(1) NOT NULL COMMENT 'VIGENTE\nANULADO',
  `int_venta` int(11) NOT NULL,
  PRIMARY KEY (`int_id`),
  KEY `ventacreditoFKventa_idx` (`int_venta`),
  CONSTRAINT `ventacreditoFKventa` FOREIGN KEY (`int_venta`) REFERENCES `venta` (`int_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta_credito`
--

LOCK TABLES `venta_credito` WRITE;
/*!40000 ALTER TABLE `venta_credito` DISABLE KEYS */;
INSERT INTO `venta_credito` VALUES (1,10,21.66,2,15,'1',5),(2,5,14.44,1,7,'1',6);
/*!40000 ALTER TABLE `venta_credito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bd_inventario'
--

--
-- Dumping routines for database 'bd_inventario'
--
/*!50003 DROP FUNCTION IF EXISTS `fun_generar_numero_documento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fun_generar_numero_documento`(
tipodocumento int) RETURNS varchar(16) CHARSET latin1
BEGIN

	Declare result char(16);
	
	IF tipodocumento= 1 THEN
		set @result = (select 
						(case max(ifnull(d.var_numero,0))
							when
								'99999' then right(concat((ifnull(d.var_numero,0) + 2)),5)
							else right(concat('00000',(ifnull(max(d.var_numero),0) + 1)),5)
						end)
					from
						documento d
		where d.int_tipodocumento = tipodocumento);
	END IF;
	
	IF tipodocumento= 2 THEN
		set @result = (select 
						(case max(ifnull(d.var_numero,0))
							when
								'99999999' then right(concat((ifnull(d.var_numero,0) + 2)),8)
							else right(concat('00000000',(ifnull(max(d.var_numero),0) + 1)),8)
						end)
					from
						documento d
		where d.int_tipodocumento = tipodocumento);
	END IF;
	
	IF tipodocumento= 3 THEN
		set @result = (select 
						(case max(ifnull(d.var_numero,0))
							when
								'99999999' then right(concat((ifnull(d.var_numero,0) + 2)),8)
							else right(concat('00000000',(ifnull(max(d.var_numero),0) + 1)),8)
						end)
					from
						documento d
		where d.int_tipodocumento = tipodocumento);
	END IF;

	RETURN @result;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fun_generar_serie_documento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fun_generar_serie_documento`(
tipodocumento int
) RETURNS varchar(8) CHARSET latin1
BEGIN
Declare result varchar(8); 
	
	IF tipodocumento= 1 THEN
		set @result = ( select (case ifnull(max(d.var_serie),0)
			when
				'99999'
			then
				convert( right(concat('00000',
							(ifnull(max(d.var_serie),0) + 1)),3) using latin1)
			when 0 then convert( right(concat('00000', 1), 3) using latin1)
			else var_serie
		end)
		from
			documento d
		where d.int_tipodocumento = tipodocumento);
	END IF;

	IF tipodocumento= 2  THEN
		set @result = ( select (case ifnull(max(d.var_serie),0)
			when
				'9999'
			then
				convert( right(concat('0000',
							(ifnull(max(d.var_serie),0) + 1)),4) using latin1)
			when 0 then convert( right(concat('0000', 1), 4) using latin1)
			else var_serie
		end)
		from
			documento d
		where d.int_tipodocumento = tipodocumento);
	END IF;
	
	IF tipodocumento= 3 THEN
		set @result = ( select (case ifnull(max(d.var_serie),0)
			when
				'9999'
			then
				convert( right(concat('0000',
							(ifnull(max(d.var_serie),0) + 1)),4) using latin1)
			when 0 then convert( right(concat('0000', 1), 4) using latin1)
			else var_serie
		end)
		from
			documento d
		where d.int_tipodocumento = tipodocumento);
	END IF;
	

	RETURN @result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `fun_obtener_mes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `fun_obtener_mes`(
nro_mes int
) RETURNS varchar(12) CHARSET latin1
BEGIN
Declare result varchar(12); 
	
	set @result = (SELECT
					CASE nro_mes when 1 then "ENERO"
					when 2 then "FEBRERO"
					when 3 then "MARZO"
					when 4 then "ABRIL"
					when 5 then "MAYO"
					when 6 then "JUNIO"
					when 7 then "JULIO"
					when 8 then "AGOSTO"
					when 9 then "SEPTIEMBRE"
					when 10 then "OCTUBRE"
					when 11 then "NOVIEMBRE"
					when 12  then "DECIEMBRE"
					END ) ;


	RETURN @result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_generar_pagos_prestamos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_generar_pagos_prestamos`()
BEGIN
	DECLARE cantidad int;
	DECLARE contador int;

	SET @contador = 1;
	SET @fechaActual = (select current_date());
	
	CREATE TEMPORARY TABLE lista_temporal
	(
	Fila_id int(11) NOT NULL,
	int_prestamo int(11),
	dat_fechamaxpago date NOT NULL,
	dec_montototal decimal(9,3),
	dec_montototal_desc decimal(9,3),
	dec_montocuota decimal(9,2),
	dec_montointeres decimal(9,2),
	montoacumulado decimal(9,2),
	PRIMARY KEY (`Fila_id`),
	UNIQUE KEY `Fila_id` (`Fila_id`)
	) ;

	SET @i = 0;
	create temporary table adicional (
	SELECT (@i := @i + 1) Fila_id, 
	dp.int_prestamo,
	DATE(MAX(dp.dat_fechamaxpago)) as dat_fechamaxpago,
	p.dec_montototal,
	(p.dec_montototal-SUM(dp.dec_montopagado)) as dec_montototal_desc,
	dp.dec_montocuota, 
	CONVERT(((p.dec_montototal-SUM(dp.dec_montopagado))*(p.int_tasa/100)), decimal(9,2))  as dec_montointeres,
		   SUM(dp.dec_montopagado+dp.dec_interespagado) as montoacumulado 
	FROM detalle_prestamo dp
	inner join prestamo p on p.int_id = dp.int_prestamo
	where p.var_estado=1
	group by dp.int_prestamo);
	
	insert into lista_temporal 
	(Fila_id, 
	int_prestamo,
	dat_fechamaxpago,
	dec_montototal,
	dec_montototal_desc,
	dec_montocuota,
	dec_montointeres,
	montoacumulado) 
	(select Fila_id,
	int_prestamo,
	dat_fechamaxpago,
	dec_montototal,
	dec_montototal_desc,
	dec_montocuota,
	dec_montointeres,
	montoacumulado 
	from adicional);

	SET @cantidad = ( SELECT COUNT(*) FROM lista_temporal);

	WHILE @cantidad >= @contador DO
		set @fechalt = ( SELECT dat_fechamaxpago FROM 
				         lista_temporal where Fila_id = @contador);
		set @montoTotal = ( SELECT dec_montototal FROM 
				         lista_temporal where Fila_id = @contador);
		set @montoAcumulado = ( SELECT montoacumulado FROM 
				         lista_temporal where Fila_id = @contador);

		IF @fechaActual>= @fechalt  THEN
			IF @montoAcumulado<@montoTotal THEN
		   
			set @id_prestamo = ( SELECT int_prestamo FROM 
				         lista_temporal where Fila_id = @contador);
		   set @montocuota = ( SELECT dec_montocuota FROM 
				         lista_temporal where Fila_id = @contador);

		   set @montointeres = ( SELECT dec_montointeres FROM 
				         lista_temporal where Fila_id = @contador);

			call `sp_insert_detalle_prestamo`( @id_prestamo,
				  (select last_day(@fechaActual)),IFNULL(@montocuota,0),IFNULL(@montointeres,0));

			delete from lista_temporal where Fila_id = @contador;
			END IF;
		END IF;
		
		SET @contador=@contador+1;
		SET @cantidad = ( SELECT COUNT(*) FROM lista_temporal );

	END WHILE;
		
	DROP TEMPORARY TABLE adicional;
	DROP TEMPORARY TABLE lista_temporal;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_cierre_caja` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cierre_caja`(
IN id_caja int
)
BEGIN
	
	set @ingreso_efectivo = (select efectivo from get_reporte_cuadre_caja 
						 where fecha = current_date()  and TipoOperacion='INGRESO');
	set @ingreso_tarjeta = (select Tarjeta from get_reporte_cuadre_caja 
							 where fecha = current_date()  and TipoOperacion='INGRESO');
	set @deposito_efectivo = (select efectivo from get_reporte_cuadre_caja 
							 where fecha = current_date()  and TipoOperacion='DEPOSITO');
	set @deposito_tarjeta = (select Tarjeta from get_reporte_cuadre_caja 
							 where fecha = current_date()  and TipoOperacion='DEPOSITO');
	set @retiro_efectivo = (select efectivo from get_reporte_cuadre_caja 
							 where fecha = current_date()  and TipoOperacion='RETIRO');
	set @retiro_tarjeta = (select Tarjeta from get_reporte_cuadre_caja 
							 where fecha = current_date()  and TipoOperacion='RETIRO');

	set @total_ingreso = (@ingreso_efectivo + @deposito_efectivo+@deposito_tarjeta + @ingreso_tarjeta);
	set @total_egreso = (@retiro_tarjeta + @retiro_efectivo);
	-- set @totalcaja = (@ingreso_efectivo + @deposito_efectivo) - (@retiro_tarjeta + @deposito_tarjeta + @ingreso_tarjeta);

	select @total_ingreso,@total_egreso,
		  @totalcaja;
	set @fecha = (select DATE(c.dat_fechaInicio) from caja c where c.int_id=2);

	UPDATE `bd_inventario`.`caja`
	SET
	`dat_fechaCierre` = current_date(),
	`dec_ingreso` = @total_ingreso,
	`dec_egreso` = @total_egreso,
	`var_estado` = '2'
	WHERE `int_id` = id_caja;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_detalleprestamo_byId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_detalleprestamo_byId`(IN 
id int)
BEGIN

	set @fila=0;
	select 
        `dp`.`int_id` AS `int_id`,
        (select fun_obtener_mes(month(`dp`.`dat_fechamaxpago`))) AS `mes`,
        cast(`dp`.`dat_fechamaxpago` as date) AS `fecha_pagar`,
		@fila:=@fila+1 as nroCuota,
        `dp`.`dec_montocuota` AS `cuota`,
        `dp`.`dec_montointeres` AS `interes_mensual`,
        (`dp`.`dec_montocuota`+`dp`.`dec_montointeres`) AS `monto_apagar`,
		(dp.dec_montopagado + dp.dec_interespagado) as monto_pagado,
		(p.dec_montototal - SUM(dp.dec_montopagado + dp.dec_interespagado)) as monto_restante ,
        (case `dp`.`var_estado`
            when 0 then 'Anulada'
            when 1 then 'Pendiente'
            when 2 then 'Cancelada'
        end) AS `estado`
    from
        (`detalle_prestamo` `dp`
        join `prestamo` `p` ON ((`p`.`int_id` = `dp`.`int_prestamo`)))
	where p.int_id = id
	group by int_id, mes,fecha_pagar,cuota;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_producto_byCodigoBarra` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_producto_byCodigoBarra`(
IN codigoBarra VARCHAR(11)
)
BEGIN
	select * from producto p where p.var_codigoBarra = codigoBarra;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_reporte_prestamo_byId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_reporte_prestamo_byId`(IN 
id int)
BEGIN
		set @fila=0;
	select 
		`p`.`int_id` AS `int_prestamo`,
        cast(`p`.`dat_fechacreacion` as date) AS `dat_fechacreacion`,
        `p`.`int_cliente` AS `int_cliente`,
        `c`.`var_busqueda` AS `cliente`,
        `p`.`dec_montototal` AS `dec_montototal`,
        `dp`.`int_id` AS `int_id`,
		`p`.`int_tasa` AS `int_tasa`,
		`p`.`dec_cuotames` AS `dec_cuotames`,
        `p`.`int_modalidadpago` AS `int_modalidadpago`,
        `u`.`var_nombreapellido` AS `trabajador`,
		  (select 
                `c`.`var_descripcion`
            from
                `constante` `c`
            where
                ((`c`.`int_valor` = `p`.`int_modalidadpago`)
                    and (`c`.`int_clase` = 8))) AS `modalidad`,
        (case `p`.`var_estado`
            when 0 then 'Anulada'
            when 1 then 'Pendiente'
            when 2 then 'Cancelada'
        end) AS `estado_prestamo`,
        (select fun_obtener_mes(month(`dp`.`dat_fechamaxpago`))) AS `mes`,
        cast(`dp`.`dat_fechamaxpago` as date) AS `fecha_pagar`,
		@fila:=@fila+1 as nroCuota,
        `dp`.`dec_montocuota` AS `cuota`,
        `dp`.`dec_montointeres` AS `interes_mensual`,
        (`dp`.`dec_montocuota`+`dp`.`dec_montointeres`) AS `monto_apagar`,
		(dp.dec_montopagado + dp.dec_interespagado) as monto_pagado,
		(p.dec_montototal - SUM(dp.dec_montopagado + dp.dec_interespagado)) as monto_restante ,
        (case `dp`.`var_estado`
            when 0 then 'Anulada'
            when 1 then 'Pendiente'
            when 2 then 'Cancelada'
        end) AS `estado`
    from
        `detalle_prestamo` `dp`
        join `prestamo` `p` ON ((`p`.`int_id` = `dp`.`int_prestamo`))
		join `cliente` `c` ON ((`c`.`int_id` = `p`.`int_cliente`))
        join `usuario` `u` ON ((`u`.`int_id` = `p`.`int_usuario`))
	where p.int_id = id
	group by int_id, mes,fecha_pagar,cuota;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_serienumero_documento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_serienumero_documento`(
IN tipodocumento int
)
BEGIN

	DECLARE cantReg int;
	set @cantReg= (SELECT COUNT(*) FROM documento d where d.int_tipodocumento =tipodocumento);

	IF (@cantReg > 0) THEN
		select fun_generar_serie_documento(tipodocumento) as serie, 
		fun_generar_numero_documento(tipodocumento) as numero;
	ELSE
		select max(cd.var_serie) as serie, 
		max(cd.var_numero) as numero
		from codigo_documento cd
		where cd.int_tipo = tipodocumento
		order by 1 desc;
	END IF;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_caja` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_caja`(
IN usuario int,
IN saldoinicial decimal(9,2)
)
BEGIN

	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `caja`);

	INSERT INTO `caja`
	(`int_id`,
	`dat_fechaInicio`,
	`dat_fechaCierre`,
	`int_usuario`,
	`dec_saldoinicial`,
	`dec_ingreso`,
	`dec_egreso`,
	`var_estado`)
	VALUES
	(@id,
	CURRENT_TIMESTAMP(),
	null,
	usuario,
	saldoinicial,
	0.0,
	0.0,
	'1');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_categoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_categoria`(
IN descripcion varchar(200),
IN estado nchar(1)
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `categoria`);
	
	INSERT INTO `categoria`
	(`int_id`,
	`var_descripcion`,
	`var_estado`)
	VALUES
	(@id,
	descripcion,
	estado);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_cliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_cliente`(
IN nombre VARCHAR(45),
IN dni char(8),
IN appaterno VARCHAR(45),
IN apmaterno VARCHAR(45),
IN direccion VARCHAR(45),
IN telefono VARCHAR(45),
IN busqueda VARCHAR(200),
IN ruc VARCHAR(11)
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `cliente`);

	INSERT INTO `cliente`
	(`int_id`,
	`var_nombre`,
	`var_dni`,
	`var_apepaterno`,
	`var_apematerno`,
	`var_direccion`,
	`var_telefono`,
	`dat_fecharegistro`,
	`var_busqueda`,
	`var_estado`,
	`var_ruc`)
	VALUES
	(@id,
	nombre,
	dni,
	appaterno,
	apmaterno,
	direccion,
	telefono,
	(select current_timestamp()),
	busqueda,
	'1',
	ruc);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_codigo_documento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_codigo_documento`(
IN tipodocumento int,
IN seriedoc varchar(8),
IN numerodoc varchar(12)
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `codigo_documento`);

	INSERT INTO `codigo_documento`
	(`int_id`,
	`int_tipo`,
	`var_serie`,
	`var_numero`)
	VALUES
	(@id,
	tipodocumento,
	seriedoc,
	numerodoc);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_constante` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_constante`(
IN descripcion varchar(100),
IN clase int,
INOUT codigo int
)
BEGIN

	DECLARE ultimo_valor int;
	DECLARE ultimo_id int;
	set @ultimo_id = (SELECT (MAX(int_id)+1) as id FROM constante);
	set @ultimo_valor = (SELECT (MAX(int_valor)+1) as valor FROM constante 
						 WHERE int_clase=clase);
	
	INSERT INTO `bd_inventario`.`constante`
	(`int_id`,
	`var_descripcion`,
	`int_clase`,
	`int_valor`)
	VALUES
	(@ultimo_id,
	descripcion,
	clase,
	@ultimo_valor);

	set codigo = @ultimo_id;
	select codigo;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_cronogramapago` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_cronogramapago`(
in numerocuota int,
in fechapago timestamp,
in monto decimal(9,2),
in acuenta decimal(9,2),
in estado char(1),
in ventacredito int
)
BEGIN

	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `cronograma_pago`);

	INSERT INTO `cronograma_pago`
	(`int_id`,
	`int_numerocuota`,
	`dat_fecharegistro`,
	`dat_fechapago`,
	`dat_fechapagorealizado`,
	`dec_monto`,
	`dec_acuenta`,
	`var_estado`,
	`int_ventacredito`)
	VALUES
	(@id,
	numerocuota,
	current_timestamp(),
	fechapago,
	null,
	monto,
	acuenta,
	estado,
	ventacredito);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_detalle_ordeningreso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_detalle_ordeningreso`(
IN precioUnitario decimal(9,2),
IN cantidad decimal(9,2),
IN importe decimal(9,2),
IN producto int,
IN ordenIngreso int
)
BEGIN

	DECLARE id int;
    DECLARE stockActual decimal(9,2);
	DECLARE precioUnit decimal(9,2);

	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `detalle_ordeningreso`);
	
	INSERT INTO `detalle_ordeningreso`
	(`int_id`,
	`dec_preciounitario`,
	`dec_cantidad`,
	`dec_importe`,
	`int_producto`,
	`int_ordeningreso`,
	`var_estado`)
	VALUES
	(@id,
	precioUnitario,
	cantidad,
	importe,
	producto,
	ordenIngreso,
	'1');

	set @stockActual = (SELECT p.dec_stock FROM Producto p 
						WHERE p.int_id = producto);
	set @precioUnit = (SELECT p.dec_preciounitario FROM Producto p 
						WHERE p.int_id = producto);
	set @porcentaje = ((SELECT p.dec_utilidadbrutaporcentaje FROM Producto p 
						WHERE p.int_id = producto)/100);
	set @precioventaAnt = (SELECT p.dec_precioventa FROM Producto p 
						WHERE p.int_id = producto);
	set @precioventa = if (@porcentaje > 0,(((precioUnitario+@precioUnit)/2)*@porcentaje)+((precioUnitario+@precioUnit)/2),@precioventaAnt);
	
	set @precio_Unitario = if (@precioUnit > 0,((precioUnitario+@precioUnit)/2),precioUnitario);

	UPDATE `producto`
	SET
	`dec_preciounitario` = @precio_Unitario,
	`dec_stock` = (@stockActual + cantidad),
	dec_precioventa = @precioventa
	WHERE `int_id` = producto;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_detalle_ordensalida` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_detalle_ordensalida`(
IN cantidad decimal(9,2),
IN producto int,
IN ordensalida int,
IN estado char(1)
)
BEGIN

	DECLARE id int;
	DECLARE stockActual decimal(9,2);
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `detalle_ordensalida`);

	INSERT INTO `detalle_ordensalida`
	(`int_id`,
	`dec_cantidad`,
	`int_producto`,
	`int_ordensalida`,
	`var_estado`)
	VALUES
	(@id,
	cantidad,
	producto,
	ordensalida,
	estado);

	set @stockActual = (SELECT p.dec_stock FROM Producto p 
						WHERE p.int_id = producto);
	
	UPDATE `producto`
	SET
	`dec_stock` = (@stockActual - cantidad)
	WHERE `int_id` = producto;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_detalle_prestamo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_detalle_prestamo`(
in prestamo int,
in fechamaxpago timestamp,
in montocuota decimal(9,2),
in montointeres decimal(9,2)
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `detalle_prestamo`);

	INSERT INTO `detalle_prestamo`
	(`int_id`,
	`int_prestamo`,
	`dat_fechamaxpago`,
	`dec_montocuota`,
	`dec_montointeres`,
	`dec_montopagado`,
	`dec_interespagado`,
	`var_estado`)
	VALUES
	(@id,
	prestamo,
	fechamaxpago,
	montocuota,
	montointeres,
	0.0,
	0.0,
	'1');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_documento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_documento`(
in tipodocumento int,
in serie VARCHAR(10),
in numero VARCHAR(20),
in tipooperacioninterna int,
in numoperacioninterna int
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `documento`);

		INSERT INTO `documento`
		(`int_id`,
		`dat_fechaRegistro`,
		`int_tipodocumento`,
		`var_serie`,
		`var_numero`,
		`int_tipooperacionInterna`,
		`int_numerooperacionInterna`)
		VALUES
		(
		@id,
		CURRENT_TIMESTAMP(),
		tipodocumento,
		serie,
		numero,
		tipooperacioninterna,
		numoperacioninterna);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_lista_pagoprestamo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_lista_pagoprestamo`(
in detalleprestamo int,
in montocuota decimal(9,2),
in montoInteres decimal(9,2),
in usuario int
)
BEGIN

	DECLARE id int;
	DECLARE montoC decimal(9,2);
	DECLARE montoI decimal(9,2);
	DECLARE monto1 decimal(9,2);
	DECLARE monto2 decimal(9,2);

	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `lista_pagoprestamo`);
	
	INSERT INTO `lista_pagoprestamo`
	(`int_id`,
	`int_detalleprestamo`,
	`dat_fecharegistro`,
	`dec_monto`,
	int_usuario)
	VALUES
	(@id,
	detalleprestamo,
	CURRENT_TIMESTAMP(),
	(montocuota+montoInteres),
	usuario);

	set @montoC = (select dec_montocuota  from detalle_prestamo where int_id = detalleprestamo);
	set @montoI = (select dec_montointeres  from detalle_prestamo where int_id = detalleprestamo);
	set @monto1 = (select dec_montopagado from detalle_prestamo where int_id = detalleprestamo);
	set @monto2 = (select dec_interespagado from detalle_prestamo where int_id = detalleprestamo);
	
	UPDATE `detalle_prestamo`
	SET
	`dec_montopagado` = (@monto1+montocuota),
	`dec_interespagado` = (@monto2+montoInteres),
	`var_estado` = if ((@monto1+montocuota)>=@montoC and (@monto2+montoInteres)>=@montoI ,'2','1')
	WHERE `int_id` = detalleprestamo;

	set @id_prestamo = (SELECT DISTINCT
	p.int_id
	FROM detalle_prestamo dp
	inner join prestamo p on p.int_id = dp.int_prestamo
	where p.var_estado=1 and dp.int_id = detalleprestamo
	group by dp.int_prestamo);
	
	set @montototal = (SELECT
	p.dec_montototal
	FROM detalle_prestamo dp
	inner join prestamo p on p.int_id = dp.int_prestamo
	where p.var_estado=1 and dp.int_id = detalleprestamo
	group by dp.int_prestamo);

	set @montoacumulado = (SELECT
	SUM(dp.dec_montopagado+dp.dec_interespagado)
	FROM detalle_prestamo dp
	inner join prestamo p on p.int_id = dp.int_prestamo
	where p.var_estado=1 and dp.int_id = detalleprestamo
	group by dp.int_prestamo);

	UPDATE `bd_inventario`.`prestamo`
	SET
	`var_estado` = if (@montoacumulado>=@montototal,'2','1')
	WHERE `int_id` = @id_prestamo;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_marca` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_marca`(
IN descripcion varchar(200),
IN estado nchar(1)
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `marca`);
	
	INSERT INTO `marca`
	(`int_id`,
	`var_descripcion`,
	`var_estado`)
	VALUES
	(@id,
	descripcion,
	estado);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_movimiento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_movimiento`(
IN monto decimal(9,2),
IN venta int,
IN tipooperacion int,
IN motivo varchar(500),
IN tipodocumento int,
IN documento varchar(30),
IN tipopago int,
IN usuario int
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `movimiento`);

	INSERT INTO `movimiento`
	(`int_id`,
	`dat_fecharegistro`,
	`dec_monto`,
	`int_venta`,
	`int_tipooperacion`,
	`var_motivo`,
	`int_tipodocumento`,
	`var_documento`,
	`int_tipopago`,
	 int_usuario,
	`var_estado`)
	VALUES
	(@id,
	CURRENT_TIMESTAMP(),
	monto,
	venta,
	tipooperacion,
	motivo,
	tipodocumento,
	documento,
	tipopago,
	usuario,
	'1');

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_ordeningreso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_ordeningreso`(
IN docserie VARCHAR(6),
IN docnumero VARCHAR(12),
In comentario VARCHAR(500),
IN motivo int,
IN proveedor int,
IN usuario int,
IN tipodocumento int,
IN fechaEmision timestamp,
IN monto_total decimal(9,2),
INOUT codigo int
)
BEGIN
	
	DECLARE id int;
	DECLARE id_mov int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `orden_ingreso`);

	INSERT INTO `orden_ingreso`
	(`int_id`,
	`var_documentoserie`,
	`var_documentonumero`,
	`dat_fecharegistro`,
	`var_comentario`,
	`int_motivo`,
	`int_proveedor`,
	`var_estado`,
	`int_usuario`,
	`int_tipodocumento`)
	VALUES
	(@id,
	docserie,
	docnumero,
	fechaEmision,
	comentario,
	motivo,
	proveedor,
	'1',
	usuario,
	tipodocumento);

	IF motivo = 1 THEN

		set @id_mov = (select (IFNULL(max(int_id),0) + 1) as id from `movimiento`);

		INSERT INTO `movimiento`
		(`int_id`,
		`dat_fecharegistro`,
		`dec_monto`,
		`int_venta`,
		`int_tipooperacion`,
		`var_motivo`,
		`int_tipodocumento`,
		`var_documento`,
		`int_tipopago`,
		 int_usuario,
		`var_estado`)
		VALUES
		(@id_mov,
		CURRENT_TIMESTAMP(),
		monto_total,
		0,
		1,
		'Retiro de Dinero para pago de compras de Productos a Proveedor',
		tipodocumento,
		'000 - 00000',
		1,
		usuario,
		'1');

	END IF;	

	set codigo = @id;
	select codigo;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_ordensalida` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_ordensalida`(
IN comentario VARCHAR(500),
IN fechaRegistro TIMESTAMP,
IN motivo INT,
IN estado CHAR(1),
IN usuario INT,
IN idventa INT,
INOUT codigo int
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `orden_salida`);

	INSERT INTO `orden_salida`
	(`int_id`,
	`var_comentario`,
	`dat_fecharegistro`,
	`int_motivo`,
	`var_estado`,
	`int_usuario`,
	 int_venta)
	VALUES
	(@id,
	comentario,
	fechaRegistro,
	motivo,
	estado,
	usuario,
	idventa);

	set codigo = @id;
	select codigo;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_prestamo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_prestamo`(
IN cliente int,
IN montototal decimal(9,2),
IN tasa int,
IN cuotames decimal(9,2),
IN modalidadpago int,
IN estado char(1),
IN usuario int,
IN garantia varchar(300)
)
BEGIN

	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `prestamo`);

	INSERT INTO `prestamo`
	(`int_id`,
	`dat_fechacreacion`,
	`int_cliente`,
	`dec_montototal`,
	`int_tasa`,
	`dec_cuotames`,
	`int_modalidadpago`,
	`var_estado`,
	`int_usuario`,
	 var_garantia)
	VALUES
	(@id,
	CURRENT_TIMESTAMP(),
	cliente,
	montototal,
	tasa,
	cuotames,
	modalidadpago,
	estado,
	usuario,
	garantia);
	-- Inicializar detalle prestamos , generando el primer mes
	call `sp_insert_detalle_prestamo`( @id, (select LAST_DAY(NOW())),
			cuotames, CONVERT((tasa/100)*montototal, decimal(9,2))
	);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_producto`(
IN codigoBarra varchar(11),
IN tipo int,
IN categoria int,
IN stockMinimo decimal(9,2),
IN stockMaximo decimal(9,2),
IN preciounitario decimal(9,2),
IN utilidadMonetaria decimal(9,2),
IN utilidadPorcentaje decimal(9,2),
IN descripcion varchar(500),
IN marca int,
IN precioventa decimal(9,2),
IN stock decimal(9,2),
INOUT codigo int
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `producto`);

	INSERT INTO `producto`
	(`int_id`,
	`var_codigoBarra`,
	`int_tipo`,
	`int_categoria`,
	`dec_stockMinimo`,
	`dec_stockMaximo`,
	`int_estado`,
	`dec_preciounitario`,
	`dec_utilidadbrutamonetaria`,
	`dec_utilidadbrutaporcentaje`,
	`var_descripcion`,
	`int_marca`,
	`dec_precioventa`,
	`dat_fecharegistro`,
	`dec_stock`)
	VALUES
	(@id,
	codigoBarra,
	tipo,
	categoria,
	stockMinimo,
	stockMaximo,
	'1',
	preciounitario,
	utilidadMonetaria,
	utilidadPorcentaje,
	descripcion,
	marca,
	precioventa,
	CURRENT_TIMESTAMP(),
	stock);
	
	set codigo = @id;
	select codigo;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_proveedor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_proveedor`(
IN ruc varchar(15),
IN razonsocial varchar(500),
IN telefono varchar(20),
IN direccion varchar(500),
INOUT codigo INT
)
BEGIN

	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `bd_inventario`.`proveedor`);

	INSERT INTO `bd_inventario`.`proveedor`
	(`int_id`,
	`var_ruc`,
	`var_razonsocial`,
	`var_telefono`,
	`var_direccion`,
	`var_estado`,
	`dat_fecharegistro`)
	VALUES
	(@id,
	ruc,
	razonsocial,
	telefono,
	direccion,
	'1',
	current_timestamp());

	set codigo = @id;
	select codigo;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_transaccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_transaccion`(
IN venta int,
IN producto int,
IN preciounitario decimal(9,2),
IN cantidad decimal(9,2),
IN importe decimal(9,2)
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `transaccion`);

	INSERT INTO `transaccion`
	(`int_venta`,
	`int_producto`,
	`dec_preciounitario`,
	`dec_cantidad`,
	`dec_importe`,
	`int_id`)
	VALUES
	(venta,
	producto,
	preciounitario,
	cantidad,
	importe,
	@id);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_usuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_usuario`(
IN dni varchar(8),
IN nomape varchar(60),
IN idname varchar(30),
IN pass varchar(30),
IN telefono varchar(30),
INOUT codigo int
)
BEGIN

	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `bd_inventario`.`usuario`);

	INSERT INTO `bd_inventario`.`usuario`
	(`int_id`,
	`var_idname`,
	`var_password`,
	`var_nombreapellido`,
	`var_dni`,
	`var_telefono`,
	`var_estado`,
	`dat_fecharegistro`)
	VALUES
	(@id,
	idname,
	pass,
	nomape,
	dni,
	telefono,
	'1',
	current_timestamp());

	set codigo = @id;
	select codigo;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_venta` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_venta`(
in cliente int,
in totalfacturado decimal(9,2),
in subtotal decimal(9,2),
in descuentoporce decimal(9,2),
in descuentomonetario decimal(9,2),
in acuenta decimal(9,2),
in saldo decimal(9,2),
in tipoventa int,
in usuario int,
in igv decimal(9,2),
in igvmonto decimal(9,2),
in estado char(1),
in formapago int,
inout codigo int
)
BEGIN
	
	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `venta`);

	INSERT INTO `venta`
	(`int_id`,
	`dat_fecharegistro`,
	`int_cliente`,
	`dec_totalfacturado`,
	`dec_subtotal`,
	`dec_descuentomonetario`,
	`dec_descuentoporcentaje`,
	`dec_acuenta`,
	`dec_saldo`,
	`var_estado`,
	`int_tipoventa`,
	dec_igv,
	dec_igvmonto,
	`int_usuario`,
	int_formapago)
	VALUES
	(@id,
	CURRENT_TIMESTAMP(),
	cliente,
	totalfacturado,
	subtotal,
	descuentomonetario,
	descuentoporce,
	acuenta,
	saldo,
	estado,
	tipoventa,
	igv,
	igvmonto,
	usuario,
	formapago);

	set codigo = @id;
	select codigo;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_venta_anulado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_venta_anulado`(
in id_venta int,
in motivo varchar(500),
in usuario int
)
BEGIN

	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `venta_anulado`);

	INSERT INTO `venta_anulado`
	(`int_id`,
	`int_venta`,
	`dat_fecharegistro`,
	`var_motivo`,
	`int_usuario`)
	VALUES
	(@id,
	id_venta,
	CURRENT_TIMESTAMP(),
	motivo,
	usuario);
	
	UPDATE venta
	SET `var_estado`=0
	WHERE int_id =id_venta ;

	SET @id_mov = (select int_id from movimiento where int_venta = id_venta);

	UPDATE movimiento
	SET `var_estado`=0
	WHERE int_id = @id_mov;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_venta_credito` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_venta_credito`(
in nrocuota int,
in montocuota decimal(9,2),
in formapago int,
in frecuenciapago int,
in estado char(1),
in idventa int,
inout codigo int
)
BEGIN

	DECLARE id int;
	set @id = (select (IFNULL(max(int_id),0) + 1) as id from `venta_credito`);

	INSERT INTO `venta_credito`
	(`int_id`,
	`int_nrocuota`,
	`dec_montocuota`,
	`int_formapago`,
	`int_frecuenciapago`,
	`var_estado`,
	`int_venta`)
	VALUES
	(@id,
	nrocuota,
	montocuota,
	formapago,
	frecuenciapago,
	estado,
	idventa);

	set codigo = @id;
	select codigo;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_lista_producto_byCondicion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_lista_producto_byCondicion`(
IN palabra varchar(200),
IN condicion int
)
BEGIN

	IF condicion = 1 then -- Descripcion
		select * from producto as p 
		where (p.var_estado<>0 and p.var_descripcion like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;

	IF condicion = 2 then -- Tipo Producto
		select * from producto as p 
		inner join constante c on c.int_valor = p.int_tipo and c.int_clase = 1
		where (p.var_estado<>0 and c.var_descripcion like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;

	IF condicion = 3 then -- Categoria
		select * from producto as p
		inner join constante c on c.int_valor = p.int_tipo and c.int_clase = 2
		where (p.var_estado<>0 and p.var_descripcion like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;

	IF condicion = 4 then -- Marca
		select * from producto as p
		inner join constante c on c.int_valor = p.int_tipo and c.int_clase = 3
		where (p.var_estado<>0 and p.var_idname like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;

	IF condicion = 5 then -- Codigo Barra
		select * from producto as p
		where (p.var_estado<>0 and p.int_codigoBarra like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_lista_proveedor_byCondicion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_lista_proveedor_byCondicion`(
IN palabra varchar(200),
IN condicion int
)
BEGIN
	
	IF condicion = 1 then -- ruc
		select * from Proveedor as p 
		where (  p.var_estado <> 0  and  p.var_ruc like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;

	IF condicion = 2 then -- razon social
		select * from Proveedor as p 
		where (p.var_estado <> 0 and p.var_razonsocial like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_lista_usuario_byCondicion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_lista_usuario_byCondicion`(
IN palabra varchar(200),
IN condicion int
)
BEGIN
	
	IF condicion = 1 then -- DNI
		select * from usuario as u 
		where (u.var_estado<>0 and u.var_dni like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;

	IF condicion = 2 then -- NOmbre y Apellido
		select * from usuario as u 
		where (u.var_estado<>0 and u.var_nombreapellido like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;

	IF condicion = 3 then -- ID NAme
		select * from usuario as u 
		where (u.var_estado<>0 and p.var_idname like CONCAT('%', palabra, '%'))
		order by int_id asc;
	END IF;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rpt_cuadre_caja` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_rpt_cuadre_caja`(
IN fechaver date
)
BEGIN
	DECLARE ingreso_efectivo decimal(9,2);
	DECLARE ingreso_tarjeta decimal(9,2);

	set @ingreso_efectivo = (select efectivo from get_reporte_cuadre_caja 
						 where fecha = fechaver and TipoOperacion='INGRESO');
	set @ingreso_tarjeta = (select Tarjeta from get_reporte_cuadre_caja 
							 where fecha = fechaver and TipoOperacion='INGRESO');
	set @deposito_efectivo = (select efectivo from get_reporte_cuadre_caja 
							 where fecha = fechaver and TipoOperacion='DEPOSITO');
	set @deposito_tarjeta = (select Tarjeta from get_reporte_cuadre_caja 
							 where fecha = fechaver and TipoOperacion='DEPOSITO');
	set @retiro_efectivo = (select efectivo from get_reporte_cuadre_caja 
							 where fecha = fechaver and TipoOperacion='RETIRO');
	set @retiro_tarjeta = (select Tarjeta from get_reporte_cuadre_caja 
							 where fecha = fechaver and TipoOperacion='RETIRO');

	set @totalcaja = (ifnull(@ingreso_efectivo,0) + ifnull(@deposito_efectivo,0)) 
					 - (ifnull(@retiro_tarjeta,0) + ifnull(@retiro_efectivo,0) + ifnull(@deposito_tarjeta,0) + ifnull(@ingreso_tarjeta,0));

	select DATE(dat_fechaInicio) as FechaInicio, DATE(dat_fechaCierre) as FechaFin,
		  CONVERT(ifnull(dec_saldoinicial,0), decimal(9,2)) as dec_saldoinicial, 
		  CONVERT(ifnull(@ingreso_efectivo,0), decimal(9,2)) as ing_efec, 
		  CONVERT(ifnull(@ingreso_tarjeta,0) , decimal(9,2))as ing_tar,
		  CONVERT(ifnull(@deposito_efectivo,0) , decimal(9,2)) as dep_efec,
		  CONVERT(ifnull(@deposito_tarjeta,0), decimal(9,2)) as depo_tar,
		  CONVERT(ifnull(@retiro_efectivo,0) , decimal(9,2)) as re_efec, 
		  CONVERT(ifnull(@retiro_tarjeta,0), decimal(9,2)) as re_tar, 
		  CONVERT(ifnull(@totalcaja+dec_saldoinicial,0), decimal(9,2)) as totalcaja
	from caja where DATE(dat_fechaInicio) = fechaver;
	
	/*select 
		  @ingreso_efectivo as ing_efec,@ingreso_tarjeta as ing_tar,
		  @deposito_efectivo as dep_efec,@deposito_tarjeta as depo_tar,
		  @retiro_efectivo as re_efec, @retiro_tarjeta as re_tar, @totalcaja as totalcaja ;*/

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_categoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_categoria`(
in id int,
in descripcion VARCHAR(200),
in estado char(1)
)
BEGIN

UPDATE `categoria`
SET
`var_descripcion` = descripcion,
`var_estado` = estado
WHERE `int_id` = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_cliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_cliente`(
in id int,
in nombre VARCHAR(45),
in dni CHAR(8),
in appaterno VARCHAR(45),
in apmaterno VARCHAR(45),
in direccion VARCHAR(45),
in telefono VARCHAR(20),
in busqueda VARCHAR(200),
in ruc CHAR(11)
)
BEGIN
	UPDATE `cliente`
	SET
	`var_nombre` = nombre,
	`var_dni` = dni,
	`var_apepaterno` =appaterno,
	`var_apematerno` =apmaterno,
	`var_direccion` = direccion,
	`var_telefono` = telefono,
	`var_busqueda` = var_busqueda,
	`var_estado` = var_estado,
	`var_ruc` = ruc
	WHERE `int_id` = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_constante` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_constante`(
in id int,
in descripcion VARCHAR(500),
in clase int
)
BEGIN

	UPDATE `constante`
	SET
	`var_descripcion` = descripcion,
	`int_clase` = clase
	WHERE `int_id` = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_cronogramapago` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_cronogramapago`(
in id_cp int,
in acuenta_anterio decimal(9,2),
in acuenta decimal(9,2),
in estado char(1),
in idventa int
)
BEGIN

	UPDATE `cronograma_pago`
	SET
	`dat_fechapagorealizado` = current_timestamp(),
	`dec_acuenta` = (acuenta+acuenta_anterio),
	`var_estado` = estado
	WHERE `int_id` = id_cp;

	set @estado = if (((select v.dec_acuenta from venta v where v.int_id=idventa) + acuenta) >= 
				   (select v.dec_totalfacturado from venta v where v.int_id=idventa),'1','2');

	set @acuenta_final = ((select v.dec_acuenta from venta v where v.int_id=idventa) + acuenta);
	set @saldo_final = ((select v.dec_saldo from venta v where v.int_id=idventa) - acuenta);

	UPDATE venta
	SET
	`dec_acuenta` = @acuenta_final,
	`dec_saldo` = @saldo_final,
	`var_estado` = @estado
	WHERE `int_id` = idventa;

	call sp_insert_movimiento(
		acuenta,
		idventa,
		2,
		CONCAT('Pago de Cuota Nro id',' - ',id_cp),
		1,
		'000 - 00000',
		1,
		1);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_marca` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_marca`(
in id int,
in descripcion varchar(200),
in estado char(1)
)
BEGIN

UPDATE `marca`
SET
`var_descripcion` = descripcion,
`var_estado` = estado
WHERE `int_id` = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_producto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_producto`(
in id int,
in codigobarra VARCHAR(11),
in tipo int,
in categoria int,
in stockminimo DECIMAL(9,2),
in stockmaximo DECIMAL(9,2),
in estado CHAR(1),
in preciounitario DECIMAL(9,2),
in utilidadbrutamonetaria DECIMAL(9,2),
in utilidadbrutaporcentaje DECIMAL(9,2),
in descripcion VARCHAR(500),
in marca int,
in precioventa DECIMAL(9,2),
in unidadMedida int  
)
BEGIN

	UPDATE `producto`
	SET
	`var_codigoBarra` = codigobarra,
	`int_tipo` = tipo,
	`int_categoria` = categoria,
	`dec_stockMinimo` = stockminimo,
	`dec_stockMaximo` = stockmaximo,
	`int_estado` = estado,
	`dec_preciounitario` = preciounitario,
	`dec_utilidadbrutamonetaria` = utilidadbrutamonetaria,
	`dec_utilidadbrutaporcentaje` = utilidadbrutaporcentaje,
	`var_descripcion` = descripcion,
	`int_marca` = marca,
	`dec_precioventa` = precioventa,
	`int_unidadMedida` = unidadMedida
	WHERE `int_id` = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_proveedor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_proveedor`(
in id int,
in ruc VARCHAR(15),
in razonsocial VARCHAR(500),
in telefono VARCHAR(20),
in direccion VARCHAR(500),
in estado CHAR(1)
)
BEGIN

	UPDATE `proveedor`
	SET
	`int_id` = id,
	`var_ruc` = ruc,
	`var_razonsocial` = razonsocial,
	`var_telefono` = telefono,
	`var_direccion` = direccion,
	`var_estado` = estado
	WHERE `int_id` = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_usuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_usuario`(
in id int,
in idname VARCHAR(30),
in pass VARCHAR(20),
in nomape VARCHAR(60),
in dni VARCHAR(8),
in telefono VARCHAR(15),
in estado CHAR(1)
)
BEGIN
UPDATE `bd_inventario`.`usuario`
SET
`int_id` = id,
`var_idname` = idname,
`var_password` = pass,
`var_nombreapellido` = nomape,
`var_dni` = dni,
`var_telefono` = telefono,
`var_estado` = estado
WHERE `int_id` = id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `get_lista_cliente_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_cliente_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_cliente_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_cliente_all` AS (select `c`.`int_id` AS `id`,`c`.`var_busqueda` AS `cliente`,`c`.`var_direccion` AS `direccion`,`c`.`var_dni` AS `dni`,`c`.`var_estado` AS `estado`,`c`.`var_telefono` AS `telefono`,`c`.`var_ruc` AS `ruc`,`c`.`dat_fecharegistro` AS `fecharegistro` from `cliente` `c`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lista_cronogramapago_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_cronogramapago_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_cronogramapago_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_cronogramapago_all` AS select `cp`.`int_id` AS `int_id`,`cp`.`int_ventacredito` AS `id_ventacredito`,`cp`.`int_numerocuota` AS `int_numerocuota`,cast(`cp`.`dat_fechapago` as date) AS `dat_fechapago`,cast(`cp`.`dat_fechapagorealizado` as date) AS `dat_fechapagorealizado`,`cp`.`dec_monto` AS `dec_monto`,`cp`.`dec_acuenta` AS `dec_acuenta`,(case when ((curdate() > cast(`cp`.`dat_fechapago` as date)) and (`cp`.`dec_acuenta` < `cp`.`dec_monto`)) then 'Moroso' when (`cp`.`var_estado` = '2') then 'Cancelada' when (`cp`.`var_estado` = '1') then 'Programada' end) AS `estado` from `cronograma_pago` `cp` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lista_detalleprestamo_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_detalleprestamo_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_detalleprestamo_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_detalleprestamo_all` AS select `dp`.`int_id` AS `int_id`,(select `fun_obtener_mes`(month(`dp`.`dat_fechamaxpago`))) AS `mes`,`dp`.`int_prestamo` AS `int_prestamo`,cast(`dp`.`dat_fechamaxpago` as date) AS `dat_fechamaxpago`,`p`.`dec_montototal` AS `dec_montototal`,`dp`.`dec_montocuota` AS `dec_montocuota`,`dp`.`dec_montointeres` AS `dec_montointeres`,`dp`.`dec_montopagado` AS `dec_montopagado`,`dp`.`dec_interespagado` AS `dec_interespagado`,(case `dp`.`var_estado` when 0 then 'Anulada' when 1 then 'Pendiente' when 2 then 'Cancelada' end) AS `estado` from (`detalle_prestamo` `dp` join `prestamo` `p` on((`p`.`int_id` = `dp`.`int_prestamo`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lista_ordeningreso_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_ordeningreso_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_ordeningreso_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_ordeningreso_all` AS select `oi`.`int_id` AS `id`,`oi`.`var_documentoserie` AS `serie`,`oi`.`var_documentonumero` AS `numero`,`oi`.`dat_fecharegistro` AS `fecha`,`oi`.`var_comentario` AS `comentario`,`p`.`int_id` AS `id_proveedor`,`p`.`var_razonsocial` AS `proveedor`,`oi`.`int_motivo` AS `id_motivo`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_clase` = 5) and (`c`.`int_valor` = `oi`.`int_motivo`))) AS `Motivo`,`oi`.`int_tipodocumento` AS `id_documento`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_clase` = 3) and (`c`.`int_valor` = `oi`.`int_tipodocumento`))) AS `Documento`,`u`.`var_nombreapellido` AS `Personal`,(case `oi`.`var_estado` when 0 then 'Anulada' when 1 then 'Habilitada' end) AS `Estado` from ((`orden_ingreso` `oi` join `proveedor` `p` on((`p`.`int_id` = `oi`.`int_proveedor`))) join `usuario` `u` on((`u`.`int_id` = `oi`.`int_usuario`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lista_ordensalida_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_ordensalida_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_ordensalida_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_ordensalida_all` AS select `os`.`int_id` AS `id`,`os`.`dat_fecharegistro` AS `fecha`,`os`.`var_comentario` AS `comentario`,`os`.`int_motivo` AS `id_motivo`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_clase` = 5) and (`c`.`int_valor` = `os`.`int_motivo`))) AS `motivo`,ifnull((select ifnull(`c`.`var_busqueda`,'') from (`venta` `v` join `cliente` `c` on((`c`.`int_id` = `v`.`int_cliente`))) where (`v`.`int_id` = `os`.`int_venta`)),'') AS `Cliente`,`u`.`int_id` AS `id_personal`,`u`.`var_nombreapellido` AS `personal`,ifnull((select sum(ifnull(`dos`.`dec_cantidad`,0)) from `detalle_ordensalida` `dos` where ((`dos`.`int_ordensalida` = `os`.`int_id`) and (`dos`.`var_estado` <> 0))),0) AS `cantidad`,(case `os`.`var_estado` when 0 then 'Inhabilitada' when 1 then 'Habilitada' end) AS `estado` from (`orden_salida` `os` join `usuario` `u` on((`u`.`int_id` = `os`.`int_usuario`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lista_prestamo_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_prestamo_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_prestamo_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_prestamo_all` AS select `p`.`int_id` AS `int_id`,cast(`p`.`dat_fechacreacion` as date) AS `dat_fechacreacion`,`p`.`dec_montototal` AS `dec_montototal`,`p`.`int_cliente` AS `int_cliente`,`c`.`var_busqueda` AS `cliente`,`p`.`int_tasa` AS `int_tasa`,`p`.`dec_cuotames` AS `dec_cuotames`,`p`.`int_modalidadpago` AS `int_modalidadpago`,`p`.`var_garantia` AS `garantia`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `p`.`int_modalidadpago`) and (`c`.`int_clase` = 8))) AS `modalidad`,(case `p`.`var_estado` when 0 then 'Anulada' when 1 then 'Pendiente' when 2 then 'Cancelada' end) AS `estado` from (`prestamo` `p` join `cliente` `c` on((`c`.`int_id` = `p`.`int_cliente`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lista_producto_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_producto_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_producto_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_producto_all` AS (select `p`.`int_id` AS `id`,`p`.`var_codigoBarra` AS `codigobarra`,`p`.`var_descripcion` AS `producto`,`p`.`dec_preciounitario` AS `preciounitario`,`p`.`dec_precioventa` AS `precioventa`,`m`.`var_descripcion` AS `Marca`,`c`.`var_descripcion` AS `Categoria`,ifnull((select `ct`.`var_descripcion` from `constante` `ct` where ((`ct`.`int_valor` = `p`.`int_tipo`) and (`ct`.`int_clase` = 1))),'') AS `TipoProducto`,`p`.`dec_stock` AS `stock_actual`,`p`.`dec_stockMinimo` AS `stock_minimo`,`p`.`int_estado` AS `int_estado`,(case `p`.`int_estado` when 0 then 'imhabilitado' when 1 then 'habilitado' end) AS `estado` from ((`producto` `p` join `marca` `m` on((`m`.`int_id` = `p`.`int_marca`))) join `categoria` `c` on((`c`.`int_id` = `p`.`int_categoria`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lista_venta_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lista_venta_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lista_venta_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lista_venta_all` AS (select `v`.`int_id` AS `id`,`v`.`dat_fecharegistro` AS `fechaBusq`,date_format(`v`.`dat_fecharegistro`,'%d/%m/%Y') AS `fecha`,`v`.`int_cliente` AS `id_cliente`,`c`.`var_busqueda` AS `cliente`,`v`.`dec_subtotal` AS `subtotal`,`v`.`dec_descuentoporcentaje` AS `descuento`,`v`.`dec_igv` AS `igv`,`v`.`dec_totalfacturado` AS `montoFacturado`,`v`.`dec_acuenta` AS `montoPagado`,`v`.`dec_saldo` AS `saldo`,`u`.`var_nombreapellido` AS `personal`,`v`.`int_formapago` AS `id_formapago`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_clase` = 4) and (`c`.`int_valor` = `v`.`int_formapago`))) AS `formaPago`,`d`.`int_tipodocumento` AS `id_tipodocumento`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_clase` = 6) and (`c`.`int_valor` = `d`.`int_tipodocumento`))) AS `documento`,`d`.`var_serie` AS `serie`,`d`.`var_numero` AS `numero`,`v`.`var_estado` AS `var_estado`,(case `v`.`int_tipoventa` when 1 then 'Contado' when 2 then 'Credito' end) AS `tipo_venta`,(case `v`.`var_estado` when 0 then 'Anulada' when 1 then 'Pagada' when 2 then 'Pendiente' when 3 then 'Pendiente' end) AS `estado` from (((`venta` `v` join `cliente` `c` on((`c`.`int_id` = `v`.`int_cliente`))) join `usuario` `u` on((`u`.`int_id` = `v`.`int_usuario`))) join `documento` `d` on(((`d`.`int_tipooperacionInterna` = 1) and (`d`.`int_numerooperacionInterna` = `v`.`int_id`)))) order by 1 desc,2 desc) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_lsita_movimiento_stock_all`
--

/*!50001 DROP TABLE IF EXISTS `get_lsita_movimiento_stock_all`*/;
/*!50001 DROP VIEW IF EXISTS `get_lsita_movimiento_stock_all`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_lsita_movimiento_stock_all` AS select cast(`oi`.`dat_fecharegistro` as date) AS `fechaBusqueda`,date_format(`oi`.`dat_fecharegistro`,'%d/%m/%Y %H:%i') AS `dat_fecharegistro`,`doi`.`int_producto` AS `int_producto`,concat(`p`.`var_descripcion`,' - ',ifnull((select `ct`.`var_descripcion` from `constante` `ct` where ((`ct`.`int_valor` = `p`.`int_tipo`) and (`ct`.`int_clase` = 1))),''),' - ',`m`.`var_descripcion`,' - ',`c`.`var_descripcion`) AS `producto`,sum(`doi`.`dec_cantidad`) AS `dec_cantidad`,'ENTRADA' AS `tipo`,`u`.`int_id` AS `int_usuario`,`u`.`var_nombreapellido` AS `personal` from (((((`detalle_ordeningreso` `doi` join `orden_ingreso` `oi` on((`oi`.`int_id` = `doi`.`int_ordeningreso`))) join `producto` `p` on((`p`.`int_id` = `doi`.`int_producto`))) join `marca` `m` on((`m`.`int_id` = `p`.`int_marca`))) join `categoria` `c` on((`c`.`int_id` = `p`.`int_categoria`))) join `usuario` `u` on((`u`.`int_id` = `oi`.`int_usuario`))) group by 1,3,7 union all (select cast(`os`.`dat_fecharegistro` as date) AS `fechaBusqueda`,date_format(`os`.`dat_fecharegistro`,'%d/%m/%Y %H:%i') AS `dat_fecharegistro`,`dos`.`int_producto` AS `int_producto`,concat(`p`.`var_descripcion`,' - ',ifnull((select `ct`.`var_descripcion` from `constante` `ct` where ((`ct`.`int_valor` = `p`.`int_tipo`) and (`ct`.`int_clase` = 1))),''),' - ',`m`.`var_descripcion`,' - ',`c`.`var_descripcion`) AS `producto`,sum((`dos`.`dec_cantidad` * -(1))) AS `dec_cantidad`,'SALIDA' AS `tipo`,`u`.`int_id` AS `int_usuario`,`u`.`var_nombreapellido` AS `personal` from (((((`detalle_ordensalida` `dos` join `orden_salida` `os` on((`os`.`int_id` = `dos`.`int_ordensalida`))) join `producto` `p` on((`p`.`int_id` = `dos`.`int_producto`))) join `marca` `m` on((`m`.`int_id` = `p`.`int_marca`))) join `categoria` `c` on((`c`.`int_id` = `p`.`int_categoria`))) join `usuario` `u` on((`u`.`int_id` = `os`.`int_usuario`))) group by 1,3,7) order by 2,4 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_cuadre_caja`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_cuadre_caja`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_cuadre_caja`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_cuadre_caja` AS select cast(`m`.`dat_fecharegistro` as date) AS `fecha`,ifnull(sum((case `m`.`int_tipopago` when 1 then ifnull(`m`.`dec_monto`,0.00) end)),0.00) AS `Efectivo`,ifnull(sum((case when (`m`.`int_tipopago` <> 1) then ifnull(`m`.`dec_monto`,0.00) end)),0.00) AS `Tarjeta`,ucase((select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `m`.`int_tipooperacion`) and (`c`.`int_clase` = 2)))) AS `TipoOperacion` from `movimiento` `m` where (`m`.`int_venta` = 0) group by cast(`m`.`dat_fecharegistro` as date),ucase((select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `m`.`int_tipooperacion`) and (`c`.`int_clase` = 2)))) union all (select cast(`m`.`dat_fecharegistro` as date) AS `fecha`,ifnull(sum((case `m`.`int_tipopago` when 1 then ifnull(`m`.`dec_monto`,0.00) end)),0.00) AS `Efectivo`,ifnull(sum((case when (`m`.`int_tipopago` <> 1) then ifnull(`m`.`dec_monto`,0.00) end)),0.00) AS `Tarjeta`,'INGRESO' AS `TipoOperacion` from `movimiento` `m` where ((`m`.`int_venta` <> 0) and (`m`.`var_estado` <> 0)) group by cast(`m`.`dat_fecharegistro` as date),'INGRESO') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_documento_cronogrampago`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_documento_cronogrampago`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_cronogrampago`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_documento_cronogrampago` AS select `cp`.`int_id` AS `id_cp`,`v`.`int_id` AS `id_venta`,`v`.`dec_totalfacturado` AS `dec_totalfacturado`,`vc`.`dec_montocuota` AS `dec_montocuota`,`vc`.`int_nrocuota` AS `int_nrocuota`,sum(`t`.`dec_cantidad`) AS `cantidadprod`,`c`.`var_busqueda` AS `cliente`,`cp`.`int_numerocuota` AS `int_numerocuota`,cast(`cp`.`dat_fechapago` as date) AS `dat_fechapago`,cast(`cp`.`dat_fechapagorealizado` as date) AS `dat_fechapagorealizado`,`cp`.`dec_monto` AS `dec_monto`,`cp`.`dec_acuenta` AS `dec_acuenta`,`u`.`var_nombreapellido` AS `var_nombreapellido`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `v`.`int_tipoventa`) and (`c`.`int_clase` = 4))) AS `FormaPago`,(case when ((curdate() > cast(`cp`.`dat_fechapago` as date)) and (`cp`.`dec_acuenta` < `cp`.`dec_monto`)) then 'Moroso' when (`cp`.`var_estado` = '2') then 'Cancelada' when (`cp`.`var_estado` = '1') then 'Programada' end) AS `estado` from (((((`cronograma_pago` `cp` join `venta_credito` `vc` on((`vc`.`int_id` = `cp`.`int_ventacredito`))) join `venta` `v` on(((`v`.`int_id` = `vc`.`int_venta`) and (`v`.`var_estado` <> 0)))) join `transaccion` `t` on((`t`.`int_venta` = `v`.`int_id`))) join `cliente` `c` on((`c`.`int_id` = `v`.`int_cliente`))) join `usuario` `u` on((`u`.`int_id` = `v`.`int_usuario`))) group by `v`.`int_id`,`vc`.`int_id`,`cp`.`int_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_documento_ordeningreso`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_documento_ordeningreso`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_ordeningreso`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_documento_ordeningreso` AS select `oi`.`int_id` AS `int_id`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `oi`.`int_tipodocumento`) and (`c`.`int_clase` = 3))) AS `TipoDocumento`,concat(`oi`.`var_documentoserie`,' - ',`oi`.`var_documentonumero`) AS `documento`,cast(`oi`.`dat_fecharegistro` as date) AS `fechaEmision`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `oi`.`int_motivo`) and (`c`.`int_clase` = 5))) AS `Motivo`,`oi`.`var_comentario` AS `var_comentario`,`pv`.`var_razonsocial` AS `proveedor`,`pv`.`var_ruc` AS `ruc`,`u`.`var_nombreapellido` AS `responsable`,`p`.`var_descripcion` AS `producto`,`m`.`var_descripcion` AS `Marca`,`ct`.`var_descripcion` AS `Categoria`,ifnull((select `ct`.`var_descripcion` from `constante` `ct` where ((`ct`.`int_valor` = `p`.`int_tipo`) and (`ct`.`int_clase` = 1))),'') AS `TipoProducto`,`doi`.`dec_cantidad` AS `dec_cantidad`,`doi`.`dec_preciounitario` AS `dec_preciounitario`,`doi`.`dec_importe` AS `dec_importe` from ((((((`orden_ingreso` `oi` join `detalle_ordeningreso` `doi` on((`doi`.`int_ordeningreso` = `oi`.`int_id`))) join `proveedor` `pv` on((`pv`.`int_id` = `oi`.`int_proveedor`))) join `usuario` `u` on((`u`.`int_id` = `oi`.`int_usuario`))) join `producto` `p` on((`p`.`int_id` = `doi`.`int_producto`))) join `marca` `m` on((`m`.`int_id` = `p`.`int_marca`))) join `categoria` `ct` on((`ct`.`int_id` = `p`.`int_categoria`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_documento_ordensalida`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_documento_ordensalida`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_ordensalida`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_documento_ordensalida` AS select `os`.`int_id` AS `int_id`,cast(`os`.`dat_fecharegistro` as date) AS `fechaEmision`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `os`.`int_motivo`) and (`c`.`int_clase` = 5))) AS `Motivo`,`os`.`var_comentario` AS `var_comentario`,`u`.`var_nombreapellido` AS `responsable`,`p`.`var_descripcion` AS `producto`,`m`.`var_descripcion` AS `Marca`,`ct`.`var_descripcion` AS `Categoria`,ifnull((select `ct`.`var_descripcion` from `constante` `ct` where ((`ct`.`int_valor` = `p`.`int_tipo`) and (`ct`.`int_clase` = 1))),'') AS `TipoProducto`,`dos`.`dec_cantidad` AS `dec_cantidad` from (((((`orden_salida` `os` join `detalle_ordensalida` `dos` on((`dos`.`int_ordensalida` = `os`.`int_id`))) join `usuario` `u` on((`u`.`int_id` = `os`.`int_usuario`))) join `producto` `p` on((`p`.`int_id` = `dos`.`int_producto`))) join `marca` `m` on((`m`.`int_id` = `p`.`int_marca`))) join `categoria` `ct` on((`ct`.`int_id` = `p`.`int_categoria`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_documento_prestamo`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_documento_prestamo`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_prestamo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_documento_prestamo` AS select `p`.`int_id` AS `int_prestamo`,cast(`p`.`dat_fechacreacion` as date) AS `dat_fechacreacion`,`p`.`int_cliente` AS `int_cliente`,`c`.`var_busqueda` AS `cliente`,`p`.`dec_montototal` AS `dec_montototal`,`p`.`int_tasa` AS `int_tasa`,`p`.`dec_cuotames` AS `dec_cuotames`,`p`.`int_modalidadpago` AS `int_modalidadpago`,`u`.`var_nombreapellido` AS `trabajador`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `p`.`int_modalidadpago`) and (`c`.`int_clase` = 8))) AS `modalidad`,(case `p`.`var_estado` when 0 then 'Anulada' when 1 then 'Pendiente' when 2 then 'Cancelada' end) AS `estado_prestamo`,`dp`.`int_id` AS `int_id`,(select `fun_obtener_mes`(month(`dp`.`dat_fechamaxpago`))) AS `mes`,cast(`dp`.`dat_fechamaxpago` as date) AS `dat_fechamaxpago`,`dp`.`dec_montocuota` AS `dec_montocuota`,`dp`.`dec_montointeres` AS `dec_montointeres`,`dp`.`dec_montopagado` AS `dec_montopagado`,`dp`.`dec_interespagado` AS `dec_interespagado`,(case `dp`.`var_estado` when 0 then 'Anulada' when 1 then 'Pendiente' when 2 then 'Cancelada' end) AS `estado_detalle` from (((`detalle_prestamo` `dp` join `prestamo` `p` on((`p`.`int_id` = `dp`.`int_prestamo`))) join `cliente` `c` on((`c`.`int_id` = `p`.`int_cliente`))) join `usuario` `u` on((`u`.`int_id` = `p`.`int_usuario`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_documento_venta`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_documento_venta`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_documento_venta`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_documento_venta` AS (select `v`.`int_id` AS `int_id`,`v`.`dec_subtotal` AS `dec_subtotal`,`v`.`dec_igv` AS `dec_igv`,`v`.`dec_igvmonto` AS `dec_igvmonto`,`v`.`dec_descuentoporcentaje` AS `dec_descuentoporcentaje`,(`v`.`dec_descuentomonetario` * -(1)) AS `descuentoMonetario`,`v`.`dec_totalfacturado` AS `dec_totalfacturado`,`v`.`dec_acuenta` AS `dec_acuenta`,`v`.`dec_saldo` AS `dec_saldo`,`u`.`var_nombreapellido` AS `var_nombreapellido`,cast(`v`.`dat_fecharegistro` as date) AS `fechaEmision`,`c`.`var_busqueda` AS `cliente`,`c`.`var_direccion` AS `direccion`,`c`.`var_ruc` AS `var_ruc`,`c`.`var_dni` AS `var_dni`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `v`.`int_tipoventa`) and (`c`.`int_clase` = 4))) AS `FormaPago`,ifnull((select concat(`d`.`var_serie`,' - ',`d`.`var_numero`) from `documento` `d` where (`d`.`int_numerooperacionInterna` = `v`.`int_id`)),'') AS `documento`,ifnull((select (select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_clase` = 6) and (`c`.`int_valor` = `d`.`int_tipodocumento`))) from `documento` `d` where (`d`.`int_numerooperacionInterna` = `v`.`int_id`)),'') AS `TipoDocumento`,`p`.`var_descripcion` AS `producto`,`m`.`var_descripcion` AS `Marca`,`ct`.`var_descripcion` AS `Categoria`,ifnull((select `ct`.`var_descripcion` from `constante` `ct` where ((`ct`.`int_valor` = `p`.`int_tipo`) and (`ct`.`int_clase` = 1))),'') AS `TipoProducto`,`t`.`dec_preciounitario` AS `dec_preciounitario`,`t`.`dec_cantidad` AS `dec_cantidad`,`t`.`dec_importe` AS `dec_importe` from ((((((`venta` `v` join `transaccion` `t` on((`t`.`int_venta` = `v`.`int_id`))) join `producto` `p` on((`p`.`int_id` = `t`.`int_producto`))) join `usuario` `u` on((`u`.`int_id` = `v`.`int_usuario`))) join `cliente` `c` on((`c`.`int_id` = `v`.`int_cliente`))) join `marca` `m` on((`m`.`int_id` = `p`.`int_marca`))) join `categoria` `ct` on((`ct`.`int_id` = `p`.`int_categoria`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_movimiento_caja`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_movimiento_caja`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_movimiento_caja`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_movimiento_caja` AS (select cast(`m`.`dat_fecharegistro` as date) AS `FechaRegistro`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `m`.`int_tipooperacion`) and (`c`.`int_clase` = 2))) AS `TipoOperacion`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `m`.`int_tipopago`) and (`c`.`int_clase` = 4))) AS `TipoPago`,`u`.`var_nombreapellido` AS `Usuario`,`m`.`var_motivo` AS `motivo`,ifnull(`m`.`dec_monto`,0.00) AS `Monto` from (`movimiento` `m` join `usuario` `u` on((`m`.`int_usuario` = `u`.`int_id`))) where (`m`.`int_venta` = 0) order by 2,3) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `get_reporte_venta_detallejcaja`
--

/*!50001 DROP TABLE IF EXISTS `get_reporte_venta_detallejcaja`*/;
/*!50001 DROP VIEW IF EXISTS `get_reporte_venta_detallejcaja`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `get_reporte_venta_detallejcaja` AS select cast(`m`.`dat_fecharegistro` as date) AS `FechaRegistro`,'VENTAS' AS `TipoOperacion`,ucase((select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `v`.`int_tipoventa`) and (`c`.`int_clase` = 4)))) AS `FormaPago`,`cl`.`var_busqueda` AS `var_busqueda`,`v`.`dec_totalfacturado` AS `MontoTotal`,sum(`m`.`dec_monto`) AS `Acuenta`,`v`.`dec_saldo` AS `Saldo`,`m`.`var_documento` AS `Documento`,(select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `m`.`int_tipodocumento`) and (`c`.`int_clase` = 6))) AS `TipoDocumento` from ((`movimiento` `m` join `venta` `v` on((`v`.`int_id` = `m`.`int_venta`))) join `cliente` `cl` on((`cl`.`int_id` = `v`.`int_cliente`))) group by `v`.`int_id` union all (select cast(`v`.`dat_fecharegistro` as date) AS `FechaRegistro`,'ANULACION' AS `TipoOperacion`,ucase((select `c`.`var_descripcion` from `constante` `c` where ((`c`.`int_valor` = `v`.`int_tipoventa`) and (`c`.`int_clase` = 4)))) AS `FormaPago`,`cl`.`var_busqueda` AS `var_busqueda`,(`v`.`dec_totalfacturado` * -(1)) AS `MontoTotal`,0.00 AS `Acuenta`,0.00 AS `Saldo`,'000-00000' AS `Documento`,'Sin Documento' AS `TipoDocumento` from ((`venta` `v` join `cliente` `cl` on((`cl`.`int_id` = `v`.`int_cliente`))) join `venta_anulado` `va` on((`va`.`int_venta` = `v`.`int_id`))) group by `v`.`int_id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_generar_codigobarra_serie`
--

/*!50001 DROP TABLE IF EXISTS `v_generar_codigobarra_serie`*/;
/*!50001 DROP VIEW IF EXISTS `v_generar_codigobarra_serie`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_generar_codigobarra_serie` AS select concat('P',convert(right(concat('0000000000',(ifnull(max(right(`producto`.`var_codigoBarra`,10)),0) + 1)),10) using latin1)) AS `serie` from `producto` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-12 21:23:42
